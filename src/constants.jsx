

//---CONSTANTS-----
import {Redirect} from "react-router-dom";
import React from "react";
import moment from "moment";
import {decryptData} from "./Services/EncryptDecryptServices";

export const companyName = "WebOccult Technologies";


//------HELMETS--------
export const loginPageHelmet = "Login-HRMS";
export const dashBoardHelmet = "WebOccult Technologies";

//-----LOCAL STORAGE CONSTANTS-----
export const  tokenKey = "t";
export const  userData = "d1";
export const  userPermissions = "d2";
export const userRoutesPermissions = "r";

//-------LOGIN KEYS----
export const SECRET_KEY = "23423423v3e";
export const SECRET_IV = "1231x123d123123123123";

 const  LIVE = 'https://hrms-api.ammazza.me/api/';
 const  STAGING = "http://192.168.1.40:9999/api/";
 export const ENV = {
    LIVE,
    STAGING
}
// export const CURRENT_ENV = ENV.STAGING



//-------------ROUTES-----------------
export const homePage = "/app/toggl/toggle-entries";
export const toggleEntries = "/app/toggl/toggle-entries";
export const toggleReport = "/app/toggl/toggle-report";
export const loginRoute = "/login";
//----ADMIN ROUTES-------
export const clients = "/app/client";
export const tags = "/app/tags";
export const projects = "/app/projects"
export const designation = "/app/admin/designations";
export const departments = "/app/admin/departments";
export const roles = "/app/admin/roles";
export const permissions = "/app/admin/permissions";
export const employees = "/app/admin/employees";
export const modulesRoute = "/app/admin/module";



//--PERMISSION CONSTANTS=----
export const DashBoardP = "Dashboard";
export const DesignationsP = "Designations";
export const DepartmentsP = "Departments";
export const ModulesP = "Modules";
export const AdminP = "Admin";
export const RolesAndPermissionsP = "Roles & Permissions";
export const EmployeesP = "Employees";
export const toggleEntriesP = "Toggl Dashboard";
export const toggleReportP = "Toggl Report";
export const ProjectsP = "Projects";
export const RolesP = "Roles";
export const TagsP = "Tags";
export const ClientsP = "Client";
export const permissionRoutes = {
    toggleReportP,
    toggleEntriesP,
    ProjectsP,
    TagsP,
    ClientsP,
    EmployeesP,
    RolesAndPermissionsP,
    AdminP,
    ModulesP,
    DesignationsP,
    DashBoardP,
    DepartmentsP,
    RolesP
}
//--PERMISSION TYPES---
export const readP = "read";
export const updateP = "update";
export const globalP = "isglobal";
export const csvP = "csv";
export const addP = "add";
export const billableP = "isbillable";
export const deleteP = "delete";
export const permissionTypes = {
    readP,
    updateP,
    globalP,
    csvP,
    addP,
    billableP,
    deleteP
}


export function checkAllDataInLocalStorage() {
    if(
        localStorage.getItem(tokenKey) != null &&
        localStorage.getItem(userData) != null &&
        localStorage.getItem(userPermissions) != null &&
        localStorage.getItem(userRoutesPermissions) != null
    ) {
        return true;
    } else {
        return false;
    }
}

//--CONSTANT GLOBAL FUNCTIONS
export  function getCurrentUserProjectIDMangerList() {
    if(localStorage.getItem(userData) != null) {
        const manager_project_ids = JSON.parse(decryptData(localStorage.getItem(userData))).manager_project_ids;
        // console.log("getCurrentUserProjectIDMangerList:", manager_project_ids)
        return manager_project_ids != null ? manager_project_ids : [];
    } else {
        return  [];
    }
}
export  function getCurrentUser() {
    if(localStorage.getItem(userData) != null) {
        const currentUser = JSON.parse(decryptData(localStorage.getItem(userData)));
        // console.log("getCurrentUserProjectIDMangerList:", manager_project_ids)
        return currentUser != null ? currentUser : {};
    } else {
        return {};
    }

}

export  function getCurrentUserPermissions() {
    if(localStorage.getItem(userPermissions) != null) {
        const userPermissionsData = JSON.parse(decryptData(localStorage.getItem(userPermissions)));
        // console.log("getCurrentUserPermissions:", userPermissionsData)
        return userPermissionsData != null ? userPermissionsData : [];
    } else {
        return  [];
    }

}

export  function getCurrentUserRoutesPermissions() {
    if(decryptData(localStorage.getItem(userRoutesPermissions))) {
        const userRoutesPermissionsData = JSON.parse(decryptData(localStorage.getItem(userRoutesPermissions)));
        // console.log("getCurrentUserRoutesPermissions:", userRoutesPermissionsData)
        return userRoutesPermissionsData != null ? userRoutesPermissionsData : [];
    } else {
        return [];
    }

}

// export getUsersFilterRoutes() {}



export function saveFileAsCSV (name,data) {
    let blobData = new Blob([atob(data)], {type: 'text/csv'});
    let csvURL = window.URL.createObjectURL(blobData);
    //window.open(csvURL);
    // then commenting out the window.open & replacing
    // with this allowed a file name to be passed out
    let tempLink = document.createElement('a');
    tempLink.href = csvURL;
    tempLink.setAttribute('download', name+'.csv');
    tempLink.click();
}


export function copyToClip(str) {
    function listener(e) {
        e.clipboardData.setData("text/html", str);
        e.clipboardData.setData("text/plain", str);
        e.preventDefault();
    }
    document.addEventListener("copy", listener);
    document.execCommand("copy");
    document.removeEventListener("copy", listener);
};



export const preventALinkDefault = (e) => {
    e.preventDefault();
}

export function wc_hex_is_light(color) {
    const hex = color.replace('#', '');
    const c_r = parseInt(hex.substr(0, 2), 16);
    const c_g = parseInt(hex.substr(2, 2), 16);
    const c_b = parseInt(hex.substr(4, 2), 16);
    const brightness = ((c_r * 299) + (c_g * 587) + (c_b * 114)) / 1000;
    return brightness > 155;
}



export const groupBy = function(xs, key) {
    return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};


export const getDaysArray = (start, end) => {
    let arr=[];
    let dt=new Date(start);
    for(; dt<=end; dt.setDate(dt.getDate()+1)){
        arr.push(`${new Date(dt).getFullYear()}-${new Date(dt).getMonth()}-${new Date(dt).getDate()}`);
    }
    return arr;
};

export function convertUTCDateToLocalDate(date) {
    console.log("timezone offset -> ",date.getTime(),date.getTimezoneOffset());

    const newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
    const offset = date.getTimezoneOffset() / 60;
    const hours = date.getHours();
    newDate.setHours(hours - offset);
    return newDate;
}


export function getDate(dateString) {
        // console.log("getDate", moment(dateString).format("dddd, MMMM Do YYYY"));
        return moment(new Date(dateString)).format("dddd, Do MMMM YYYY")
}

export function getFormattedDate(dateString) {
    // console.log("getDate", moment(dateString).format("dddd, MMMM Do YYYY"));
    return moment(new Date(dateString)).format("YYYY-MM-DD");
}

export function getFormattedTime(timeString) {
    // console.log("getFormattedTime",timeString,convertUTCDateToLocalDate(new Date(timeString)));
    // return moment(convertUTCDateToLocalDate(new Date(timeString))).format("hh:mm A");
    return moment(new Date(timeString)).format("hh:mm A");
}



export function setFormattedDateAndTime(date,time) {
    const now = new Date(`${date} ${time}`);
    // const utc = new Date(now.getTime() + (now.getTimezoneOffset() * 60000));
    // console.log(moment(now, ["YYYY-MM-DD h:mm A"]).format("YYYY-MM-DD HH:mm:ss"));
    return moment(now, ["YYYY-MM-DD hh:mm A"]).format("YYYY-MM-DD HH:mm:ss");
}


export function splitTime(time) {
    if(time != null) {
        time = time.split(':');
        let hours = parseInt(time[0]);
        let min = parseInt(time[1]);
        let seconds = time[2] != null ? parseInt(time[2]) : 0;
        return [hours,min,seconds];
    } else {
        return [0,0,0]
    }
}

export   function formatTime(seconds) {
    return [
        parseInt(seconds / 60 / 60),
        parseInt(seconds / 60 % 60),
        parseInt(seconds % 60)
    ]
        .join(":")
        .replace(/\b(\d)\b/g, "0$1")
}



function get_timeDifference(strtdatetime,endTime,isNow) {
    var datetime = new Date(strtdatetime).getTime();
    var now = isNow ?new Date(endTime).getTime() : new Date(endTime).getTime();
    if (isNaN(datetime)) {
        return "";
    }
    //console.log(datetime + " " + now);
    if (datetime < now) {
        var milisec_diff = now - datetime;
    } else {
        var milisec_diff = datetime - now;
    }
    var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));
    var date_diff = new Date(milisec_diff);
    var msec = milisec_diff;
    var hh = Math.floor(msec / 1000 / 60 / 60);
    msec -= hh * 1000 * 60 * 60;
    var mm = Math.floor(msec / 1000 / 60);
    msec -= mm * 1000 * 60;
    var ss = Math.floor(msec / 1000);
    msec -= ss * 1000
    var daylabel = "";
    if (days > 0) {
        var grammar = " ";
        if (days > 1) grammar = "s "
        var hrreset = days * 24;
        hh = hh - hrreset;
        daylabel = days + " Day" + grammar ;
    }
    //  Format Hours
    var hourtext = '00';
    hourtext = String(hh);
    if (hourtext.length == 1) { hourtext = '0' + hourtext };
    //  Format Minutes
    var mintext = '00';
    mintext = String(mm);
    if (mintext.length == 1) { mintext = '0' + mintext };
    //  Format Seconds
    var sectext = '00';
    sectext = String(ss);
    if (sectext.length == 1) { sectext = '0' + sectext };
    var msectext = '00';
    msectext = String(msec);
    msectext = msectext.substring(0, 1);
    if (msectext.length == 1) { msectext = '0' + msectext };
    return hourtext + ":" + mintext + ":" + sectext;
    // return daylabel + hourtext + ":" + mintext + ":" + sectext + ":" + msectext;
}

export function getTimeDifference(startTime,endTime) {
    // var date1 = new Date(startTime);
    // var date2 = new Date(endTime);
    // var diff = date2.getTime() - date1.getTime();
    // const startTimeArray = splitTime(moment(startTime,["hh:mm A"]).format("HH:mm"));
    // let endTimeArray = splitTime(moment(endTime,["hh:mm A"]).format("HH:mm"));
    // let startTimeS = startTimeArray[0] * 3600 +  startTimeArray[1] * 60;
    // let endTimeS = endTimeArray[0] * 3600 +  endTimeArray[1] * 60;
    // let diffHours =
    //     // endTimeArray[0] - startTimeArray[0];
    //     Math.abs(endTimeArray[0] - startTimeArray[0]);
    // let diffMin =
    //     // endTimeArray[1] - startTimeArray[1];
    //     Math.abs(endTimeArray[1] - startTimeArray[1]);
    // console.log("getTimeDifference",startTime, endTime,diff,);
    // return `${diffHours.toString().length === 2 ? diffHours : "0"+diffHours }:${diffMin.toString().length === 2 ? diffMin : "0"+diffMin }:00`
    // return formatTime(Math.abs(endTimeS - startTimeS));
    return get_timeDifference(startTime,endTime,false);

}
export function getTimeDifference2(startTime,endTime) {
    //"2022-01-23 17:40:57"
    // const startTimeArray = splitTime(moment(startTime).format("HH:mm:ss"));
    // let endTimeArray = splitTime(moment(endTime).format("HH:mm:ss"));
    // let startTimeS = startTimeArray[0] * 3600 +  startTimeArray[1] * 60;
    // let endTimeS = endTimeArray[0] * 3600 +  endTimeArray[1] * 60;
    // let diffHours = Math.abs(endTimeArray[0] - startTimeArray[0]) ;
    // let diffMin = Math.abs(endTimeArray[1] - startTimeArray[1]);
    // let diffSen = Math.abs(endTimeArray[2] - startTimeArray[2]);
    // console.log("getTimeDifference2",diffHours,diffMin,diffSen);
    // let timeFormatted = formatTime(Math.abs(endTimeS - startTimeS));
    let timeArray = get_timeDifference(startTime,endTime,true).split(":");
    // console.log("getTimeDifference2",timeArray);
    return[parseInt(timeArray[0]),parseInt(timeArray[1]),parseInt(timeArray[2])];
}


export function getTimeDifferenceBool(startTime,endTime,diffInHours) {
    let startTimeArray = splitTime(moment(startTime,["hh:mm A"]).format("HH:mm"));
    let endTimeArray = splitTime(moment(endTime,["hh:mm A"]).format("HH:mm"));
    let diffHours = Math.abs(endTimeArray[0] - startTimeArray[0]) ;
    let diffMin = Math.abs(endTimeArray[1] - startTimeArray[1]);
    if(diffHours > diffInHours) {
        return true;
    } else {
        return  false;
    }
}

function isSameDay(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() &&
        d1.getDate() === d2.getDate() &&
        d1.getMonth() === d2.getMonth();
}


export function checkDateIsYesterday(entryDate) {
    const date = new Date();
    date.setDate(date.getDate() - 1);

    if(isSameDay(date,new Date(entryDate))) {
        return "Yesterday";
    } else {
        return entryDate.toString();
    }

}





export function getTotalTime(dataList) {
        let totalTime = [0,0,0];
        // console.log("dataLIST",dataList)
        dataList.forEach(
            (entry) => {
                // console.log(entry.user_duration);
                    let entryTime =  splitTime(entry.user_duration);
                    // console.log(entryTime,entry.user_duration)
                    totalTime[0] = totalTime[0] + entryTime[0];
                    totalTime[1] = totalTime[1] + entryTime[1];
                    totalTime[2] = totalTime[2] + entryTime[2];
            }
        )
    let totalSeconds = totalTime[2] + totalTime[1] * 60 + totalTime[0] * 3600;

        // console.log("totalTime",totalTime);
        return formatTime(totalSeconds);
    // `${totalTime[0].toString().length === 2 ? totalTime[0] : "0"+totalTime[0] }:${totalTime[1].toString().length === 2 ? totalTime[1] : "0"+totalTime[1] }:00`
}


//-----REDIRECT FUNCTION
export  const authCheck = () => {
        return localStorage.getItem(tokenKey) != null ? <Redirect to={'/app/main/dashboard'}/> : <Redirect to={'/login'}/>
}
