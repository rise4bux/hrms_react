import {apiGet, apiPost} from "./axiosServices";
import {
    addClientAPIString,
    addDepartmentAPIString, addDesignationAPIString, addEmployeeAPIString, addModuleAPIString,
    addProjectAPIString, addRolesAPIString,
    addTagAPIString,
    addToggleEntryAPIString,
    assignMemberToProjectAPIString,
    copyEntryAPIString, deleteEmployeeAPIString, getAllEmployeesAPIString,
    getAllProjectsAPIString,
    getAllTagsAPIString,
    getAllUsersAPIString,
    getClientByUserAPIString,
    getClientsAPIString,
    getCSVFileAPIString,
    getDepartmentsAPIString,
    getDesignationsAPIString,
    getFilterResultsAPIString, getModulesAPIString,
    getProjectsAPIString, getRolesAPIString,
    getTagsAPIString,
    getTeamMembersAPIString,
    getToggleEntries, getUserPermissionsAPIString,
    getUserProjectsList, getUserRoutesAPIString,
    getUserTimerStatusAPIString,
    loginWithID,
    loginWithIDPass,
    loginWithToken,
    pauseTimerAPIString,
    playTimerAPIString,
    removeClientAPIString,
    removeDepartmentAPIString, removeDesignationAPIString,
    removeMemberFromProjectAPIString, removeModuleAPIString,
    removeProjectAPIString, removeRolesAPIString,
    removeTagAPIString,
    removeToggleEntryAPIString, stopTimerAPIString,
    updateBillableDurationAPIString,
    updateClientAPIString,
    updateDepartmentAPIString, updateDesignationAPIString, updateEmployeeAPIString,
    updateManagerRightsOfMemberAPIString, updateModuleAPIString, updatePermissionByRoleAPIString,
    updateProjectAPIString, updateRolesAPIString,
    updateTagAPIString,
    updateToggleDateAndTimeAPIString,
    updateToggleDescriptionAPIString,
    updateToggleProjectAPIString,
    updateToggleTagAPIString
} from "./apiConstants";
import React from "react";
import {ENV, tokenKey, userData, userPermissions, userRoutesPermissions} from "../constants";
import {toggleCrudActions} from "../Redux/Actions/Actions";
import moment from "moment";
import {toast} from "react-toastify";
import {decryptData, encryptionData} from "./EncryptDecryptServices";
import axios from "axios";



 function getUserID() {
     if(localStorage.getItem(userData) != null) {
         const userID = JSON.parse(decryptData(localStorage.getItem(userData))).id
         // console.log("user id is :", userID)
         return userID != null ? userID : "";
     } else {
         return "";
     }

}
function getTimerStatus() {
    // let timerStatus = JSON.parse(localStorage.getItem(userData)).id
    // console.log("user id is :", userID)
    // return timerStatus != null ? timerStatus : false;
}



const getUserRoutesAPI = () => {
     let userID = getUserID();
     if(userID != "") {
         let payload = {
             user_id: userID,
         }
         return apiPost(getUserRoutesAPIString,payload).then((res) => {
             if(res != null && res != "") {
                 // console.log(res.data);
                 //ADDING USER TOKEN TO LOCAL STORAGE
                 localStorage.setItem(userRoutesPermissions,encryptionData(JSON.stringify(res.data)));
                 // if(res.message != null && res.message != "") {
                 //     toast.success(res.message);
                 // }
                 return {status: true, data: res.data};
             } else {
                 return {status: false, data: res.data};
             }
         });
     } else {
         return {status: false, data: {}};
     }


}

const getUserPermissionsAPI = () => {
    let userID = getUserID();
    if(userID != "") {
        let payload = {
            user_id: userID,
        }
        return apiPost(getUserPermissionsAPIString,payload).then((res) => {
            if(res != null && res != "") {
                // console.log(res);
                //ADDING USER TOKEN TO LOCAL STORAGE
                localStorage.setItem(userPermissions,encryptionData(JSON.stringify(res.permissions_data)));
                // if(res.message != null && res.message != "") {
                //     toast.success(res.message);
                // }
                return{status: true, data: res.data};
            } else {
                return {status: false, data: res.data};
            }
        });
    } else {
        return {status: false, data: {}};
    }
}


 const loginUserWithEmailAndPassword =  (email,password) => {
     let payload = {
        email: email,
        password: password,
    }
    // return
   return   apiPost(loginWithIDPass,payload).then((res) => {
        if(res != null && res != "") {
            // console.log("loginWithIDPass",res);
            // //ADDING USER TOKEN TO LOCAL STORAGE
            // localStorage.setItem(tokenKey,res.data.token.toString());
            // localStorage.setItem(userData,JSON.stringify(res.data));
            // // if(res.message != null && res.message != "") {
            // //     toast.success(res.message);
            // // }
            // console.log(res.data);
            // //ADDING USER TOKEN TO LOCAL STORAGE
            // localStorage.setItem(tokenKey,res.data.token.toString());
            // localStorage.setItem(userData,JSON.stringify(res.data));
            // if(res.message != null && res.message != "") {
            //     toast.success(res.message);
            // }
            localStorage.setItem(tokenKey,encryptionData(res.data.token.toString()));
            localStorage.setItem(userData,encryptionData(JSON.stringify(res.data)));
            localStorage.setItem(userPermissions,encryptionData(JSON.stringify(res.permissions_data)));
            return getUserRoutesAPI().then((r) =>  {
                return {status: true, data: res.data};
            })
            //{status: true, data: res.data};
            // loginUserWithID(res.data.id);

        } else {
            return {status: false, data: res.data};
        }
    });
}


 const loginUserWithID = (userID) => {
    let payload = {
        user_id: userID
    }
   return apiPost(loginWithID,payload).then((res) => {
        if(res != null && res != "") {
            // console.log(res.data);
            //ADDING USER TOKEN TO LOCAL STORAGE
            localStorage.setItem(tokenKey,encryptionData(res.data.token.toString()));
            localStorage.setItem(userData,encryptionData(JSON.stringify(res.data)));

            // if(res.message != null && res.message != "") {
            //     toast.success(res.message);
            // }
            return getUserRoutesAPI().then((r) =>  {
                return {status: true, data: res.data};
            })

        } else {
            return {status: false, data: res.data};
        }
    });

}


const loginUserWithEmail = (email) => {
    let payload = {
        email: email
    }
    return apiPost(loginWithToken,payload).then((res) => {
        if(res != null && res != "") {
            console.log(res.data);
            //ADDING USER TOKEN TO LOCAL STORAGE
            localStorage.setItem(tokenKey,encryptionData(JSON.stringify(res.data.token)));
            localStorage.setItem(userData,encryptionData(JSON.stringify(res.data)));
            localStorage.setItem(userPermissions,encryptionData(JSON.stringify(res.permissions_data)));
            // if(res.message != null && res.message != "") {
            //     toast.success(res.message);
            // }
            return getUserRoutesAPI().then((r) =>  {
                return {status: true, data: res.data};
            })
        } else {
            return {status: false, data: res.data};
        }
    });

}

const loginUserWithToken = (token) => {
    let payload = {
        token: token
    }
    return apiPost(loginWithToken,payload).then((res) => {
        if(res != null && res != "") {
            // console.log(res.data);
            //ADDING USER TOKEN TO LOCAL STORAGE
            localStorage.setItem(tokenKey,encryptionData(JSON.stringify(res.data.token)));
            localStorage.setItem(userData,encryptionData(JSON.stringify(res.data)));
            localStorage.setItem(userPermissions,encryptionData(JSON.stringify(res.permissions_data)));
            // if(res.message != null && res.message != "") {
            //     toast.success(res.message);
            // }
            return getUserRoutesAPI().then((r) =>  {
                return {status: true, data: res.data};
            })
        } else {
            return {status: false, data: res.data};
        }
    });

}

 const registerUser = () => {

}

//----GET ALL PROJECTS AND TAGS----
const getAllProjects = () => {
    let payload = {
        user_id: getUserID()
    }
    return apiPost(getAllProjectsAPIString,payload).then((res)=> {
        // console.log("getAllProjects",res);
        if(res != null && res != "") {
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const getAllTags = () => {
    let payload = {
        user_id: getUserID()
    }
    return apiPost(getAllTagsAPIString,payload).then((res)=> {
        // console.log("getAllTags",res);
        if(res != null && res != "") {
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}


//-----GET USER PROJECTS LIST---------------
const getProjectsList = () => {
    let payload = {
        user_id: getUserID()
    }
    return apiPost(getUserProjectsList,payload).then((res)=> {
        // console.log("getProjects",res);
        if(res != null && res != "") {
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}


//---------------GET USER TOGGLES LIST-----------------
const getToggleEntriesList = (offset) => {
    let payload = {
        user_id: getUserID(),
        offset: offset,
        limit: 10
    }
    return apiPost(getToggleEntries,payload).then((res)=> {
        // console.log("getToggleEntries",res);
        if(res != null && res != "") {
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const apiData = {
    loginUserWithEmailAndPassword,
    loginUserWithID,
    loginUserWithToken,
    loginUserWithEmail,
    registerUser,
    getProjectsList,
    getToggleEntriesList,
    getUserRoutesAPI,
    getUserPermissionsAPI
};


//----TOGGLE CRUD---
const addToggleEntryAPI = (payloadData,dispatch) => {
    let payload = {
       from_time: payloadData.startTime,
        to_time: payloadData.endTime,
        description: payloadData.description,
        project_id: payloadData.selectedProject.id,
        tag_id: payloadData.selectedTag.id,
        date:payloadData.selectedDate,
        user_id: getUserID()
    }
    // console.log("",payload);
    return apiPost(addToggleEntryAPIString,payload).then((res)=> {
        console.log("addToggleEntry",res);
        if(res != null && res != "") {
            let reduxData = res.data;
            reduxData['projects'] = payloadData.selectedProject;
            reduxData['tags'] = [payloadData.selectedTag];
            dispatch(toggleCrudActions.addToggleEntry(reduxData))
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeToggleEntryAPI = (toggleEntryID) => {
    let payload = {
        id : toggleEntryID
    }
    return apiPost(removeToggleEntryAPIString,payload).then((res)=> {
        // console.log("removeToggleEntry",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const updateToggleDescriptionAPI = (payloadData,id) => {
    let payload = {
        id :id,
        description: payloadData.description
    }
    return apiPost(updateToggleDescriptionAPIString,payload).then((res)=> {
        // console.log("updateToggleDescription",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const updateToggleProjectAPI = (id,projectID) => {
    // console.log("updateToggleProjectAPI",id,projectID)
    let payload = {
        id : id,
        project_id: projectID
    }
    return apiPost(updateToggleProjectAPIString,payload).then((res)=> {
        // console.log("updateToggleProject",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const updateToggleTagAPI = (id,tagID) => {
    let payload = {
        id : id,
        tag_id: tagID
    }
    return apiPost(updateToggleTagAPIString,payload).then((res)=> {
        // console.log("updateToggleTag",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const updateDateAndTimeAPI = (payloadData,id) => {
    let payload = {
        id : id,
        from_time: payloadData.startTime,
        to_time:payloadData.endTime,
        date: payloadData.selectedDate
    }
    // console.log("updateDateAndTime",payloadData,payload);
    return apiPost(updateToggleDateAndTimeAPIString,payload).then((res)=> {
        // console.log("updateDateAndTime",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const copyEntryAPI = (date) => {
    let payload = {
        "user_id": getUserID(),
        "date" : date
    }
    return apiPost(copyEntryAPIString,payload).then((res)=> {
        if(res != null && res != "") {
            // if(res.message != null && res.message != "") {
            //     toast.success(res.message);
            // }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const toggleCrudAPI = {
    addToggleEntryAPI,
    removeToggleEntryAPI,
    updateToggleDescriptionAPI,
    updateToggleProjectAPI,
    updateToggleTagAPI,
    updateDateAndTimeAPI,
    copyEntryAPI
}


//-----CLIENTS----------
const getClientsAPI = (offset,limit) => {
    let payload = {
        // id: 10,
        offset: offset,
        limit: limit,
    }
    return apiPost(getClientsAPIString,payload).then((res)=> {
        // console.log("getClientsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data,total_count: 0};
        }
    });
}
const updateClientAPI = (payloadData) => {
    let payload = {
        id: payloadData.id,
        name: payloadData.name,
        status: payloadData.status
    }
    return apiPost(updateClientAPIString,payload).then((res)=> {
        // console.log("updateClientAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeClientAPI = (id) => {
    let payload = {
        id: id //client_id
        // offset: 10,
        // limit: 10,
    }
    return apiPost(removeClientAPIString,payload).then((res)=> {
        // console.log("removeClientAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const addClientAPI = (payloadData) => {
    let payload = {
       name: payloadData.name,
        status: payloadData.status,
    }
    return apiPost(addClientAPIString,payload).then((res)=> {
        // console.log("addClientAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const clientsAPI = {
    getClientsAPI,
    addClientAPI,
    updateClientAPI,
    removeClientAPI,
}


//--TAGS API--------
const getTagsAPI = (offset,limit) => {
    let payload = {
        // id: 10,
        offset: offset,
        limit: limit,
        // is_default: 1,
    }
    return apiPost(getTagsAPIString,payload).then((res)=> {
        // console.log("getTagsAPI",res);
        if(res != null && res != "") {

            return {status: true, data: res.data,total_count : res.total_count};
        } else {
            return {status: false, data: res.data,total_count : 0};
        }
    });
}
const updateTagAPI = (payloadData) => {
    let payload = {
        id: payloadData.id,
        name: payloadData.name,
        status: payloadData.status,
        is_default: payloadData.is_default
    }
    return apiPost(updateTagAPIString,payload).then((res)=> {
        // console.log("updateTagAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeTagAPI = (id) => {
    let payload = {
        id: id //client_id
        // offset: 10,
        // limit: 10,
    }
    return apiPost(removeTagAPIString,payload).then((res)=> {
        // console.log("removeTagAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const addTagAPI = (payloadData) => {
    let payload = {
        name: payloadData.name,
        // project_id : payloadData.project_id,
        status: payloadData.status,
        is_default: payloadData.is_default
    }
    return apiPost(addTagAPIString,payload).then((res)=> {
        // console.log("addTagAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const tagsAPI = {
    getTagsAPI,
    addTagAPI,
    updateTagAPI,
    removeTagAPI,
}


//------PROJECTS--------
const getProjectsAPI = (offset,limit) => {
    let payload = {
        // id: 10,
        offset: offset,
        limit: limit,
    }
    return apiPost(getProjectsAPIString,payload).then((res)=> {
        // console.log("getProjectsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const searchProjectsAPI = (searchString) => {
    let payload = {
        offset: 0,
        limit: 30,
        search: searchString
    }
    return apiPost(getProjectsAPIString,payload).then((res)=> {
        // console.log("searchProjectsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const updateProjectAPI = (payloadData) => {
    let payload = {
        id: payloadData.id,
        name: payloadData.name,
        client_id: payloadData.client_id,
        color_hex: payloadData.color_hex,
        use_default_tag : payloadData.use_default_tag,
        status: payloadData.status,
        tags: payloadData.tags[0].id == null ? payloadData.tags : payloadData.tags.map((t) => t.id),
        created_tags: payloadData.created_tags,
    }
    // console.log("proejct update",payloadData);
    return apiPost(updateProjectAPIString,payload).then((res)=> {
        // console.log("updateProjectAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeProjectAPI = (id) => {
    let payload = {
        id: id //client_id
        // offset: 10,
        // limit: 10,
    }
    return apiPost(removeProjectAPIString,payload).then((res)=> {
        // console.log("removeProjectAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const addProjectAPI = (payloadData) => {
    let payload = {
        user_id: getUserID(),
        id: payloadData.id,
        name: payloadData.name,
        client_id: payloadData.client_id,
        color_hex: payloadData.color_hex,
        status: payloadData.status,
        use_default_tag : payloadData.use_default_tag,
        tags: payloadData.tags,
        created_tags: payloadData.created_tags,
    }
    // console.log("addProjectAPI payloads",payloadData,payload);
    return apiPost(addProjectAPIString,payload).then((res)=> {
        // console.log("addProjectAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const projectsAPI = {
    getProjectsAPI,
    addProjectAPI,
    updateProjectAPI,
    removeProjectAPI,
    searchProjectsAPI,
}

//------START TIMER--------------
const getUserTimerStatusAPI = () => {
    let payload = {
        user_id: getUserID()
    }
    return apiPost(getUserTimerStatusAPIString,payload).then((res)=> {
        // console.log("getUserTimerStatusAPI",res);
        if(res != null && res !== "") {

            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const playEntryTimerAPI = (payloadData) => {
    let payload = {
        description: payloadData.description,
        project_id: payloadData.selectedProject.id,
        tag_id: payloadData.selectedTag.id,
        user_id: getUserID()
    }
    return apiPost(playTimerAPIString,payload).then((res)=> {
        // console.log("playEntryTimerAPI",res);
        if(res != null && res !== "") {

            // if(res.message != null && res.message != "") {
            //     toast.success(res.message);
            // }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const pauseEntryTimerAPI = (payloadData) => {
    let payload = {
        user_id: getUserID(),
        description: payloadData.description,
        project_id: payloadData.selectedProject.id,
        tag_id: payloadData.selectedTag.id,
    }
    return apiPost(pauseTimerAPIString,payload).then((res)=> {
        // console.log("pauseEntryTimerAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const stopEntryTimerAPI = (payloadData) => {
    let payload = {
        user_id: getUserID(),
        description: payloadData.description,
        project_id: payloadData.selectedProject.id,
        tag_id: payloadData.selectedTag.id,
    }
    return apiPost(stopTimerAPIString,payload).then((res)=> {
        // console.log("pauseEntryTimerAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const timerAPI = {
    playEntryTimerAPI,
    pauseEntryTimerAPI,
    getUserTimerStatusAPI,
    stopEntryTimerAPI,
}

//-----ASSIGNING PROJECT TO MEMBERS APIS
const getAllUsersAPI = (offset,limit) => {
    let payload = {
        // id: 10,
        offset: offset,
        limit: limit,
    }
    return apiPost(getAllUsersAPIString,payload).then((res)=> {
        // console.log("getAllUsersAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const assignMemberToProjectAPI = (payloadData) => {
    let payload = {
        user_id: payloadData.user_id, // member user id
        is_manager: payloadData.is_manager,
        project_id: payloadData.project_id,
    }
    return apiPost(assignMemberToProjectAPIString,payload).then((res)=> {
        // console.log("assignMemberToProjectAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeMemberFromProjectAPI = (payloadData) => {
    let payload = {
        user_id: payloadData.user_id,
        project_id:payloadData.project_id
    }
    return apiPost(removeMemberFromProjectAPIString,payload).then((res)=> {
        // console.log("removeMemberFromProjectAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const updateManageRightsOfMember = (payloadData) => {
    let payload = {
        user_id: payloadData.user_id, // member user id
        is_manager: payloadData.is_manager,
        project_id: payloadData.project_id,
    }
    return apiPost(updateManagerRightsOfMemberAPIString,payload).then((res)=> {
        // console.log("UpdateManageRightsOfMember",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const membersAPI = {
    getAllUsersAPI,
    assignMemberToProjectAPI,
    removeMemberFromProjectAPI,
    updateManageRightsOfMember
}


//----REPORTS------------
const getClientsByUserProjectsAPI = () => {
    let payload = {
      user_id: getUserID()
    }
    return apiPost(getClientByUserAPIString,payload).then((res)=> {
        // console.log("getClientsByUserProjectsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const getTeamMembersAPI = () => {
    let payload = {
        user_id: getUserID()
    }
    return apiPost(getTeamMembersAPIString,payload).then((res)=> {
        // console.log("getTeamMembersAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const getFilterResultsAPI = (payloadData,offset,limit) => {
    let payload = {
        "offset": offset,
        "limit" : limit,
        "user_id": getUserID(),
        "project_ids": payloadData.project_ids, // [ 56,35 ]
        "tag_ids" : payloadData.tag_ids, // [ 56,35 ]
        "client_ids" : payloadData.client_ids, // [ 153,63 ]
        "team_ids" : payloadData.team_ids, // [ 56,63 ]
        "from_date" : payloadData.from_date,
        "to_date" : payloadData.to_date,
        "search" : payloadData.search,
    }
    return apiPost(getFilterResultsAPIString,payload).then((res)=> {
        // console.log("getFilterResultsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_records: res.total_records,total_hours: res.chartData.total_seconds,billable_total_hours:res.chartData.billable_seconds,hours: res.chartData.hours,chart_data: res.chartData};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const updateBillableDuration = (payloadData) => {
    let payload = {
        id: payloadData.id, //toggle tile entry ID,
        duration: payloadData.duration,
    }
    return apiPost(updateBillableDurationAPIString,payload).then((res)=> {
        // console.log("updateBillableDuration",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const downloadCSVFileAPI = (payloadData) => {
    let payload = {
        "user_id": getUserID(),
        "project_ids": payloadData.project_ids, // [ 56,35 ]
        "tag_ids" : payloadData.tag_ids, // [ 56,35 ]
        "client_ids" : payloadData.client_ids, // [ 153,63 ]
        "team_ids" : payloadData.team_ids, // [ 56,63 ]
        "from_date" : payloadData.from_date,
        "to_date" : payloadData.to_date,
    }
    // return fetch(ENV.STAGING + getCSVFileAPIString,{ method: "POST",: JSON.stringify(payload)}).then((res) => {
    //     console.log("downloadCSVFileAPI",res);
    //     if(res != null && res !== "") {
    //         // if(res.message != null && res.message != "") {
    //         //     toast.success(res.message);
    //         // }
    //         return {status: true, data: res.data};
    //     } else {
    //         return {status: false, data: res.data};
    //     }
    //     }
    // );
    return apiPost(getCSVFileAPIString,payload).then((res)=> {
        // console.log("downloadCSVFileAPI",res);
        if(res != null && res != "") {
            // if(res.message != null && res.message != "") {
            //     toast.success(res.message);
            // }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const reportsAPI = {
    updateBillableDuration,
    getFilterResultsAPI,
    getTeamMembersAPI,
    getClientsByUserProjectsAPI,
    downloadCSVFileAPI,
}


//-----DEPARTMENTS----
const getDepartmentsAPI = (offset,limit) => {
    let payload = {
        // id: 10,
        offset: offset,
        limit: limit,
    }
    return apiPost(getDepartmentsAPIString,payload).then((res)=> {
        // console.log("getDepartmentsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data,total_count: 0};
        }
    });
}
const updateDepartmentAPI = (payloadData) => {
    let payload = {
        id: payloadData.id,
        name: payloadData.name,
        status: payloadData.status
    }
    return apiPost(updateDepartmentAPIString,payload).then((res)=> {
        // console.log("updateDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeDepartmentAPI = (id) => {
    let payload = {
        id: id //client_id
        // offset: 10,
        // limit: 10,
    }
    return apiPost(removeDepartmentAPIString,payload).then((res)=> {
        // console.log("removeDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const addDepartmentAPI = (payloadData) => {
    let payload = {
        name: payloadData.name,
        status: payloadData.status,
    }
    return apiPost(addDepartmentAPIString,payload).then((res)=> {
        // console.log("addDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const departmentAPI = {
    getDepartmentsAPI,
    updateDepartmentAPI,
    removeDepartmentAPI,
    addDepartmentAPI,
}


//-----DESIGNATIONS----
const getDesignationsAPI = (offset,limit) => {
    let payload = {
        // id: 10,
        offset: offset,
        limit: limit,
    }
    return apiPost(getDesignationsAPIString,payload).then((res)=> {
        // console.log("getDepartmentsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data,total_count: 0};
        }
    });
}
const updateDesignationAPI = (payloadData) => {
    let payload = {
        id: payloadData.id,
        name: payloadData.name,
        status: payloadData.status
    }
    return apiPost(updateDesignationAPIString,payload).then((res)=> {
        // console.log("updateDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeDesignationAPI = (id) => {
    let payload = {
        id: id //client_id
        // offset: 10,
        // limit: 10,
    }
    return apiPost(removeDesignationAPIString,payload).then((res)=> {
        // console.log("removeDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const addDesignationAPI = (payloadData) => {
    let payload = {
        name: payloadData.name,
        status: payloadData.status,
    }
    return apiPost(addDesignationAPIString,payload).then((res)=> {
        // console.log("addDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const designationAPI = {
    getDesignationsAPI,
    updateDesignationAPI,
    removeDesignationAPI,
    addDesignationAPI,
}


//-----ROLES----
const getRolesAPI = (offset,limit) => {
    let payload = {
        // user_id: getUserID(),
        offset: offset,
        limit: limit,
    }
    return apiPost(getRolesAPIString,payload).then((res)=> {
        // console.log("getDepartmentsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data,total_count: 0};
        }
    });
}
const updateRoleAPI = (payloadData) => {
    let payload = {
        id: payloadData.id,
        name: payloadData.name,
        status: payloadData.status
    }
    return apiPost(updateRolesAPIString,payload).then((res)=> {
        // console.log("updateDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeRoleAPI = (id) => {
    let payload = {
        id: id //client_id
        // offset: 10,
        // limit: 10,
    }
    return apiPost(removeRolesAPIString,payload).then((res)=> {
        // console.log("removeDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const addRoleAPI = (payloadData) => {
    let payload = {
        name: payloadData.name,
        status: payloadData.status,
    }
    return apiPost(addRolesAPIString,payload).then((res)=> {
        // console.log("addDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const rolesAPI = {
    getRolesAPI,
    updateRoleAPI,
    removeRoleAPI,
    addRoleAPI,
}


//---EMPLOYEES----
const getAllEmployeesAPI = (offset,limit) => {
    let payload = {
        // id: 10,
        offset: offset,
        limit: limit,
    }
    return apiPost(getAllEmployeesAPIString,payload).then((res)=> {
        // console.log("getDepartmentsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data,total_count: 0};
        }
    });
}
const updateEmployeeAPI = (payloadData) => {
    let payload = {
        "id" : payloadData.id,
        "name" : payloadData.name,
        "email" : payloadData.email,
        "role_id" : payloadData.role_id.toString(),
        "password" : payloadData.password,
        "department_id" : payloadData.department_id,
        "designation_id" : payloadData.designation_id,
        "gender" : payloadData.gender,
        "date_of_birth" : payloadData.date_of_birth , //"07/11/1999",
        "blood_group" : payloadData.blood_group,//"a +",
        "pancard" : payloadData.payload,
        "aadhar_card" : payloadData.aadhar_card,
        "driving_licence_no" :payloadData.driving_licence_no == null ? "" : payloadData.driving_licence_no,
        "passport_no" :payloadData.passport_no == null ? "" : payloadData.passport_no,
        "bio" :payloadData.bio == null ? "" : payloadData.bio,
        "uan_no" : payloadData.uan_no == null ? "" :payloadData.uan_no,
        "esic_no" :payloadData.esic_no == null ? "" : payloadData.esic_no,
        "company_email" : payloadData.company_email == null ? "" :payloadData.company_email,
        "date_of_joining" : payloadData.date_of_joining == null ? "" :payloadData.date_of_joining,
        "aniversary_date" : payloadData.aniversary_date,
        "status": payloadData.status,
        "marital_status" : payloadData.marital_status == null ? "" :payloadData.marital_status,
        "report_heads" : payloadData.report_heads == null ? [] :payloadData.report_heads//[ 11 ,12 ]
    }
    // console.log("updateEmployeeAPI",payload);
    return apiPost(updateEmployeeAPIString,payload).then((res)=> {
        // console.log("updateDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeEmployeeAPI = (id) => {
    let payload = {
        id: id //client_id
        // offset: 10,
        // limit: 10,
    }
    return apiPost(deleteEmployeeAPIString,payload).then((res)=> {
        // console.log("removeDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const addEmployeeAPI = (payloadData) => {
    let payload = {
        "name" : payloadData.name,
        "email" : payloadData.email,
        "role_id" : payloadData.role_id,
        "password" : payloadData.password,
        "department_id" : payloadData.department_id,
        "designation_id" : payloadData.designation_id,
        "gender" : payloadData.gender,
        "date_of_birth" : payloadData.date_of_birth , //"07/11/1999",
        "blood_group" : payloadData.blood_group,//"a +",
        "pancard" : payloadData.payload,
        "aadhar_card" : payloadData.aadhar_card,
        "driving_licence_no" : payloadData.driving_licence_no,
        "passport_no" : payloadData.passport_no,
        "bio" : payloadData.bio,
        "uan_no" : payloadData.uan_no,
        "esic_no" : payloadData.esic_no,
        "company_email" : payloadData.company_email,
        "date_of_joining" : payloadData.date_of_joining,
        "aniversary_date" : payloadData.aniversary_date,
        "status": payloadData.status,
        "marital_status" : payloadData.marital_status,
        "report_heads" : payloadData.report_heads//[ 11 ,12 ]
    }
    // console.log("Add Employee Data", payload);
    return apiPost(addEmployeeAPIString,payload).then((res)=> {
        // console.log("addEmployeeAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const employeesAPI = {
    addEmployeeAPI,
    removeEmployeeAPI,
    updateEmployeeAPI,
    getAllEmployeesAPI,
}


//----PERMISSION API-----------
const updatePermissionByRoleAPI = (payloadData) => {

    let payload = payloadData;
    // console.log("update permission", payload);
    return apiPost(updatePermissionByRoleAPIString,payload).then((res)=> {
        // console.log("removeDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const permissionsAPI = {
    updatePermissionByRoleAPI
}


//------MODULES API----------------
const getModulesAPI = (offset,limit) => {
    let payload = {
        // user_id: getUserID(),
        offset: offset,
        limit: limit,
    }
    return apiPost(getModulesAPIString,payload).then((res)=> {
        // console.log("getDepartmentsAPI",res);
        if(res != null && res != "") {
            return {status: true, data: res.data,total_count: res.total_count};
        } else {
            return {status: false, data: res.data,total_count: 0};
        }
    });
}
const updateModuleAPI = (payloadData) => {
    let payload = {
        name: payloadData.name,
        url: payloadData.module_url,
        parent_id : payloadData.parent_id,
        id: payloadData.id
    }
    return apiPost(updateModuleAPIString,payload).then((res)=> {
        // console.log("updateDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const removeModuleAPI = (id) => {
    let payload = {
        id: id //client_id
        // offset: 10,
        // limit: 10,
    }
    return apiPost(removeModuleAPIString,payload).then((res)=> {
        // console.log("removeDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
const addModuleAPI = (payloadData) => {
    let payload = {
        name: payloadData.name,
        url: payloadData.module_url,
        parent_id : payloadData.parent_id
    }
    return apiPost(addModuleAPIString,payload).then((res)=> {
        // console.log("addDepartmentAPI",res);
        if(res != null && res != "") {
            if(res.message != null && res.message != "") {
                toast.success(res.message);
            }
            return {status: true, data: res.data};
        } else {
            return {status: false, data: res.data};
        }
    });
}
export const modulesAPI = {
    getModulesAPI,
    updateModuleAPI,
    removeModuleAPI,
    addModuleAPI,
}
