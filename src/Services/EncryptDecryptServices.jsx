import {SECRET_IV, SECRET_KEY} from "../constants";

const CryptoJS = require("crypto-js");


const Sha256 = CryptoJS.SHA256;
const Hex = CryptoJS.enc.Hex;
const Utf8 = CryptoJS.enc.Utf8;
const Base64 = CryptoJS.enc.Base64;
const AES = CryptoJS.AES;


const key = Sha256(SECRET_KEY).toString(Hex).substr(0,32); // Use the first 32 bytes (see 2.)
const iv = Sha256(SECRET_IV).toString(Hex).substr(0,16);

export const encryptionData = (data) => {
    const output = AES.encrypt(data, Utf8.parse(key), {
        iv: Utf8.parse(iv),
    }).toString();
    return Utf8.parse(output).toString(Base64);
}

export const decryptData = (data) => {
    const decrypted = AES.decrypt(atob(data), Utf8.parse(key), {
        iv: Utf8.parse(iv),
    }).toString(Utf8);
    return decrypted;
}