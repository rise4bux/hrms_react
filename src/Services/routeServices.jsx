import ToggleReport from "../MainPage/Employees/Toggle/toggleReport";
import ToggleEntries from "../MainPage/Employees/Toggle/toggleEntries";
import Projects from "../MainPage/Projects/projects";
import Tags from "../MainPage/Tags/tags";
import Clients from "../MainPage/Clients/clients";
import Designations from "../MainPage/Administration/Designations/Designations";
import Departments from "../MainPage/Administration/Departments/Departments";
import Roles from "../MainPage/Administration/Roles/Roles";
import Employees from "../MainPage/Administration/Employees/Employees";
import Permissions from "../MainPage/Administration/Permissions/Permissions";
import Modules from "../MainPage/Administration/Modules/Modules";



// export const routeService = [
//     {
//         id: 1,
//         icon: "la la-key",
//         module_url: "admin",
//         url: "/app/admin",
//         name: "Admin",
//         children: [
//             {
//                 id: 13,
//                 icon: "",
//                 url: "/app/admin/employees",
//                 module_url: "employees",
//                 name: "Employees",
//                 children: []
//             },
//             {
//                 id: 2,
//                 icon: "",
//                 url: "/app/admin/department",
//                 module_url: "department",
//                 name: "Departments",
//                 children: []
//             },
//             {
//                 id: 3,
//                 icon: "",
//                 url: "/app/admin/designation",
//                 module_url: "designation",
//                 name: "Designations",
//                 children: []
//             },
//             {
//                 id: 4,
//                 icon: "",
//                 url: "/app/admin/roles",
//                 module_url: "roles",
//                 name: "Roles",
//                 children: []
//             },
//             {
//                 id: 5,
//                 icon: "",
//                 url: "/app/admin/permissions-roles",
//                 module_url: "permissions",
//                 name: "Roles & Permissions",
//                 children: []
//             },
//             {
//                 id: 6,
//                 icon: "",
//                 url: "/app/admin/modules",
//                 module_url: "modules",
//                 name: "Modules",
//                 children: []
//             },
//
//         ]
//     },
//     {
//         id: 7,
//         icon: "la la-dashboard",
//         module_url: "toggl",
//         url: "/app/toggl",
//         name: "Toggl",
//         children: [
//             {
//                 id: 8,
//                 icon: "",
//                 url: "/app/toggl/toggle-entries",
//                 module_url: "toggle-entries",
//                 name: "Toggl Entries",
//                 children: []
//             },
//             {
//                 id: 9,
//                 icon: "",
//                 // "la la-pie-chart",
//                 url: "/app/toggl/toggle-report",
//                 module_url: "toggle-report",
//                 name: "Toggl Report",
//                 children: []
//             }]
//     },
//     {
//         id: 10,
//         icon:  "la la-rocket",
//         url: "/app/projects",
//         module_url: "projects",
//         name: "Projects",
//         children: []
//     },
//     {
//         id: 11,
//         icon: "la la-puzzle-piece",
//         url: "/app/tags",
//         module_url: "tags",
//         name: "Tags",
//         children: []
//     },
//     {
//         id: 12,
//         icon: "la la-users",
//         url: "/app/clients",
//         module_url: "client",
//         name: "Clients",
//         children: []
//     },
//
//
// ]

export const homePage = "toggle-entries";
export const homePageURL = "/app/toggl/toggle-entries";
export const loginRouteURL = "/login";
const togglReportModule = "toggle-report";
const togglEntriesModule = "toggle-entries";
const projectModule = "projects";
const tagsModule = "tags";
const clientModule = "client";
const designationModule = 'designations';
const departmentModule = "department";
const rolesModule = "roles";
const permissionsModule = "permissions";
const employeesModule = "employee";
const modulesModule = "modules"

export const getRouteComponent = (routePath) => {
    // console.log("getRouteCompoenet",routePath);
    switch (routePath) {
        case togglReportModule:
            return ToggleReport;
        case togglEntriesModule:
            return ToggleEntries;
        case projectModule:
            return Projects;
        case tagsModule:
            return Tags;
        case clientModule:
            return Clients;
        case designationModule:
            return Designations;
        case departmentModule:
            return Departments;
        case rolesModule:
            return Roles;
        case employeesModule:
            return Employees;
        case modulesModule:
            return Modules;
        case permissionsModule:
            return Permissions;
        default:
            return "";
    }
}
