export const loginWithID = "login";
export const loginWithToken = "login-by-token";
export const loginWithIDPass = "login-by-password";
export const getAllProjectsAPIString = "get-projects";
export const getAllTagsAPIString = "get-projects";
export const getUserProjectsList = "get-projects-by-toggle-entries";
export const getToggleEntries = "get-toggle-entries";
//----TOGGLE CRUDS------------
export const addToggleEntryAPIString = "add-entry";
export const removeToggleEntryAPIString = "remove-entry";
export const updateToggleDescriptionAPIString = "update-description";
export const updateToggleProjectAPIString = "update-project-by-toggle-entries";
export const updateToggleTagAPIString = "update-tag-by-toggle-entries";
export const updateToggleDateAndTimeAPIString = "update-date-and-time";
export const copyEntryAPIString = "copy-entry";
//----CLIENTS----
export const getClientsAPIString = "get-clients";
export const updateClientAPIString = "update-client";
export const removeClientAPIString = "remove-client";
export const addClientAPIString = "add-client";
//---TAGS---
export const getTagsAPIString = "get-tags";
export const addTagAPIString = "add-tag";
export const removeTagAPIString = "remove-tag";
export const updateTagAPIString = "update-tag";
//---PROJECTS---
export const getProjectsAPIString = "get-projects";
export const addProjectAPIString = "add-project";
export const removeProjectAPIString = "remove-project";
export const updateProjectAPIString = "update-project";

//-------PROJECT TIMER---------
export const getUserTimerStatusAPIString = "get-timer";
export const playTimerAPIString = "play-timer";
export const pauseTimerAPIString = "pause-timer";
export const stopTimerAPIString = "stop-timer";

//----PROJECT MEMBERS API
export const getAllUsersAPIString = "get-users";
export const  assignMemberToProjectAPIString= "assign-member-to-project";
export const  removeMemberFromProjectAPIString= "remove-member-from-project";
export const updateManagerRightsOfMemberAPIString = "update-manager-rights-of-member";


//----REPORTS SECTION----
export const getClientByUserAPIString = "get-clients-by-user-project";
export const updateBillableDurationAPIString = "update-billable-duration";
export const getTeamMembersAPIString = "get-team-members";
export const getFilterResultsAPIString = "get-filter-results";
export const getCSVFileAPIString = "get-csv-file";


//----DEPARTMENT SECTION----
export const getDepartmentsAPIString = "get-departments";
export const updateDepartmentAPIString = "update-department";
export const removeDepartmentAPIString = "remove-department";
export const addDepartmentAPIString = "add-department";


//----DESIGNATION SECTION----
export const getDesignationsAPIString = "get-designations";
export const updateDesignationAPIString = "update-designation";
export const removeDesignationAPIString = "remove-designation";
export const addDesignationAPIString = "add-designation";



//----ROLES SECTION----
export const getRolesAPIString = "get-roles";
export const updateRolesAPIString = "update-role";
export const removeRolesAPIString = "remove-role";
export const addRolesAPIString = "add-role";

//----EMPLOYEES SECTION----------
export const getAllEmployeesAPIString = "get-employees";
export const addEmployeeAPIString = "add-employee";
export const updateEmployeeAPIString = "update-employee";
export const deleteEmployeeAPIString = "remove-employee";

//---PERMISSIONS SECTION--------
export const getUserRoutesAPIString = "get-route";
export const updatePermissionByRoleAPIString = "update-permission-by-role";
export const getUserPermissionsAPIString  = "get-user-permissions";

//----MODULES SECTION-----
export const getModulesAPIString = "get-modules";
export const updateModuleAPIString = "update-module";
export const removeModuleAPIString = "remove-module";
export const addModuleAPIString = "add-module";