import axios from "axios";
import regeneratorRuntime from "regenerator-runtime";
import {toast} from "react-toastify";
import {ENV} from "../constants";

// const axios = require('axios').default;

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL
       // "http://192.168.1.72:8000/api"
    // ENV.LIVE, //-----SET LIVE/STAGING------

});
// Add a response interceptor
// api.interceptors.response.use(function (response) {
//     // Any status code that lie within the range of 2xx cause this function to trigger
//     // Do something with response data
//     console.log(response.status);
//     return response;
// }, function (error) {
//     // Any status codes that falls outside the range of 2xx cause this function to trigger
//     // Do something with response error
//     console.log(error);
//     return Promise.reject(error);
// });



const setHeaders = (name, value) => {

    api.defaults.headers[name] = value
}


export const apiGet = async (url,payload) => {
    let response = "";
    await api.get(url, {
        params: JSON.stringify(payload)}).then((res) => {
            if(res.status === 200) {
                response = res;
            }
        },
        (onRejected) => {
            // console.log(onRejected.response.data.message);
            toast.error(onRejected.response.data.message);
        }
    );
    return response;
}

export const apiPost = async (url,payload) => {
    let response = "";
    await api.post(url,payload,).then(
        (res) => {
            // console.log(res);
            if(res.status === 200) {
                response = res.data;

            }
        },
        (onRejected) => {
            // console.log("rejected",onRejected.response.data.message);
            if(typeof  onRejected.response.data.message === "string") {
                toast.error(onRejected.response.data.message);
            } else {
                onRejected.response.data.message.forEach((message) => {
                    toast.error(message);
                })
            }

        }
    )
    return response;
}
