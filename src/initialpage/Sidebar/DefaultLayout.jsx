/**
 * App Routes
 */
import React, {Component, useEffect} from 'react';
import {Link, Redirect, Route, useHistory, withRouter} from 'react-router-dom';

// router service
import routerService from "../../router_service";

import Header from './header.jsx';
import SidebarContent from './sidebar';
import {authCheck, dashBoardHelmet, getCurrentUserRoutesPermissions, tokenKey} from "../../constants";
import ToggleEntries from "../../MainPage/Employees/Toggle/toggleEntries";
import Projects from "../../MainPage/Projects/projects";
import ToggleReport from "../../MainPage/Employees/Toggle/toggleReport";
import Tags from "../../MainPage/Tags/tags";
import Clients from "../../MainPage/Clients/clients";
import Designations from "../../MainPage/Administration/Designations/Designations";
import Departments from "../../MainPage/Administration/Departments/Departments";
import Roles from "../../MainPage/Administration/Roles/Roles";
import Employees from "../../MainPage/Administration/Employees/Employees";
import {getRouteComponent, homePageURL, loginRouteURL, routeService} from "../../Services/routeServices";
import {Helmet} from "react-helmet";




const DefaultLayout = (props) => {
		const { match } = props;
		// console.log("DEFAULT LAYOUT CALLED",location.pathname,document.referrer);
	const history = useHistory();
	useEffect(() => {
		console.log("default layout",location.pathname);
		// if(location.pathname === "/login") {
		// 	history.push(loginRouteURL);
		// }
	},[]);

	const getRoutes = (routes) => {
		return routes.map((route,key) => {
			if(route.children == null || route.children.length === 0) {
				return <Route key={key} path={route.url} component={getRouteComponent(route.module_url)} />
			} else {
				return getRoutes(route.children);
			}
		})
	}


		return (

			<>
				<Header/>
				<div>
					{getRoutes(getCurrentUserRoutesPermissions())}
					{/*{getRoutes(routeService)}*/}
					{/*{routerService && routerService.map((route,key)=>*/}
					{/*	<Route key={key} path={`${match.url}/${route.path}`} component={route.component} />*/}
					{/*)}*/}
				</div>
				<SidebarContent/>

			</>
		);
	
}
export default withRouter(DefaultLayout);