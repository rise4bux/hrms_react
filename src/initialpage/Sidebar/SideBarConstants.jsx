export const togglModuleName = "Toggl";
export const togglEntriesModuleName = "Toggl Entries";
export const togglReportModuleName = "Toggl Report";


export const projectsModuleName = "Projects";
export const clientsModuleName = "Clients";
export const tagsModuleName = "Tags";

export const adminModuleName = "Administrator";
export const rolesAndPermissionsModuleName = "Roles";
export const createPermissionsModuleName = "Roles & Permissions";
export const designationModuleName = "Designation";
export const departmentModuleName = "Department";
export const modulesModuleName = "Modules";
export const employeesModuleName = "Employees";