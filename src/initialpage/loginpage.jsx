/**
 * Signin Firebase
 */

import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { headerlogo } from "../Entryfile/imagepath.jsx";
import { GoogleLogin } from "@react-oauth/google";
import jwt_decode from "jwt-decode";

import {
  checkAllDataInLocalStorage,
  companyName,
  dashBoardHelmet,
  tokenKey,
} from "../constants";
import { apiData } from "../Services/apiServices";
import { Link, useHistory, withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";
import { actions } from "../Redux/Actions/Actions";
import {
  decryptData,
  encryptData,
  encryptionData,
} from "../Services/EncryptDecryptServices";
import { homePageURL, loginRouteURL } from "../Services/routeServices";
import "../assets/css/google-sign-in.css";

const Loginpage = () => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  let history = useHistory();
  const authToken = localStorage.getItem(tokenKey);
  const [pageLoading, setPageLoading] = useState(true);
  useEffect(() => {
    // console.log("state login",location.pathname);
    if (location.pathname.split("/")[2] != null) {
      let encryptedData = location.pathname.split("/")[2];
      // console.log("redirected from old",encryptedData);
      // history.push(toggleEntries);
      setPageLoading(true);
      apiData.loginUserWithToken(encryptedData).then((res) => {
        if (res.status) {
          if (checkAllDataInLocalStorage()) {
            history.push("/");
            dispatch(actions.setUserLoginState(true));
            dispatch(actions.setUserData(res.data));
          } else {
            dispatch(actions.setUserLoginState(false));
            history.push(loginRouteURL);
          }
        }
        setPageLoading(false);
      });
    } else {
      setPageLoading(false);
      // console.log("new user",location.pathname);
      if (checkAllDataInLocalStorage()) {
        dispatch(actions.setUserLoginState(true));
        history.push("/");
      } else {
        dispatch(actions.setUserLoginState(false));
        history.push(loginRouteURL);
      }
    }
  }, [authToken]);

  // const re =
  //   "^[a-zA-Z0-9.a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\\.[a-zA-Z]+";
  // const validate = () => {
  //   if (!email.match(re)) {
  //     emailRef.current.style.border = "2px solid red";
  //   } else {
  //     emailRef.current.style.border = "1px solid #e3e3e3";
  //   }
  //   if (password.trim() == "") {
  //     passwordRef.current.style.border = "2px solid red";
  //   } else {
  //     passwordRef.current.style.border = "1px solid #e3e3e3";
  //   }
  //   if (email.match(re) && password.trim() != "") {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // };

  const loginClicked = async (e) => {
    // const { user_id, email } = await signInWithGoogle();
    var decodedUserData = jwt_decode(e?.credential);
    // console.log("loginClicked", decodedUserData?.email);
    const response = await apiData.loginUserWithEmail(decodedUserData?.email);
    if (response && response?.status) {
      history.push(loginRouteURL);
    }
  };

  return (
    <>
      <Helmet>
        <title>{dashBoardHelmet}</title>
        <meta name="description" content="Login page" />
      </Helmet>
      <>
        <div
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            MsTransform: "translate(-50%, -50%)",
            transform: "translate(-50%, -50%)",
          }}
        >
          <div className="container-login">
            {/* <div className="left">
            <div>
                <div className="login">Login</div>
                <div className="eula">Login with your Company Gmail</div>
              </div>
           
            </div> */}
            <div className="right">
              <img
                src={headerlogo}
                alt={companyName}
                style={{ width: "220px" }}
              />
              <div
                style={{
                  margin: "0px auto",
                  marginTop: "50px",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                <div className="eula">
                  Login with your Company Gmail to continue
                </div>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <GoogleLogin
                    onSuccess={loginClicked}
                    onError={(e) => {
                      console.log("on error", e);
                    }}
                  />
                </div>

                {/* <button type="button" className="login-with-google-btn" onClick={loginClicked}>
                  Sign in with Google
                </button> */}
              </div>
            </div>
          </div>
        </div>
      </>
    </>
  );
};

export default withRouter(Loginpage);
