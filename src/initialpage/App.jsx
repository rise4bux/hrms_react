import React, {Component, useEffect, useState} from 'react';
import {Redirect, Route, Switch, useHistory} from 'react-router-dom';
// We will create these two pages in a moment
//Authendication
import LoginPage from './loginpage'
import RegistrationPage from './RegistrationPage'
import ForgotPassword from './forgotpassword'
import OTP from './otp'
import LockScreen from './lockscreen'
import ApplyJobs from './ApplyJob';

//Main App
import DefaultLayout from './Sidebar/DefaultLayout';
import Settinglayout from './Sidebar/Settinglayout';
import Tasklayout from './Sidebar/tasklayout';
import Emaillayout from './Sidebar/emaillayout';
import chatlayout from './Sidebar/chatlayout';

import uicomponents from '../MainPage/UIinterface/components';
//Error Page
import Error404 from '../MainPage/Pages/ErrorPage/error404';
import Error500 from '../MainPage/Pages/ErrorPage/error500';


// import 'Assets/css/font-awesome.min.css';

import $ from 'jquery';
import {toast, ToastContainer} from "react-toastify";
import {checkAllDataInLocalStorage, getCurrentUserRoutesPermissions, tokenKey} from "../constants";
import {checkPermission} from "../permissionConstants";
import {getRouteComponent, homePageURL, loginRouteURL} from "../Services/routeServices";
import {InitialPage} from "./initialPage";
import Header from "./Sidebar/header";
import SidebarContent from "./Sidebar/sidebar";
import toggleEntries from "../MainPage/Employees/Toggle/toggleEntries";
import {apiData} from "../Services/apiServices";
import {actions} from "../Redux/Actions/Actions";
import {connect, useDispatch} from "react-redux";
import {Spin} from "antd";
import Employees from '../MainPage/Administration/Employees/Employees';
// window.jQuery = $;
// window.$ = $;
// import UserPage from './pages/UserPage'
/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
const getRoutes = (routes) => {
    // console.log("routePage",)
    return routes.map((route,key) => {
        if(route.children == null || route.children.length === 0) {
            return <Route key={key} path={route.url} component={getRouteComponent(route.module_url)} />
        } else {
            return getRoutes(route.children);
        }
    })

}
//
// export const App = () => {
//
//     useEffect(() => {
//         console.log("inapp useeffect");
//         if (location.pathname.includes("by-pass-login")
//             // && document.referrer.includes("hrms.weboccult.com")
//         ) {
//             console.log("includes", location.pathname);
//             let encryptedData = location.pathname.split("/")[2];
//             // console.log("redirected from old",encryptedData);
//             // history.push(toggleEntries);
//             // setPageLoading(true);
//             apiData.loginUserWithToken(encryptedData).then((res) => {
//                 if (res.status) {
//                     if (checkAllDataInLocalStorage()) {
//                         // history.push("/");
//                         dispatch(actions.setUserLoginState(true));
//                         dispatch(actions.setUserData(res.data));
//                     } else {
//                         dispatch(actions.setUserLoginState(false));
//                         // history.push(loginRouteURL);
//                     }
//                 }
//                 // setPageLoading(false);
//             })
//         } else {
//             console.log("not found");
//         }
//     },[])
//
//
//
//     return (
//         <>
//             {checkAllDataInLocalStorage() ?
//                 <>
//                     <Header/>
//                     <div>
//                         {getRoutes(getCurrentUserRoutesPermissions())}
//                     </div>
//                     <SidebarContent/>
//                 </>
//                 :
//                 <Redirect to={loginRouteURL} />
//             }
//         </>
//     )
// }
const loginBoxStyle = {
    background: "#232526",
    border: "1px solid #232526",
    color: "white"
}
const loadingDivStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    height: "50vh"
}

  class App extends Component {

    componentDidMount() {
        // console.log("APP");

    }
      state = {
          redirect: false,
          toLogin: false,
      };

      renderRedirect = () => {
          if(this.state.redirect){
              if(this.state.toLogin) {
                  return (
                      <Redirect
                          to={loginRouteURL}
                      />
                  );
              } else {
                  return (
                      <Redirect
                          to="/"
                      />
                  );
              }

          } else {
              return (
                  <div style={loadingDivStyle}>
                      <Spin size="large" spinning={true}></Spin>
                      <h4 style={{color: "whitesmoke", marginTop: "10px"}}>redirecting...please wait...!!</h4>
                  </div>
              );
          }
      };

      noRouteSelected = () =>   {
          if(getRouteComponent(location.pathname.split("/").pop()) === "") {
              // console.log("no route found",location.pathname);
              return  <Redirect to={homePageURL} />
          } else {
              // console.log("route found")
          }
  }

       render(){
            const { location, match, user } = this.props;

           //---FROM OLD HRMS ROUTE-----
           if(location.pathname.includes("by-pass-login")
               && document.referrer.includes("hrms.weboccult.com")
           ) {
               let encryptedData = location.pathname.split("/")[2];
               apiData.loginUserWithToken(encryptedData).then((res) => {
                   if (res.status) {
                       if (checkAllDataInLocalStorage()) {
                           this.props.setUserLoginState(true);
                           this.props.setUserData(true);
                           this.setState({ redirect: true , toLogin: false});
                          return  <Redirect to={"/"} />
                       } else {
                           this.props.setUserLoginState(false);
                           this.setState({ redirect: true,  toLogin: true });
                       }
                   }
               })
               return this.renderRedirect()
           } else {
               if(!checkAllDataInLocalStorage()) {
                   return (<Redirect to={loginRouteURL} />);
               } else {
                   return (
                       <>
                           <Header/>
                           <div>
                           {/* <Route key={'emp'} path={'/employee'} component={Employees} /> */}
                               {getRoutes(getCurrentUserRoutesPermissions())}
                               {this.noRouteSelected()}
                           </div>
                           <SidebarContent/>
                       </>

                   )
               }
           }
        }

}
const mapDispatchToProps = (dispatch) => {
    return {
        setUserLoginState: () => dispatch(actions.setUserLoginState()),
        setUserData: () => dispatch(actions.setUserData())
    }
};

export default connect(null, mapDispatchToProps)(App)
