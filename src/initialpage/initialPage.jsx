import React,{useEffect} from "react";
import {Redirect, Route, Switch, useHistory} from "react-router-dom";
import {loginRouteURL} from "../Services/routeServices";
import {tokenKey} from "../constants";
import DefaultLayout from "./Sidebar/DefaultLayout";


export const InitialPage = () => {

    const history = useHistory()
    useEffect(() => {
        console.log("Initial PAGE");
        if(localStorage.getItem(tokenKey) != null) {
            console.log(location.pathname)
            history.push(location.pathname);
            // return (<Redirect to={location.pathname} />);
        }
        else {
            history.push(loginRouteURL);
            // return (<Redirect to={loginRouteURL} />);
        }
    },[])

    return (
        <Switch>
            <Route  path="/app" component={DefaultLayout} />
        </Switch>
    )



}