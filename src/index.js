import React from "react";
import ReactDOM from "react-dom";
import Main from "./Entryfile/Main";
import store from "./Redux/Store/store";
import {Provider} from "react-redux";
import {loginState, tokenKey} from "./constants";
import { GoogleOAuthProvider } from '@react-oauth/google';


store.subscribe(() => {
        // console.log(store.getState())
    }
)
ReactDOM.render(
    <GoogleOAuthProvider clientId={process.env.GOOGLE_SIGN_IN_OAUTH_CLIENT_ID}>
    <Provider store={store}>
    <Main/>
    </Provider>
    </GoogleOAuthProvider>
    ,
    document.getElementById('app'));

if (module.hot) { // enables hot module replacement if plugin is installed
 module.hot.accept();
}