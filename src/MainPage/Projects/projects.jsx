
import React, { useState,useEffect } from 'react';
import { Helmet } from "react-helmet";
import {Link, useHistory} from 'react-router-dom';
import {Button, Divider, Table, Modal, Input, Empty, Checkbox, Badge, Tag, Tooltip} from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "../paginationfunction"
import "../antdstyle.css"
import {
    dashBoardHelmet, getCurrentUser,
    permissionRoutes,
    permissionTypes,
    preventALinkDefault,
} from "../../constants";
import {clientsAPI, membersAPI, projectsAPI, tagsAPI} from "../../Services/apiServices";
import {useDispatch, useSelector} from "react-redux";
import {clientActions} from "../../Redux/Actions/ClientsActions";
import { Spin } from 'antd';
import {projectActions} from "../../Redux/Actions/ProjectActions";
import SelectClientButton from "./ProjectComponents/SelectClientButton";
import {memberActions} from "../../Redux/Actions/MembersActions";
import {MembersBlock} from "./ProjectComponents/MembersBlock";
import {toast} from "react-toastify";
import {checkPermission} from "../../permissionConstants";
import {homePageURL, loginRouteURL} from "../../Services/routeServices";
import SelectStatus from "../Administration/Employees/Components/SelectStatus";
import SelectTagsButton from "./ProjectComponents/SelectTagsButton";
import {tagsActions} from "../../Redux/Actions/TagsActions";
import ViewDataTile from "../../CustomComponents/ViewDataTile";
import {searchStyle, tableStyles} from "../../global_styles";

//-----PROJECT PAGE DATA OBJECT-----
const projectDataObj = {
    "id": 0,
    "name": "",
    "client_id": null,
    "status": 1,
    "use_default_tag": 0,
    "client": null,
    "created_tags": [],
    "color_hex" : "#ffffff",
    "members": [
    ],
    "tags": [
    ]
}
//-----PROJECT PAGE STYLES-------
const inputRowStyle = {
    // justifyContent: "center"
    margin: "20px 0px 10px 0px"

}

const  dialogButtonStyle = {
    border: "1px solid #f43b48",
    borderRadius: "10px",
    background: "#f43b48",
    color: "white"
}

const spinnerStyle = {
    display: "flex",
    justifyContent: "center",
    marginTop: "20px"
}

const addTagsButtonStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textTransform: "none"
    // margin: "0px 30px 0px 0px"
}

const Projects = () => {




    const dispatch = useDispatch();
    const allProjectsList = useSelector((store)=> store.db.allProjects);
    const allClientsList = useSelector((store)=> store.db.allClients);
    const allUsersList = useSelector((store)=> store.db.allUsers);
    const allTagsList = useSelector((store)=> store.db.allTags);
    const totalProjects =  useSelector((store)=> store.db.totalProjects);
    const [uiProjectsList,setUIProjectsList] = useState([...allProjectsList]);
    const [newProject,setNewProject] = useState( projectDataObj);
    const [showForm,setShowForm] = useState(false);
    const [showUpdateForm,setShowUpdateForm] = useState(false);
    const [showMemberForm,setShowMemberForm] = useState(false);
    const [toUpdateProject,setToUpdateProject] = useState(projectDataObj);
    const [state,setState] = useState(false);
    const [loading,setLoading] = useState(false);

    const [viewForm,setViewForm] = useState(false);
    const [toViewData,setToViewData] = useState({});

    const viewModel = () => {
        return ( <Modal
                centered
                footer={null}
                visible={viewForm}
                destroyOnClose={true}
                style={{
                    // background: "black",
                    // border: "1px solid rgba(255, 255, 255,0.25)",
                    // borderRadius: "10px",
                    // color: "white",
                }}
                bodyStyle={{
                    // background: "black",
                    //     // "rgba(0, 0, 0, 0.5)",
                    // color: "white",
                }}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onCancel={() => {setViewForm(false); setToViewData(projectDataObj)}}
            >
                <div>
                    <div><h4>Project: {toViewData.name}</h4></div>
                    <Divider  style={{
                        // background: "#DD1845",
                        margin: "10px 5px 10px 0px"}} />
                   <ViewDataTile parameters={{label: "Name",data: toViewData.name}}/>
                    <ViewDataTile parameters={{label: "Color_Hex",data: toViewData.color_hex}} isColorData={true}/>
                    <ViewDataTile parameters={{label: "Client",data: toViewData.clients}} isListData={true}/>
                    <ViewDataTile parameters={{label: "Members",data: toViewData.members}} isListData={true}/>
                    <ViewDataTile parameters={{label: "Tags",data: toViewData.tags}} isListData={true}/>
                </div>
            </Modal>
        )
    }
    const projectsColumn = [
        {
            title: 'Name',
            dataIndex: 'name',
            render: (text, record) => (
                <h2 className="table-avatar">
                    {text}
                </h2>
            ),
            sorter: (a, b) => a.name.length - b.name.length,
        },
        // {
        //     title: 'Tags',
        //     dataIndex: 'tags',
        //     width: "30%",
        //     render: (text, record) => (
        //         <>
        //             {record.tags.map((tag,key) => {
        //                 return (<Tag color="rgba(255, 255, 255,0.3)" key={tag.id}>{tag.name}</Tag>)
        //             })}
        //         </>
        //
        //     ),
        // },
        {
            title: 'Client',
            dataIndex: 'client',
            render: (text, record) => (
                <span onClick={() => {
                    // console.log(record)
                }
                }>{record.client != null ? record.client.name : "No Client"}</span>
            ),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            render: (text, record) => (
                <a href="#" className="btn btn-sm btn-rounded dropdown-toggle" style={{background: "rgba(255, 255, 255,0.3)", color: "white"}} onClick={preventALinkDefault} aria-expanded="false">
                    {record.status === 1 ? "On Going" : "Completed" } </a>
            ),
        },
        {
            title: 'Action',
            render: (text, record) => (
                <div style={tableStyles.actionButtonDivStyle}>
                    <Tooltip title="view record" placement="left">
                        <button style={tableStyles.actionButtonStyles}  onClick={() => {
                            if(checkPermission(permissionRoutes.ProjectsP,permissionTypes.readP)) {
                                // console.log("view data",record);
                                setToViewData(record);
                                setViewForm(true);
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }

                        }}><i className="fa fa-eye" ></i></button>
                    </Tooltip>

                    <button style={tableStyles.actionButtonStyles} onClick={() => {
                        if(checkPermission(permissionRoutes.ProjectsP,permissionTypes.updateP)) {
                            setShowMemberForm(true);
                            setToUpdateProject(record);
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }

                    }}><i className="fa fa-plus mr-2" />Members</button>

                    <button style={tableStyles.actionButtonStyles} onClick={() => {
                        if(checkPermission(permissionRoutes.ProjectsP,permissionTypes.updateP)) {
                            // console.log("record",record);
                            setShowUpdateForm(true);
                            setToUpdateProject(record);
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }

                    }}><i className="fa fa-pencil" />
                        {/*Edit*/}
                    </button>
                    <button style={tableStyles.actionButtonStyles}  onClick={() => {
                        if(checkPermission(permissionRoutes.ProjectsP,permissionTypes.deleteP)) {
                            onDeleteModel(record)
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }

                    }}><i className="fa fa-trash-o"/></button>

                </div>
                // <div className="dropdown dropdown-action text-right">
                //     <a href="#" className="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="material-icons">more_vert</i></a>
                //     <div className="dropdown-menu dropdown-menu-right">
                //         <Button className="dropdown-item" onClick={() => {
                //             if(checkPermission(permissionRoutes.ProjectsP,permissionTypes.updateP)) {
                //                 setShowUpdateForm(true);
                //                 setToUpdateProject(record);
                //             } else {
                //                 toast.error("You Don't have Permission for this!");
                //             }
                //
                //         }}><i className="fa fa-pencil m-r-5" /> Edit</Button>
                //         <Button className="dropdown-item" onClick={() => {
                //             if(checkPermission(permissionRoutes.ProjectsP,permissionTypes.deleteP)) {
                //                 onDeleteModel(record)
                //             } else {
                //                 toast.error("You Don't have Permission for this!");
                //             }
                //
                //            }}><i className="fa fa-trash-o m-r-5" /> Delete</Button>
                //     </div>
                // </div>
            ),
        },

    ]
    const [columns,setColumns] = useState(projectsColumn)
    //-----API CALLS USE EFFECT-----
    useEffect( ()=> {
        // console.log("updated");
        let isMounted = true;
        if(allUsersList.length == 0) {
            membersAPI.getAllUsersAPI(0,30).then((r) => {
                if(r.status && isMounted) {
                    // console.log("PROJECTS PAGE",r.data);
                    dispatch(memberActions.setAllUsers({data: r.data , total_count: r.total_count}));
                }
            });
        }
        if(allClientsList.length == 0) {
            clientsAPI.getClientsAPI(0,30).then((r) => {
                if(r.status  && isMounted) {
                    // console.log("CLIENTS PAGE",r.data);
                    dispatch(clientActions.setAllClients({data: r.data , total_count: r.total_count}));
                }
            });
        }
        if(allTagsList.length == 0) {
            tagsAPI.getTagsAPI(0,30).then((r) => {
                if(r.status  && isMounted) {
                    // console.log("CLIENTS PAGE",r.data);
                    dispatch(tagsActions.setAllTags({data: r.data , total_count: r.total_count}));
                }
            });
        }
        if(allProjectsList.length === 0) {
            projectsAPI.getProjectsAPI(0,30).then((r) => {
                if(r.status  && isMounted) {
                    // console.log("getProjectsAPI PAGE",r.data,r.total_count);
                    dispatch(projectActions.setAllProjects({data: r.data , total_count: r.total_count}));
                }
            });
    } else {
            setColumns(projectsColumn);
        }
        return () => { isMounted = false };
    }, [state]);
    useEffect( ()=>{
        // console.log("toUpdateTag updated!",toUpdateProject);
    }, [toUpdateProject]);
    useEffect( ()=>{
        // console.log("allProjectsList updated!");
        if(searchStatus) {
            setUIProjectsList(allProjectsList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
        } else {
            setUIProjectsList([...allProjectsList]);
        }
    }, [allProjectsList]);
    //--------------------------------------


    //---------ADD NEW PROJECT-----------------
    const newProjectName = (e) => {
        setNewProject({...newProject,name: e.target.value});
    }
    const newProjectColor = (e) => {
        setNewProject({...newProject,color_hex: e.target.value});
    }
    const newProjectStatusSetToInActive = () => {
        setNewProject({...newProject,status: 0})
    }
    const newProjectStatusSetToActive = () => {
        setNewProject({...newProject,status: 1})
    }
    const addProject = async () => {
        // console.log("addProject called",newProject)
        if(newProject.name == "") {
            toast.error("Enter Project Name!");
        }
        else {
            setLoading(true);
            await projectsAPI.addProjectAPI(newProject).then((res) => {
                if(res.status) {
                    let currentUser = getCurrentUser();
                    let currentProject = newProject;
                    currentProject.id = res.data.id;
                    currentProject.use_default_tag= res.data.use_default_tag;
                    currentProject.tags = res.data.tags;
                    currentProject.members = [{
                        id: currentUser.id,
                        name: currentUser.name,
                        pivot: {
                            project_id: res.data.id,
                            user_id: currentUser.id,
                            is_manager: 1,
                        }
                    }]
                    setShowForm(false);
                    dispatch(projectActions.addProject(currentProject))
                    setToUpdateProject(currentProject);
                    // setShowMemberForm(true);
                    setNewProject({...projectDataObj});
                    setState(!state);
                }
                setLoading(false);
            })
        }

    }
    const onAddProjectSelectDefaultChange = (status) => {
        if(status) {

            setNewProject({...newProject,use_default_tag: 1})
        }else {
            setNewProject({...newProject,use_default_tag: 0,tags: []})
        }
    }
    const onUpdateProjectSelectDefaultChange = (status) => {
        if(status) {

            setToUpdateProject({...toUpdateProject,use_default_tag: 1})
        }else {
            setToUpdateProject({...toUpdateProject,use_default_tag: 0})
        }
    }
    const addProjectModel = () => {
        return ( <Modal
                centered
                visible={showForm}
                destroyOnClose={true}
                confirmLoading={loading}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={async () => {await addProject()}}
                onCancel={() => {setShowForm(false); setNewProject(projectDataObj)}}
            >
                <div>
                    <div><h4>Add Project</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={newProject.name} onChange={newProjectName}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                        <SelectClientButton  projectData={newProject} setProjectData={setNewProject} fromCreate={true}/>
                    </div>
                    <div style={inputRowStyle}>
                        <span style={{marginBottom:"8px",marginRight: "10px",marginLeft: "4px",color: "black",fontSize: "14px"}} >Default Tags:</span>
                        <Checkbox
                        checked={newProject.use_default_tag === 1}
                        onChange={(e) => {
                          onAddProjectSelectDefaultChange(e.target.checked);
                        }}
                        />
                        <SelectTagsButton  parameters={{setNewData: setNewProject,newData: newProject}} fromCreate={true}/>
                    </div>
                    <div style={inputRowStyle}>
                        <div style={inputRowStyle}>
                            <Input
                                addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Color:</span>}
                                defaultValue={newProject.color_hex}  onChange={newProjectColor}
                                type="color"
                            >
                            </Input>
                        </div>
                        {/*<span style={inputClientsLabelStyle}>Hex Color: </span>*/}
                        {/*<input  style={addColorInputStyle} defaultValue={newProject.color_hex}  onChange={newProjectColor}  type="color" />*/}
                    </div>

                    <div style={inputRowStyle}>
                        <SelectStatus parameters={{setNewData: setNewProject,newData:newProject}} forProject = {true}/>

                    </div>
                </div>
            </Modal>
        )
    }

    //--------REMOVE PROJECT-----------
    const removeProject = async (record) => {
        // console.log("removeProject called",record)
        await projectsAPI.removeProjectAPI(record.id).then((res) => {
            if(res.status) {
                setState(!state);
                dispatch(projectActions.removeProject({id: record.id}))
            }        })
    }
    const onDeleteModel =  (entry) => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to delete this Project?',
            okText: "YES",
            cancelText: 'NO',
            onOk: async () => {await removeProject(entry)},
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }
    //-----------UPDATE PROJECT-----------
    const updateProjectName = (e) => {
        setToUpdateProject({...toUpdateProject,name: e.target.value});
    }
    const updateProjectHexColor = (e) => {
        setToUpdateProject({...toUpdateProject,color_hex: e.target.value});
    }
    const updateProjectStatusSetToInActive = () => {
        setToUpdateProject({...toUpdateProject,status: 0})
    }
    const updateProjectStatusSetToActive = () => {
        setToUpdateProject({...toUpdateProject,status: 1})
    }
    const updateProject = () => {
        if(toUpdateProject.name == "") {
            toast.error("Enter Project Name!");
        } else {
            setLoading(true);
            // console.log("UPDATE PROJECT!!",toUpdateProject);
            projectsAPI.updateProjectAPI(toUpdateProject).then((res) => {
                if(res.status) {
                    let currentProject = toUpdateProject;
                    currentProject.id = res.data.id;
                    currentProject.use_default_tag = res.data.use_default_tag == null ? 0 : 1;
                    currentProject.tags = res.data.tags;
                    setShowForm(false);
                    setShowUpdateForm(false);
                    dispatch(projectActions.updateProject(currentProject));
                    setShowMemberForm(false);
                    setState(!state);
                }
                setLoading(false);

            })
        }

    }
    const updateProjectModel = ()  => {
        return ( <Modal
                centered
                visible={showUpdateForm}
                confirmLoading={loading}
                destroyOnClose={true}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={updateProject}
                onCancel={() => {setShowUpdateForm(false); setToUpdateProject(projectDataObj)}}
            >

                <div>
                    {/*<div><h4>Update Project</h4></div>*/}
                    {/*<Divider />*/}
                    <ul className="nav nav-tabs nav-tabs-bottom">
                        <li className="nav-item"><a className="nav-link active" href="#basictab1" data-toggle="tab">Update Project</a></li>
                        {/*<li className="nav-item"><a className="nav-link" href="#basictab2" data-toggle="tab">Update Members</a></li>*/}
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane show active" id="basictab1">
                            <div style={inputRowStyle}>
                                <Input
                                    addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                                    defaultValue={toUpdateProject.name} onChange={updateProjectName}
                                >
                                </Input>
                            </div>
                            <div style={inputRowStyle}>
                                <SelectClientButton   projectData={toUpdateProject} setProjectData={setToUpdateProject} fromCreate={true}/>
                            </div>
                            <div style={inputRowStyle}>
                                {/*<div style={{*/}
                                {/*    display: "flex",*/}
                                {/*    alignItems: "center",*/}
                                {/*    justifyContent: "space-between",*/}
                                {/*    // padding: "0px 20px"*/}
                                {/*}}>*/}
                                {/*    <div style={{display:"inline-block"}}>*/}
                                {/*        <span style={{marginBottom:"8px",marginRight: "10px",marginLeft: "4px",color: "black",fontSize: "14px"}} >Default Tags:</span>*/}
                                {/*        <Checkbox*/}
                                {/*            checked={toUpdateProject.use_default_tag === 1}*/}
                                {/*            onChange={(e) => {*/}
                                {/*                onUpdateProjectSelectDefaultChange(e.target.checked);*/}
                                {/*            }}*/}
                                {/*        />*/}
                                {/*    </div>*/}
                                {/*    <span style={*/}
                                {/*        {*/}
                                {/*            border: "1px solid #d9d9d9",*/}
                                {/*            padding: "2px 8px",*/}
                                {/*            margin: "0px 0px 4px 0px",*/}
                                {/*            fontSize:"12px",*/}
                                {/*            background: "#fafafa",*/}
                                {/*            color: "black",*/}
                                {/*            cursor: "pointer"*/}
                                {/*        }*/}
                                {/*    } onClick={() => {*/}
                                {/*        console.log("REMOVE ALL")}}>remove all</span>*/}
                                {/*</div>*/}
                                <span style={{marginBottom:"8px",marginRight: "10px",marginLeft: "4px",color: "black",fontSize: "14px"}} >Default Tags:</span>
                                <Checkbox
                                    checked={toUpdateProject.use_default_tag === 1}
                                    onChange={(e) => {
                                        onUpdateProjectSelectDefaultChange(e.target.checked);
                                    }}
                                />
                                <SelectTagsButton  parameters={{setNewData: setToUpdateProject,newData: toUpdateProject}} fromUpdate={true}/>
                            </div>
                            <div style={inputRowStyle}>
                                <div style={inputRowStyle}>
                                    <Input
                                        addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Color:</span>}
                                        defaultValue={toUpdateProject.color_hex}  onChange={updateProjectHexColor}
                                        type="color"
                                    >
                                    </Input>
                                </div>
                                {/*<span style={inputClientsLabelStyle}>Hex Color: </span>*/}
                                {/*<input  style={addColorInputStyle} defaultValue={newProject.color_hex}  onChange={newProjectColor}  type="color" />*/}
                            </div>

                            <div style={inputRowStyle}>
                                <SelectStatus parameters={{setNewData: setToUpdateProject,newData:toUpdateProject}} forProject={true}/>

                            </div>
                 </div>
                    </div>
                </div>
            </Modal>
        )
    }
    const addMemberModel = () => {
        return ( <Modal
                centered
                visible={showMemberForm}
                confirmLoading={loading}
                destroyOnClose={true}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={updateProject}
                onCancel={() => {setShowMemberForm(false); setToUpdateProject(projectDataObj)}}
            >
                <div>
                    <ul className="nav nav-tabs nav-tabs-bottom">
                        <li className="nav-item"><a className="nav-link active" href="#basictab2" data-toggle="tab">Update Members</a></li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane show active" id="basictab1">
                            <MembersBlock currentProject={toUpdateProject} updateCurrentProject={setToUpdateProject}/>
                        </div>
                    </div>
                </div>
            </Modal>
        )
    }
    //-------SEARCH PROJECT---------------
    const [searchStatus,setSearchStatus] = useState(false)
    const [searchString,setSearchString] = useState("");
    const filterProjectList = async (e) => {
        setSearchString(e.target.value);
        if(e.target.value !== "") {
            setSearchStatus(true);
        }  else {
            setSearchStatus(false);
        }
        if(allProjectsList.length !== totalProjects && !paginationTableLoading && searchString.length <= 1) {
            setPaginationTableLoading(true);
         await projectsAPI.getProjectsAPI(allProjectsList.length,totalProjects).then((r) => {
                if(r.status) {
                    dispatch(projectActions.setAllProjects({data: r.data , total_count: r.total_count}));
                    setPaginationTableLoading(false);
                }
            })
        } else {
            setUIProjectsList(allProjectsList.filter((i) => i.name.toLowerCase().match(e.target.value.toLowerCase())));

        }
    }
    const [paginationTableLoading,setPaginationTableLoading] = useState(false);
    //----DATA TABLE-----------
    const getProjectsDataTable = () => {
        if(allProjectsList.length === 0) {
            return <div style={spinnerStyle}><Spin size="large"/></div>
        } else {
            return  <>
                <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <Table className="table-striped"
                                   pagination= { {total : searchStatus ? uiProjectsList.length : totalProjects,
                                       onChange: (page, pageSize) => {
                                       if(searchStatus) {
                                       } else {
                                           // console.log("onchange",page,pageSize,allProjectsList.length)
                                           if(allProjectsList.length <=  (page -1) * 10) {
                                               setPaginationTableLoading(true);
                                               projectsAPI.getProjectsAPI(allProjectsList.length,page * 10).then((r) => {
                                                   if(r.status) {
                                                       // console.log("getProjectsAPI PAGE",r.data);
                                                       dispatch(projectActions.setAllProjects({data: r.data , total_count: r.total_count}));
                                                       setUIProjectsList([...uiProjectsList,...r.data]);
                                                       // setPaginationLength(r.total_count);
                                                       // console.log("getTagsAPI PAGE 2",uiProjectsList);
                                                       setPaginationTableLoading(false);
                                                   }
                                               });
                                           } else {
                                           }

                                       }

                                       },
                                       showTotal : (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
                                       showSizeChanger : false,onShowSizeChange: onShowSizeChange ,itemRender : itemRender } }
                                   style = {{overflowX : 'auto'}}
                                   columns={columns}
                                   loading={paginationTableLoading}
                                   dataSource={uiProjectsList}
                                   rowKey={record => record.id}
                            />
                        </div>
                    </div>
                </div></>
        }
    }
    //-------------------------------
    return (
        <div className="page-wrapper">
            <Helmet>
                <title>Projects</title>
                <meta name="description" content="Projects Page"/>
            </Helmet>
            {/* Page Content */}
            <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                    {/*Search Filter */}
                    <div className="row filter-row">
                        <div className="col-sm-6 col-md-8">
                            <div className="col">
                                <h3 className="page-title text-white">Projects</h3>
                                <ul className="breadcrumb text-white">
                                    <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Dashboard</Link></li>
                                    {/*/TODO: change to dashboard when created */}
                                    <li className="breadcrumb-item active text-white">Projects</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <div className="row filter-row">
                                <div className="col-sm-6 col-md-9">
                                    <Input allowClear type="text"  value={searchString}   placeholder="Search Projects" style={searchStyle} onChange={filterProjectList} />
                                </div>
                                <div className="col-sm-6 col-md-3">
                                    <Button className="btn btn-primary mr-1"  style={addTagsButtonStyle} onClick={()=> {
                                        if(checkPermission(permissionRoutes.ProjectsP,permissionTypes.addP)) {
                                            setShowForm(true);
                                        } else {
                                            toast.error("You Don't have Permission for this!");
                                        }
                                       }} ><span>Create</span></Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Search Filter */}
                </div>
                {/*/Page Header */}
                {checkPermission(permissionRoutes.ProjectsP, permissionTypes.readP) ? getProjectsDataTable() :  <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                                                                                               description="You Have No Permission To Read!"/>}
            </div>
            {/*VIEW MODEL*/}
            {viewModel()}
            {/*VIEW MODEL*/}
            {/* /Page Content */}
            {/* Add Client Modal */}
            {addProjectModel()}
            {/* /Add Client Modal */}
            {/*ADD MEMBER MODEL*/}
            {addMemberModel()}
            {/*ADD MEMBER MODEL*/}
            {/* Edit Client Modal */}
            {updateProjectModel()}
            {/* /Edit Client Modal */}
        </div>
    );
}
export default Projects;
