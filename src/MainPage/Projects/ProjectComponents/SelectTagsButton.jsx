import React, {useEffect, useRef, useState} from "react";
import {Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {tagsAPI} from "../../../Services/apiServices";
import {tagsActions} from "../../../Redux/Actions/TagsActions";



export default  function SelectTagsButton({parameters,fromUpdate=false})  {

    const dbList = useSelector((store)=> store.db.allTags);
    const total = useSelector((store) => store.db.totalTags);
    const [uiList,setUIList] = useState([]);
    const [selectedTags,setSelectedTags] = useState([...parameters.newData.tags.map((t) => t.name)]);
    const [usersLoading,setUsersLoading] = useState(false);
    const dispatch = useDispatch();
    const [updateProjectTempTags,setUpdateProjectTempTags] = useState([...parameters.newData.tags])
    useEffect(()=> {
        // console.log("selcted tag",parameters);
        if(parameters.newData.use_default_tag === 0) {
            // const defaultTagsList = dbList.filter((tag) => tag.is_default == 1);
            // let filterlist = defaultTagsList.filter((tag) => selectedTags.includes(tag.name));
            // let nameFilterList = filterlist.map((t) => t.name);
            // setSelectedTags(   selectedTags.filter((tag) => !nameFilterList.includes(tag)));
            if(!fromUpdate) {
                setSelectedTags([])
            } else {
                // setSelectedTags([])
                parameters.setNewData({...parameters.newData,tags: updateProjectTempTags})
                setSelectedTags([...updateProjectTempTags.map((t) => t.name)]);
            }
            // setSelectedTags(parameters.newData.tags.map((t) => t.name));


        } else if(parameters.newData.use_default_tag === 1){
            setUpdateProjectTempTags([...parameters.newData.tags]);
            const defaultTagsList = dbList.filter((tag) => tag.is_default == 1);
            // console.log("all default selected",defaultTagsList);
            setSelectedTags([...defaultTagsList.map((t) => t.name)]);
            parameters.setNewData({...parameters.newData,tags: defaultTagsList.map((t) => t.id)})
        } else {
            // console.log("in this")
            setSelectedTags(parameters.newData.tags.map((t) => t.name))
        }
    },[parameters.newData.use_default_tag])

    function handleChange(value) {
        // console.log(`selected ${value}`);
        let tagsNameArray = value.slice(",");
        // setSelectedTags([...tagsNameArray]);
        let filterlist = dbList.filter((tag) => tagsNameArray.includes(tag.name));
        let idFilterList = filterlist.map((t) => t.id);
        let nameFilterList = filterlist.map((t) => t.name);
        setSelectedTags([...tagsNameArray]);
        // console.log(tagsNameArray,filterlist,tagsNameArray.filter((tag) => !nameFilterList.includes(tag)));
        parameters.setNewData({...parameters.newData,tags: idFilterList ,created_tags :tagsNameArray.filter((tag) => !nameFilterList.includes(tag)) })
        // console.log("parms",parameters);
    }

    const getAllList = () => {
        // console.log("getTagsAPI",dbList.length,total);
        if(dbList.length != total) {
            setUsersLoading(true);
            tagsAPI.getTagsAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    // console.log("getTagsAPI",r.data);
                    dispatch(tagsActions.setAllTags({data: r.data , total_count: r.total_count}));
                }
                setUsersLoading(false);
            });
        }


    }
    const onClickDropDown = (e) => {
    }
    useEffect(()=> {
        getAllList();
    },[])
    useEffect(
        ()=> {
            setUIList([...dbList]);
        }, [dbList]);

    const buttonStyle = {
        display: "flex",
        verticalAlign: "middle",
        border: "1px solid #d9d9d9"
    }
    const buttonLabelStyle = {
        verticalAlign: "middle",
        background: "#fafafa",
        borderRight: "1px solid #d9d9d9",
        padding: "5px 10px",
        color: "black"
    }
    const selectStyle = {
        border: "none",
        outline: "none",
        flexGrow: "1",
        maxHeight: "100px",
        overflowY: "auto",
    }
    return (
        <div style={buttonStyle}>
             <span style={buttonLabelStyle} >
                                    Tags:</span>
            <Select
                showSearch
                style={selectStyle}
                placeholder="Please select a Tags"
                bordered={false}
                allowClear={true}
                mode="tags"
                onChange={handleChange}
                value={selectedTags}
                // defaultValue={parameters.newData.tags.map((t) => t.name)}
                onClick={onClickDropDown}
                optionFilterProp="children"
            >
                {uiList.map((data,key) => {
                    return <Select.Option key={data.name} value={data.name}>
                        {data.name}
                    </Select.Option>
                })}
            </Select>
        </div>


    )
}