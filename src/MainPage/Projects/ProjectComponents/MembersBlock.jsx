import SelectMemberButton from "./SelectMemberButton";
import {Divider} from "antd";
import MemberTile from "./MemberTile";
import React from "react";
import {membersAPI} from "../../../Services/apiServices";
import {memberActions} from "../../../Redux/Actions/MembersActions";
import {useDispatch} from "react-redux";


 export  const MembersBlock = ({currentProject,updateCurrentProject}) => {


    const dispatch = useDispatch();
    const inputClientsLabelStyle = {
        fontWeight: "600",

    }
     const addSelectedMember = (item) => {
         // console.log("addSelectedMember",item);
     }
     const removeMember = (item) => {
         // console.log("removeMember",item);
         membersAPI.removeMemberFromProjectAPI({
             user_id: item.id, // member user id
             project_id: currentProject.id,
         }).then((res) => {
             if(res.status) {
                 let localData = {
                     id: item.id,
                     name: item.name,
                     pivot: {
                         "project_id": currentProject.id,
                         "user_id": item.id,
                         "is_manager": 0
                     }
                 }
                 const membersList = currentProject.members.filter((m) => m.id != item.id);
                 updateCurrentProject({...currentProject,members : membersList});
                 dispatch(memberActions.removeMemberFromProject({member: localData ,project_id: currentProject.id}))
             }
         })
     }
     const updateRights = (item) => {
         // console.log("updateRights",item);
         membersAPI.updateManageRightsOfMember({
             user_id: item.id, // member user id
             project_id: currentProject.id,
             is_manager: item.pivot.is_manager == 1 ? 0 : 1
         }).then((res) => {
             if(res.status) {
                 let localData = {
                     id: item.id,
                     name: item.name,
                     pivot: {
                         "project_id": currentProject.id,
                         "user_id": item.id,
                         "is_manager": item.pivot.is_manager == 1 ? 0 : 1
                     }
                 }
                 let membersList = currentProject.members;
                 const memberIndex = membersList.findIndex((m) => m.id == item.id);
                 const member = membersList[memberIndex];
                 member.pivot.is_manager = !item.pivot.is_manager;
                 membersList[memberIndex] = member;
                 updateCurrentProject({...currentProject,members : membersList});
                 dispatch(memberActions.updateManagerRightsOfMember({member: localData ,project_id: currentProject.id}))
             }
         });
     }

    const membersBlockStyle = {
        maxHeight: "300px",
        overflowY: "auto",
        // background: "whitesmoke",
        borderRadius: "10px",
        margin: "10px 0px 0px 0px",
        padding: "10px 10px"
    }
     return ( <div>
        <div className="row filter-row">
            <div className="col-sm-6 col-md-8">
            </div>
            <div className="col-sm-6 col-md-3">
                <SelectMemberButton fromCreate={false} setProjectData={updateCurrentProject} projectData={currentProject}/>
            </div>
            <div className="col-sm-6 col-md-1">
            </div>
        </div>
        <div style={membersBlockStyle}>
            {currentProject.members.length == 0 ? <div>No Members</div> :
                currentProject.members.map((item, index) => <MemberTile key={index} parameters={{addSelectedMember: addSelectedMember,item: item,removeMember : removeMember,updateRights : updateRights} }/>)
            }
        </div>
    </div>)
}