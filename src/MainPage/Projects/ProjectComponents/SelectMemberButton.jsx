import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {Button, Dropdown, Menu, Spin} from "antd";
import {wc_hex_is_light} from "../../../constants";
import {projectButtonDropDownStyles, tagButtonDropDownStyles} from "../../Employees/Toggle/toggleConstants";
import ProjectListTile from "../../Employees/Toggle/ToggleComponents/ProjectListTile";
import SearchInput from "../../Employees/Toggle/ToggleComponents/SearchInput";
import {clientsAPI, membersAPI, projectsAPI, tagsAPI} from "../../../Services/apiServices";
import {tagsActions} from "../../../Redux/Actions/TagsActions";
import {projectActions} from "../../../Redux/Actions/ProjectActions";
import {clientActions} from "../../../Redux/Actions/ClientsActions";
import {memberActions} from "../../../Redux/Actions/MembersActions";
import SearchIcon from "../../Employees/Toggle/ToggleElements/SearchIcon";
import TagIcon from "../../Employees/Toggle/ToggleElements/TagIcon";
import {PlusCircleFilled} from "@ant-design/icons";
import {toast} from "react-toastify";
/*
* {
    "id": 2318,
    "project_id": 687,
    "name": "Development Training",
    "status": 1,
    "project": {
        "id": 687,
        "name": "3G-GPT AI/Flutter"
    }
}
* */
export default  function SelectMemberButton({projectData,fromCreate = false,setProjectData})  {
    const dbAllUsersList  = useSelector((store) => store.db.allUsers );
    const totalUsers = useSelector((store) => store.db.totalUsers );
    const [allUsersList,setAllUsersList] = useState([]);
    const buttonRef = useRef();
    const dispatch = useDispatch();
    const [addingProject,setAddingProject] = useState(false);

    //---CHANGE BUTTON STYLE ACCORDING TO SELECTED PROJECT----------




    useEffect(()=> {
        getAllUsersList();

    },[])
    useEffect(
        ()=> {
            setAllUsersList(dbAllUsersList);
        }, [dbAllUsersList])

    const onFilterList = (text) => {
        setAllUsersList(dbAllUsersList.filter((i) => i.name.toLowerCase().match(text.toLowerCase())));
    }
    const setSelectedUser = async (item) => {

        if( projectData.members.find((i) => i.id === item.id) == null) {
            membersAPI.assignMemberToProjectAPI({
                user_id: item.id, // member user id
                is_manager: 0,
                project_id: projectData.id,
            }).then((res) => {
                if(res.status) {
                    let localData = {
                        id: item.id,
                        name: item.name,
                        pivot: {
                            "project_id": projectData.id,
                            "user_id": item.id,
                            "is_manager": 0
                        }
                    }
                    setProjectData({...projectData,members : [...projectData.members,localData]});
                    dispatch(memberActions.assignMemberToProject({member: localData ,project_id: projectData.id}))
                }
            })
        } else {
            toast.error("Member already in Project!");
        }


        // setAddingProject(false);
    }
    const [usersLoading,setUsersLoading] = useState(false);
    const getAllUsersList = () => {
        if(dbAllUsersList.length != totalUsers) {
            setUsersLoading(true);
            membersAPI.getAllUsersAPI(30,totalUsers).then((r) => {
                if(r.status) {
                    // console.log("getAllUsersAPI",r.data);
                    dispatch(memberActions.setAllUsers({data: r.data , total_count: r.total_count}));
                    // setAllUsersList(r.data);
                    // setAllUsersList(dbAllUsersList.filter((i) => i.name.toLowerCase().match(text.toLowerCase())));
                }
                setUsersLoading(false);
            });
        }


    }
    const prevScrollY = useRef(0);
    const [goingUp, setGoingUp] = useState(false);
    const onScroll = (e) => {
        const currentScrollY = e.target.scrollTop;
        if (prevScrollY.current < currentScrollY && goingUp) {
            setGoingUp(false);
        }
        if (prevScrollY.current > currentScrollY && !goingUp) {
            setGoingUp(true);
        }
        prevScrollY.current = currentScrollY;
        if(currentScrollY + e.target.clientHeight === e.target.scrollHeight) {
            // console.log("load more..");
            // loadMoreUsers();
        }
        // console.log(goingUp, currentScrollY,currentScrollY + e.target.clientHeight,e.target.scrollHeight);
    };

    const loadMoreUsers = () => {
        membersAPI.getAllUsersAPI(dbAllUsersList.length,30).then((r) => {
            if(r.status) {
                // console.log("getAllUsersAPI",r.data);
                dispatch(memberActions.setAllUsers({data: r.data , total_count: r.total_count}));
                setAllUsersList(dbAllUsersList);
            }
        });
    }





    const divStyle = () =>{
        if(fromCreate) {
            return {width: "100%"}
        } else {
            return  {}
        }
    }
    const dropDownBoxStyle = {
        // padding: "5px 5px",
        maxHeight: "300px",
        overflowX: "auto",
        overflowY: "auto",
        border: "1px solid #f5f5f5",
        borderRadius: "10px",
        width: "250px",
        right: "auto",
        left: "0"
    }
    const inputStyle = {
        display: "inline-block",
        border : "none",
        background: "inherit",
        marginLeft: "5px",
        outline: "none",
        width: "80%",
        color: "#696969"
    }
    const searchBarStyle = {
        padding: "10px",
        margin: "10px 5px 0px 5px",
        borderBottom: "1px solid rgba(255,255,255,0.3)",
        background: "inherit",
        display: "flex",
        justifyContent: "start",
        borderRadius: "10px",
        alignItems: "center"
    }
    const menuStyle = {
        padding: "2px 0px 15px 0px",
        background: "whitesmoke",
        borderRadius: "5px"
    }
    const menu = (
        <Menu style={menuStyle}>
            <SearchInput params={{onFilterList: onFilterList}}/>
            {/*<Menu.Item*/}
            {/*           style={{color: "#f43b48",*/}
            {/*               margin: "0px 4px",*/}
            {/*               textAlign: "center",*/}
            {/*               background: "whitesmoke",*/}
            {/*               borderRadius: "10px",*/}
            {/*               border: "1px solid white"*/}
            {/*           }}*/}
            {/*           key={"no project"}*/}
            {/*           onClick={(e)=> {setSelectedProject({})}}>*/}
            {/*    Clear Selected*/}
            {/*    /!*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*!/*/}
            {/*</Menu.Item>*/}
            {allUsersList.filter((u) => u.status == 1).length != 0 ?
                allUsersList.filter((u) => u.status == 1).map((item,index) =>
                    (  <Menu.Item itemID={item.id}
                                  style={{color: item.color_hex,
                                      margin: "0px 4px",
                                      // textAlign: "center",
                                      background: projectData.members.find((i) => i.id === item.id) != null ? "whitesmoke" : "white",
                                      borderRadius: "10px",
                                      border: "1px solid whitesmoke"
                                  }}
                                  key={index}
                                  onClick={(e)=> {setSelectedUser(item)}}>
                        {item.name}
                        {/*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*/}
                    </Menu.Item>)
                ) :
                <Menu.Item
                    style={{color: "#696969",
                        margin: "0px 4px",
                        textAlign: "center",
                        background: "whitesmoke",
                        borderRadius: "10px",
                        border: "1px solid whitesmoke"
                    }}
                    disabled
                    key={"no tag"}
                    onClick={null}>
                    No results found!
                    {/*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*/}
                </Menu.Item>
            }

        </Menu>
    );
    const buttonStyle = {
        background: "#f43b48",
        border: "1px solid #f43b48",
        borderRadius: "5px",
        color: "white",
        fontWeight: "500",
        fontSize: "14px",
        padding: "0px 8px",
        display: "flex",
        alignItems: "center",
        height: "28px"

    }

    return (
        <Dropdown
            destroyPopupOnHide={true}
            onVisibleChange={(e) => {
                if(!e) {
                    setAllUsersList(dbAllUsersList);
                }
            }}
            overlay={menu} placement="bottomRight" trigger="click"  overlayStyle={{maxHeight:350,overflowY:'scroll'}} >
            <Button style={buttonStyle} loading={usersLoading} icon={<PlusCircleFilled style={{ verticalAlign: "middle" }} />} >Add Member</Button>
        </Dropdown>
        // <div className="btn-group" style={divStyle()}>
        //     <Button  style={projectButtonDropDownStyles.projectButtonStyle} className="btn-sm btn-primary dropdown-toggle  mr-1"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        //         Add Member
        //     </Button>
        //     <div className="dropdown-menu dropdown-menu-left" onScroll={onScroll} style={dropDownBoxStyle}>
        //         <div style={searchBarStyle}>
        //             <SearchIcon />
        //             <input type="search" style={inputStyle} autoFocus={true} onChange={(e) => {
        //                 onFilterList(e.target.value)
        //             }} placeholder="Search"/>
        //         </div>
        //         <div className="dropdown-divider" style={{margin: "5px 10px 5px 10px"}}/>
        //         {allUsersList.length == 0 ?     (<Button className="dropdown-item"  key={"EMPTY"} onClick={null}>EMPTY!</Button>) : allUsersList.map((item,index) =>
        //             (<div key={index}><ProjectListTile parameters={{setSelectedProject: setSelectedUser,item: item}}/></div>)
        //         )}
        //     </div>
        // </div>
    );
}