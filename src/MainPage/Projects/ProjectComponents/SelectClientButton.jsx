import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {Button, Select, Spin} from "antd";
import {wc_hex_is_light} from "../../../constants";
import {projectButtonDropDownStyles} from "../../Employees/Toggle/toggleConstants";
import ProjectListTile from "../../Employees/Toggle/ToggleComponents/ProjectListTile";
import SearchInput from "../../Employees/Toggle/ToggleComponents/SearchInput";
import {clientsAPI, projectsAPI, tagsAPI} from "../../../Services/apiServices";
import {tagsActions} from "../../../Redux/Actions/TagsActions";
import {projectActions} from "../../../Redux/Actions/ProjectActions";
import {clientActions} from "../../../Redux/Actions/ClientsActions";
/*
* {
    "id": 2318,
    "project_id": 687,
    "name": "Development Training",
    "status": 1,
    "project": {
        "id": 687,
        "name": "3G-GPT AI/Flutter"
    }
}
* */
export default  function SelectClientButton({projectData,fromCreate = false,setProjectData})  {
    const dbAllClientsList  = useSelector((store) => store.db.allClients );
    const totalClients  = useSelector((store) => store.db.totalClients );
    const [allClientsList,setAllClientsList] = useState([]);
    const dispatch = useDispatch();

    //---CHANGE BUTTON STYLE ACCORDING TO SELECTED PROJECT----------
    useEffect(() => {
        getAlClientsList();
    }, []);
    useEffect(
        ()=> {
            // console.log("projectData",projectData);

            setAllClientsList(dbAllClientsList);
        }, [dbAllClientsList]
    )
    const onFilterList = (text) => setAllClientsList(dbAllClientsList.filter((i) => i.name.toLowerCase().match(text.toLowerCase())))
    const setSelectedClient = async (item) => {
        // setAddingProject(true);
        // console.log("item",item);
        if(item == null) {
            setProjectData({...projectData,client_id: 1,client: {id : 1, name: "No Client"}})
        }
        if(setProjectData != null) {
            console.log("set project data called!");
            setProjectData({...projectData,client : {id : item.id, name: item.name},client_id: item.id})
        }
        if(fromCreate) {

        } else {
            // //    TODO: update the existing item data
            // console.log("set project data called! 2");
            // let currentData ={...projectData,client : {id : item.id, name: item.name},client_id: item.id};
            // projectsAPI.updateProjectAPI(currentData).then((res) => {
            //     if(res.status) {
            //         dispatch(projectActions.updateProject(currentData))
            //     }
            // })
        }

        // setAddingProject(false);
    }

    const getAlClientsList = () => {
        if(dbAllClientsList.length != totalClients) {
            clientsAPI.getClientsAPI(dbAllClientsList.length,totalClients).then((r) => {
                if(r.status) {
                    // console.log("getClientsAPI PAGE",r.data);
                    dispatch(clientActions.setAllClients({data: r.data , total_count: r.total_count}));
                }
            });
        }

    }
    const prevScrollY = useRef(0);
    const [goingUp, setGoingUp] = useState(false);
    const onScroll = (e) => {
        const currentScrollY = e.target.scrollTop;
        if (prevScrollY.current < currentScrollY && goingUp) {
            setGoingUp(false);
        }
        if (prevScrollY.current > currentScrollY && !goingUp) {
            setGoingUp(true);
        }
        prevScrollY.current = currentScrollY;
        if(currentScrollY + e.target.clientHeight === e.target.scrollHeight) {
            // console.log("load more..");
            // loadMoreClients();
        }
        // console.log(goingUp, currentScrollY,currentScrollY + e.target.clientHeight,e.target.scrollHeight);
    };

    const loadMoreClients = () => {
        clientsAPI.getClientsAPI(dbAllClientsList.length,30).then((r) => {
            if(r.status) {
                // console.log("getAllProjectsList PAGE",r.data);
                dispatch(clientActions.setAllClients({data: r.data , total_count: r.total_count}));
                setAllClientsList(dbAllClientsList);
            }
        });
    }




    const divStyle = () =>{

        if(fromCreate) {
            return {width: "100%",}
        } else {
            return  {}
        }
    }
    const projectTileStyle = {
        padding: "5px 10px",
        cursor: "pointer",


    }
    const dropDownBoxStyle = {
        maxHeight: "250px",
        overflowX: "hidden",
        overflowY: "auto",
        border: "1px solid #f5f5f5",
        padding: "0px 0px 10px 5px",
        borderRadius: "10px",
        width: "100%"
    }

    const projectButtonStyle = {
        outline: "none",
        border: "none",
        // background: "",
        width: "100%",
        fontSize: "14px",
        borderRadius: "10px",
        textAlign: "start",
        color: "black",
        padding: "8px 20px",

    }
    const buttonStyle = {
        display: "flex",
        verticalAlign: "middle",
        border: "1px solid #d9d9d9"
    }
    const buttonLabelStyle = {
        verticalAlign: "middle",
        background: "#fafafa",
        borderRight: "1px solid #d9d9d9",
        padding: "5px 10px",
        color: "black"
    }
    const selectStyle = {
        border: "none",
        outline: "none",
        flexGrow: "1"
    }

    function handleChange(value) {
        // console.log(`selected ${value}`);
        const item = allClientsList.find((p) => p.id === value);
        setSelectedClient(item)
    }
    return (
        <div style={buttonStyle}>
             <span style={buttonLabelStyle} >
                                    Client:</span>
            <Select
                showSearch
                style={selectStyle}
                allowClear={true}
                placeholder="Please select a Client"
                bordered={false}
                onChange={handleChange}
                defaultValue={projectData.client == null || projectData.client.id == null ? null : projectData.client.id}
                optionFilterProp="children"
            >
                {allClientsList.map((data,key) => {
                    return <Select.Option key={key} value={data.id}>
                        {data.name}
                    </Select.Option>
                })}
            </Select>
        </div>
        // <div className="btn-group" style={divStyle()}>
        //     <button  style={projectButtonStyle} className="dropdown-toggle"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        //         {/*"Select Client"*/}
        //         {projectData.client == null || projectData.client.name == null ? "Select Client" : projectData.client.name}
        //     </button>
        //     <div className="dropdown-menu" onScroll={onScroll} style={dropDownBoxStyle}>
        //         <SearchInput params={{onFilterList: onFilterList}}/>
        //         {allClientsList.length == 0 ?     (<Button className="dropdown-item"  key={"EMPTY"} onClick={null}>EMPTY!</Button>) : allClientsList.map((item,index) =>
        //             (<div  key={index} style={projectTileStyle} onClick={(e) => {setSelectedClient(item)}}><span>{item.name}</span></div>)
        //         )}
        //     </div>
        // </div>
    );
}