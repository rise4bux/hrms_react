import {Button, Modal} from "antd";
import React, {useEffect} from "react";
import {projectButtonDropDownStyles} from "../../Employees/Toggle/toggleConstants";
import SearchInput from "../../Employees/Toggle/ToggleComponents/SearchInput";
import ProjectListTile from "../../Employees/Toggle/ToggleComponents/ProjectListTile";
import {useSelector} from "react-redux";

export default function MemberTile({parameters}) {
    const dropDownItemStyle  = {
        padding: "8px 0px 8px 10px",
        margin: "0px auto",
        background:
            "rgba(0,0,0,0.1)",
        width: "100%",
        border: "1px solid whitesmoke",
        borderRadius: "10px",
            marginBottom: "5px",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        color:"black",
        fontSize: "14px",
        fontWeight: "400"

    }
    const textStyle = {
        // width: "100%",
        // color: parameters.item.color_hex,
        // textAlign: "center"
    }

const DropDownMenuStyle = {
            background: "whitesmoke",
    border: "none",
    borderRadius: "10px",
    padding: "0px"
}

const memberTileButtonStyle = {
        outline: "none",
    border: "none",
    borderRadius: "5px",
    fontSize: "12px",
    padding: "5px 10px",
    margin: "0px 5px 0px 0px",
    background: "rgb(244, 59, 72)",
    color:"white",
}

const getMemberTileStyle = (status) => {
        if(status) {
            return {
                outline: "none",
                border: "none",
                borderRadius: "5px",
                fontSize: "12px",
                padding: "5px 10px",
                margin: "0px 5px 0px 0px",
                background: "rgb(244, 59, 72)",
                color:"white",
            }
        } else {
            return {
                outline: "none",
                border: "none",
                borderRadius: "5px",
                fontSize: "12px",
                padding: "5px 10px",
                margin: "0px 5px 0px 0px",
                background: "rgba(0,0,0,0.4)",
                color:"white",
            }
        }

}
    const  dialogButtonStyle = {
        border: "1px solid #f43b48",
        borderRadius: "10px",
        background: "#f43b48",
        color: "white"
    }
    const onDeleteModel =  (entry) => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to remove this member?',
            okText: "YES",
            cancelText: 'NO',
            onOk: async () => {await parameters.removeMember(entry)},
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }

    return (
        <div    style={dropDownItemStyle} onClick={() => {parameters.addSelectedMember(parameters.item)}}>
            <span style={textStyle}>{parameters.item.name}
                {parameters.item.pivot.is_manager == 0 ? "" : " (Manager)"}
            </span>
            <span>

            <button  style={getMemberTileStyle(parameters.item.pivot.is_manager == 0)} onClick={() => {parameters.updateRights(parameters.item)}}  >{parameters.item.pivot.is_manager == 0 ? "Give Manager Rights" : "Revoke Manager Rights"}</button>
                 <button style={memberTileButtonStyle} onClick={() => {onDeleteModel(parameters.item)}} ><i className="fa fa-trash-o"/></button>
            </span>
        </div>
    )

}