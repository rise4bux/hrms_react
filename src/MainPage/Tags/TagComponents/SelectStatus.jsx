import React, {useEffect, useRef, useState} from "react";
import {styles} from "../../Administration/Employees/EmployeesConstants";
import {Select} from "antd";


export default  function SelectStatus({parameters,fromUpdate=false})  {

    function handleChange(value) {
        // console.log(`selected ${value}`);
        parameters.setNewData({...parameters.newData,status:value })
    }

    const onClickDropDown = (e) => {
    }
    useEffect(()=> {

    },[])



    const list = [
        {
            name: "Active",
            value: 1
        },
        {
            name:  "InActive",
            value: 0
        },

    ]
    return (
        <div style={styles.buttonStyle}>
             <span style={styles.buttonLabelStyle} >
                                    Status:</span>
            <Select
                style={styles.selectStyle}
                placeholder="Please select a Status"
                bordered={false}
                defaultValue={parameters.newData.status}
                onChange={handleChange}
                onClick={onClickDropDown}

            >
                {list.map((data,key) => {
                    return <Select.Option key={key} value={data.value}>
                        {data.name}
                    </Select.Option>
                })}
            </Select>
        </div>


    )
}