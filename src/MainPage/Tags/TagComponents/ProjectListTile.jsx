import {Button} from "antd";
import Dot from "../../Employees/Toggle/ToggleElements/Dot";
import React from "react";

export default function ProjectListTile({parameters}) {
    const dropDownItemStyle  = {
        padding: "0px 10px",
        // margin: "0px auto",

    }
    const textStyle = {
        width: "100%",
        color: parameters.item.color_hex,
        // textAlign: "center"
    }

    return (
        <Button
            className="dropdown-item"
            style={dropDownItemStyle}
            key={parameters.item.id}
            onClick={() => {parameters.setSelectedProject(parameters.item);}}
        >
            <span style={textStyle}>{parameters.item.name}</span>
        </Button>
    )

}