import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {Button, Select, Spin} from "antd";
import {wc_hex_is_light} from "../../../constants";
import {projectButtonDropDownStyles} from "../../Employees/Toggle/toggleConstants";
import SearchInput from "../../Employees/Toggle/ToggleComponents/SearchInput";
import {projectsAPI, tagsAPI} from "../../../Services/apiServices";
import {tagsActions} from "../../../Redux/Actions/TagsActions";
import {projectActions} from "../../../Redux/Actions/ProjectActions";
import ProjectListTile from "./ProjectListTile";
/*
* {
    "id": 2318,
    "project_id": 687,
    "name": "Development Training",
    "status": 1,
    "project": {
        "id": 687,
        "name": "3G-GPT AI/Flutter"
    }
}
* */
export default  function SelectProjectButton({tagData,fromCreate = false,setTagData})  {
    const dbAllProjectsList  = useSelector((store) => store.db.allProjects );
    const totalProjects  = useSelector((store) => store.db.totalProjects );
    const [allProjectList,setAllProjectList] = useState([]);
    const buttonRef = useRef();
    const dispatch = useDispatch();
    const [addingProject,setAddingProject] = useState(false);

    //---CHANGE BUTTON STYLE ACCORDING TO SELECTED PROJECT----------
    const  changeButtonStyle = () => {

    }
    const onFilterList = (text) => setAllProjectList(dbAllProjectsList.filter((i) => i.name.toLowerCase().match(text.toLowerCase())))
    const setSelectedProject = async (item) => {
        // setAddingProject(true);
        // console.log("item",item);
        if(fromCreate) {
            if(setTagData != null) {
                // console.log("set tag data called!");
                setTagData({...tagData,project : {id : item.id, name: item.name},project_id: item.id})
            }
        } else {
        //    TODO: update the existing item data
        //     console.log("set tag data called! 2");
            let currentData ={...tagData,project : {id : item.id, name: item.name},project_id: item.id};
            tagsAPI.updateTagAPI(currentData).then((res) => {
                if(res.status) {
                    dispatch(tagsActions.updateTag(currentData))
                }
            })
        }

        // setAddingProject(false);
    }

const getAllProjectsList = () => {
        if(dbAllProjectsList.length != totalProjects) {
            projectsAPI.getProjectsAPI(dbAllProjectsList.length,totalProjects).then((r) => {
                if(r.status) {
                    // console.log("getAllProjectsList PAGE",r.data);
                    dispatch(projectActions.setAllProjects({data: r.data , total_count: r.total_count}));
                }
            });
        }
}
    const prevScrollY = useRef(0);
    const [goingUp, setGoingUp] = useState(false);
    const onScroll = (e) => {
        const currentScrollY = e.target.scrollTop;
        if (prevScrollY.current < currentScrollY && goingUp) {
            setGoingUp(false);
        }
        if (prevScrollY.current > currentScrollY && !goingUp) {
            setGoingUp(true);
        }
        prevScrollY.current = currentScrollY;
        if(currentScrollY + e.target.clientHeight === e.target.scrollHeight) {
            // console.log("load more..");
            // loadMoreProjects();
        }
        // console.log(goingUp, currentScrollY,currentScrollY + e.target.clientHeight,e.target.scrollHeight);
    };

    const loadMoreProjects = () => {
        projectsAPI.getProjectsAPI(dbAllProjectsList.length,30).then((r) => {
            if(r.status) {
                // console.log("getAllProjectsList PAGE",r.data);
                dispatch(projectActions.setAllProjects({data: r.data , total_count: r.total_count}));
                setAllProjectList(dbAllProjectsList);
            }
        });
    }

useEffect(() => {
    getAllProjectsList();
}, []);
    useEffect(
        ()=> {
            // console.log("tagData",tagData);

            setAllProjectList(dbAllProjectsList);
            changeButtonStyle();
        }, [dbAllProjectsList]
    )


const divStyle = () =>{
    if(fromCreate) {
        return {width: "100%"}
    } else {
        return  {}
    }
}
const projectTileStyle = {
        padding: "5px 10px",
    cursor: "pointer",


}
const dropDownBoxStyle = {
    maxHeight: "250px",
    overflowX: "hidden",
    overflowY: "auto",
    border: "1px solid #f5f5f5",
    padding: "0px 0px 10px 5px",
    borderRadius: "10px",
    width: "100%"
}

const projectButtonStyle = {
        outline: "none",
    border: "none",
    // background: "",
    width: "100%",
    fontSize: "14px",
    borderRadius: "10px",
    textAlign: "start",
    color: "black",
    padding: "8px 20px"
}
    const buttonStyle = {
        display: "flex",
        verticalAlign: "middle",
        border: "1px solid #d9d9d9"
    }
    const buttonLabelStyle = {
        verticalAlign: "middle",
        background: "#fafafa",
        borderRight: "1px solid #d9d9d9",
        padding: "5px 10px",
        color: "black"
    }
    const selectStyle = {
        border: "none",
        outline: "none",
        flexGrow: "1"
    }

    function handleChange(value) {
        // console.log(`selected ${value}`);
        const item = allProjectList.find((p) => p.id === value);
        setSelectedProject(item)
    }
    return (
        <div style={buttonStyle}>
             <span style={buttonLabelStyle} >
                                    Project:</span>
            <Select
                showSearch
                style={selectStyle}
                // allowClear={true}
                placeholder="Please select a Project"
                bordered={false}
                onChange={handleChange}
                defaultValue={tagData.project != null ? tagData.project.id  : tagData.project_id}
                optionFilterProp="children"
            >
                {allProjectList.map((data,key) => {
                    return <Select.Option key={key} value={data.id}>
                        {data.name}
                    </Select.Option>
                })}
            </Select>
        </div>
        // <div className="btn-group" style={divStyle()}>
        //     <button   style={projectButtonStyle} className="dropdown-toggle"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        //         {/*Select Project*/}
        //         {tagData.project == null || tagData.project.name == null ? "Select Project" : }
        //     </button>
        //     <div className="dropdown-menu" onScroll={onScroll} style={dropDownBoxStyle}>
        //         <SearchInput params={{onFilterList: onFilterList}}/>
        //         {allProjectList.length == 0 ?     (<Button className="dropdown-item"  key={"EMPTY"} onClick={null}>EMPTY!</Button>) : allProjectList.map((item,index) =>
        //             (<div key={index} style={projectTileStyle} onClick={(e) => {setSelectedProject(item)}}><span>{item.name}</span></div>)
        //         )}
        //     </div>
        // </div>
    );
}