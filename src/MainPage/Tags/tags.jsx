import React, { useState,useEffect } from 'react';
import { Helmet } from "react-helmet";
import {Link, useHistory} from 'react-router-dom';
import {Button, Divider, Table, Modal, Input, Empty, Tooltip, Checkbox, Tag, Dropdown, Menu} from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "../paginationfunction"
import "../antdstyle.css"
import {
    dashBoardHelmet,

    permissionRoutes,
    permissionTypes,

} from "../../constants";
import {projectsAPI, tagsAPI} from "../../Services/apiServices";
import {useDispatch, useSelector} from "react-redux";
import { Spin } from 'antd';
import {tagsActions} from "../../Redux/Actions/TagsActions";
import {projectActions} from "../../Redux/Actions/ProjectActions";
import SelectProjectButton from "./TagComponents/SelectProjectButton";
import {toast} from "react-toastify";
import {checkPermission} from "../../permissionConstants";
import {homePageURL, loginRouteURL} from "../../Services/routeServices";
import SelectStatus from "../Administration/Employees/Components/SelectStatus";
import ViewDataTile from "../../CustomComponents/ViewDataTile";
import {CaretDownOutlined} from "@ant-design/icons";
import SearchInput from "../Employees/Toggle/ToggleComponents/SearchInput";
import {searchStyle, tableStyles} from "../../global_styles";

//------TAG DATA OBJECT
const tagDatObj = {
    id: 0,
    project_id: 0,
    name: "",
    status: 1,
    is_default: 0,
    project: {}
}
//----------TAG PAGE STYLES------
const inputRowStyle = {
    // justifyContent: "center"
    margin: "20px 0px 10px 0px"

}

const  dialogButtonStyle = {
    border: "1px solid #f43b48",
    borderRadius: "10px",
    background: "#f43b48",
    color: "white"
}

const spinnerStyle = {
    display: "flex",
    justifyContent: "center",
    marginTop: "20px"
}

const addTagsButtonStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textTransform: "none"
    // margin: "0px 30px 0px 0px"
}
//--------------------------------------------
const Tags = () => {








    const dispatch = useDispatch();
    const allTagsList = useSelector((store)=> store.db.allTags);
    const allProjectsList = useSelector((store)=> store.db.allProjects);
    const totalTags =  useSelector((store)=> store.db.totalTags);
    const [uiTotalTags,setUITotalTags] = useState(totalTags);
    const [uiTagsList,setUITagsList] = useState([...allTagsList]);
    const [newTag,setNewTag] = useState( tagDatObj);
    const [showForm,setShowForm] = useState(false);
    const [showUpdateForm,setShowUpdateForm] = useState(false);
    const [toUpdateTag,setToUpdateTag] = useState(tagDatObj);
    const [state,setState] = useState(false);
    const [loading,setLoading] = useState(false);

    useEffect(() => {
        if(allTagsList.length !== totalTags && !paginationTableLoading) {
            setPaginationTableLoading(true);
            tagsAPI.getTagsAPI(allTagsList.length,totalTags).then((r) => {
                if(r.status) {
                    dispatch(tagsActions.setAllTags({data: r.data , total_count: r.total_count}));
                    setUITagsList(allTagsList);
                    setUITotalTags(r.total_count);
                }
                setPaginationTableLoading(false);
            })
        } else {
            setUITagsList(allTagsList);
            setUITotalTags(totalTags);
        }

    },[])
    const tagsTypeList = [
        {
            "id" : 1,
            "name" : "All Tags",
        },
        {
            "id" : 2,
            "name" : "Default Tags",
        },
        {
            "id" : 3,
            "name" : "General Tags",
        }
    ]
    const [selectedTagsType,setSelectedTagsType] = useState(tagsTypeList[0]);
    useEffect(() => {
        // console.log("change in selected tags");
        if(allTagsList.length != totalTags) {
            setPaginationTableLoading(true);
            tagsAPI.getTagsAPI(allTagsList.length,totalTags).then((r) => {
                if(r.status) {
                    dispatch(tagsActions.setAllTags({data: r.data , total_count: r.total_count}));
                    setUiTagsList();
                    // console.log("tagsss",allTagsList);
                }
                setPaginationTableLoading(false);
            })
        } else {
            setUiTagsList();
        }

    },[allTagsList,selectedTagsType])

    const setUiTagsList = () => {
        // console.log("selected tag",selectedTagsType)

        switch (selectedTagsType.id) {
            case 1:
                // console.log("case1");
                setUITagsList(allTagsList);
                setUITotalTags(totalTags);
                break;
            case 2:
                // console.log("case2");
                setUITagsList(allTagsList.filter((tag) => tag.is_default === 1));
                setUITotalTags(allTagsList.filter((tag) => tag.is_default === 1).length);
                break;
            case 3:
                // console.log("case3");
                setUITagsList(allTagsList.filter((tag) => tag.is_default === 0));
                setUITotalTags(allTagsList.filter((tag) => tag.is_default === 0).length)
                break;
            default:
                // console.log("cased");
                setUITagsList(allTagsList);
                setUITotalTags(totalTags);
                break;
        }
    }

    const [viewForm,setViewForm] = useState(false);
    const [toViewData,setToViewData] = useState({});

    const viewModel = () => {
        return ( <Modal
                centered
                footer={null}
                visible={viewForm}
                destroyOnClose={true}
                style={{
                    // background: "black",
                    border: "1px solid rgba(255, 255, 255,0.25)",
                    borderRadius: "10px",
                    // color: "white",
                }}
                bodyStyle={{
                    // background: "black",
                    // "rgba(0, 0, 0, 0.5)",
                    // color: "white",
                }}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onCancel={() => {setViewForm(false); setToViewData(tagDatObj)}}
            >
                <div>
                    <div><h4>Project: {toViewData.name}</h4></div>
                    <Divider  style={{background: "whitesmoke",margin: "10px 5px 10px 0px"}} />
                    <ViewDataTile parameters={{label: "Name",data: toViewData.name}}/>
                    <ViewDataTile parameters={{label: "Projects",data: toViewData.projects}} isListData={true}/>

                    {/*<ViewDataTile parameters={{label: "Color_Hex",data: toViewData.color_hex}} isColorData={true}/>*/}
                    {/*<ViewDataTile parameters={{label: "Client",data: toViewData.clients}} isListData={true}/>*/}
                    {/*<ViewDataTile parameters={{label: "Tags",data: toViewData.tags}} isListData={true}/>*/}
                </div>
            </Modal>
        )
    }

    const tagsColumn = [
        {
            title: 'Name',
            dataIndex: 'name',
            render: (text, record) => (
                <h2 className="table-avatar">
                    {/*<Link to="/app/profile/employee-profile" className="avatar"><img alt="" src={record.image} /></Link>*/}
                    {/*<Link to="/app/profile/employee-profile"></Link>*/}
                    {record.name}
                </h2>
            ),
            // sorter: (a, b) => a.name.length - b.name.length,
        },

        // {
        //     title: 'Project',
        //     dataIndex: 'project',
        //     render: (text, record) => (
        //         // <SelectProjectButton tagData={record} />
        //         // <>No Project</>
        //         <span style={{background: "rgba(255, 255, 255, 0.3)" ,
        //             borderRadius: "10px",padding: "8px 10px",fontSize:"14px"}}>{record.project == null || record.project.name == null ? "No Project" : record.project.name}</span>
        //     ),
        // },
        {
            title: 'Status',
            dataIndex: 'status',
            render: (text, record) => (
                <div className="dropdown">
                    <a href="#" className="btn btn-sm  dropdown-toggle" style={{background: "rgba(255, 255, 255,0.3)", borderRadius: "10px",color: "white"}} data-toggle="dropdown" aria-expanded="false">
                        <i className={record.status === 1? "fa fa-dot-circle-o text-success" : "fa fa-dot-circle-o text-danger"} /> {record.status === 1 ? "Active" : "Inactive" } </a>
                    <div className="dropdown-menu">
                        <Button className="dropdown-item" style={tableStyles.statusButtonsStyle} onClick={() => {
                            if(checkPermission(permissionRoutes.TagsP,permissionTypes.updateP)) {
                                tagStatusSelectToActive(record);
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }
                            }}><i className="fa fa-dot-circle-o text-success" /> <span style={tableStyles.statusButtonsInnerSpanStyle}>{"Active"}</span></Button>
                        <Button  className="dropdown-item" onClick={() => {
                            if(checkPermission(permissionRoutes.TagsP,permissionTypes.updateP)) {
                                tagStatusSelectToInActive(record);
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }

                           }}><i className="fa fa-dot-circle-o text-danger" /> <span style={tableStyles.statusButtonsInnerSpanStyle}>{"Inactive"}</span></Button>
                        {/*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-success" /> Active</a>*/}
                        {/*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-danger" /> Inactive</a>*/}
                    </div>
                </div>
            ),
        },
        {
            title: 'Tags',
            dataIndex: 'is_default',
            render: (text, record) => (
                <Tag color="rgba(255, 255, 255,0.3)"  style={{margin: "4px"}} key={record.id}>{record.is_default === 1 ? "Default" : "General"}</Tag>
            ),
            // sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: 'Action',
            render: (text, record) => (
                <div style={tableStyles.actionButtonDivStyle}>

                    <Tooltip title="view record" placement="left">
                        <button style={tableStyles.actionButtonStyles}  onClick={() => {
                            if(checkPermission(permissionRoutes.TagsP,permissionTypes.readP)) {
                                // console.log("view data",record);
                                setToViewData(record);
                                setViewForm(true);
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }

                        }}><i className="fa fa-eye" ></i></button>
                    </Tooltip>
                    <button style={tableStyles.actionButtonStyles} onClick={() => {
                        if(checkPermission(permissionRoutes.TagsP,permissionTypes.updateP)) {
                            setShowUpdateForm(true);
                            // console.log(record);
                            setToUpdateTag(record);
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }

                    }}><i className="fa fa-pencil" />
                        {/*Edit*/}
                    </button>
                    <button style={tableStyles.actionButtonStyles} onClick={() => {
                        if(checkPermission(permissionRoutes.TagsP,permissionTypes.deleteP)) {
                            onDeleteModel(record)
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }

                    }}><i className="fa fa-trash-o" /></button>
                </div>
            ),
        },

    ]
    const [columns,setColumns] = useState(tagsColumn)


    //----PAGE LOAD API CALLS-----
    useEffect( ()=>{
        // console.log("updated");
        let isMounted = true;
        if(allProjectsList.length === 0) {
            projectsAPI.getProjectsAPI(0,30).then((r) => {
                if(r.status  && isMounted) {
                    // console.log("getProjectsAPI PAGE",r.data);
                    dispatch(projectActions.setAllProjects({data: r.data , total_count: r.total_count}));
                }
            });
        } else {
        }
        if(allTagsList.length == 0) {
            tagsAPI.getTagsAPI(0,10).then((r) => {
                if(r.status && isMounted) {
                    // console.log("TAGS PAGE",r.data);
                    dispatch(tagsActions.setAllTags({data: r.data , total_count: r.total_count}));
                    setUITagsList(r.data);
                    setUITotalTags(r.total_count);
                }
            });
        } else {
            setColumns(tagsColumn);
        }
        return () => { isMounted = false };
    }, [state]);
    useEffect( ()=>{
        // console.log("toUpdateTag updated!",toUpdateTag);
    }, [toUpdateTag]);

    //-----ALL TAG LIST UPDATES UI----
    useEffect( ()=>{
        // console.log("allTags updated!");
        if(searchStatus) {
            setUITagsList(allTagsList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));

        } else {
            setUITagsList([...allTagsList]);
        }
    }, [allTagsList]);
    //-----------------------------
    const tagStatusSelectToInActive = (record) => {
        // console.log("selected",record);
        if(record.status !== 0) {
            record.status = 0;
            tagsAPI.updateTagAPI(record).then((res) => {
                if(res.status) {
                    dispatch(tagsActions.updateTag(record))
                }
            })
        }


    }
    const tagStatusSelectToActive = (record) => {
        // console.log("selected",record);
        if(record.status !== 1) {
            record.status = 1;
            tagsAPI.updateTagAPI(record).then((res) => {
                if(res.status) {
                    dispatch(tagsActions.updateTag(record))
                }
            })
        }


    }

    //-----------NEW TAG--------------------
    const newTagName = (e) => {
        setNewTag({...newTag,name: e.target.value});
    }
    const newTagStatusSetToInActive = () => {
        setNewTag({...newTag,status: 0})
    }
    const newTagStatusSetToActive = () => {
        setNewTag({...newTag,status: 1})
    }
    const addTag = async () => {
        // console.log("add tag called",newTag);
        if(newTag.name == "") {
            toast.error("Enter Tag Name!");
        }
        // else if(newTag.project == {} || newTag.project.id == null) {
        //     toast.error("Please select a Project!")
        // }
        else {
            setLoading(true);
            await tagsAPI.addTagAPI(newTag).then((res) => {
                if(res.status) {
                    let currentTagData = newTag;
                    currentTagData.id = res.data.id;
                    setShowForm(false);
                    setState(!state);
                    // console.log("ADDED",res.data);
                    dispatch(tagsActions.addTag(currentTagData))
                    setNewTag(tagDatObj);
                }
                setLoading(false);
            })
        }

    }
    const addClientDropDownStyle = {
        display: "inline-block",
        outline: "none",
        borderRadius: "10px",
        // border: "1px solid  #f0f0f0",
        fontSize: "16px",
        width: "80%",
        margin : "0px 0px 0px 7px"
    }
    const statusButtonStyle = {
        borderRadius:"10px",
        background: "rgba(204, 204, 204,0.5)",
        border: "none",
        outline: "none",
        padding: "5px 15px",
        fontWeight: "500",
        fontSize: '14px'
    }
    const addTagModel = () => {
        return ( <Modal
                centered
                visible={showForm}
                destroyOnClose={true}
                confirmLoading={loading}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={async () => {await addTag()}}
                onCancel={() => {setShowForm(false); setNewTag(tagDatObj)}}
            >
                <div>
                    <div><h4>Add Tag</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        {/*<span style={inputClientsLabelStyle}>Name : </span>*/}
                        {/*<input  style={addClientInputStyle}  type="text" />*/}
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={newTag.name} onChange={newTagName}
                        >
                        </Input>
                    </div>

                    {/*<div style={inputRowStyle}>*/}
                    {/*    <SelectProjectButton tagData={newTag} fromCreate={true} setTagData={setNewTag}/>*/}
                    {/*    /!*<span style={inputClientsLabelStyle}>Project : </span>*!/*/}
                    {/*    /!*<div className="dropdown" style={addClientDropDownStyle}>*!/*/}
                    {/*    /!*</div>*!/*/}
                    {/*</div>*/}
                    <div style={inputRowStyle}>
                        <SelectStatus parameters={{setNewData: setNewTag,newData:newTag}}/>
                    </div>
                    <div style={inputRowStyle}>
                        <span style={{marginBottom:"8px",marginRight: "10px",marginLeft: "4px",color: "black",fontSize: "14px"}} >Default Tag:</span>
                        <Checkbox
                            checked={newTag.is_default === 1}
                            onChange={(e) => {
                                setNewTag({...newTag,is_default: e.target.checked ? 1 : 0})
                            }}
                        />
                    </div>
                </div>
            </Modal>
        )
    }
    //----------REMOVE TAG---------------
    const removeTag = async (record) => {
        // console.log("removeClient called",record)
        await tagsAPI.removeTagAPI(record.id).then((res) => {
            if(res.status) {
                setState(!state);
                dispatch(tagsActions.removeTag({id: record.id}))
            }        })
    }
    const onDeleteModel =  (entry) => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to delete this Tag?',
            okText: "YES",
            cancelText: 'NO',
            onOk: async () => {await removeTag(entry)},
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }
    //------------UPDATE TAG------------
    const updateTagName = (e) => {
        setToUpdateTag({...toUpdateTag,name: e.target.value});
    }
    const updateTagStatusSetToInActive = () => {
        setToUpdateTag({...toUpdateTag,status: 0})
    }
    const updateTagStatusSetToActive = () => {
        setToUpdateTag({...toUpdateTag,status: 1})
    }
    const updateTag = () => {
        setLoading(true);
        // console.log("UPDATE CLIENT!!",toUpdateTag);
        tagsAPI.updateTagAPI(toUpdateTag).then((res) => {
            if(res.status) {
                setShowUpdateForm(false);
                setLoading(false);
                setSearchString("");
                setSearchStatus(false);
                setUITagsList(allTagsList);
                setState(!state);
                dispatch(tagsActions.updateTag(toUpdateTag))
            }
        })
    }
    const updateTagModel = ()  => {
        return ( <Modal
                centered
                visible={showUpdateForm}
                confirmLoading={loading}
                destroyOnClose={true}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={updateTag}
                onCancel={() => {setShowUpdateForm(false); setToUpdateTag(tagDatObj)}}
            >
                <div>
                    <div><h4>Add Tag</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        {/*<span style={inputClientsLabelStyle}>Name : </span>*/}
                        {/*<input  style={addClientInputStyle}  type="text" />*/}
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={toUpdateTag.name} onChange={updateTagName}
                        >
                        </Input>
                    </div>
                    {/*<div style={inputRowStyle}>*/}
                    {/*    <SelectProjectButton tagData={toUpdateTag} fromCreate={true} setTagData={setToUpdateTag}/>*/}
                    {/*    /!*<span style={inputClientsLabelStyle}>Project : </span>*!/*/}
                    {/*    /!*<div className="dropdown" style={addClientDropDownStyle}>*!/*/}
                    {/*    /!*</div>*!/*/}
                    {/*</div>*/}
                    <div style={inputRowStyle}>
                        <SelectStatus parameters={{setNewData: setToUpdateTag,newData:toUpdateTag}}/>
                    </div>
                    <div style={inputRowStyle}>
                        <span style={{marginBottom:"8px",marginRight: "10px",marginLeft: "4px",color: "black",fontSize: "14px"}} >Default Tag:</span>
                        <Checkbox
                            checked={toUpdateTag.is_default === 1}
                            onChange={(e) => {
                                setToUpdateTag({...toUpdateTag,is_default: e.target.checked ? 1 : 0})
                            }}
                        />
                    </div>
                </div>
                {/*<div>*/}
                {/*    <div>*/}
                {/*        <h4>Update Tag</h4>*/}
                {/*    </div>*/}
                {/*    <Divider />*/}
                {/*    <div style={inputRowStyle}>*/}
                {/*        <span style={inputClientsLabelStyle}>Name : </span>*/}
                {/*        <input  style={addClientInputStyle}  type="text" />*/}
                {/*    </div>*/}
                {/*    <div style={inputRowStyle}>*/}
                {/*        <span style={inputClientsLabelStyle}>Project : </span>*/}
                {/*        <div className="dropdown" style={addClientDropDownStyle}>*/}
                {/*            <SelectProjectButton tagData={toUpdateTag} fromCreate={true} setTagData={setToUpdateTag}/>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*    <div style={inputRowStyle}>*/}
                {/*        <span style={inputClientsLabelStyle}>Status : </span>*/}
                {/*        <div className="dropdown" style={addClientDropDownStyle}>*/}
                {/*            <a href="#" style={statusButtonStyle} className="btn btn-white btn-sm  dropdown-toggle" data-toggle="dropdown" aria-expanded="false">*/}
                {/*                <i className={toUpdateTag.status === 1 ? "fa fa-dot-circle-o text-success" : "fa fa-dot-circle-o text-danger"} /> {toUpdateTag.status === 1  ? "Active" : "Inactive"} </a>*/}
                {/*            <div className="dropdown-menu">*/}
                {/*                <Button className="dropdown-item" style={clientStatusButtonsStyle} onClick={updateTagStatusSetToActive} ><i className="fa fa-dot-circle-o text-success" /> <span style={clientStatusButtonsInnerSpanStyle}>{"Active"}</span></Button>*/}
                {/*                <Button  className="dropdown-item" onClick={updateTagStatusSetToInActive}><i className="fa fa-dot-circle-o text-danger" /> <span style={clientStatusButtonsInnerSpanStyle}>{"Inactive"}</span></Button>*/}
                {/*                /!*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-success" /> Active</a>*!/*/}
                {/*                /!*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-danger" /> Inactive</a>*!/*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</div>*/}
            </Modal>
        )
    }

    //-------SEARCH TAG-----------------
    const [searchStatus,setSearchStatus] = useState(false)
    const [searchString,setSearchString] = useState("");
    const filterTagList = (e) => {
        setSearchString(e.target.value);
        // console.log(e.target.value)
        if(e.target.value !== "") {
            setSearchStatus(true);
        } else {
            setSearchStatus(false);
        }
        if(allTagsList.length !== totalTags && !paginationTableLoading  && searchString.length <= 1) {
            setPaginationTableLoading(true);
            tagsAPI.getTagsAPI(allTagsList.length,totalTags).then((r) => {
                if(r.status) {
                    dispatch(tagsActions.setAllTags({data: r.data , total_count: r.total_count}));
                }
                setPaginationTableLoading(false);
            })
        } else {
            setUITagsList(allTagsList.filter((i) => i.name.toLowerCase().match(e.target.value.toLowerCase())));
            // console.log(allTagsList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
        }
    }
    //------------------------------------------------
    const [paginationTableLoading,setPaginationTableLoading] = useState(false);
    //----DATA TABLE------
    const getTagsDataTable = () => {

        if(allTagsList.length === 0) {
            return <div style={spinnerStyle}><Spin size="large"/></div>
        } else {
            return  <>
                <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <Table className="table-striped"
                                   pagination= { {total :
                                           searchString ?  uiTagsList.length : uiTotalTags,
                                       onChange: (page, pageSize) => {
                                           // console.log("onchange",page,pageSize,allTagsList.length)
                                           if(searchStatus) {

                                           } else {
                                               if(allTagsList.length <=  (page -1) * 10) {
                                                   setPaginationTableLoading(true);
                                                   tagsAPI.getTagsAPI(allTagsList.length,page * 10).then((r) => {
                                                       if(r.status) {
                                                           // console.log("getTagsAPI PAGE",r.data);
                                                           dispatch(tagsActions.setAllTags({data: r.data , total_count: r.total_count}));
                                                           setUITagsList([...uiTagsList,...r.data]);
                                                           // setPaginationLength(r.total_count);
                                                           // console.log("getTagsAPI PAGE 2",uiTagsList);
                                                           setPaginationTableLoading(false);
                                                       }
                                                   });
                                               } else {
                                               }
                                           }
                                       },
                                       showTotal : (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
                                       showSizeChanger : false,onShowSizeChange: onShowSizeChange ,itemRender : itemRender } }
                                   style = {{overflowX : 'auto'}}
                                   columns={columns}
                                   loading={paginationTableLoading}
                                // bordered
                                   dataSource={uiTagsList}
                                   rowKey={record => record.id}
                                   // onChange={console.log("change table")}
                            />
                        </div>
                    </div>
                </div></>
        }
    }
    const menuStyle = {
        padding: "0px 0px 0px 0px",
        background: "whitesmoke",
        borderRadius: "5px",
        scroll: "hidden"
    }
    const buttonStyle = {
        fontSize: "12px",
        fontWeight: "500",
        verticalAlign: "middle",
        color: "white",
        background: "rgba(0,0,0,1)",
        border: "1px solid rgba(0,0,0,1)",
        padding: "4px 10px"
    }


    const menu = (
        <Menu style={menuStyle}>
            {tagsTypeList.map((tagType,index) => {
                return (
                    <Menu.Item key={index}
                               style={{color: "black",
                                   margin: "0px 4px",
                                   // textAlign: "center",
                                   background: selectedTagsType.id === tagType.id ? "whitesmoke" : "white",
                                   borderRadius: "10px",
                                   border: "1px solid whitesmoke",
                               }}
                               onClick={() => {
                                   // console.log("menu item clicked")
                                   setSelectedTagsType(tagType)}}
                    >
                        <span>{tagType.name}</span>
                    </Menu.Item>
                )
            })}



        </Menu>
    );
    //------------------
    return (
        <div className="page-wrapper">
            <Helmet>
                <title>Tags</title>
                <meta name="description" content="Tags Page"/>
            </Helmet>
            {/* Page Content */}
            <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                    {/*Search Filter */}
                    <div className="row filter-row">
                        <div className="col-sm-6 col-md-7">
                            <div className="col">
                                <h3 className="page-title text-white">Tags</h3>
                                <ul className="breadcrumb text-white">
                                    <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Dashboard</Link></li>
                                    {/*/TODO: change to dashboard when created */}

                                    <li className="breadcrumb-item active text-white">Tags</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-5">
                            <div style={{display:"flex"}}>
                                <Dropdown
                                    overlay={menu}  trigger="click">
                                    <Button className="btn  mr-1"  style={{   display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        textTransform: "none",
                                        border: "none",
                                        outline: "none",
                                        background: "rgba(255,255,255,0.2)",
                                        color: "white",
                                    }} onClick={()=> {
                                    }} ><span>{selectedTagsType.name}</span><CaretDownOutlined style={{verticalAlign: "middle"}} /></Button>
                                    {/*<Button  style={buttonStyle}><DownOutlined /></Button>*/}
                                </Dropdown>
                                <Input  allowClear type="text"  value={searchString} placeholder="Search Tags" style={searchStyle} onChange={filterTagList} />
                                <Button className="btn btn-primary mr-1"  style={addTagsButtonStyle} onClick={()=> {
                                    if(checkPermission(permissionRoutes.TagsP,permissionTypes.addP)) {
                                        setShowForm(true)
                                    } else {
                                        toast.error("You Don't have Permission for this!");
                                    }
                                }} ><span>Create</span></Button>
                            </div>
                            {/*<div className="row filter-row">*/}
                                {/*<div className="col-sm-6 col-md-3" style={{padding: "5px 0px 0px 0px"}}>*/}
                                    {/*<div>  <span style={{*/}
                                    {/*    color: "white",*/}
                                    {/*    marginRight: "10px",*/}
                                    {/*    paddingTop: "5px",*/}
                                    {/*    verticalAlign: "middle"*/}
                                    {/*}}>Show Default Tags:</span>*/}
                                    {/*    <Checkbox*/}
                                    {/*        checked={showDefaultTags}*/}
                                    {/*        onChange={(e) => {*/}
                                    {/*            setShowDefaultTags(e.target.checked);*/}
                                    {/*            setShowNonDefaultTags(!e.target.checked);*/}
                                    {/*        }}*/}
                                    {/*    /></div>*/}
                                    {/*<div>  <span style={{*/}
                                    {/*    color: "white",*/}
                                    {/*    marginRight: "10px",*/}
                                    {/*    paddingTop: "5px",*/}
                                    {/*    verticalAlign: "middle"*/}
                                    {/*}}>Show Non Default Tags:</span>*/}
                                    {/*    <Checkbox*/}
                                    {/*        checked={showNonDefaultTags}*/}
                                    {/*        onChange={(e) => {*/}
                                    {/*            setShowDefaultTags(!e.target.checked);*/}
                                    {/*            setShowNonDefaultTags(e.target.checked);*/}
                                    {/*        }}*/}
                                    {/*    /></div>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
                {/*/Page Header */}
                {checkPermission(permissionRoutes.TagsP, permissionTypes.readP) ? getTagsDataTable() : <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                                                                                          description="You Have No Permission To Read!"/>}
            </div>
             {/*VIEW MODEL*/}
             {viewModel()}
            {/*VIEW MODEL*/}
            {/* /Page Content */}
            {/* Add Client Modal */}
            {addTagModel()}
            {/* /Add Client Modal */}
            {/* Edit Client Modal */}
            {updateTagModel()}
            {/* /Edit Client Modal */}
        </div>
    );
}
export default Tags;
