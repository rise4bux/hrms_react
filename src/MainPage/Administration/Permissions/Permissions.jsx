import React, {useEffect, useState} from "react";
import {
    permissionRoutes,
    permissionTypes
} from "../../../constants";
import {Helmet} from "react-helmet";
import {Button, Checkbox, Divider, Empty, Input, Modal, Skeleton, Spin, Switch} from "antd";
import {checkPermission} from "../../../permissionConstants";
import {toast} from "react-toastify";
import {Link, useHistory} from "react-router-dom";
import RoleTile from "./Components/RoleTile";
import { Collapse } from 'antd';
const { Panel } = Collapse;
import {CaretRightOutlined, PlusOutlined} from '@ant-design/icons';
import {useDispatch, useSelector} from "react-redux";
import {permissionsAPI, rolesAPI} from "../../../Services/apiServices";
import {rolesActions} from "../../../Redux/Actions/RolesActions";
import {homePageURL, loginRouteURL} from "../../../Services/routeServices";
import SelectStatus from "../Employees/Components/SelectStatus";
import PermissionBoxes from "./Components/PermissionBoxes";
import PermissionsTable from "./Components/PermissionsTable";
import SelectRolesButton from "./Components/SelectRolesButton";

const inputRowStyle = {
    margin: "20px 0px 10px 0px"

}
const addInputStyle = {
    display: "inline-block",
    outline: "none",
    border: "1px solid  #f0f0f0",
    color: "black",
    borderRadius: "10px",
    fontSize: "14px",
    width: "80%",
    margin: "0px 0px 0px 9px",
    padding: "8px 10px"
}

const inputLabelStyle = {
    fontWeight: "600",
    color: "black"

}
const  dialogButtonStyle = {
    border: "1px solid #f43b48",
    borderRadius: "10px",
    background: "#f43b48",
    color: "white"
}
const statusButtonsStyle = {
    display: "flex",
    justifyContent: "start",
    alignItems: "center",

}
const statusButtonsInnerSpanStyle = {
    margin: "0px 0px 0px 10px"

}

const addDropDownStyle = {
    display: "inline-block",
    outline: "none",
    borderRadius: "10px",
    // border: "1px solid  #f0f0f0",
    fontSize: "16px",
    width: "80%",
    margin : "0px 0px 0px 7px"
}
const statusButtonStyle = {
    borderRadius:"10px",
    background: "rgba(204, 204, 204,0.5)",
    border: "none",
    outline: "none",
    padding: "5px 15px",
    fontWeight: "500",
    fontSize: '14px'
}


const rolesTableStyle = {

    overflowY: "auto",
    height: "360px",
    background: "#232526",
    borderRadius: "10px",

}
const rolesTableListBoxStyle = {
    border: "none"
    // "1px solid #232526",
}
const rolesOuterDivStyle = {
    background: "rgba(255, 255, 255, 0.25)",
    padding: "10px 10px",
    borderRadius: "10px",
    height: "437px",
}

const moduleListStyle = {};
const moduleDivStyle = {
    border: "none",
    background: "#232526",
    borderRadius: "10px",
    height: "375px",
    overflowY: "auto"
}
const moduleOuterDivStyle = {
    background: "rgba(255, 255, 255, 0.25)",
    padding: "15px 15px 15px 10px",

    margin: "0px 0px 10px 10px",
    overflowY: "auto",
    borderRadius: "10px",


}

const tableHeaderStyle = {
    background: "#DD1845",
    color: "white",
    border: "none",
}

const tableSpanStyle = {
    color: "white",
    border: "none",
    boxShadow: "0px 0px 0px 0px #DD1845",
    verticalAlign: "middle",
    fontSize:"14px"

}
const buttonStyle = {
    fontSize: "12px",
    fontWeight: "500",
    verticalAlign: "middle",
    padding: "4px 10px"
}
const buttonStyle3 = {
    fontSize: "12px",
    fontWeight: "500",
    verticalAlign: "middle",
    color: "white",
    background: "rgba(0,0,0,1)",
    border: "1px solid rgba(0,0,0,1)",
    padding: "4px 10px"
}
//--------------------------------------------------------
const tableHeaderList = [

    "add",
    "csv",
    "delete",
    "isbillable",
    "isglobal",
    "read",
    "update"
]

const dataObj =  {
    "id": 1,
    "name": "",
    "status": 1,
    "permission": []
}



const Permissions = () => {


    //-----AUTH CHECK-----
    let history = useHistory();
    const userStatus =  useSelector((store) => store.db.userStatus);
    // useEffect(
    //     () => {
    //         if(!userStatus) {
    //             history.push(loginRouteURL);
    //         }
    //     }, []
    // )



    const [selectedRole,setSelectedRole] = useState(dataObj);
    const dispatch = useDispatch();
    const dbList = useSelector((store)=> store.db.allRoles);
    const total = useSelector((store)=> store.db.totalRoles);
    const [uiList,setUIList] = useState([...dbList]);
    const [newData,setNewData] = useState({id: 0, name: "", status: 1});
    const [showForm,setShowForm] = useState(false);
    const [showUpdateForm,setShowUpdateForm] = useState(false);
    const [toUpdateData,setToUpdateData] = useState({id: 0, name: "", status: 1});
    const [state,setState] = useState(false);
    const [loading,setLoading] = useState(false);
    const [paginationTableLoading,setPaginationTableLoading] = useState(false);


    useEffect( ()=>{
        let isMounted = true;
        if(dbList.length == 0) {
            rolesAPI.getRolesAPI(0,10).then((r) => {
                if(r.status && isMounted) {
                    dispatch(rolesActions.setAll({data: r.data , total_count: r.total_count}));
                    setUIList(r.data);
                    setSelectedRole(r.data[0]);
                    $('.roles-menu > ul').children('li').removeClass(" active");
                    $('.roles-menu > ul > li:first-child').addClass(" active");
                }
            });
        } else {
            setUIList([...dbList]);
            setSelectedRole(dbList[0]);
            $('.roles-menu > ul').children('li').removeClass(" active");
            $('.roles-menu > ul > li:first-child').addClass(" active");
        }
        return () => { isMounted = false };
    }, [state]);
    //---- SEARCH UI LIST SET----
    useEffect( ()=>{
        if(searchStatus) {
            setUIList(dbList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
        } else {
            setUIList([...dbList]);
        }
    }, [dbList]);


    const addRoleButtonClick = (e) => {
        // e.preventDefault();
        // console.log("on add role button");
        if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.addP)) {
            setShowForm(true);
        } else {
            toast.error("You Don't have Permission for this!");
        }
    }
    const roleTileClick = (e,role) => {
        // console.log("roleTileClick",e,role);
        e.preventDefault();
        $(e.target).parent().parent("ul").children().removeClass(" active")
        $(e.target).parent().addClass(" active");
        setSelectedRole({...role});
    }


    //-----ADD ----
    const newName = (e) => {
        setNewData({...newData,name: e.target.value});
    }
    const newStatusSetToInActive = () => {
        setNewData({...newData,status: 0})
    }
    const newStatusSetToActive = () => {
        setNewData({...newData,status: 1})
    }
    const addTheData = async () => {
        if(newData.name == "") {
            toast.error("Enter Role Name!");
        }  else {
            setLoading(true);
            await rolesAPI.addRoleAPI(newData).then((res) => {
                if(res.status) {
                    setNewData({id: 0, name: "", status: 1});
                    setShowForm(false);
                    setState(!state);
                    dispatch(rolesActions.add(res.data))
                }
                setLoading(false);
            })

        }

    }
    const addModel = () => {
        return ( <Modal
                centered
                visible={showForm}
                destroyOnClose={true}
                confirmLoading={loading}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={async () => {await addTheData()}}
                onCancel={() => {
                    setShowForm(false);
                    setNewData({id: 0, name: "", status: 1});
                } }
            >
                <div>
                    <div><h4>Add Role</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={newData.name} onChange={newName}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                        <SelectStatus parameters={{setNewData: setNewData,newData:newData}}/>

                    </div>
                </div>
            </Modal>
        )
    }

    //----REMOVE----
    const removeTheData = async (record) => {
        await rolesAPI.removeRoleAPI(record.id).then((res) => {
            if(res.status) {
                setState(!state);
                dispatch(rolesActions.remove(record))
                setUIList([...dbList.filter((entry,index) => entry.id != record.id)]);
            }        })
    }
    const onDeleteModel =  (entry) => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to delete this Role?',
            okText: "YES",
            cancelText: 'NO',
            onOk: async () => {await removeTheData(entry)},
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }

    //---UPDATE -----
    const updateName = (e) => {
        setToUpdateData({...toUpdateData,name: e.target.value});
    }
    const updateStatusSetToInActive = () => {
        setToUpdateData({...toUpdateData,status: 0})
    }
    const updateStatusSetToActive = () => {
        setToUpdateData({...toUpdateData,status: 1})
    }
    const updateTheData = () => {
        setLoading(true);
        // console.log("UPDATE CLIENT!!",toUpdateClient);
        rolesAPI.updateRoleAPI(toUpdateData).then((res) => {
            if(res.status) {
                setShowUpdateForm(false);
                setSearchString("");
                setSearchStatus(false);
                setUIList(dbList);
                dispatch(rolesActions.update(toUpdateData))
                setState(!state);
            }
            setLoading(false);

        })
    }
    const updateModel = ()  => {
        return ( <Modal
                centered
                visible={showUpdateForm}
                confirmLoading={loading}
                destroyOnClose={true}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={updateTheData}
                onCancel={() => {setShowUpdateForm(false); setToUpdateData({id: 0, name: "", status: 1})}}
            >
                <div>
                    <div><h4>Update Role</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={toUpdateData.name} onChange={updateName}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                        <SelectStatus parameters={{setNewData: setToUpdateData,newData:toUpdateData}}/>
                    </div>
                </div>
            </Modal>
        )
    }
    //----SEARCH CLIENT LIST------
    const [searchStatus,setSearchStatus] = useState(false)
    const [searchString,setSearchString] = useState("");
    const filterList = (e) => {
        setSearchString(e.target.value);
        // console.log(e.target.value)
        if(e.target.value !== "") {
            setSearchStatus(true);
        } else {
            setSearchStatus(false);
        }
        if(dbList.length !== total && !paginationTableLoading  && searchString.length <= 1) {
            setPaginationTableLoading(true);
            rolesAPI.getRolesAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    dispatch(rolesActions.setAll({data: r.data , total_count: r.total_count}));
                }
                setPaginationTableLoading(false);
            })
        } else {
            setUIList(dbList.filter((i) => i.name.toLowerCase().match(e.target.value.toLowerCase())));
        }
    }

    const setPermissionListModule = (e,list,moduleIDs) => {
        list.forEach((roleModule) => {
            // console.log("list",roleModule);
            if(roleModule.children == null) {
                // console.log("no child",roleModule.permissions);
                if(moduleIDs.includes(roleModule.id)) {
                    roleModule.permissions.forEach((permission) => {
                        if(e) {
                            permission.selected = 1
                        } else {
                            permission.selected = 0
                        }
                    })
                }
            } else {
                // console.log("child",roleModule.children.map((cm) => cm.id));
                setPermissionListModule(e,roleModule.children,moduleIDs);
            }
        })
    }

    const setModulesList = (list,modulesIdList) => {
        list.forEach((roleModule) => {
            if(roleModule.children != null) {
                setModulesList(roleModule.children,modulesIdList);
            } else {
                modulesIdList.push(roleModule.id);
            }
        })
    }
    const getSelectedModuleID = (recordList) => {
        let modulesIDList =  recordList.map((cm) => cm.id);
        setModulesList(recordList,modulesIDList);
        // console.log(recordList,modulesIDList);
        return modulesIDList;
    }
    const onModuleCheckBoxChange =  (e,moduleIDs) => {
        let tempSelectedRole = {...selectedRole};
        // console.log("my module",moduleIDs);
        setPermissionListModule(e,tempSelectedRole.permission,moduleIDs);

        // console.log("tempSelectedRole",tempSelectedRole);
        setSelectedRole({...tempSelectedRole});
        dispatch(rolesActions.update(selectedRole));
        callPermissionAPI().then((res) => {
            if(!res) {
                let tempSelectedRole = {...selectedRole};
                setPermissionListModule(e,tempSelectedRole.permission,moduleIDs);
                setSelectedRole({...tempSelectedRole});
                dispatch(rolesActions.update(selectedRole));
            }
        });
    }
    //---------------PERMISSION APIS---------------
    const callPermissionAPI = async () => {
        let moduleData = [];
        await setPermissionFOrAPIList( selectedRole.permission,moduleData)
        let payload = {
            role_id: selectedRole.id,
            module_data: moduleData
        }
        // console.log("setPermissionList",payload);
        return permissionsAPI.updatePermissionByRoleAPI(payload).then((res) => {
            // if(res.status) {
            //     apiData.loginUserWithID(getCurrentUser().id);
            // }
            return res.status;
        })
        // return true;
    }
    const setPermissionFOrAPIList = (list,moduleData) => {
        list.forEach((roleModule) => {
            if(roleModule.children == null) {
                let temp = {
                    id: roleModule.id,
                    permissions: roleModule.permissions.map((p) => {return {id: p.id, status: p.selected};})
                };
                if(roleModule.permissions.filter((p) => p.selected === 1).length > 0) {
                    moduleData.push(temp);
                } else {

                }
            } else {
                // let temp = {
                //     id: roleModule.id,
                //     permissions: [
                //         {
                //             "id": 1,
                //             "status": 1
                //         },
                //         {
                //             "id": 5,
                //             "status": 1
                //         },
                //         {
                //             "id": 3,
                //             "status": 1
                //         },
                //         {
                //             "id": 6,
                //             "status": 1
                //         },
                //         {
                //             "id": 7,
                //             "status": 1
                //         },
                //         {
                //             "id": 4,
                //             "status": 1
                //         },
                //         {
                //             "id": 2,
                //             "status": 1
                //         }
                //     ]
                // };
                // moduleData.push(temp);
                setPermissionFOrAPIList(roleModule.children,moduleData)
            }
        })
    }
    //-------MODULE ACCESS PANELS----
    const getPanelTile = (module) => {
        return  <Panel header={module.name} collapsible="disabled" showArrow={false} key={module.id}
                       extra={
            <Switch
                checked={module.permissions.find((p) => p.selected === 0) == null}
                onChange={(e) => {onModuleCheckBoxChange(e,[module.id])}}
                // checked={module.status == 1 ? true : false}
                // onChange={(e) => {console.log(e)}}
            />
        }
        >
        </Panel>
    }
    let count = 0;
    const countChildrenList = (list) => {
        for(let i = 0; i < list.length ; i++) {
            if(list[i].children == null) {
                if(list[i].permissions.find((p) => p.selected === 0) != null) {
                    count++;
                    return;
                }
            } else {
                countChildrenList(list[i].children);
            }
        }
    }
    const checkRecordChildren = (childList) => {
        count = 0;
        countChildrenList(childList);
        // console.log("count",count);
        return count === 0;
    }
    const getCollapsibleTile = (module) => {
        return   <Panel header={module.name} key={module.id}
                        extra={
            <span onClick={(e) => {e.stopPropagation();}}>
                {/*<PermissionBoxes moduleData={{}}/>*/}
                 <Switch checked={checkRecordChildren(module.children)} onChange={(e) => {  onModuleCheckBoxChange(!checkRecordChildren(module.children), getSelectedModuleID(module.children))}}/>
                </span>
           }
            >
            <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />} expandIconPosition="right">
                {getModulesList(module.children)}
            </Collapse>
        </Panel>
    }

    const getModulesList = (list) => {
       return list.map((module,key) => module.children == null ||module.children.length == 0  ? getPanelTile(module) :getCollapsibleTile(module))
    }

    //---EDIT ROLE CLICKED----------
    const editRoleClick = () => {
        // console.log("editRoleClick",e,$(e.target).parent().parent().parent().parent().parent("ul").children().removeClass(" active"));
        // $(e.target).parent().parent().parent().parent().parent("ul").children().removeClass(" active")
        // $(e.target).parent().parent().parent().parent().addClass(" active");
        if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.updateP)) {
            // console.log("record",record);
            setToUpdateData(selectedRole);
            setShowUpdateForm(true);
        } else {
            toast.error("You Don't have Permission for this!");
        }
    }
    //---------------DELETE ROLE CLICKED------------------
    const deleteRoleClick = () => {
        // console.log("deleteRoleClick",selectedRole);
        // $(e.target).parent().parent().parent().parent().parent("ul").children().removeClass(" active")
        // $(e.target).parent().parent().parent().parent().addClass(" active");
        if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.deleteP)) {
            onDeleteModel(selectedRole)
        } else {
            toast.error("You Don't have Permission for this!");
        }
    }


   const getTableDataTarget = (id) => {
    return ".multi-collapse" + id.toString();
   }
    const readDataTarget = (id) => {
        return "collapse multi-collapse" + id.toString();
    }
    //----------------------------------
    return (
        <div className="page-wrapper">
            <Helmet>
                <title>Roles & Permissions</title>
                <meta name="description" content="Login page"/>
            </Helmet>
            {/* Page Content */}
            <div className="content container-fluid">
                {/* Page Header */}
                {/*<div className="page-header">*/}
                {/*    <div className="row">*/}
                {/*        <div className="col-sm-12">*/}
                {/*            <h3 className="page-title text-white"></h3>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</div>*/}
                <div className="page-header">
                    {/*Search Filter */}
                    <div className="col">
                        <h3 className="page-title text-white">Roles &amp; Permissions</h3>
                        <ul className="breadcrumb text-white">
                            <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Dashboard</Link></li>
                            <li className="breadcrumb-item active text-white">Permissions</li>
                        </ul>
                    </div>
                    {/* Search Filter */}
                </div>
                {/* /Page Header */}
                <div style={{
                    background: "rgba(255, 255, 255, 0.25)",
                    padding: "15px 10px 10px 10px",
                    margin: "0px 0px 10px 0px",
                    borderRadius: "10px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                }}>
                  <div>
                      <Button className="btn  mr-1"  style={buttonStyle3} onClick={()=> {
                          if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.updateP)) {
                              addRoleButtonClick();
                          } else {
                              toast.error("You Don't have Permission for this!");
                          }
                      }} ><span><PlusOutlined style={{verticalAlign: "middle"}}/></span></Button>
                      <div style={{display:"inline-block"}}>

                          <SelectRolesButton parameters={{
                              selectedRole: selectedRole,
                              setSelectedRole: setSelectedRole
                          }} onClick={roleTileClick} onDeleteClick={deleteRoleClick} onEditClick={editRoleClick} />
                      </div>
                  </div>
                    <div style={{display:"inline-block"}}>

                        <Button className="btn  mr-1"  style={buttonStyle3} onClick={()=> {
                            if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.updateP)) {
                                editRoleClick();
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }
                        }} ><span>Edit {selectedRole.name}</span></Button>
                        <Button className="btn mr-1"  style={buttonStyle3} onClick={()=> {
                            if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.updateP)) {
                                deleteRoleClick();
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }
                        }} ><span>Remove {selectedRole.name}</span></Button>


                    </div>
                </div>

                {checkPermission(permissionRoutes.RolesAndPermissionsP, permissionTypes.readP) ?
                    <div>
                    {/*<div className="row">*/}
                    {/*    <div className="col-sm-4 col-md-4 col-lg-4 col-xl-4">*/}
                    {/*        <div style={rolesOuterDivStyle}>*/}
                    {/*            <a href="#" className="btn btn-primary btn-block" onClick={addRoleButtonClick}><i className="fa fa-plus" /> Add Role</a>*/}
                    {/*            <div className="roles-menu" style={rolesTableStyle}>*/}
                    {/*                    <ul style={rolesTableListBoxStyle}>*/}
                    {/*                        {uiList.map((role,key) => <RoleTile onClick={roleTileClick} onDeleteClick={deleteRoleClick} onEditClick={editRoleClick} key={role.id} tileData={role}/>)}*/}
                    {/*                    </ul>*/}

                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*    <div className="col-sm-8 col-md-8 col-lg-8 col-xl-8">*/}
                    {/*        <div style={moduleOuterDivStyle}>*/}
                    {/*            <h6 className="card-title m-b-10 text-white" style={{marginLeft: "10px"}}>Module Access</h6>*/}
                    {/*            <div  style={moduleDivStyle}>*/}
                    {/*                <Collapse accordion  expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />} expandIconPosition="right">*/}
                    {/*                    {getModulesList(selectedRole.permission)}*/}
                    {/*                </Collapse>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}

                    {/*</div>*/}
                    <PermissionsTable dataParams={{selectedRole: selectedRole,setSelectedRole: setSelectedRole,
                        deleteRoleClick:deleteRoleClick,
                        roleTileClick:roleTileClick,
                        editRoleClick:editRoleClick
                    }} />

                </div> :  <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                 description="You Have No Permission To Read!"/>}
            </div>
            {/* /Page Content */}
            {/* Add Client Modal */}
            {addModel()}
            {/* /Add Client Modal */}
            {/* Edit Client Modal */}
            {updateModel()}
            {/* /Edit Client Modal */}
        </div>
    );
}
export default Permissions;


// <div style={tableOuterDivStyle}>
//     <div style={{
//         display:"flex",
//         justifyContent:"space-between",
//         alignItems:"center",
//         margin: "5px",
//         padding: "5px"
//     }}>
//         <h6 className="text-white" style={{margin: "0px",verticalAlign:"middle",fontSize: "24px"}}>Module Permissions</h6>
//         <div>
//             <Button className="btn btn-primary mr-1"  style={buttonStyle} onClick={()=> {
//                 if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.updateP)) {
//                     grantAllPermissions(true)
//                 } else {
//                     toast.error("You Don't have Permission for this!");
//                 }
//             }} ><span>Grant All</span></Button>
//             <Button className="btn btn-primary mr-1"  style={buttonStyle} onClick={()=> {
//                 if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.updateP)) {
//                     grantAllPermissions(false)
//                 } else {
//                     toast.error("You Don't have Permission for this!");
//                 }
//             }} ><span>Remove All</span></Button>
//
//
//         </div>
//     </div>
//
//     {/*<h6 className="m-b-20 text-white">Grant all Permissions <Checkbox*/}
//     {/*    style={{marginRight: "4px"}}*/}
//     {/*    checked={checkAllPermissions()}*/}
//     {/*    onChange={grantAllPermissions}*/}
//     {/*/></h6>*/}
//     <div className="table-responsive">
//         <table className="table  custom-table" style={{borderRadius: "10px"}}>
//             <thead>
//             <tr style={tableHeaderStyle}>
//                 <th style={tableSpanStyle} key="permission">Modules</th>
//                 {/*<input type="checkbox" defaultChecked />*/}
//                 {tableHeaderList.map((headerName,key) =>
//                     <th style={tableSpanStyle} className="text-center" key={key}>
//                         {/*<Checkbox*/}
//                         {/*    style={{marginRight: "4px"}}*/}
//                         {/*    value={checkHeaderCheckBoxStatus(headerName)}*/}
//                         {/*    onChange={(e) => {onHeaderCheckBoxChange(e,headerName)}}*/}
//                         {/*/>*/}
//                         <span>{headerName}</span></th>)}
//             </tr>
//             </thead>
//             <tbody>
//             {selectedRole.permission.length != 0 ? selectedRole.permission.map((module,key) => {
//                 if(module.children == null) {
//                     return (
//                         <tr key={module.id}>
//                             <td className="text-white">  <Checkbox
//                                 style={{marginRight: "4px"}}
//                                 checked={module.permissions.find((p) => p.selected === 0) == null}
//                                 onChange={(e) => {onModuleCheckBoxChange(e.target.checked,module.id)}}
//                             /><span>{module.name}</span></td>
//                             {module.permissions.map((permission,key) => {
//                                 return <td  className="text-center" key={key}>
//                                     {/*{permission.name}*/}
//                                     <Checkbox
//                                         // style={{marginRight: "4px"}}
//                                         checked={permission.selected === 0 ? false : true}
//                                         onChange={(e) => {checkboxStatusChange(e,module,permission)}}
//                                     />
//                                 </td>
//                             })}
//                         </tr>
//                     )
//                 } else {
//                     return   <>
//                         <tr key={module.id}
//                             data-toggle="collapse"
//                             data-target={getTableDataTarget(module.id)}
//                         >
//
//
//                             <td className="text-white">
//                                 <CaretRightOutlined rotate={90} />
//                                 <Checkbox
//                                     style={{marginRight: "4px"}}
//                                     // checked={module.permissions.find((p) => p.selected === 0) == null}
//                                     // onChange={(e) => {onModuleCheckBoxChange(e.target.checked,module.id)}}
//                                 /><span>{module.name}</span></td>
//                             <td className="text-center"><Checkbox
//                                 style={{marginRight: "4px"}}
//                             /></td>
//                             <td className="text-center"> <Checkbox
//                                 style={{marginRight: "4px"}}
//                             /></td>
//                             <td className="text-center"><Checkbox
//                                 style={{marginRight: "4px"}}
//                             /></td>
//                             <td className="text-center"><Checkbox
//                                 style={{marginRight: "4px"}}
//                             /></td>
//                             <td className="text-center"><Checkbox
//                                 style={{marginRight: "4px"}}
//                             /></td>
//                             <td className="text-center"><Checkbox
//                                 style={{marginRight: "4px"}}
//                             /></td>
//                             <td className="text-center"><Checkbox
//                                 style={{marginRight: "4px"}}
//                             /></td>
//                         </tr>
//                         {
//                             module.children.map((m,key) => {
//                                 return (
//                                     <tr key={m.id} className={readDataTarget(module.id)} style={{borderLeft: "4px solid red",margin: "5px"}}>
//
//                                         <td className="text-white"> <Checkbox
//                                             style={{marginRight: "4px"}}
//                                             checked={m.permissions.find((p) => p.selected === 0) == null}
//                                             onChange={(e) => {onModuleCheckBoxChange(e.target.checked,m.id)}}
//                                         /><span>{m.name}</span></td>
//                                         {m.permissions.map((permission,key) => {
//                                             return <td key={key} className="text-center">
//                                                 {/*{permission.name}*/}
//                                                 <Checkbox
//                                                     // style={{marginRight: "4px"}}
//                                                     checked={permission.selected === 0 ? false : true}
//                                                     onChange={(e) => {checkboxStatusChange(e,m,permission)}}
//                                                 />
//                                             </td>
//                                         })}
//                                     </tr>
//                                 )
//                             })
//                         }
//
//                     </>
//
//
//                 }
//             }) : <></>}
//
//             </tbody>
//         </table>
//     </div>
// </div>