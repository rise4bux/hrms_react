import React, {useEffect, useState} from "react";


 const RoleTile = ({onClick,tileData,onEditClick,onDeleteClick}) => {

    const [state,setState] = useState(false)
    useEffect(() => {
        // console.log("tile Data",tileData);
        setState(!state);
    },[tileData])


     const roleTileStyle = {
         background: "#232526",
         color: "white",
         outline: "none",
         // borderBottom: "1px solid red",

     }

     const rolesActionStyles = {
         // background: "whitesmoke",
         // // color: "#DD1845",
         // marginLeft: "5px"
     }

    return (
        <li style={roleTileStyle}>
            <a href="#" onClick={(e) => {onClick(e,tileData)}} className="text-white border-0">{tileData.name}
                <span className="role-action">
                         <span className="action-circle large" onClick={(e) => {onEditClick(e,tileData)}} style={rolesActionStyles} >
                           <i className="material-icons">edit</i>
                         </span>
                         <span className="action-circle large delete-btn" onClick={(e) => onDeleteClick(e,tileData)} style={rolesActionStyles}>
                           <i className="material-icons">delete</i>
                         </span>
                       </span>
            </a>
        </li>
    )
}

export default RoleTile;