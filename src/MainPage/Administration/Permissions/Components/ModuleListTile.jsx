import React from "react";
import { Switch } from 'antd';
import { Collapse } from 'antd';
const { Panel } = Collapse;

const ModuleListTile = ({moduleData}) => {


    const moduleListTileStyle = {
        background: "#232526",
        color: "white",
        outline: "none",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        border: "1px solid #232526",
        borderRadius: "10px"
    }


    return (

        <li className="list-group-item" style={moduleListTileStyle}>
            {moduleData.name}
            {/*<Switch  onChange={(e) => {console.log(e)}}/>*/}
        </li>
    );
}

export default ModuleListTile;