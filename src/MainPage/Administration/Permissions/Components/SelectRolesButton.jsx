import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {CaretDownOutlined, CheckCircleFilled, DownOutlined, LoadingOutlined, PlusCircleFilled} from '@ant-design/icons';
import {Menu, Dropdown, Button, Space, Empty, Select} from 'antd';
import RoleTile from "./RoleTile";
import SearchInput from "../../../Employees/Toggle/ToggleComponents/SearchInput";
import {checkPermission} from "../../../../permissionConstants";
import {permissionRoutes, permissionTypes} from "../../../../constants";
import {toast} from "react-toastify";
const antIcon = <LoadingOutlined style={{ fontSize: 16,fontWeight: "800", color: "white" }} spin />;

export default  function SelectRolesButton({parameters,onClick,onDeleteClick,onEditClick})  {
    const dbList  = useSelector((store) => store.db.allRoles );
    const [uiList,setUIList] = useState([]);
    const buttonRef = useRef();
    const dispatch = useDispatch();

    const onFilterList = (text) => setUIList(dbList.filter((i) => i.name.toLowerCase().match(text.toLowerCase())))
    const setSelected =  (item) => {
        // console.log("item",item);
        parameters.setSelectedRole({...item});
    }


    useEffect(
        ()=> {
            setUIList([...dbList])
        }, [,parameters.selectedRole,dbList]
    )

    const menuStyle = {
        padding: "2px 0px 15px 0px",
        background: "whitesmoke",
        borderRadius: "5px"
    }
    const menu = (
        <Menu style={menuStyle}>
            <SearchInput params={{onFilterList: onFilterList}}/>
            {uiList.length != 0 ?
                uiList.map((role,index) =>
                    (  <Menu.Item itemID={role.id}
                                  style={{color: "black",
                                      margin: "0px 4px",
                                      // textAlign: "center",
                                      background: role.id === parameters.selectedRole.id ? "whitesmoke" : "white",
                                      borderRadius: "10px",
                                      border: "1px solid whitesmoke",
                                  }}
                                  key={index}
                                  onClick={(e)=> {setSelected(role)}}
                        >
                            {/*<RoleTile onClick={onClick} onDeleteClick={onDeleteClick} onEditClick={onEditClick} key={role.id} tileData={role}/>*/}
                        {role.name}
                        {/*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*/}
                    </Menu.Item>
                    )
                ) :
                <Menu.Item
                    style={{color: "#696969",
                        margin: "0px 4px",
                        textAlign: "center",
                        background: "whitesmoke",
                        borderRadius: "10px",
                        border: "1px solid whitesmoke"
                    }}
                    disabled
                    key={"no roles"}
                    onClick={null}>
                    <Empty className="dropDownEmpty" style={{marginTop: "30px",color: "black",background: "white"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                           description="No Data" />
                </Menu.Item>
            }

        </Menu>
    );


    const buttonStyle = {
        fontSize: "12px",
        fontWeight: "500",
        verticalAlign: "middle",
        color: "white",
        background: "rgba(0,0,0,1)",
        border: "1px solid rgba(0,0,0,1)",
        padding: "4px 10px"
    }

    return (
        <Dropdown

            destroyPopupOnHide={true}
            onVisibleChange={(e) => {
                if(!e) {
                    setUIList(dbList);
                }
            }}
            overlay={menu}  trigger="click" overlayStyle={{maxHeight:350,overflowY:'auto'}}>
            <Button className="btn  mr-1"  style={buttonStyle} onClick={()=> {
                if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.addP)) {
                } else {
                    toast.error("You Don't have Permission for this!");
                }
            }} ><span>{parameters.selectedRole.name != ""?parameters.selectedRole.name:"Select A Role"}</span><CaretDownOutlined style={{verticalAlign: "middle"}} /></Button>
            {/*<Button  style={buttonStyle}><DownOutlined /></Button>*/}
        </Dropdown>

    );
}