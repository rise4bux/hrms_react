import React, {useEffect, useState} from "react";
import {Button, Checkbox, Modal, Table} from "antd";
import {permissionsAPI} from "../../../../Services/apiServices";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";
import {useDispatch} from "react-redux";
import {checkPermission} from "../../../../permissionConstants";
import {permissionRoutes, permissionTypes} from "../../../../constants";
import {toast} from "react-toastify";
import SelectRolesButton from "./SelectRolesButton";

const tableOuterDivStyle = {
    background: "rgba(255, 255, 255, 0.25)",
    padding: "15px 10px 10px 10px",
    // margin: "20px 0px 10px 10px",
    borderRadius: "10px",
    minHeight: "500px"
}
const tableHeaderList = [

    "add",
    "csv",
    "delete",
    "isbillable",
    "isglobal",
    "read",
    "update"
]
const buttonStyle = {
    fontSize: "12px",
    fontWeight: "500",
    verticalAlign: "middle",
    padding: "4px 10px"
}
const buttonStyle1 = {
    fontSize: "12px",
    padding: "0px  10px",
    margin: "2px 10px",
    fontWeight: "500",
    verticalAlign: "middle",
}


export default function PermissionsTable({dataParams}) {


    const [tableData,setTableData] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        // console.log("Permission Table",dataParams.selectedRole);
        setTableData([...dataParams.selectedRole.permission])
    },[dataParams.selectedRole])
    //----GRANT ALL MODULE PERMISSIONS----

    let count = 0;
    const countChildrenList = (list) => {
             for(let i = 0; i < list.length ; i++) {
                 if(list[i].children == null) {
                     if(list[i].permissions.find((p) => p.selected === 0) != null) {
                         count++;
                         return;
                     }
                 } else {
                     countChildrenList(list[i].children);
                 }
             }
    }
    const checkRecordChildren = (childList) => {
        count = 0;
        countChildrenList(childList);
        // console.log("count",count);
        return count === 0;
    }

    const setModulesList = (list,modulesIdList) => {
        list.forEach((roleModule) => {
            if(roleModule.children != null) {
                setModulesList(roleModule.children,modulesIdList);
            } else {
                modulesIdList.push(roleModule.id);
            }
        })
    }
    const getSelectedModuleID = (recordList) => {
        let modulesIDList =  recordList.map((cm) => cm.id);
        setModulesList(recordList,modulesIDList);
        // console.log(recordList,modulesIDList);
        return modulesIDList;
    }
    const columns = [
        {
            title: 'Module',
            dataIndex: 'Module',
            key: 'Module',
            width: "50%",
            render: (text,record,index) => {
                return (
                    <span >
                       <Checkbox
                           checked={record.permissions != null ? record.permissions.find((p) => p.selected === 0) == null : checkRecordChildren(record.children)}
                           onChange={(e) => {
                               // moduleCheckBoxChange(e.target.checked,record);
                               if (record.children == null) {
                                   onModuleCheckBoxChange(e.target.checked, [record.id])
                               } else {
                                   // console.log("has children");
                                   onModuleCheckBoxChange(!checkRecordChildren(record.children), getSelectedModuleID(record.children))
                               }
                           }}
                       />
                        <span className="text-white m-l-5" style={{fontSize: "14px"}}>{record.name}</span>

                    </span>
                )
            },
        },
        { title: 'add', dataIndex: 'add', key: 'add',align: "center",
        render: (text,record,index) => {
            if(record.children == null) {
                return (<Checkbox checked={record.permissions != null ? record.permissions[0].selected === 1 ? true : false : false} onChange={(e) => {checkboxStatusChange(e, record, record.permissions[0])}}/>)
            } else {
                return <></>
            }

        }
        },
        { title: 'csv', dataIndex: 'csv', key: 'csv' ,align: "center",
            render: (text,record,index) => {
                if(record.children == null) {
                    return (    <Checkbox
                        checked={record.permissions != null ? record.permissions[1].selected === 1 ? true: false : false}
                        onChange={(e) => {checkboxStatusChange(e,record,record.permissions[1])}}
                    />)
                } else {
                    return <></>
                }

            }
        },
        { title: 'delete', dataIndex: 'delete', key: 'delete' ,align: "center",
            render: (text,record,index) => {
                if(record.children == null) {
                    return (    <Checkbox
                        checked={record.permissions != null ? record.permissions[2].selected === 1 ? true: false : false}
                        onChange={(e) => {checkboxStatusChange(e,record,record.permissions[2])}}
                    />)
                } else {
                    return <></>
                }

            }
        },
        { title: 'isbillable', dataIndex: 'isbillable', key: 'isbillable' ,align: "center",
            render:(text,record,index)=> {
                if(record.children == null) {
                    return (    <Checkbox
                        checked={record.permissions != null ? record.permissions[3].selected === 1 ? true: false : false}
                        onChange={(e) => {checkboxStatusChange(e,record,record.permissions[3])}}
                    />)
                } else {
                    return <></>
                }

            }
        },
        { title: 'isglobal', dataIndex: 'isglobal', key: 'isglobal' ,align: "center",
            render: (text,record,index) => {
                if(record.children == null) {
                    return (    <Checkbox
                        checked={record.permissions != null ? record.permissions[4].selected === 1 ? true: false : false}
                        onChange={(e) => {checkboxStatusChange(e,record,record.permissions[4])}}
                    />)
                } else {
                    return <></>
                }

            }
        },
        { title: 'read', dataIndex: 'read', key: 'read',align: "center",
            render: (text,record,index) => {
                if(record.children == null) {
                    return (    <Checkbox
                        checked={record.permissions != null ? record.permissions[5].selected === 1 ? true: false : false}
                        onChange={(e) => {checkboxStatusChange(e,record,record.permissions[5])}}
                    />)
                } else {
                    return <></>
                }

            }
        },
        { title: 'update', dataIndex: 'update', key: 'update',align: "center",
            render: (text,record,index) => {
                if(record.children == null) {
                    return (    <Checkbox
                        checked={record.permissions != null ? record.permissions[6].selected === 1 ? true: false : false}
                        onChange={(e) => {checkboxStatusChange(e,record,record.permissions[6])}}
                    />)
                } else {
                    return <></>
                }

            }
        },

    ];

    //---------------PERMISSION APIS---------------
    const callPermissionAPI = async () => {
        let moduleData = [];
        await setPermissionFOrAPIList( dataParams.selectedRole.permission,moduleData)
        let payload = {
            role_id: dataParams.selectedRole.id,
            module_data: moduleData
        }
        // console.log("setPermissionList",payload);
        return permissionsAPI.updatePermissionByRoleAPI(payload).then((res) => {
            // if(res.status) {
            //     apiData.loginUserWithID(getCurrentUser().id);
            // }
            return res.status;
        })
        // return true;
    }
    const setPermissionFOrAPIList = (list,moduleData) => {
        list.forEach((roleModule) => {
            if(roleModule.children == null) {
                let temp = {
                    id: roleModule.id,
                    permissions: roleModule.permissions.map((p) => {return {id: p.id, status: p.selected};})
                };
                if(roleModule.permissions.filter((p) => p.selected === 1).length > 0) {
                    moduleData.push(temp);
                } else {

                }
            } else {
                // let temp = {
                //     id: roleModule.id,
                //     permissions: [
                //         {
                //             "id": 1,
                //             "status": 1
                //         },
                //         {
                //             "id": 5,
                //             "status": 1
                //         },
                //         {
                //             "id": 3,
                //             "status": 1
                //         },
                //         {
                //             "id": 6,
                //             "status": 1
                //         },
                //         {
                //             "id": 7,
                //             "status": 1
                //         },
                //         {
                //             "id": 4,
                //             "status": 1
                //         },
                //         {
                //             "id": 2,
                //             "status": 1
                //         }
                //     ]
                // };
                // moduleData.push(temp);
                setPermissionFOrAPIList(roleModule.children,moduleData)
            }
        })
    }
    //--------------------------------------------------
//    ----------SINGLE CHECKBOX PERMISSION---------------------
    const setPermissionList = (list,module,permission) => {
        list.forEach((roleModule) => {
            if(roleModule.children == null) {
                if(roleModule.id === module.id) {
                    if(permission.selected === 0) {
                        roleModule.permissions.find((p) => p.name === permission.name).selected = 1

                    } else {
                        roleModule.permissions.find((p) => p.name === permission.name).selected = 0
                    }
                }
            } else {
                setPermissionList(roleModule.children,module,permission);
            }
        });
    }
    const checkboxStatusChange =  (e,module,permission) => {
        let tempSelectedRole = {...dataParams.selectedRole};
        setPermissionList(tempSelectedRole.permission,module,permission);
        dataParams.setSelectedRole({...tempSelectedRole});
        dispatch(rolesActions.update(dataParams.selectedRole));
        callPermissionAPI().then((res) => {
            if(!res) {
                let tempSelectedRole = {...dataParams.selectedRole};
                setPermissionList(tempSelectedRole.permission,module,permission);
                dataParams.setSelectedRole({...tempSelectedRole});
                dispatch(rolesActions.update(dataParams.selectedRole));
            }
        });
    }

    //--------------------------------MODULES PERMISSIONS SECTION--------------------
    const setPermissionListModule = (e,list,moduleIDs) => {
        list.forEach((roleModule) => {
            // console.log("list",roleModule);
            if(roleModule.children == null) {
                // console.log("no child",roleModule.permissions);
                if(moduleIDs.includes(roleModule.id)) {
                    console.log("found",roleModule)
                    roleModule.permissions.forEach((permission) => {
                        if(e) {
                            permission.selected = 1
                        } else {
                            permission.selected = 0
                        }
                    })
                } else {
                    console.log("not found",roleModule)
                }
            } else {
                console.log("child",);
                setPermissionListModule(e,roleModule.children,moduleIDs);
            }
        })
    }

    const onModuleCheckBoxChange =  (e,moduleIDs) => {
        let tempSelectedRole = {...dataParams.selectedRole};
        console.log("my module",moduleIDs);
        setPermissionListModule(e,tempSelectedRole.permission,moduleIDs);

        console.log("tempSelectedRole",tempSelectedRole);
        dataParams.setSelectedRole({...tempSelectedRole});
        dispatch(rolesActions.update(dataParams.selectedRole));
        callPermissionAPI().then((res) => {
            if(!res) {
                let tempSelectedRole = {...dataParams.selectedRole};
                setPermissionListModule(e,tempSelectedRole.permission,moduleIDs);
                dataParams.setSelectedRole({...tempSelectedRole});
                dispatch(rolesActions.update(dataParams.selectedRole));
            }
        });
    }
    //-----------------------------------------------------------------
    //---------ALL PERMISSIONS GRANT CHECKBOX SECTION--------------------
    const setPermissionListAll = (list,e) => {
        list.forEach((roleModule) => {
            if(roleModule.children == null) {
                if(e) {
                    roleModule.permissions.forEach((p) => p.selected = 1);
                } else {
                    roleModule.permissions.forEach((p) => p.selected = 0);
                }
            }else {
                setPermissionListAll(roleModule.children,e);
            }
        })
    }
    const [allPermissions,setAllPermissions] = useState(false);
    const grantAllPermissions = (e) => {
        let tempSelectedRole = {...dataParams.selectedRole};
        setPermissionListAll(tempSelectedRole.permission,e);
        dataParams.setSelectedRole({...tempSelectedRole});
        callPermissionAPI().then((res) => {
            if(!res) {
                let tempSelectedRole = {...dataParams.selectedRole};
                setPermissionListAll(tempSelectedRole.permission,e);
                dataParams.setSelectedRole({...tempSelectedRole});
            }
        });
    }
    const checkAllPermissionsList =  (list,status) => {
        list.forEach((roleModule) => {
            if(roleModule.children == null) {
                roleModule.permissions.forEach((p) => {
                    status.push(p.selected);
                })
            } else {
                checkAllPermissionsList(roleModule.children,status)
            }
        });
    }
    useEffect(() => {
        // checkAllPermissions();
        // setAllPermissions(!allPermissions);
    },[dataParams.selectedRole])
    const checkAllPermissions = async () => {
        var status = []
        checkAllPermissionsList(dataParams.selectedRole.permission,status)

        //      .then((res) => {
        //
        // });
        // console.log("status",status,status.filter((i) => i === 0).length,status.filter((i) => i === 1).length == status.length);
        // setAllPermissionStatus(status.filter((i) => i === 1).length == status.length);
        return status.filter((i) => i === 1).length == status.length
    }
    //---------------------------------------------------------

    //------HEADER PERMISSIONS SECTION--------------
    // const setPermissionListHeader = (e,list,headerNae) => {
    //     list.forEach((roleModule) => {
    //         if(roleModule.children == null) {
    //             if(e.target.checked) {
    //                 roleModule.permissions.find((p) => p.name === headerName).selected = 1
    //
    //             } else {
    //                 roleModule.permissions.find((p) => p.name === headerName).selected = 0
    //             }
    //         } else {
    //             setPermissionListHeader(e,roleModule.children,headerName);
    //         }
    //
    //     })
    // }
    // const onHeaderCheckBoxChange = (e,headerName) => {
    //     let tempSelectedRole = {...selectedRole};
    //     setPermissionListHeader(e,tempSelectedRole.permission,headerName)
    //     setSelectedRole({...tempSelectedRole});
    // }
    // const checkHeaderCheckBoxStatus = (e,name) => {
    // }
    const  dialogButtonStyle = {
        border: "1px solid #f43b48",
        borderRadius: "10px",
        background: "#f43b48",
        color: "white"
    }
    const removePermissions = () => {

    }
    const onRemoveAllPermissions =  () => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to remove all permissions?',
            okText: "YES",
            cancelText: 'NO',
            onOk: async () => {await grantAllPermissions(false)},
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }

    return (

        <div style={tableOuterDivStyle}>
            <div style={{
                display:"flex",
                justifyContent:"space-between",
                alignItems:"center",
                margin: "5px",
                padding: "5px"
            }}>
                <h6 className="text-white" style={{margin: "0px",verticalAlign:"middle",fontSize: "24px"}}>Module Permissions</h6>

                <div>
                    <div style={{display:"inline-block"}}>
                        {/*<SelectRolesButton parameters={{*/}
                        {/*    selectedRole: dataParams.selectedRole,*/}
                        {/*    setSelectedRole: dataParams.setSelectedRole*/}
                        {/*}} onClick={dataParams.roleTileClick} onDeleteClick={dataParams.deleteRoleClick} onEditClick={dataParams.editRoleClick} />*/}
                    </div>
                    <div style={{display:"inline-block"}}>

                        <Button className="btn btn-primary mr-1"  style={buttonStyle} onClick={()=> {
                            if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.updateP)) {
                                grantAllPermissions(true)
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }
                        }} ><span>Grant All</span></Button>
                        <Button className="btn btn-primary mr-1"  style={buttonStyle} onClick={()=> {
                            if(checkPermission(permissionRoutes.RolesAndPermissionsP,permissionTypes.updateP)) {
                                onRemoveAllPermissions();
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }
                        }} ><span>Remove All</span></Button>


                    </div>
                </div>

            </div>
            <Table
                columns={columns}
                pagination={false}
                size="small"
                rowKey="id"
                expandable={{
                    rowExpandable: record => record.children != null,
                }}
                dataSource={tableData}
            />
        </div>

    )

}
