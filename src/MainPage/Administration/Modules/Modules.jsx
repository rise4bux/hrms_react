
//-----CLIENT PAGE STYLES----\
import React, {useEffect, useState} from "react";
import {
    dashBoardHelmet,
    permissionRoutes,
    permissionTypes,
} from "../../../constants";
import {Helmet} from "react-helmet";
import {Button, Divider, Empty, Input, Modal, Spin, Table} from "antd";
import {checkPermission} from "../../../permissionConstants";
import {toast} from "react-toastify";
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import { modulesAPI} from "../../../Services/apiServices";
import {onShowSizeChange, itemRender} from "../../paginationfunction";
import {modulesActions} from "../../../Redux/Actions/ModulesActions";
import SelectModuleButton from "./Components/SelectModuleButton";
import {homePageURL, loginRouteURL} from "../../../Services/routeServices";
import {searchStyle, tableStyles} from "../../../global_styles";
import ViewDataTile from "../../../CustomComponents/ViewDataTile";

const inputRowStyle = {
    margin: "20px 0px 10px 0px"

}
const addInputStyle = {
    display: "inline-block",
    outline: "none",
    border: "1px solid  #f0f0f0",
    color: "black",
    borderRadius: "10px",
    fontSize: "14px",
    width: "80%",
    margin: "0px 0px 0px 9px",
    padding: "8px 10px"
}

const inputLabelStyle = {
    fontWeight: "600",
    color: "black"

}
const  dialogButtonStyle = {
    border: "1px solid #f43b48",
    borderRadius: "10px",
    background: "#f43b48",
    color: "white"
}
const statusButtonsStyle = {
    display: "flex",
    justifyContent: "start",
    alignItems: "center",

}
const statusButtonsInnerSpanStyle = {
    margin: "0px 0px 0px 10px"

}
const spinnerStyle = {
    display: "flex",
    justifyContent: "center",
    marginTop: "20px"
}
const addButtonStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textTransform: "none"
    // margin: "0px 30px 0px 0px"
}
const tableDivStyle = {
    background: "black",
    padding: "10px 10px",
    borderRadius: "10px"
}

const pageStyle = {
    // background: "#232526",
    // borderRadius: "10px",
}

//--------------------------------------------------------
const modulesDataObj =  {
    "id": 0,
    "name": "",
    "parent_id": 0,
    "module_url": "",
    "childs": []
};


const Modules = () => {



    const dispatch = useDispatch();
    const dbList = useSelector((store)=> store.db.allModules);
    const total = useSelector((store)=> store.db.totalModules);
    const [uiList,setUIList] = useState([...dbList]);
    const [newData,setNewData] = useState(modulesDataObj);
    const [showForm,setShowForm] = useState(false);
    const [showUpdateForm,setShowUpdateForm] = useState(false);
    const [toUpdateData,setToUpdateData] = useState(modulesDataObj);
    const [state,setState] = useState(false);
    const [loading,setLoading] = useState(false);
    const [paginationTableLoading,setPaginationTableLoading] = useState(false);

    //---TABLE COLUMNS DATA-------
    const columnsData = [
        {
            title: 'ID',
            dataIndex: 'id',
            width: '1%',
            render: (text, record) =>{
                return  (
                    <h2 className="table-avatar">
                        {text}
                    </h2>
                )
            },
        },
        {
            title: 'Name',
            dataIndex: 'name',
            render: (text, record) =>{
                return  (
                    <h2 className="table-avatar">
                        {text}
                    </h2>
                )
            },
        },
        {
            title: 'Url',
            dataIndex: 'module_url',
            render: (text, record) =>{
                return  (
                    <h2 className="table-avatar">
                        {text}
                    </h2>
                )
            },
        },

        {
            title: 'Action',
            render: (text, record) => (
                <div style={tableStyles.actionButtonDivStyle}>
                    <button style={tableStyles.actionButtonStyles} onClick={() => {
                        if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                            console.log("record",record);
                            setToUpdateData(record);
                            setShowUpdateForm(true);
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }

                    }}><i className="fa fa-pencil"/>
                        {/*Edit*/}
                    </button>
                    <button style={tableStyles.actionButtonStyles}  onClick={() => {
                        if(checkPermission(permissionRoutes.ClientsP,permissionTypes.deleteP)) {
                            onDeleteModel(record)
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }
                    }}><i className="fa fa-trash-o" /></button>
                </div>
            ),
        },
    ];
    const [columns,setColumns] = useState(columnsData)
    //------GET ALL CLIENTS API (limit : 10)
    useEffect( ()=>{
        let isMounted = true;
        if(dbList.length == 0) {
            modulesAPI.getModulesAPI(0,50).then((r) => {
                if(r.status && isMounted) {
                    dispatch(modulesActions.setAll({data: r.data , total_count: r.total_count}));
                    setUIList(r.data);
                }
            });
        } else {
            setColumns(columnsData);
        }
        return () => { isMounted = false };
    }, [state]);
    //---- SEARCH UI LIST SET----
    useEffect( ()=>{
        if(searchStatus) {
            setUIList(dbList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
        } else {
            setUIList([...dbList]);
        }
    }, [dbList]);
    //---------------------------------------------

    //-----ADD CLIENT----
    const newName = (e) => {
        setNewData({...newData,name: e.target.value});
    }
    const newURL = (e) => {
        setNewData({...newData,module_url: e.target.value});
    }

    const addTheData = async () => {
        // console.log("data add",newData);
        if(newData.name == "") {
            toast.error("Enter Module Name!");
        }  else {
            setLoading(true);
            await modulesAPI.addModuleAPI(newData).then((res) => {
                if(res.status) {
                    setNewData({...modulesDataObj});
                    setShowForm(false);
                    // console.log("data add",res.data);
                    dispatch(modulesActions.add(res.data))
                    setState(!state);
                }
                setLoading(false);
            })

        }

    }


    const addModel = () => {
        return ( <Modal
                centered
                visible={showForm}
                destroyOnClose={true}
                confirmLoading={loading}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={async () => {await addTheData()}}
                onCancel={() => {
                    setShowForm(false);
                    setNewData({...modulesDataObj});
                } }
            >
                <div>
                    <div><h4>Add Module</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={newData.name} onChange={newName}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Url:</span>}
                            defaultValue={newData.module_url} onChange={newURL}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                      <SelectModuleButton parameters={{setNewData: setNewData, newData: newData}} />
                    </div>

                </div>
            </Modal>
        )
    }
    //----REMOVE CLIENT----
    const removeTheData = async (record) => {
        await modulesAPI.removeModuleAPI(record.id).then((res) => {
            if(res.status) {
                setState(!state);
                dispatch(modulesActions.remove(record))
                setUIList([...dbList.filter((entry,index) => entry.id != record.id)]);
            }        })
    }
    const onDeleteModel =  (entry) => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to delete this Module?',
            okText: "YES",
            cancelText: 'NO',
            onOk: async () => {await removeTheData(entry)},
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }
    //---UPDATE CLIENT-----
    const updateName = (e) => {
        setToUpdateData({...toUpdateData,name: e.target.value});
    }
    const updateURL = (e) => {
        setToUpdateData({...toUpdateData,module_url: e.target.value});
    }
    const updateStatusSetToInActive = () => {
        setToUpdateData({...toUpdateData,status: 0})
    }
    const updateStatusSetToActive = () => {
        setToUpdateData({...toUpdateData,status: 1})
    }
    const updateTheData = () => {
        setLoading(true);
        // console.log("UPDATE CLIENT!!",toUpdateClient);
        modulesAPI.updateModuleAPI(toUpdateData).then((res) => {
            if(res.status) {
                setShowUpdateForm(false);
                setLoading(false);
                setSearchString("");
                setSearchStatus(false);
                setUIList(dbList);
                dispatch(modulesActions.update(toUpdateData))
                setState(!state);
            }
        })
    }
    const updateModel = ()  => {
        return ( <Modal
                centered
                visible={showUpdateForm}
                confirmLoading={loading}
                destroyOnClose={true}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={updateTheData}
                onCancel={() => {setShowUpdateForm(false); setToUpdateData({...modulesDataObj})}}
            >
                <div>
                    <div><h4>Update Module : {toUpdateData.name}</h4></div>

                    <Divider />
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={toUpdateData.name} onChange={updateName}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Url:</span>}
                            defaultValue={toUpdateData.module_url} onChange={updateURL}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                        <SelectModuleButton parameters={{setNewData: setToUpdateData, newData: toUpdateData}}  fromUpdate={true}/>
                    </div>
                    <Divider />
                    {toUpdateData.parent_id  != null && toUpdateData.parent_id != 0 ? <ViewDataTile  compactTile={true} isListData={true} parameters={{label: "Parent",data: dbList.filter((m) => m.id == toUpdateData.parent_id)}}/> : <></>}
                    { toUpdateData.childs.length != 0 ? <ViewDataTile compactTile={true} isListData={true} parameters={{label: "Children",data: toUpdateData.childs}}/> : <></>}
                </div>
            </Modal>
        )
    }
    //----SEARCH CLIENT LIST------
    const [searchStatus,setSearchStatus] = useState(false)
    const [searchString,setSearchString] = useState("");
    const filterList = (e) => {
        setSearchString(e.target.value);
        // console.log(e.target.value)
        if(e.target.value !== "") {
            setSearchStatus(true);
        } else {
            setSearchStatus(false);
        }
        if(dbList.length !== total && !paginationTableLoading  && searchString.length <= 1) {
            setPaginationTableLoading(true);
            modulesAPI.getModulesAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    dispatch(modulesActions.setAll({data: r.data , total_count: r.total_count}));
                }
                setPaginationTableLoading(false);
            })
        } else {
            setUIList(dbList.filter((i) => i.name.toLowerCase().match(e.target.value.toLowerCase())));
        }
    }
    //-------DATA TABLE-----
    const getDataTable = () => {
        if(dbList.length === 0) {
            return <div style={spinnerStyle}><Spin size="large"/></div>
        } else {
            return  <div style={tableDivStyle}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <Table className="table-striped text-white"
                                   pagination= { {total : searchStatus ? uiList.length : total,
                                       onChange: (page, pageSize) => {
                                           if(searchStatus) {
                                           } else {
                                               if(dbList.length <=  (page -1) * 10) {
                                                   setPaginationTableLoading(true);
                                                   modulesAPI.getModulesAPI(dbList.length,page * 10).then((r) => {
                                                       if(r.status) {
                                                           // console.log("CLIENTS PAGE",r.data);
                                                           dispatch(modulesActions.setAll({data: r.data , total_count: r.total_count}));
                                                           setUIList([...uiList,...r.data]);
                                                           setPaginationTableLoading(false);
                                                       }
                                                   });
                                               } else {
                                               }
                                           }
                                       },
                                       showTotal : (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
                                       showSizeChanger : false,onShowSizeChange: onShowSizeChange ,itemRender : itemRender }
                                   }
                                   style = {{overflowX : 'auto'}}
                                   columns={columns}
                                   loading={paginationTableLoading}
                                   dataSource={uiList}
                                   rowKey={record => record.id}
                                // onChange={console.log("change")}
                            />
                        </div>
                    </div>
                </div></div>
        }
    }
    //----------------------
    return (
        <div className="page-wrapper" style={pageStyle}>
            <Helmet>
                <title>Modules</title>
                <meta name="description" content="Clients Page"/>
            </Helmet>
            {/* Page Content */}
            <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                    {/*Search Filter */}
                    <div className="row filter-row">
                        <div className="col-sm-6 col-md-8">
                            <div className="col">
                                <h3 className="page-title text-white">Modules</h3>
                                <ul className="breadcrumb text-white">
                                    <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Administrator</Link></li>
                                    {/*/TODO: change to dashboard when created */}
                                    <li className="breadcrumb-item active text-white">Modules</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <div className="row filter-row">
                                <div className="col-sm-6 col-md-9">
                                    <Input  allowClear type="text"  value={searchString} placeholder="Search Modules" style={searchStyle} onChange={filterList} />
                                </div>
                                <div className="col-sm-6 col-md-3">
                                    <Button className="btn btn-primary mr-1"  style={addButtonStyle} onClick={
                                        ()=> {
                                            if(checkPermission(permissionRoutes.ClientsP,permissionTypes.addP)) {
                                                setShowForm(true);
                                            } else {
                                                toast.error("You Don't have Permission for this!");
                                            }
                                        }} ><span>Create</span></Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Search Filter */}
                </div>
                {/*/Page Header */}
                {checkPermission(permissionRoutes.ModulesP, permissionTypes.readP) ? getDataTable() :  <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                                                                                       description="You Have No Permission To Read!"/>}
            </div>
            {/* /Page Content */}
            {/* Add Client Modal */}
            {addModel()}
            {/* /Add Client Modal */}
            {/* Edit Client Modal */}
            {updateModel()}
            {/* /Edit Client Modal */}
        </div>
    );
}
export default Modules;
