import React, {useEffect, useRef, useState} from "react";
import {Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {designationAPI, membersAPI, modulesAPI, rolesAPI} from "../../../../Services/apiServices";
import {memberActions} from "../../../../Redux/Actions/MembersActions";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";
import {designationActions} from "../../../../Redux/Actions/DesignationActions";
import {modulesActions} from "../../../../Redux/Actions/ModulesActions";
import {styles} from "../../Employees/EmployeesConstants";


export default  function SelectModuleButton({parameters,fromUpdate=false})  {

    function handleChange(value) {

        let selectedModule = dbList.find((module) => module.id == value);
        // console.log(`selected ${value}`,selectedModule);
        parameters.setNewData({...parameters.newData,parent_id: value})
        if(selectedModule != null) {
            if(selectedModule.childs != null && selectedModule.childs.length !== 0) {
                setChildrenList([selectedModule.childs])
            } else {
                if(selectedModule.parent_id === 0) {
                    setChildrenList([])
                }
            }
        }

    }

    const dbList = useSelector((store)=> store.db.allModules);
    const total = useSelector((store) => store.db.totalModules);
    const [uiList,setUIList] = useState([]);
    const [usersLoading,setUsersLoading] = useState(false);
    const dispatch = useDispatch();
    const getAllList = () => {
        if(dbList.length !== total) {
            setUsersLoading(true);
            modulesAPI.getModulesAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    // console.log("getModulesAPI",r.data);
                    dispatch(modulesActions.setAll({data: r.data , total_count: r.total_count}));
                }
                setUsersLoading(false);
            });
        } else {
            setUIList(dbList);
        }


    }
    const onClickDropDown = (e) => {
    }


    useEffect(()=> {
        getAllList();
    },[])
    useEffect(
        ()=> {
            setUIList([...dbList]);

        }, [dbList]);


    const buttonStyle = {
        display: "flex",
        verticalAlign: "middle",
        border: "1px solid #d9d9d9",
        marginBottom: "10px"
    }
    const [childrenList,setChildrenList] = useState([])


    return (
        <>
            <div style={buttonStyle}>
             <span style={styles.buttonLabelStyle} >
                                    Module:</span>
                <Select
                    style={styles.selectStyle}
                    placeholder="Please select a Module"
                    bordered={false}
                    allowClear={true}
                    onClear={() => {setChildrenList([])}}
                    // defaultValue={fromUpdate ? parameters.newData.designation_id : null}
                    onChange={handleChange}
                    onClick={onClickDropDown}

                >
                    {uiList.map((data,key) => {
                        return <Select.Option key={key} value={data.id}>
                            {data.name}
                        </Select.Option>
                    })}
                </Select>
            </div>
            {childrenList.map((child,key) => {
                    return (
                        <div style={buttonStyle} key={key}>
             <span style={styles.buttonLabelStyle} >
                                 {fromUpdate ? "Update Module:" :"Module:"}</span>
                            <Select
                                style={styles.selectStyle}
                                placeholder="Please select a Module"
                                bordered={false}
                                onChange={handleChange}
                                onClick={onClickDropDown}
                            >
                                {child.map((data,key) => {
                                    return <Select.Option key={key} value={data.id}>
                                        {data.name}
                                    </Select.Option>
                                })}
                            </Select>
                        </div>
                    )
            })
            }
        </>
    )
}