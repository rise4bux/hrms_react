
import React, {useEffect, useState} from "react";
import {
    dashBoardHelmet,
    permissionRoutes,
    permissionTypes,

} from "../../../constants";
import {Helmet} from "react-helmet";
import {Button, Divider, Empty, Input, Modal, Spin, Table} from "antd";
import {checkPermission} from "../../../permissionConstants";
import {toast} from "react-toastify";
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {rolesAPI} from "../../../Services/apiServices";
import {onShowSizeChange, itemRender} from "../../paginationfunction";
import {rolesActions} from "../../../Redux/Actions/RolesActions";
import {homePageURL, loginRouteURL} from "../../../Services/routeServices";

const inputRowStyle = {
    margin: "20px 0px 10px 0px"

}
const addInputStyle = {
    display: "inline-block",
    outline: "none",
    border: "1px solid  #f0f0f0",
    color: "black",
    borderRadius: "10px",
    fontSize: "14px",
    width: "80%",
    margin: "0px 0px 0px 9px",
    padding: "8px 10px"
}

const inputLabelStyle = {
    fontWeight: "600",
    color: "black"

}
const  dialogButtonStyle = {
    border: "1px solid #f43b48",
    borderRadius: "10px",
    background: "#f43b48",
    color: "white"
}
const statusButtonsStyle = {
    display: "flex",
    justifyContent: "start",
    alignItems: "center",

}
const statusButtonsInnerSpanStyle = {
    margin: "0px 0px 0px 10px"

}
const spinnerStyle = {
    display: "flex",
    justifyContent: "center",
    marginTop: "20px"
}
const addButtonStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textTransform: "none"
    // margin: "0px 30px 0px 0px"
}
const tableDivStyle = {
    background: "black",
    padding: "10px 10px",
    borderRadius: "10px"
}
const  searchStyle = {
    margin: "0px 0px 10px 0px",
    // padding: "0px 0px 0px 10px",
    height: "35px",
    // background: "rgba(255, 255, 255,0.3)",
    border: "1px solid transparent",
    color: "white"
}
const pageStyle = {
    // background: "#232526",
    // borderRadius: "10px",
}
const actionButtonDivStyle = {
    display: "flex",
    justifyContent: "end",
    alignItems: "center"
}
const actionButtonStyles = {
    border: "none",
    outline: "none",
    background : "rgba(255, 255, 255, 0.3)",
    color: "white",
    margin: "0px 3px",
    borderRadius: "10px",
    fontSize: "14px",
    padding: "5px 10px",

}
const addDropDownStyle = {
    display: "inline-block",
    outline: "none",
    borderRadius: "10px",
    // border: "1px solid  #f0f0f0",
    fontSize: "16px",
    width: "80%",
    margin : "0px 0px 0px 7px"
}
const statusButtonStyle = {
    borderRadius:"10px",
    background: "rgba(204, 204, 204,0.5)",
    border: "none",
    outline: "none",
    padding: "5px 15px",
    fontWeight: "500",
    fontSize: '14px'
}
//--------------------------------------------------------
const Roles = () => {


    //-----AUTH CHECK-----
    let history = useHistory();
    const userStatus =  useSelector((store) => store.db.userStatus);
    // useEffect(
    //     () => {
    //         if(!userStatus) {
    //             history.push(loginRouteURL);
    //         }
    //     }, []
    // )



    const dispatch = useDispatch();
    const dbList = useSelector((store)=> store.db.allRoles);
    const total = useSelector((store)=> store.db.totalRoles);
    const [uiList,setUIList] = useState([...dbList]);
    const [newData,setNewData] = useState({id: 0, name: "", status: 1});
    const [showForm,setShowForm] = useState(false);
    const [showUpdateForm,setShowUpdateForm] = useState(false);
    const [toUpdateData,setToUpdateData] = useState({id: 0, name: "", status: 1});
    const [state,setState] = useState(false);
    const [loading,setLoading] = useState(false);
    const [paginationTableLoading,setPaginationTableLoading] = useState(false);

    //---TABLE COLUMNS DATA-------
    const columnsData = [
        {
            title: 'Name',
            dataIndex: 'name',
            render: (text, record) =>{
                return  (
                    <h2 className="table-avatar">
                        {text}
                    </h2>
                )
            },
            sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: 'Status',
            dataIndex: 'status',
            render: (text, record) => {
                return (
                    <div className="dropdown">
                        <a href="#" className="btn  btn-sm btn-rounded dropdown-toggle" style={{background: "rgba(255, 255, 255,0.3)", color: "white"}} data-toggle="dropdown"
                           aria-expanded="false">
                            <i className={record.status === 1 ? "fa fa-dot-circle-o text-success" : "fa fa-dot-circle-o text-danger"}/> {record.status === 1 ? "Active" : "Inactive"}
                        </a>
                        <div className="dropdown-menu">
                            <Button className="dropdown-item" style={statusButtonsStyle} onClick={() => {
                                if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                                    statusSelectToActive(record);

                                } else {
                                    toast.error("You Don't have Permission for this!");
                                }
                            }}><i className="fa fa-dot-circle-o text-success"/> <span
                                style={statusButtonsInnerSpanStyle}>{"Active"}</span></Button>
                            <Button className="dropdown-item" onClick={() => {
                                if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                                    statusSelectToInActive(record);
                                } else {
                                    toast.error("You Don't have Permission for this!");
                                }
                            }}><i className="fa fa-dot-circle-o text-danger"/> <span
                                style={statusButtonsInnerSpanStyle}>{"Inactive"}</span></Button>
                            {/*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-success" /> Active</a>*/}
                            {/*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-danger" /> Inactive</a>*/}
                        </div>
                    </div>
                )
            },
        },
        {
            title: 'Action',
            render: (text, record) => (
                <div style={actionButtonDivStyle}>
                    <button style={actionButtonStyles} onClick={() => {
                        if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                            // console.log("record",record);
                            setToUpdateData(record);
                            setShowUpdateForm(true);
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }

                    }}><i className="fa fa-pencil"/>
                        {/*Edit*/}
                    </button>
                    <button style={actionButtonStyles}  onClick={() => {
                        if(checkPermission(permissionRoutes.ClientsP,permissionTypes.deleteP)) {
                            onDeleteModel(record)
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }
                    }}><i className="fa fa-trash-o" /></button>
                </div>
            ),
        },
    ];
    const [columns,setColumns] = useState(columnsData)
    //------GET ALL CLIENTS API (limit : 10)
    useEffect( ()=>{
        let isMounted = true;
        if(dbList.length == 0) {
            rolesAPI.getRolesAPI(0,10).then((r) => {
                if(r.status && isMounted) {
                    dispatch(rolesActions.setAll({data: r.data , total_count: r.total_count}));
                    setUIList(r.data);
                }
            });
        } else {
            setColumns(columnsData);
        }
        return () => { isMounted = false };
    }, [state]);
    //---- SEARCH UI LIST SET----
    useEffect( ()=>{
        if(searchStatus) {
            setUIList(dbList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
        } else {
            setUIList([...dbList]);
        }
    }, [dbList]);
    //---------------------------------------------
    const statusSelectToInActive = (record) => {
        if(record.status !== 0) {
            record.status = 0;
            rolesAPI.updateRoleAPI(record).then((res) => {
                if(res.status) {
                    dispatch(rolesActions.update(record))
                }
            })
        }


    }
    const statusSelectToActive = (record) => {
        if(record.status !== 1) {
            record.status = 1;
            rolesAPI.updateRoleAPI(record).then((res) => {
                if(res.status) {
                    dispatch(rolesActions.update(record))
                }
            })
        }


    }
    //-----ADD CLIENT----
    const newName = (e) => {
        setNewData({...newData,name: e.target.value});
    }
    const newStatusSetToInActive = () => {
        setNewData({...newData,status: 0})
    }
    const newStatusSetToActive = () => {
        setNewData({...newData,status: 1})
    }
    const addTheData = async () => {
        if(newData.name == "") {
            toast.error("Enter Role Name!");
        }  else {
            setLoading(true);
            await rolesAPI.addRoleAPI(newData).then((res) => {
                if(res.status) {
                    setNewData({id: 0, name: "", status: 1});
                    setShowForm(false);
                    setState(!state);
                    dispatch(rolesActions.add(res.data))
                }
                setLoading(false);
            })

        }

    }


    const addModel = () => {
        return ( <Modal
                centered
                visible={showForm}
                destroyOnClose={true}
                confirmLoading={loading}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={async () => {await addTheData()}}
                onCancel={() => {
                    setShowForm(false);
                    setNewData({id: 0, name: "", status: 1});
                } }
            >
                <div>
                    <div><h4>Add Role</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <span style={inputLabelStyle}>Name : </span>
                        <input  style={addInputStyle}  defaultValue={newData.name} onChange={newName} type="text" />
                    </div>
                    <div style={inputRowStyle}>
                        <span style={inputLabelStyle}>Status : </span>
                        <div className="dropdown" style={addDropDownStyle}>
                            <a href="#" style={statusButtonStyle} className="btn btn-white btn-sm  dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i className={newData.status === 1 ? "fa fa-dot-circle-o text-success" : "fa fa-dot-circle-o text-danger"} /> {newData.status === 1  ? "Active" : "Inactive"} </a>
                            <div className="dropdown-menu">
                                <Button className="dropdown-item" style={statusButtonsStyle} onClick={newStatusSetToActive}><i className="fa fa-dot-circle-o text-success" /> <span style={statusButtonsInnerSpanStyle}>{"Active"}</span></Button>
                                <Button  className="dropdown-item" onClick={newStatusSetToInActive}><i className="fa fa-dot-circle-o text-danger" /> <span style={statusButtonsInnerSpanStyle}>{"Inactive"}</span></Button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        )
    }
    //----REMOVE CLIENT----
    const removeTheData = async (record) => {
        await rolesAPI.removeRoleAPI(record.id).then((res) => {
            if(res.status) {
                setState(!state);
                dispatch(rolesActions.remove(record))
                setUIList([...dbList.filter((entry,index) => entry.id != record.id)]);
            }        })
    }
    const onDeleteModel =  (entry) => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to delete this Role?',
            okText: "YES",
            cancelText: 'NO',
            onOk: async () => {await removeTheData(entry)},
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }
    //---UPDATE CLIENT-----
    const updateName = (e) => {
        setToUpdateData({...toUpdateData,name: e.target.value});
    }
    const updateStatusSetToInActive = () => {
        setToUpdateData({...toUpdateData,status: 0})
    }
    const updateStatusSetToActive = () => {
        setToUpdateData({...toUpdateData,status: 1})
    }
    const updateTheData = () => {
        setLoading(true);
        // console.log("UPDATE CLIENT!!",toUpdateClient);
        rolesAPI.updateRoleAPI(toUpdateData).then((res) => {
            if(res.status) {
                setShowUpdateForm(false);
                setLoading(false);
                setSearchString("");
                setSearchStatus(false);
                setUIList(dbList);
                setState(!state);
                dispatch(rolesActions.update(toUpdateData))
            }
        })
    }
    const updateModel = ()  => {
        return ( <Modal
                centered
                visible={showUpdateForm}
                confirmLoading={loading}
                destroyOnClose={true}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={updateTheData}
                onCancel={() => {setShowUpdateForm(false); setToUpdateData({id: 0, name: "", status: 1})}}
            >
                <div>
                    <div><h4>Update Role</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <span style={inputLabelStyle}>Name : </span>
                        <input  style={addInputStyle}  defaultValue={toUpdateData.name} onChange={updateName} type="text" />
                    </div>
                    <div style={inputRowStyle}>
                        <span style={inputLabelStyle}>Status : </span>
                        <div className="dropdown" style={addDropDownStyle}>
                            <a href="#" style={statusButtonStyle} className="btn btn-white btn-sm  dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i className={toUpdateData.status === 1 ? "fa fa-dot-circle-o text-success" : "fa fa-dot-circle-o text-danger"} /> {toUpdateData.status === 1  ? "Active" : "Inactive"} </a>
                            <div className="dropdown-menu">
                                <Button className="dropdown-item" style={statusButtonsStyle} onClick={updateStatusSetToActive} ><i className="fa fa-dot-circle-o text-success" /> <span style={statusButtonsInnerSpanStyle}>{"Active"}</span></Button>
                                <Button  className="dropdown-item" onClick={updateStatusSetToInActive}><i className="fa fa-dot-circle-o text-danger" /> <span style={statusButtonsInnerSpanStyle}>{"Inactive"}</span></Button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        )
    }
    //----SEARCH CLIENT LIST------
    const [searchStatus,setSearchStatus] = useState(false)
    const [searchString,setSearchString] = useState("");
    const filterList = (e) => {
        setSearchString(e.target.value);
        // console.log(e.target.value)
        if(e.target.value !== "") {
            setSearchStatus(true);
        } else {
            setSearchStatus(false);
        }
        if(dbList.length !== total && !paginationTableLoading  && searchString.length <= 1) {
            setPaginationTableLoading(true);
            rolesAPI.getRolesAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    dispatch(rolesActions.setAll({data: r.data , total_count: r.total_count}));
                }
                setPaginationTableLoading(false);
            })
        } else {
            setUIList(dbList.filter((i) => i.name.toLowerCase().match(e.target.value.toLowerCase())));
        }
    }
    //-------DATA TABLE-----
    const getDataTable = () => {
        if(dbList.length === 0) {
            return <div style={spinnerStyle}><Spin size="large"/></div>
        } else {
            return  <div style={tableDivStyle}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <Table className="table-striped text-white"
                                   pagination= { {total : searchStatus ? uiList.length : total,
                                       onChange: (page, pageSize) => {
                                           if(searchStatus) {
                                           } else {
                                               if(dbList.length <=  (page -1) * 10) {
                                                   setPaginationTableLoading(true);
                                                   rolesAPI.getRolesAPI(dbList.length,page * 10).then((r) => {
                                                       if(r.status) {
                                                           // console.log("CLIENTS PAGE",r.data);
                                                           dispatch(rolesActions.setAll({data: r.data , total_count: r.total_count}));
                                                           setUIList([...uiList,...r.data]);
                                                           setPaginationTableLoading(false);
                                                       }
                                                   });
                                               } else {
                                               }
                                           }
                                       },
                                       showTotal : (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
                                       showSizeChanger : false,onShowSizeChange: onShowSizeChange ,itemRender : itemRender }
                                   }
                                   style = {{overflowX : 'auto'}}
                                   columns={columns}
                                   loading={paginationTableLoading}
                                   dataSource={uiList}
                                   rowKey={record => record.id}
                                // onChange={console.log("change")}
                            />
                        </div>
                    </div>
                </div></div>
        }
    }
    //----------------------
    return (
        <div className="page-wrapper" style={pageStyle}>
            <Helmet>
                <title>Roles</title>
                <meta name="description" content="Clients Page"/>
            </Helmet>
            {/* Page Content */}
            <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                    {/*Search Filter */}
                    <div className="row filter-row">
                        <div className="col-sm-6 col-md-8">
                            <div className="col">
                                <h3 className="page-title text-white">Roles</h3>
                                <ul className="breadcrumb text-white">
                                    <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Administrator</Link></li>
                                    {/*/TODO: change to dashboard when created */}
                                    <li className="breadcrumb-item active text-white">Roles</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <div className="row filter-row">
                                <div className="col-sm-6 col-md-9">
                                    <Input  allowClear type="text"  value={searchString} placeholder="Search Roles" style={searchStyle} onChange={filterList} />
                                </div>
                                <div className="col-sm-6 col-md-3">
                                    <Button className="btn btn-primary mr-1"  style={addButtonStyle} onClick={
                                        ()=> {
                                            if(checkPermission(permissionRoutes.ClientsP,permissionTypes.addP)) {
                                                setShowForm(true);
                                            } else {
                                                toast.error("You Don't have Permission for this!");
                                            }
                                        }} ><span>Create</span></Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Search Filter */}
                </div>
                {/*/Page Header */}
                {checkPermission(permissionRoutes.RolesP, permissionTypes.readP) ? getDataTable() : <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                                                                                      description="You Have No Permission To Read!"/>}
            </div>
            {/* /Page Content */}
            {/* Add Client Modal */}
            {addModel()}
            {/* /Add Client Modal */}
            {/* Edit Client Modal */}
            {updateModel()}
            {/* /Edit Client Modal */}
        </div>
    );
}
export default Roles;
