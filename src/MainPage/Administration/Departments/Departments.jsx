
//-----CLIENT PAGE STYLES----\
import React, {useEffect, useState} from "react";
import {
    dashBoardHelmet,
    permissionRoutes,
    permissionTypes,
} from "../../../constants";
import {Helmet} from "react-helmet";
import {Button, Divider, Empty, Input, Modal, Spin, Table} from "antd";
import {checkPermission} from "../../../permissionConstants";
import {toast} from "react-toastify";
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {departmentAPI} from "../../../Services/apiServices";
import {departmentActions} from "../../../Redux/Actions/DepartmentActions";
import {onShowSizeChange, itemRender} from "../../paginationfunction";
import {homePageURL, loginRouteURL} from "../../../Services/routeServices";
import SelectStatus from "../Employees/Components/SelectStatus";
import {searchStyle, tableStyles} from "../../../global_styles";

const inputRowStyle = {
    margin: "20px 0px 10px 0px"

}

const  dialogButtonStyle = {
    border: "1px solid #f43b48",
    borderRadius: "10px",
    background: "#f43b48",
    color: "white"
}

const spinnerStyle = {
    display: "flex",
    justifyContent: "center",
    marginTop: "20px"
}
const addButtonStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textTransform: "none"
    // margin: "0px 30px 0px 0px"
}
const tableDivStyle = {
    background: "black",
    padding: "10px 10px",
    borderRadius: "10px"
}

const pageStyle = {
    // background: "#232526",
    // borderRadius: "10px",
}


//--------------------------------------------------------
const Departments = () => {




    const dispatch = useDispatch();
    const dbList = useSelector((store)=> store.db.allDepartments);
    const total = useSelector((store)=> store.db.totalDepartments);
    const [uiList,setUIList] = useState([...dbList]);
    const [newData,setNewData] = useState({id: 0, name: "", status: 1});
    const [showForm,setShowForm] = useState(false);
    const [showUpdateForm,setShowUpdateForm] = useState(false);
    const [toUpdateData,setToUpdateData] = useState({id: 0, name: "", status: 1});
    const [state,setState] = useState(false);
    const [loading,setLoading] = useState(false);
    const [paginationTableLoading,setPaginationTableLoading] = useState(false);

    //---TABLE COLUMNS DATA-------
    const columnsData = [
        {
            title: 'Name',
            dataIndex: 'name',
            render: (text, record) =>{
                return  (
                    <h2 className="table-avatar">
                        {text}
                    </h2>
                )
            },
            sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: 'Status',
            dataIndex: 'status',
            render: (text, record) => {
                return (
                    <div className="dropdown">
                        <a href="#" className="btn  btn-sm btn-rounded dropdown-toggle" style={{background: "rgba(255, 255, 255,0.3)", color: "white"}} data-toggle="dropdown"
                           aria-expanded="false">
                            <i className={record.status === 1 ? "fa fa-dot-circle-o text-success" : "fa fa-dot-circle-o text-danger"}/> {record.status === 1 ? "Active" : "Inactive"}
                        </a>
                        <div className="dropdown-menu">
                            <Button className="dropdown-item" style={tableStyles.statusButtonsStyle} onClick={() => {
                                if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                                    statusSelectToActive(record);

                                } else {
                                    toast.error("You Don't have Permission for this!");
                                }
                            }}><i className="fa fa-dot-circle-o text-success"/> <span
                                style={tableStyles.statusButtonsInnerSpanStyle}>{"Active"}</span></Button>
                            <Button className="dropdown-item" onClick={() => {
                                if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                                    statusSelectToInActive(record);
                                } else {
                                    toast.error("You Don't have Permission for this!");
                                }
                            }}><i className="fa fa-dot-circle-o text-danger"/> <span
                                style={tableStyles.statusButtonsInnerSpanStyle}>{"Inactive"}</span></Button>
                            {/*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-success" /> Active</a>*/}
                            {/*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-danger" /> Inactive</a>*/}
                        </div>
                    </div>
                )
            },
        },
        {
            title: 'Action',
            render: (text, record) => (
                <div style={tableStyles.actionButtonDivStyle}>
                    <button style={tableStyles.actionButtonStyles} onClick={() => {
                        if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                            // console.log("record",record);
                            setToUpdateData(record);
                            setShowUpdateForm(true);
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }

                    }}><i className="fa fa-pencil"/>
                        {/*Edit*/}
                    </button>
                    <button style={tableStyles.actionButtonStyles}  onClick={() => {
                        if(checkPermission(permissionRoutes.ClientsP,permissionTypes.deleteP)) {
                            onDeleteModel(record)
                        } else {
                            toast.error("You Don't have Permission for this!");
                        }
                    }}><i className="fa fa-trash-o" /></button>
                </div>
            ),
        },
    ];
    const [columns,setColumns] = useState(columnsData)
    //------GET ALL CLIENTS API (limit : 10)
    useEffect( ()=>{
        let isMounted = true;
        if(dbList.length == 0) {
            departmentAPI.getDepartmentsAPI(0,10).then((r) => {
                if(r.status && isMounted) {
                    dispatch(departmentActions.setAll({data: r.data , total_count: r.total_count}));
                    setUIList(r.data);
                }
            });
        } else {
            setColumns(columnsData);
        }
        return () => { isMounted = false };
    }, [state]);
    //---- SEARCH UI LIST SET----
    useEffect( ()=>{
        if(searchStatus) {
            setUIList(dbList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
        } else {
            setUIList([...dbList]);
        }
    }, [dbList]);
    //---------------------------------------------
    const statusSelectToInActive = (record) => {
        if(record.status !== 0) {
            record.status = 0;
            departmentAPI.updateDepartmentAPI(record).then((res) => {
                if(res.status) {
                    dispatch(departmentActions.update(record))
                }
            })
        }


    }
    const statusSelectToActive = (record) => {
        if(record.status !== 1) {
            record.status = 1;
            departmentAPI.updateDepartmentAPI(record).then((res) => {
                if(res.status) {
                    dispatch(departmentActions.update(record))
                }
            })
        }


    }
    //-----ADD CLIENT----
    const newName = (e) => {
        setNewData({...newData,name: e.target.value});
    }
    const newStatusSetToInActive = () => {
        setNewData({...newData,status: 0})
    }
    const newStatusSetToActive = () => {
        setNewData({...newData,status: 1})
    }
    const addTheData = async () => {
        if(newData.name == "") {
            toast.error("Enter Department Name!");
        }  else {
            setLoading(true);
            await departmentAPI.addDepartmentAPI(newData).then((res) => {
                if(res.status) {
                    setNewData({id: 0, name: "", status: 1});
                    setShowForm(false);
                    setState(!state);
                    dispatch(departmentActions.add(res.data))
                }
                setLoading(false);
            })

        }

    }


    const addModel = () => {
        return ( <Modal
                centered
                visible={showForm}
                destroyOnClose={true}
                confirmLoading={loading}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={async () => {await addTheData()}}
                onCancel={() => {
                    setShowForm(false);
                    setNewData({id: 0, name: "", status: 1});
                } }
            >
                <div>
                    <div><h4>Add Department</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={newData.name} onChange={newName}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                        <SelectStatus parameters={{setNewData: setNewData,newData:newData}}/>
                    </div>
                </div>
            </Modal>
        )
    }
    //----REMOVE CLIENT----
    const removeTheData = async (record) => {
        await departmentAPI.removeDepartmentAPI(record.id).then((res) => {
            if(res.status) {
                setState(!state);
                dispatch(departmentActions.remove(record))
                setUIList([...dbList.filter((entry,index) => entry.id != record.id)]);
            }        })
    }
    const onDeleteModel =  (entry) => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to delete this Department?',
            okText: "YES",
            cancelText: 'NO',
            onOk: async () => {await removeTheData(entry)},
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }
    //---UPDATE CLIENT-----
    const updateName = (e) => {
        setToUpdateData({...toUpdateData,name: e.target.value});
    }
    const updateStatusSetToInActive = () => {
        setToUpdateData({...toUpdateData,status: 0})
    }
    const updateStatusSetToActive = () => {
        setToUpdateData({...toUpdateData,status: 1})
    }
    const updateTheData = () => {
        setLoading(true);
        // console.log("UPDATE CLIENT!!",toUpdateClient);
        departmentAPI.updateDepartmentAPI(toUpdateData).then((res) => {
            if(res.status) {
                setShowUpdateForm(false);
                setLoading(false);
                setSearchString("");
                setSearchStatus(false);
                setUIList(dbList);
                setState(!state);
                dispatch(departmentActions.update(toUpdateData))
            }
        })
    }
    const updateModel = ()  => {
        return ( <Modal
                centered
                visible={showUpdateForm}
                confirmLoading={loading}
                destroyOnClose={true}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={updateTheData}
                onCancel={() => {setShowUpdateForm(false); setToUpdateData({id: 0, name: "", status: 1})}}
            >
                <div>
                    <div><h4>Update Department</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                            defaultValue={toUpdateData.name} onChange={updateName}
                        >
                        </Input>
                    </div>
                    <div style={inputRowStyle}>
                        <SelectStatus parameters={{setNewData: setToUpdateData,newData:toUpdateData}}/>
                    </div>
                </div>
            </Modal>
        )
    }
    //----SEARCH CLIENT LIST------
    const [searchStatus,setSearchStatus] = useState(false)
    const [searchString,setSearchString] = useState("");
    const filterList = (e) => {
        setSearchString(e.target.value);
        // console.log(e.target.value)
        if(e.target.value !== "") {
            setSearchStatus(true);
        } else {
            setSearchStatus(false);
        }
        if(dbList.length !== total && !paginationTableLoading  && searchString.length <= 1) {
            setPaginationTableLoading(true);
            departmentAPI.getDepartmentsAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    dispatch(departmentActions.setAll({data: r.data , total_count: r.total_count}));
                }
                setPaginationTableLoading(false);
            })
        } else {
            setUIList(dbList.filter((i) => i.name.toLowerCase().match(e.target.value.toLowerCase())));
        }
    }
    //-------DATA TABLE-----
    const getDataTable = () => {
        if(dbList.length === 0) {
            return <div style={spinnerStyle}><Spin size="large"/></div>
        } else {
            return  <div style={tableDivStyle}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <Table className="table-striped text-white"
                                   pagination= { {total : searchStatus ? uiList.length : total,
                                       onChange: (page, pageSize) => {
                                           if(searchStatus) {
                                           } else {
                                               if(dbList.length <=  (page -1) * 10) {
                                                   setPaginationTableLoading(true);
                                                   departmentAPI.getDepartmentsAPI(dbList.length,page * 10).then((r) => {
                                                       if(r.status) {
                                                           // console.log("CLIENTS PAGE",r.data);
                                                           dispatch(departmentActions.setAll({data: r.data , total_count: r.total_count}));
                                                           setUIList([...uiList,...r.data]);
                                                           setPaginationTableLoading(false);
                                                       }
                                                   });
                                               } else {
                                               }
                                           }
                                       },
                                       showTotal : (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
                                       showSizeChanger : false,onShowSizeChange: onShowSizeChange ,itemRender : itemRender }
                                   }
                                   style = {{overflowX : 'auto'}}
                                   columns={columns}
                                   loading={paginationTableLoading}
                                   dataSource={uiList}
                                   rowKey={record => record.id}
                                // onChange={console.log("change")}
                            />
                        </div>
                    </div>
                </div></div>
        }
    }
    //----------------------
    return (
        <div className="page-wrapper" style={pageStyle}>
            <Helmet>
                <title>Departments</title>
                <meta name="description" content="Clients Page"/>
            </Helmet>
            {/* Page Content */}
            <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                    {/*Search Filter */}
                    <div className="row filter-row">
                        <div className="col-sm-6 col-md-8">
                            <div className="col">
                                <h3 className="page-title text-white">Departments</h3>
                                <ul className="breadcrumb text-white">
                                    <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Administrator</Link></li>
                                    {/*/TODO: change to dashboard when created */}
                                    <li className="breadcrumb-item active text-white">Departments</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <div className="row filter-row">
                                <div className="col-sm-6 col-md-9">
                                    <Input  allowClear type="text"  value={searchString} placeholder="Search Departments" style={searchStyle} onChange={filterList} />
                                </div>
                                <div className="col-sm-6 col-md-3">
                                    <Button className="btn btn-primary mr-1"  style={addButtonStyle} onClick={
                                        ()=> {
                                            if(checkPermission(permissionRoutes.ClientsP,permissionTypes.addP)) {
                                                setShowForm(true);
                                            } else {
                                                toast.error("You Don't have Permission for this!");
                                            }
                                        }} ><span>Create</span></Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Search Filter */}
                </div>
                {/*/Page Header */}
                {checkPermission(permissionRoutes.DepartmentsP, permissionTypes.readP) ? getDataTable() :  <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                                                                                       description="You Have No Permission To Read!"/>}
            </div>
            {/* /Page Content */}
            {/* Add Client Modal */}
            {addModel()}
            {/* /Add Client Modal */}
            {/* Edit Client Modal */}
            {updateModel()}
            {/* /Edit Client Modal */}
        </div>
    );
}
export default Departments;
