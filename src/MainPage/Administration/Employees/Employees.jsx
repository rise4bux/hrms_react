//-----CLIENT PAGE STYLES----\
import React, { useEffect, useState } from "react";
import {
  dashBoardHelmet,
  permissionRoutes,
  permissionTypes,
} from "../../../constants";
import { Helmet } from "react-helmet";
import { Button, Divider, Empty, Input, Modal, Spin, Table } from "antd";
import { checkPermission } from "../../../permissionConstants";
import { toast } from "react-toastify";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  departmentAPI,
  designationAPI,
  employeesAPI,
  membersAPI,
  rolesAPI,
} from "../../../Services/apiServices";
import { departmentActions } from "../../../Redux/Actions/DepartmentActions";
import { onShowSizeChange, itemRender } from "../../paginationfunction";
import { designationActions } from "../../../Redux/Actions/DesignationActions";
import { rolesActions } from "../../../Redux/Actions/RolesActions";
import { employeesActions } from "../../../Redux/Actions/EmployeesActions";
import SelectRoleButton from "./Components/SelectRoleButton";
import SelectDepartmentButton from "./Components/SelectDepartmentButton";
import SelectDesignationButton from "./Components/SelectDesignationButton";
import SelectDateButton from "./Components/SelectDateButton";
import SelectGenderButton from "./Components/SelectGenderButton";
import { memberActions } from "../../../Redux/Actions/MembersActions";
import SelectReportingHeads from "./Components/SelectReportingHeads";
import SelectMaritialStatus from "./Components/SelectMaritialStatus";
import SelectStatus from "./Components/SelectStatus";
import moment from "moment";
import { homePageURL, loginRouteURL } from "../../../Services/routeServices";
import { searchStyle, tableStyles } from "../../../global_styles";

const dialogButtonStyle = {
  border: "1px solid #f43b48",
  borderRadius: "10px",
  background: "#f43b48",
  color: "white",
};

const spinnerStyle = {
  display: "flex",
  justifyContent: "center",
  marginTop: "20px",
};
const addButtonStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  textTransform: "none",
  // margin: "0px 30px 0px 0px"
};
const tableDivStyle = {
  background: "black",
  padding: "10px 10px",
  borderRadius: "10px",
};
const pageStyle = {
  // background: "#232526",
  // borderRadius: "10px",
};

//--------------------------------------------------------
const employeeDataObj = {
  id: 0,
  name: "",
  email: "",
  role_id: "",
  password: "",
  department_id: 0,
  designation_id: 0,
  gender: "Male",
  date_of_birth: "",
  blood_group: "",
  pancard: "",
  aadhar_card: "",
  driving_licence_no: "",
  passport_no: "",
  bio: "",
  uan_no: "",
  esic_no: "",
  company_email: "",
  date_of_joining: "",
  marital_status: "",
  report_heads: [],
  aniversary_date: "",
  status: 1,
};

const Employees = () => {
  const dispatch = useDispatch();
  const dbList = useSelector((store) => store.db.allEmployees);
  const total = useSelector((store) => store.db.totalEmployees);
  const [uiList, setUIList] = useState([...dbList]);
  const [newData, setNewData] = useState({ ...employeeDataObj });
  const [showForm, setShowForm] = useState(false);
  const [showUpdateForm, setShowUpdateForm] = useState(false);
  const [toUpdateData, setToUpdateData] = useState({ ...employeeDataObj });
  const [state, setState] = useState(false);
  const [loading, setLoading] = useState(false);
  const [paginationTableLoading, setPaginationTableLoading] = useState(false);
  const dbRolesList = useSelector((store) => store.db.allRoles);
  const totalRoles = useSelector((store) => store.db.totalRoles);
  const dbDesignationList = useSelector((store) => store.db.allDesignations);
  const totalDesignation = useSelector((store) => store.db.totalDesignations);
  const dbDepartmentsList = useSelector((store) => store.db.allDepartments);
  const totalDepartments = useSelector((store) => store.db.totalDepartments);
  const dbAllUsersList = useSelector((store) => store.db.allUsers);
  const totalUsers = useSelector((store) => store.db.totalUsers);

  //---TABLE COLUMNS DATA-------
  const columnsData = [
    {
      title: "Name",
      dataIndex: "name",
      render: (text, record) => {
        return <h2 className="table-avatar">{record.name}</h2>;
      },
      // sorter: (a, b) => a.name.length - b.name.length,
    },
    {
      title: "Email",
      dataIndex: "email",
      render: (text, record) => {
        return <h2 className="table-avatar">{record.email}</h2>;
      },
    },
    {
      title: "Roles",
      dataIndex: "roles",
      render: (text, record) => {
        return (
          <a
            href="#"
            className="btn  btn-sm btn-rounded dropdown-toggle"
            style={{ background: "rgba(255, 255, 255,0.3)", color: "white" }}
            data-toggle="dropdown"
            aria-expanded="false"
          >
            {record.roles.length !== 0 ? record.roles[0].name : "No Role"}
          </a>
        );
      },
    },
    {
      title: "Status",
      dataIndex: "status",
      render: (text, record) => {
        return (
          <a
            href="#"
            className="btn  btn-sm btn-rounded dropdown-toggle"
            style={{ background: "rgba(255, 255, 255,0.3)", color: "white" }}
            data-toggle="dropdown"
            aria-expanded="false"
          >
            <i
              className={
                record.status === 1
                  ? "fa fa-dot-circle-o text-success"
                  : "fa fa-dot-circle-o text-danger"
              }
            />{" "}
            {record.status === 1 ? "Active" : "Inactive"}
          </a>
        );
      },
    },

    {
      title: "Action",
      render: (text, record) => (
        <div style={tableStyles.actionButtonDivStyle}>
          <button
            style={tableStyles.actionButtonStyles}
            onClick={() => {
              if (
                checkPermission(
                  permissionRoutes.EmployeesP,
                  permissionTypes.updateP
                )
              ) {
                // console.log("record",record);
                setRecordToProperFormat(record);
              } else {
                toast.error("You Don't have Permission for this!");
              }
            }}
          >
            <i className="fa fa-pencil" />
            {/*Edit*/}
          </button>
          <button
            style={tableStyles.actionButtonStyles}
            onClick={() => {
              if (
                checkPermission(
                  permissionRoutes.EmployeesP,
                  permissionTypes.deleteP
                )
              ) {
                onDeleteModel(record);
              } else {
                toast.error("You Don't have Permission for this!");
              }
            }}
          >
            <i className="fa fa-trash-o" />
          </button>
        </div>
      ),
    },
  ];

  const setRecordToProperFormat = (record) => {
    let toUpdateRecord = {
      id: record.id,
      name: record.name,
      email: record.email,
      role_id:
        record.roles != null && record.roles.length !== 0
          ? record.roles[0].id
          : null,
      password: record.password != null ? record.password : "",
      department_id: record.department_id,
      designation_id: record.designation_id,
      gender:
        record.personals != null
          ? record.personals.gender === "male" ||
            record.personals.gender === "Male"
            ? "Male"
            : record.personals.gender === "female" ||
              record.personals.gender === "Female"
            ? "Female"
            : "Others"
          : null,
      date_of_birth:
        record.personals == null
          ? null
          : record.personals.date_of_birth != null
          ? moment(record.personals.date_of_birth, ["DD/MM/YYYY"]).format(
              "DD/MM/YYYY"
            ) != "Invalid date"
            ? moment(record.personals.date_of_birth, ["DD/MM/YYYY"]).format(
                "DD/MM/YYYY"
              )
            : moment(record.personals.date_of_birth, ["YYYY-DD-MM"]).format(
                "DD/MM/YYYY"
              ) != "Invalid date"
            ? moment(record.personals.date_of_birth, ["YYYY-DD-MM"]).format(
                "DD/MM/YYYY"
              )
            : null
          : null,
      blood_group:
        record.personals == null ? null : record.personals.blood_group,
      pancard: record.personals == null ? null : record.personals.pancard,
      aadhar_card:
        record.personals == null ? null : record.personals.aadhar_card,
      driving_licence_no:
        record.personals == null ? null : record.personals.driving_licence_no,
      passport_no:
        record.personals == null ? null : record.personals.passport_no,
      bio: record.personals == null ? null : record.personals.bio,
      uan_no: record.personals == null ? null : record.personals.uan_no,
      esic_no: record.personals == null ? null : record.personals.esic_no,
      company_email:
        record.personals == null ? null : record.personals.company_email,
      date_of_joining:
        record.personals == null
          ? null
          : record.personals.date_of_joining != null
          ? moment(record.personals.date_of_joining, ["DD/MM/YYYY"]).format(
              "DD/MM/YYYY"
            ) != "Invalid date"
            ? moment(record.personals.date_of_joining, ["DD/MM/YYYY"]).format(
                "DD/MM/YYYY"
              )
            : moment(record.personals.date_of_joining, ["YYYY-DD-MM"]).format(
                "DD/MM/YYYY"
              ) != "Invalid date"
            ? moment(record.personals.date_of_joining, ["YYYY-DD-MM"]).format(
                "DD/MM/YYYY"
              )
            : null
          : null,
      marital_status:
        record.personals == null ? null : record.personals.marital_status,
      report_heads: record.report_heads.map((head) =>
        head.id == null ? head : head.id
      ),
      aniversary_date:
        record.personals == null
          ? null
          : record.personals.aniversary_date != null
          ? moment(record.personals.aniversary_date, ["DD/MM/YYYY"]).format(
              "DD/MM/YYYY"
            ) != "Invalid date"
            ? moment(record.personals.aniversary_date, ["DD/MM/YYYY"]).format(
                "DD/MM/YYYY"
              )
            : moment(record.personals.aniversary_date, ["YYYY-DD-MM"]).format(
                "DD/MM/YYYY"
              ) != "Invalid date"
            ? moment(record.personals.aniversary_date, ["YYYY-DD-MM"]).format(
                "DD/MM/YYYY"
              )
            : null
          : null,
      status: record.status,
    };
    setToUpdateData({ ...toUpdateRecord });
    // console.log("setRecordToProperFormat",toUpdateRecord);
    setShowUpdateForm(true);
  };
  const [columns, setColumns] = useState(columnsData);
  //------GET ALL EMPLOYEES REQUIRED DATA API------------------
  useEffect(() => {
    let isMounted = true;
    if (dbAllUsersList.length == 0) {
      membersAPI.getAllUsersAPI(0, 10).then((r) => {
        if (r.status && isMounted) {
          dispatch(
            memberActions.setAllUsers({
              data: r.data,
              total_count: r.total_count,
            })
          );
        }
      });
    }
    if (dbRolesList.length == 0) {
      rolesAPI.getRolesAPI(0, 10).then((r) => {
        if (r.status && isMounted) {
          dispatch(
            rolesActions.setAll({ data: r.data, total_count: r.total_count })
          );
        }
      });
    }
    if (dbDesignationList.length == 0) {
      designationAPI.getDesignationsAPI(0, 10).then((r) => {
        if (r.status && isMounted) {
          dispatch(
            designationActions.setAll({
              data: r.data,
              total_count: r.total_count,
            })
          );
        }
      });
    }
    if (dbDepartmentsList.length == 0) {
      departmentAPI.getDepartmentsAPI(0, 10).then((r) => {
        if (r.status && isMounted) {
          dispatch(
            departmentActions.setAll({
              data: r.data,
              total_count: r.total_count,
            })
          );
        }
      });
    }
    if (dbList.length == 0) {
      employeesAPI.getAllEmployeesAPI(0, 10).then((r) => {
        if (r.status && isMounted) {
          dispatch(
            employeesActions.setAll({
              data: r.data,
              total_count: r.total_count,
            })
          );
          setUIList(r.data);
        }
      });
    } else {
      setColumns(columnsData);
    }
    return () => {
      isMounted = false;
    };
  }, [state]);
  //---- SEARCH UI LIST SET----
  useEffect(() => {
    if (searchStatus) {
      setUIList(
        dbList.filter((i) =>
          i.name.toLowerCase().match(searchString.toLowerCase())
        )
      );
    } else {
      setUIList([...dbList]);
    }
  }, [dbList]);
  //---------------------------------------------
  const validateData = (newData, fromUpdate = false) => {
    if (newData.name == "") {
      toast.error("Enter Employee Name!");
      return false;
    } else if (newData.email == "") {
      toast.error("Enter Employee Email!");
      return false;
    }
    //  else if (newData.password == "" && !fromUpdate) {
    //   toast.error("Enter Employee Password!");
    //   return false;
    // } 
    else if (newData.role_id == "") {
      toast.error("Select Employee Role!");
      return false;
    } else if (newData.designation_id == "") {
      toast.error("Select Employee Designation!");
      return false;
    } else if (newData.department_id == "") {
      toast.error("Select Employee Department!");
      return false;
    } 
    // else if (newData.date_of_birth == "") {
    //   toast.error("Select Employee Date-of-Birth!");
    //   return false;
    // }
     else if (newData.gender == "") {
      toast.error("Select Employee Gender!");
      return false;
    } else if (newData.report_heads.length === 0) {
      toast.error("Select Reporting Heads!" && !fromUpdate);
      return false;
    } else {
      return true;
    }
  };
  const validateForUpdateData = (data) => {
    if (data.name == "" || data.name == null) {
      toast.error("Enter Employee Name!");
      return false;
    } else if (data.email == "" || data.email == null) {
      toast.error("Enter Employee Email!");
      return false;
    } else if (data.role_id == "" || data.role_id == null) {
      toast.error("Select Employee Role!");
      return false;
    } else if (data.designation_id == "" || data.designation_id == null) {
      toast.error("Select Employee Designation!");
      return false;
    } else if (data.department_id == "" || data.department_id == null) {
      toast.error("Select Employee Department!");
      return false;
    } 
    // else if (data.date_of_birth == "" || data.date_of_birth == null) {
    //   toast.error("Select Employee Date-of-Birth!");
    //   return false;
    // }
     else if (data.gender == "" || data.gender == null) {
      toast.error("Select Employee Gender!");
      return false;
    } else {
      return true;
    }
  };
  //-----ADD EMPLOYEE----
  const addTheData = async () => {
    // console.log("add Data",newData);
    if (validateData(newData)) {
      setLoading(true);
      await employeesAPI.addEmployeeAPI(newData).then((res) => {
        if (res.status) {
          setNewData({ ...employeeDataObj });
          setShowForm(false);
          setState(!state);
          dispatch(employeesActions.add(res.data));
        }
        setLoading(false);
      });
    }
  };
  const inputDivStyle = {
    margin: "5px 10px 5px 0px",
    // display: "inline-block",
  };
  const modelBodyStyle = {
    maxHeight: "500px",
    overflowY: "auto",
  };
  //---------- inputFields--------------
  const changeName = (e) => {
    setNewData({ ...newData, name: e.target.value });
  };
  const changePassword = (e) => {
    setNewData({ ...newData, password: e.target.value });
  };
  const changeEmail = (e) => {
    setNewData({ ...newData, email: e.target.value });
  };
  const changeCompanyEmail = (e) => {
    setNewData({ ...newData, company_email: e.target.value });
  };
  const changeESICnumber = (e) => {
    setNewData({ ...newData, esic_no: e.target.value });
  };
  const changeUANnumber = (e) => {
    setNewData({ ...newData, uan_no: e.target.value });
  };
  const changeBio = (e) => {
    setNewData({ ...newData, bio: e.target.value });
  };
  const changePassportNumber = (e) => {
    setNewData({ ...newData, passport_no: e.target.value });
  };
  const changeDriverLicenceNumber = (e) => {
    setNewData({ ...newData, driving_licence_no: e.target.value });
  };
  const changeAdhaarNumber = (e) => {
    setNewData({ ...newData, aadhar_card: e.target.value });
  };
  const changePanNumber = (e) => {
    setNewData({ ...newData, pancard: e.target.value });
  };
  const changeBloodGroup = (e) => {
    setNewData({ ...newData, blood_group: e.target.value });
  };
  const addModel = () => {
    return (
      <Modal
        centered
        visible={showForm}
        zIndex={"1003"}
        destroyOnClose={true}
        confirmLoading={loading}
        okButtonProps={{
          style: dialogButtonStyle,
        }}
        cancelButtonProps={{
          style: dialogButtonStyle,
        }}
        okText={<span>Create</span>}
        cancelText={<span>Cancel</span>}
        onOk={async () => {
          await addTheData();
        }}
        onCancel={() => {
          setShowForm(false);
          setNewData({ ...employeeDataObj });
        }}
      >
        <div>
          <div style={{ textAlign: "center" }}>
            <h4>Create Employee</h4>
          </div>
          <Divider />
          <div style={modelBodyStyle}>
            {/*    NAME*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>*Name:
                  </span>
                }
                value={newData.name}
                onChange={changeName}
              ></Input>
            </div>
            {/*    EMAIL*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>*Email:</span>
                }
                value={newData.email}
                onChange={changeEmail}
              ></Input>
            </div>
            {/*    PASSWORD*/}
            {/* <div style={inputDivStyle}>
                                <Input
                                    addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Password:</span>}
                                    value={newData.password}
                                    onChange={changePassword}
                                >
                                </Input>
                            </div> */}
            {/*    SELECT ROLE*/}
            <div style={inputDivStyle}>
              <SelectRoleButton
                parameters={{ setNewData: setNewData, newData: newData }}
              />
            </div>
            {/*    DEPARTMENT*/}
            <div style={inputDivStyle}>
              <SelectDepartmentButton
                parameters={{ setNewData: setNewData, newData: newData }}
              />
            </div>
            {/*    DESIGNATION*/}
            <div style={inputDivStyle}>
              <SelectDesignationButton
                parameters={{ setNewData: setNewData, newData: newData }}
              />
            </div>
            {/*    REPORTING HEADS*/}
            <div style={inputDivStyle}>
              <SelectReportingHeads
                parameters={{ setNewData: setNewData, newData: newData }}
              />
            </div>
            {/*    DATE OF BIRTH*/}
            <div style={inputDivStyle}>
              <SelectDateButton
                parameters={{
                  type: "Date-of-Birth",
                  setNewData: setNewData,
                  newData: newData,
                }}
              />
            </div>
            {/*    GENDER*/}
            <div style={inputDivStyle}>
              <SelectGenderButton
                parameters={{ setNewData: setNewData, newData: newData }}
              />
            </div>

            {/*    BLOOD GROUP*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>Blood Group:</span>
                }
                value={newData.blood_group}
                onChange={changeBloodGroup}
              ></Input>
            </div>
            {/*    PAN NO*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>Pan Number:</span>
                }
                value={newData.pancard}
                onChange={changePanNumber}
              ></Input>
            </div>
            {/*    ADDHAR NUMBER*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Adhaar Number:
                  </span>
                }
                value={newData.aadhar_card}
                onChange={changeAdhaarNumber}
              ></Input>
            </div>
            {/*    DRIVER LISENSE NUMBER*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Driver License:
                  </span>
                }
                value={newData.driving_licence_no}
                onChange={changeDriverLicenceNumber}
              ></Input>
            </div>
            {/*    PASSPORT NUMBER*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Passport Number:
                  </span>
                }
                value={newData.passport_no}
                onChange={changePassportNumber}
              ></Input>
            </div>
            {/*    BIO*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>Bio:</span>
                }
                value={newData.bio}
                onChange={changeBio}
              ></Input>
            </div>
            {/*    UAN*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>UAN:</span>
                }
                value={newData.uan_no}
                onChange={changeUANnumber}
              ></Input>
            </div>
            {/*    ESIC NO*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>ESIC:</span>
                }
                value={newData.esic_no}
                onChange={changeESICnumber}
              ></Input>
            </div>
            {/*    COMPANY EMAIL*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Company Email:
                  </span>
                }
                value={newData.company_email}
                onChange={changeCompanyEmail}
              ></Input>
            </div>
            {/*    DATE OF JOINING*/}
            <div style={inputDivStyle}>
              <SelectDateButton
                parameters={{
                  type: "Date-of-Joining",
                  setNewData: setNewData,
                  newData: newData,
                }}
              />
            </div>
            {/*    ANIVERSARY DATE*/}
            <div style={inputDivStyle}>
              <SelectDateButton
                parameters={{
                  type: "Anniversary Date",
                  setNewData: setNewData,
                  newData: newData,
                }}
              />
            </div>
            {/*    MARITAL STATUS*/}
            <div style={inputDivStyle}>
              <SelectMaritialStatus
                parameters={{ setNewData: setNewData, newData: newData }}
              />
            </div>
            {/*    STATUS*/}
            <div style={inputDivStyle}>
              <SelectStatus
                parameters={{ setNewData: setNewData, newData: newData }}
              />
            </div>
          </div>
        </div>
      </Modal>
    );
  };
  //----REMOVE CLIENT----
  const removeTheData = async (record) => {
    await employeesAPI.removeEmployeeAPI(record.id).then((res) => {
      if (res.status) {
        setState(!state);
        dispatch(employeesActions.remove(record));
        setUIList([...dbList.filter((entry, index) => entry.id != record.id)]);
      }
    });
  };
  const onDeleteModel = (entry) => {
    // console.log("delete entry");
    Modal.confirm({
      title: "Confirm",
      centered: true,
      width: "350px",
      content: "Are you sure you want to remove this Employee?",
      okText: "YES",
      cancelText: "NO",
      onOk: async () => {
        await removeTheData(entry);
      },
      okButtonProps: { style: dialogButtonStyle },
      cancelButtonProps: { style: dialogButtonStyle },
    });
  };
  //---UPDATE Data-----
  const updateName = (e) => {
    setToUpdateData({ ...toUpdateData, name: e.target.value });
  };
  const updatePassword = (e) => {
    setToUpdateData({ ...toUpdateData, password: e.target.value });
  };
  const updateEmail = (e) => {
    setToUpdateData({ ...toUpdateData, email: e.target.value });
  };
  const updateCompanyEmail = (e) => {
    setToUpdateData({ ...toUpdateData, company_email: e.target.value });
  };
  const updateESICnumber = (e) => {
    setToUpdateData({ ...toUpdateData, esic_no: e.target.value });
  };
  const updateUANnumber = (e) => {
    setToUpdateData({ ...toUpdateData, uan_no: e.target.value });
  };
  const updateBio = (e) => {
    setToUpdateData({ ...toUpdateData, bio: e.target.value });
  };
  const updatePassportNumber = (e) => {
    setToUpdateData({ ...toUpdateData, passport_no: e.target.value });
  };
  const updateDriverLicenceNumber = (e) => {
    setToUpdateData({ ...toUpdateData, driving_licence_no: e.target.value });
  };
  const updateAdhaarNumber = (e) => {
    setToUpdateData({ ...toUpdateData, aadhar_card: e.target.value });
  };
  const updatePanNumber = (e) => {
    setToUpdateData({ ...toUpdateData, pancard: e.target.value });
  };
  const updateBloodGroup = (e) => {
    setToUpdateData({ ...toUpdateData, blood_group: e.target.value });
  };
  const updateTheData = () => {
    if (validateForUpdateData(toUpdateData)) {
      setLoading(true);
      employeesAPI.updateEmployeeAPI(toUpdateData).then((res) => {
        if (res.status) {
          // console.log(res.data);
          setShowUpdateForm(false);
          setSearchString("");
          setSearchStatus(false);
          setUIList(dbList);
          dispatch(employeesActions.update(toUpdateData));
          setState(!state);
        }
        setLoading(false);
      });
    }
  };
  const updateModel = () => {
    return (
      <Modal
        centered
        zIndex={"99999"}
        visible={showUpdateForm}
        confirmLoading={loading}
        destroyOnClose={true}
        okText={<span>Update</span>}
        cancelText={<span>Cancel</span>}
        okButtonProps={{
          style: dialogButtonStyle,
        }}
        cancelButtonProps={{
          style: dialogButtonStyle,
        }}
        onOk={async () => {
          await updateTheData();
        }}
        onCancel={() => {
          setShowUpdateForm(false);
          setToUpdateData({ ...employeeDataObj });
        }}
      >
        <div>
          <div style={{ textAlign: "center" }}>
            <h4>Update Employee</h4>
          </div>
          <Divider />
          <div style={modelBodyStyle}>
            {/*    NAME*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Name
                    {/*<sup>*</sup>*/}:
                  </span>
                }
                value={toUpdateData.name}
                onChange={updateName}
              ></Input>
            </div>
            {/*    EMAIL*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>Email:</span>
                }
                value={toUpdateData.email}
                onChange={updateEmail}
              ></Input>
            </div>
            {/*    PASSWORD*/}
            {/* <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>Password:</span>
                }
                value={toUpdateData.password}
                onChange={updatePassword}
              ></Input>
            </div> */}
            {/*    SELECT ROLE*/}
            <div style={inputDivStyle}>
              <SelectRoleButton
                fromUpdate={true}
                parameters={{
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
            {/*    DEPARTMENT*/}
            <div style={inputDivStyle}>
              <SelectDepartmentButton
                fromUpdate={true}
                parameters={{
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
            {/*    DESIGNATION*/}
            <div style={inputDivStyle}>
              <SelectDesignationButton
                fromUpdate={true}
                parameters={{
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
                {/*    REPORTING HEADS*/}
                <div style={inputDivStyle}>
              <SelectReportingHeads
                fromUpdate={true}
                parameters={{
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
            {/*    DATE OF BIRTH*/}
            <div style={inputDivStyle}>
              <SelectDateButton
                fromUpdate={true}
                parameters={{
                  type: "Date-of-Birth",
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
            {/*    GENDER*/}
            <div style={inputDivStyle}>
              <SelectGenderButton
                fromUpdate={true}
                parameters={{
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
        
            {/*    BLOOD GROUP*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>Blood Group:</span>
                }
                value={toUpdateData.blood_group}
                onChange={updateBloodGroup}
              ></Input>
            </div>
            {/*    PAN NO*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>Pan Number:</span>
                }
                value={toUpdateData.pancard}
                onChange={updatePanNumber}
              ></Input>
            </div>
            {/*    ADDHAR NUMBER*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Adhaar Number:
                  </span>
                }
                value={toUpdateData.aadhar_card}
                onChange={updateAdhaarNumber}
              ></Input>
            </div>
            {/*    DRIVER LISENSE NUMBER*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Driver License:
                  </span>
                }
                value={toUpdateData.driving_licence_no}
                onChange={updateDriverLicenceNumber}
              ></Input>
            </div>
            {/*    PASSPORT NUMBER*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Passport Number:
                  </span>
                }
                value={toUpdateData.passport_no}
                onChange={updatePassportNumber}
              ></Input>
            </div>
            {/*    BIO*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>Bio:</span>
                }
                value={toUpdateData.bio}
                onChange={updateBio}
              ></Input>
            </div>
            {/*    UAN*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>UAN:</span>
                }
                value={toUpdateData.uan_no}
                onChange={updateUANnumber}
              ></Input>
            </div>
            {/*    ESIC NO*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>ESIC:</span>
                }
                value={toUpdateData.esic_no}
                onChange={updateESICnumber}
              ></Input>
            </div>
            {/*    COMPANY EMAIL*/}
            <div style={inputDivStyle}>
              <Input
                addonBefore={
                  <span style={{ verticalAlign: "middle" }}>
                    Company Email:
                  </span>
                }
                value={toUpdateData.company_email}
                onChange={updateCompanyEmail}
              ></Input>
            </div>
            {/*    DATE OF JOINING*/}
            <div style={inputDivStyle}>
              <SelectDateButton
                fromUpdate={true}
                parameters={{
                  type: "Date-of-Joining",
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
            {/*    ANIVERSARY DATE*/}
            <div style={inputDivStyle}>
              <SelectDateButton
                fromUpdate={true}
                parameters={{
                  type: "Anniversary Date",
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
            {/*    MARITAL STATUS*/}
            <div style={inputDivStyle}>
              <SelectMaritialStatus
                fromUpdate={true}
                parameters={{
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
            {/*    STATUS*/}
            <div style={inputDivStyle}>
              <SelectStatus
                fromUpdate={true}
                parameters={{
                  setNewData: setToUpdateData,
                  newData: toUpdateData,
                }}
              />
            </div>
          </div>
        </div>
      </Modal>
    );
  };
  //----SEARCH CLIENT LIST------
  const [searchStatus, setSearchStatus] = useState(false);
  const [searchString, setSearchString] = useState("");
  const filterList = (e) => {
    setSearchString(e.target.value);
    // console.log(e.target.value)
    if (e.target.value !== "") {
      setSearchStatus(true);
    } else {
      setSearchStatus(false);
    }
    if (
      dbList.length !== total &&
      !paginationTableLoading &&
      searchString.length <= 1
    ) {
      setPaginationTableLoading(true);
      employeesAPI.getAllEmployeesAPI(dbList.length, total).then((r) => {
        if (r.status) {
          dispatch(
            employeesActions.setAll({
              data: r.data,
              total_count: r.total_count,
            })
          );
        }
        setPaginationTableLoading(false);
      });
    } else {
      setUIList(
        dbList.filter((i) =>
          i.name.toLowerCase().match(e.target.value.toLowerCase())
        )
      );
    }
  };
  //-------DATA TABLE-----
  const getDataTable = () => {
    if (dbList.length === 0) {
      return (
        <div style={spinnerStyle}>
          <Spin size="large" />
        </div>
      );
    } else {
      return (
        <div style={tableDivStyle}>
          <div className="row">
            <div className="col-md-12">
              <div className="table-responsive">
                <Table
                  className="table-striped text-white"
                  pagination={{
                    total: searchStatus ? uiList.length : total,
                    onChange: (page, pageSize) => {
                      if (searchStatus) {
                      } else {
                        if (dbList.length <= (page - 1) * 10) {
                          setPaginationTableLoading(true);
                          employeesAPI
                            .getAllEmployeesAPI(dbList.length, page * 10)
                            .then((r) => {
                              if (r.status) {
                                dispatch(
                                  employeesActions.setAll({
                                    data: r.data,
                                    total_count: r.total_count,
                                  })
                                );
                                setUIList([...uiList, ...r.data]);
                                setPaginationTableLoading(false);
                              }
                            });
                        } else {
                        }
                      }
                    },
                    showTotal: (total, range) =>
                      `Showing ${range[0]} to ${range[1]} of ${total} entries`,
                    showSizeChanger: false,
                    onShowSizeChange: onShowSizeChange,
                    itemRender: itemRender,
                  }}
                  style={{ overflowX: "auto" }}
                  columns={columns}
                  loading={paginationTableLoading}
                  dataSource={uiList}
                  rowKey={(record) => record.id}
                  // onChange={console.log("change")}
                />
              </div>
            </div>
          </div>
        </div>
      );
    }
  };
  //----------------------
  return (
    <div className="page-wrapper" style={pageStyle}>
      <Helmet>
        <title>Employees</title>
        <meta name="description" content="Employees Page" />
      </Helmet>
      {/* Page Content */}
      <div className="content container-fluid">
        {/* Page Header */}
        <div className="page-header">
          {/*Search Filter */}
          <div className="row filter-row">
            <div className="col-sm-6 col-md-8">
              <div className="col">
                <h3 className="page-title text-white">Employees</h3>
                <ul className="breadcrumb text-white">
                  <li className="breadcrumb-item text-white">
                    <Link className="text-white" to={homePageURL}>
                      Administrator
                    </Link>
                  </li>
                  {/*/TODO: change to dashboard when created */}
                  <li className="breadcrumb-item active text-white">
                    Employees
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-sm-6 col-md-4">
              <div className="row filter-row">
                <div className="col-sm-6 col-md-9">
                  <Input
                    allowClear
                    type="text"
                    value={searchString}
                    placeholder="Search Employees"
                    style={searchStyle}
                    onChange={filterList}
                  />
                </div>
                <div className="col-sm-6 col-md-3">
                  <Button
                    className="btn btn-primary mr-1"
                    style={addButtonStyle}
                    onClick={() => {
                      if (
                        checkPermission(
                          permissionRoutes.EmployeesP,
                          permissionTypes.addP
                        )
                      ) {
                        setShowForm(true);
                      } else {
                        toast.error("You Don't have Permission for this!");
                      }
                    }}
                  >
                    <span>Create</span>
                  </Button>
                </div>
              </div>
            </div>
          </div>
          {/* Search Filter */}
        </div>
        {/*/Page Header */}
        {checkPermission(permissionRoutes.EmployeesP, permissionTypes.readP) ? (
          getDataTable()
        ) : (
          <Empty
            style={{ marginTop: "30px" }}
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description="You Have No Permission To Read!"
          />
        )}
      </div>
      {/* /Page Content */}
      {/* Add Client Modal */}
      {addModel()}
      {/* /Add Client Modal */}
      {/* Edit Client Modal */}
      {updateModel()}
      {/* /Edit Client Modal */}
    </div>
  );
};
export default Employees;
