import React, {useEffect, useRef, useState} from "react";
import {DatePicker, Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {membersAPI, rolesAPI} from "../../../../Services/apiServices";
import {memberActions} from "../../../../Redux/Actions/MembersActions";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";
import moment from "moment";




export default  function SelectDateButton({parameters,fromUpdate=false})  {

    function handleChange(value) {
        // console.log(`selected ${value}`,moment(value).format("DD/MM/YYYY"));
        if(parameters.type ===  "Date-of-Birth") {
            if(fromUpdate) {
                parameters.setNewData({...parameters.newData,date_of_birth:moment(value).format("DD/MM/YYYY") })

            }
            else {
                parameters.setNewData({...parameters.newData,date_of_birth:moment(value).format("DD/MM/YYYY") })

            }
        } else if(parameters.type === "Date-of-Joining") {
            parameters.setNewData({...parameters.newData,date_of_joining:moment(value).format("DD/MM/YYYY") })
        }  else if(parameters.type === "Anniversary Date") {
            parameters.setNewData({...parameters.newData,aniversary_date:moment(value).format("DD/MM/YYYY") })
        }
    }

    const onClickDropDown = (e) => {
    }
    useEffect(()=> {

    },[])

    const getDefaultData = () => {
        if(fromUpdate) {
            if(parameters.type ===  "Date-of-Birth") {
                if(parameters.newData.date_of_birth != null) {
                    return moment(parameters.newData.date_of_birth,"DD/MM/YYYY")
                } else {
                    return null;
                }
            } else if(parameters.type === "Date-of-Joining") {
                if(parameters.newData.date_of_joining != null) {
                    return moment(parameters.newData.date_of_joining,"DD/MM/YYYY")
                } else {
                    return null;
                }
            }  else if(parameters.type === "Anniversary Date") {
                // console.log(parameters.newData.aniversary_date);
                if(parameters.newData.aniversary_date != null) {
                    return moment(parameters.newData.aniversary_date,"DD/MM/YYYY")
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
    }

    const buttonStyle = {
        display: "flex",
        verticalAlign: "middle",
        border: "1px solid #d9d9d9"
    }
    const buttonLabelStyle = {
        verticalAlign: "middle",
        background: "#fafafa",
        borderRight: "1px solid #d9d9d9",
        padding: "5px 10px",
        color: "black"
    }
    const selectStyle = {
        border: "none",
        outline: "none",
        flexGrow: "1"
    }
    return (
        <div style={buttonStyle}>
             <span style={buttonLabelStyle} >
                                    {parameters.type}:</span>
            <DatePicker  style={selectStyle} placeholder={"Select " + parameters.type} onChange={handleChange}

                         defaultValue={getDefaultData()} format={"DD/MM/YYYY"}
            />
        </div>


    )
}