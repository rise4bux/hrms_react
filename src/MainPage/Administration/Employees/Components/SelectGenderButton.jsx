import React, {useEffect, useRef, useState} from "react";
import {DatePicker, Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {membersAPI, rolesAPI} from "../../../../Services/apiServices";
import {memberActions} from "../../../../Redux/Actions/MembersActions";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";
import moment from "moment";
import {styles} from "../EmployeesConstants";


export default  function SelectGenderButton({parameters,fromUpdate=false})  {

    function handleChange(value) {
        // console.log(`selected ${value}`);
        parameters.setNewData({...parameters.newData,gender:value })
    }

    const onClickDropDown = (e) => {
    }
    useEffect(()=> {

    },[])



    const genderList = [
        "Male",
        "Female",
        "Other"
    ]
    return (
        <div style={styles.buttonStyle}>
             <span style={styles.buttonLabelStyle} >
                                    *Gender:</span>
            <Select
                style={styles.selectStyle}
                placeholder="Please select a gender"
                bordered={false}
                onChange={handleChange}
                defaultValue={fromUpdate ? parameters.newData.gender : null}
                onClick={onClickDropDown}

            >
                {genderList.map((data,key) => {
                    return <Select.Option key={key} value={data}>
                        {data}
                    </Select.Option>
                })}
            </Select>
        </div>


    )
}