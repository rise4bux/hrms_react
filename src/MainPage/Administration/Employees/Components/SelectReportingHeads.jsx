import React, {useEffect, useRef, useState} from "react";
import {Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {designationAPI, membersAPI, rolesAPI} from "../../../../Services/apiServices";
import {memberActions} from "../../../../Redux/Actions/MembersActions";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";
import {designationActions} from "../../../../Redux/Actions/DesignationActions";

const options = [];
for (let i = 0; i < 100000; i++) {
    const value = `${i.toString(36)}${i}`;
    options.push({
        value,
        disabled: i === 10,
    });
}


export default  function SelectReportingHeads({parameters,fromUpdate=false})  {

    function handleChange(value) {
        // console.log(`selected ${value}`,value.slice(","));
        parameters.setNewData({...parameters.newData,report_heads : value.slice(",") })
    }
    const dbList = useSelector((store)=> store.db.allUsers);
    const total = useSelector((store) => store.db.totalUsers);
    const [uiList,setUIList] = useState([]);
    const [usersLoading,setUsersLoading] = useState(false);
    const dispatch = useDispatch();
    const getAllList = () => {
        // console.log("getAllUsersAPI",dbList.length,total);
        if(dbList.length != total) {
            setUsersLoading(true);
            membersAPI.getAllUsersAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    // console.log("getAllUsersAPI",r.data);
                    dispatch(memberActions.setAllUsers({data: r.data , total_count: r.total_count}));
                }
                setUsersLoading(false);
            });
        }


    }
    const onClickDropDown = (e) => {
    }
    useEffect(()=> {
        getAllList();
    },[])
    useEffect(
        ()=> {
            setUIList([...dbList]);
        }, [dbList]);

    const buttonStyle = {
        display: "flex",
        verticalAlign: "middle",
        border: "1px solid #d9d9d9"
    }
    const buttonLabelStyle = {
        verticalAlign: "middle",
        background: "#fafafa",
        borderRight: "1px solid #d9d9d9",
        padding: "5px 10px",
        color: "black"
    }
    const selectStyle = {
        border: "none",
        outline: "none",
        flexGrow: "1"
    }
    return (
        <div style={buttonStyle}>
             <span style={buttonLabelStyle} >
             {fromUpdate ? "" :"*"}Reporting Heads:</span>
            <Select
                showSearch
                style={selectStyle}
                placeholder="Please select a Members"
                bordered={false}
                mode="multiple"
                onChange={handleChange}
                defaultValue={fromUpdate ? parameters.newData.report_heads : []}
                onClick={onClickDropDown}
                optionFilterProp="children"
            >
                {uiList.map((data,key) => {
                    return <Select.Option key={key} value={data.id}>
                        {data.name}
                    </Select.Option>
                })}
            </Select>
        </div>


    )
}