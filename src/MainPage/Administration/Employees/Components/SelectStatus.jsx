import React, {useEffect, useRef, useState} from "react";
import {DatePicker, Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {membersAPI, rolesAPI} from "../../../../Services/apiServices";
import {memberActions} from "../../../../Redux/Actions/MembersActions";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";
import {styles} from "../EmployeesConstants";


export default  function SelectStatus({parameters,fromUpdate=false,forProject = false})  {

    function handleChange(value) {
        // console.log(`selected ${value}`);
        parameters.setNewData({...parameters.newData,status:value })
    }

    const onClickDropDown = (e) => {
    }
    // useEffect(()=> {
    //     console.log("parameters",parameters);
    // },[parameters])


    const list = [
        {
            name:forProject ? "On Going" : "Active" ,
            value: 1
        },
        {
            name: forProject ? "Completed" : "Inactive",
            value: 0
        },

    ]
    return (
        <div style={styles.buttonStyle}>
             <span style={styles.buttonLabelStyle} >
                                    Status:</span>
            <Select
                style={styles.selectStyle}
                placeholder="Please select a Status"
                bordered={false}
                defaultValue={parameters.newData.status}
                onChange={handleChange}
                onClick={onClickDropDown}

            >
                {list.map((data,key) => {
                    return <Select.Option key={key} value={data.value}>
                        {data.name}
                    </Select.Option>
                })}
            </Select>
        </div>


    )
}