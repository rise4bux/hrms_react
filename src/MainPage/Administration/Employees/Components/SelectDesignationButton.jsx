import React, {useEffect, useRef, useState} from "react";
import {Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {designationAPI, membersAPI, rolesAPI} from "../../../../Services/apiServices";
import {memberActions} from "../../../../Redux/Actions/MembersActions";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";
import {designationActions} from "../../../../Redux/Actions/DesignationActions";
import {styles} from "../EmployeesConstants";

const options = [];
for (let i = 0; i < 100000; i++) {
    const value = `${i.toString(36)}${i}`;
    options.push({
        value,
        disabled: i === 10,
    });
}


export default  function SelectDesignationButton({parameters,fromUpdate=false})  {

    function handleChange(value) {
        // console.log(`selected ${value}`);
        parameters.setNewData({...parameters.newData,designation_id: value})
    }
    const dbList = useSelector((store)=> store.db.allDesignations);
    const total = useSelector((store) => store.db.totalDesignations);
    const [uiList,setUIList] = useState([]);
    const [usersLoading,setUsersLoading] = useState(false);
    const dispatch = useDispatch();
    const getAllList = () => {
        if(dbList.length !== total) {
            setUsersLoading(true);
            designationAPI.getDesignationsAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    // console.log("getDesingaiton",r.data);
                    dispatch(designationActions.setAll({data: r.data , total_count: r.total_count}));
                }
                setUsersLoading(false);
            });
        } else {
            setUIList(dbList);
        }


    }
    const onClickDropDown = (e) => {
    }
    useEffect(()=> {
        getAllList();
    },[])
    useEffect(
        ()=> {
            setUIList([...dbList]);
        }, [dbList]);


    return (
        <div style={styles.buttonStyle}>
             <span style={styles.buttonLabelStyle} >
                                    *Designation:</span>
            <Select
                style={styles.selectStyle}
                placeholder="Please select a Designation"
                bordered={false}
                defaultValue={fromUpdate ? parameters.newData.designation_id : null}
                onChange={handleChange}
                onClick={onClickDropDown}

            >
                {uiList.map((data,key) => {
                    return <Select.Option key={key} value={data.id}>
                        {data.name}
                    </Select.Option>
                })}
            </Select>
        </div>


    )
}