import React, {useEffect, useRef, useState} from "react";
import {Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {membersAPI, rolesAPI} from "../../../../Services/apiServices";
import {memberActions} from "../../../../Redux/Actions/MembersActions";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";
import {styles} from "../EmployeesConstants";




export default  function SelectRoleButton({parameters,fromUpdate=false})  {

    function handleChange(value) {
        // console.log(`selected ${value}`);
        parameters.setNewData({...parameters.newData,role_id: value})
    }
    const dbList = useSelector((store)=> store.db.allRoles);
    const total = useSelector((store) => store.db.totalRoles);
    const [uiList,setUIList] = useState([]);
    const [usersLoading,setUsersLoading] = useState(false);
    const dispatch = useDispatch();
    const getAllList = () => {
        if(dbList.length !== total) {
            setUsersLoading(true);
            rolesAPI.getRolesAPI(dbList.length,total).then((r) => {
                if(r.status) {
                    // console.log("getRolesAPI",r.data);
                    dispatch(rolesActions.setAll({data: r.data , total_count: r.total_count}));
                }
                setUsersLoading(false);
            });
        } else {
            setUIList(dbList);
        }


    }
    const onClickDropDown = (e) => {
    }
    useEffect(()=> {
        getAllList();
    },[])
    useEffect(
        ()=> {
            setUIList([...dbList]);
        }, [dbList]);


    return (
        <div style={styles.buttonStyle}>
             <span style={styles.buttonLabelStyle} >
                                    *Role:</span>
            <Select
                style={styles.selectStyle}
                placeholder="Please select a Role"
                bordered={false}
                value={parameters.newData.role_id}
                onChange={handleChange}
                onClick={onClickDropDown}

            >
                {uiList.map((data,key) => {
                    return <Select.Option key={key} value={data.id}>
                        {data.name}
                        {/*{data.id}*/}
                    </Select.Option>
                })}
            </Select>
        </div>


    )
}