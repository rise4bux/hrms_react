import React, {useEffect, useRef, useState} from "react";
import {DatePicker, Input, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {membersAPI, rolesAPI} from "../../../../Services/apiServices";
import {memberActions} from "../../../../Redux/Actions/MembersActions";
import {rolesActions} from "../../../../Redux/Actions/RolesActions";


export default  function SelectMaritialStatus({parameters,fromUpdate=false})  {

    function handleChange(value) {
        // console.log(`selected ${value}`);
        parameters.setNewData({...parameters.newData,marital_status:value })
    }

    const onClickDropDown = (e) => {
    }
    useEffect(()=> {

    },[])


    const buttonStyle = {
        display: "flex",
        verticalAlign: "middle",
        border: "1px solid #d9d9d9"
    }
    const buttonLabelStyle = {
        verticalAlign: "middle",
        background: "#fafafa",
        borderRight: "1px solid #d9d9d9",
        padding: "5px 10px",
        color: "black"

    }
    const selectStyle = {
        border: "none",
        outline: "none",
        flexGrow: "1"
    }
    const list = [
        {
            name: "Married",
            value: 1
        },
        {
            name: "Un-Married",
            value: 0
        },

    ]
    return (
        <div style={buttonStyle}>
             <span style={buttonLabelStyle} >
                                    Marital Status:</span>
            <Select
                style={selectStyle}
                placeholder="Please select a Marital Status"
                bordered={false}
                onChange={handleChange}
                defaultValue={fromUpdate && parameters.newData.marital_status != null ? parseInt(parameters.newData.marital_status) : null}
                onClick={onClickDropDown}

            >
                {list.map((data,key) => {
                    return <Select.Option key={key} value={data.value}>
                        {data.name}
                    </Select.Option>
                })}
            </Select>
        </div>


    )
}