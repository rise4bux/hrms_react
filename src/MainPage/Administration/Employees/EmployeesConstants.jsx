
const buttonStyle = {
    display: "flex",
    verticalAlign: "middle",
    border: "1px solid #d9d9d9"
}
const buttonLabelStyle = {
    verticalAlign: "middle",
    background: "#fafafa",
    borderRight: "1px solid #d9d9d9",
    padding: "5px 10px",
    color: "black"
}
const selectStyle = {
    border: "none",
    outline: "none",
    flexGrow: "1"
}



export const styles = {
    buttonStyle,buttonLabelStyle,selectStyle
};