import React, { useState,useEffect } from 'react';
import { Helmet } from "react-helmet";
import {Link, useHistory, withRouter} from 'react-router-dom';
import {Divider, Empty, Input, Modal, Table, Tooltip} from 'antd';
import 'antd/dist/antd.css';
import {itemRender} from "../../paginationfunction"
import "../../antdstyle.css"
import {
    formatTime,
    getCurrentUserProjectIDMangerList,
    getFormattedTime,
    getTimeDifference,
    getTimeDifference2,
    permissionRoutes,
    permissionTypes,
} from "../../../constants";
import FilterComponent from "./ToggleComponents/FilterComponent";
import {reportsAPI, toggleCrudAPI} from "../../../Services/apiServices";
import moment from "moment";
import {toast} from "react-toastify";
import {checkPermission} from "../../../permissionConstants";
import TextArea from "antd/es/input/TextArea";
import {FilterDataChart} from "./ToggleComponents/FilterDataChart";
import {homePageURL, loginRouteURL} from "../../../Services/routeServices";
import {useSelector} from "react-redux";

//----DATA OBJECT----------
const defaultPayloadObj = {
    "project_ids": [],
    "tag_ids" :  [],
    "client_ids" :  [],
    "team_ids" :  [],
    "from_date" : moment(new Date()).subtract(1,"months").format('DD-MM-YYYY'),
    "to_date" : moment(new Date()).format('DD-MM-YYYY')
}
//---REPORT STYLES---
const  searchStyle = {
    margin: "0px 10px 0px 10px",
    padding: "0px 0px 0px 10px",
    height: "35px",
    background: "rgba(255, 255, 255, 0.3)",
    border: "1px solid transparent",
    width: "400px",
    color: "white",
    fontSize: "14px",
    outline: "none",
    borderRadius: "10px",
}
const searchDivStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "start",
    margin: "0px 0px 10px 0px"
}
const totalHoursDivStyle = {
    margin: "5px",
    padding: "5px 10px",
    borderRadius: "10px",
    fontSize: "18px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    background: "rgba(255, 255, 255, 0.3)",
}
const tableDivStyle = {
    background: "black",
    padding: "10px 10px",
    borderRadius: "10px"
}
const  dialogButtonStyle = {
    border: "1px solid #f43b48",
    borderRadius: "10px",
    background: "#f43b48",
    color: "white"
}
const inputRowStyle = {
    display: "flex",
    alignItems: "center",
    margin: "20px 0px 10px 0px"

}
const inputClientsLabelStyle = {
    fontWeight: "500",
    color: "black"
}

const addClientInputStyle = {
    display: "inline-block",
    outline: "none",
    border: "1px solid  #f0f0f0",
    color: "black",
    borderRadius: "10px",
    fontSize: "14px",
    width: "100%",
    background: "whitesmoke",
    margin: "0px 0px 0px 9px",
    padding: "8px 10px"
}
const textAreaStyle = {
    outline: "none",
    border: "1px solid whitesmoke",
    wordSpacing: "1px",
    whiteSpace: "normal",
    background: "whitesmoke",
    // flexGrow: "1",
    borderRadius: "10px",
    color: "black",
    fontSize: "14px",
    display: "inline-block",
    // width: "60%",
    padding: "10px 15px",
}

  function ToggleReport() {

    //-----AUTH CHECK-----
    let history = useHistory();
    const userStatus =  useSelector((store) => store.db.userStatus);
    // useEffect(
    //     () => {
    //         if(!userStatus) {
    //             history.push(loginRouteURL);
    //         }
    //     }, []
    // )

    //---------DATA---------------
    const columnsData = [
        {
            title: 'Time Entry',
            dataIndex: 'time_entry',
            // width: "30%",
            render: (text, record) =>{
                return  (
                    <Tooltip title={record.description} placement="topRight" arrowPointAtCenter  overlayStyle={{color:"black",borderRadius:"10px"}}>
                        <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "left"
                        }}>
                            <h6 style={{
                                display: "inline-block",
                                textAlign: "start",
                                textOverflow: "ellipsis",
                                whiteSpace:"nowrap",
                                overflow: "hidden",
                                maxWidth: "100px",
                                marginBottom: "0px",
                                cursor: "pointer"
                            }} className="table-avatar text-white" onClick={(e)=> {setToUpdateRecord(record);setShowEditDescription(true);}}>
                                {record.description
                                    .toString()
                                    // .substr(0,20) + "..."
                                }
                            </h6>
                            <span style={{background: record.projects.color_hex,borderRadius: "5px",padding: "2px 5px", margin: "0px 0px 0px 10px",fontSize:"12px"}}>{record.projects.name == null ? "No Project" : record.projects.name}</span>
                        </div>
                    </Tooltip>

                )
            },
            // sorter: (a, b) => a.name.length - b.name.length,
        },
        // {
        //     title: 'Projects',
        //     dataIndex: 'projects',
        //     render: (text, record) =>{
        //
        //         return  (
        //             <h6 className="table-avatar text-white">
        //                 <span style={{background: record.projects.color_hex,borderRadius: "5px",padding: "5px 8px", margin: "0px 0px 0px 10px",fontSize:"12px"}}>{record.projects.name == null ? "No Project" : record.projects.name}</span>
        //             </h6>
        //         )
        //     },
        //     // sorter: (a, b) => a.name.length - b.name.length,
        // },
        {
            title: 'Tags',
            dataIndex: 'tags',
            render: (text, record) =>{
                return  (
                    <h6 className="table-avatar text-white">
                        {record.tags != null && record.tags.length != 0 ? record.tags[0].name : "no tag"}
                    </h6>
                )
            },
            // sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: 'User',
            dataIndex: 'user',
            render: (text, record) =>{

                return  (
                    <h6 className="table-avatar text-white ">
                        {record.users != null ? record.users.name : "-"}
                    </h6>
                )
            },
            // sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: 'Billable-Duration',
            dataIndex: 'billable_duration',
            render: (text, record) =>{
                return  (
                    <h6 onClick={(e) => {checkUserCanEditBillableDuration(record)}}  className="table-avatar text-white w-100 align-content-center">
                        {record.billable_duration}
                    </h6>
                )
            },
            // sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: 'Duration',
            dataIndex: 'duration',
            render: (text, record) =>{

                return  (
                    <h6 className="table-avatar text-white">

                        {record.user_duration}
                    </h6>
                )
            },
            // sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: 'Time',
            dataIndex: 'time',
            render: (text, record) =>{

                return  (
                    <h6 className="table-avatar text-white font-12">
                        {getFormattedTime(record.user_from_time ) + " - " + getFormattedTime(record.user_to_time)}
                    </h6>
                )
            },
            // sorter: (a, b) => a.name.length - b.name.length,
        },
        {
            title: 'Date',
            dataIndex: 'date',
            render: (text, record) =>{

                return  (
                    <h6 className="table-avatar text-white">
                        {record.date}
                    </h6>
                )
            },
            // sorter: (a, b) => a.name.length - b.name.length,
        },

    ]
    const [columns,setColumns] = useState(columnsData);
    const [filterList,setFilterList] = useState([]);
    const [paginationTableLoading,setPaginationTableLoading] = useState(false);
    const [totalRecords,setTotalRecords] = useState(0);
    const [uiFilterList,setUIFilterList] = useState([]);
    const [defaultPayload,setPayload] = useState(defaultPayloadObj)
    const [totalHours,setTotalHours] = useState("00:00:00");
    const [totalBillableHours,setTotalBillableHours] = useState("00:00:00");
    const [chartData,setChartData] = useState({});
    const [currentUpdateBillableRecord,setCurrentUpdateBillableRecord] = useState("00:00:00");
    const [currentPage,changeCurrentPage] = useState(1);



    //------SEARCH FILTER------------
    const searchFilter = (payload) => {
        if(currentPage != 1) {
            changeCurrentPage(1);
        }
        setPayload(payload);
        setPaginationTableLoading(true);
        reportsAPI.getFilterResultsAPI(payload).then((res) => {
            if(res.status) {
                setFilterList(res.data);
                setTotalRecords(res.total_records);
                setTotalHours(res.total_hours);
                setTotalBillableHours(res.billable_total_hours)
                setChartData(res.chart_data);
            }
            setPaginationTableLoading(false);
        });
    }


//    -----FILTER TILE PARAMETERS------
    const filterParameters = {
    filterList: filterList,
    setFilterList: setFilterList,
    setPaginationTableLoading: setPaginationTableLoading,
    searchFilter: searchFilter,
    setTotalRecords:setTotalRecords,
        setTotalHours: setTotalHours,
        setTotalBillableHours: setTotalBillableHours,
        setChartData:setChartData,
    searchDataTable: searchDataTable
    }


//----------USE EFFECTS---------------------
useEffect(()=> {
    setUIFilterList(filterList);
},[filterList])
useEffect(() => {
    setColumns(columnsData);
},[filterList]);

//---------SEARCH------------------
const searchDataTable = (searchString) => {
    // console.log("search stireng",searchString);
    setUIFilterList(filterList.filter((i) => i.description.toLowerCase().match(searchString.toLowerCase())));

}
//---------------BILLABLE COUNTS AND UPDATES-----------------------
const changeTotalBillableDuration = (prevD,updatedD) => {
    if(totalBillableHours != "00:00:00") {
        const tDarray = formatTime(totalBillableHours).split(":");
        const prevDarray = prevD.split(":");
        const updateDarray = updatedD.split(":");
        let tdSeconds = parseInt(tDarray[0]) * 3600 +  parseInt(tDarray[1]) * 60 +  parseInt(tDarray[2]);
        let udSeconds = parseInt(updateDarray[0]) * 3600 +  parseInt(updateDarray[1]) * 60 +  parseInt(updateDarray[2]);
        let pdSeconds = parseInt(prevDarray[0]) * 3600 +  parseInt(prevDarray[1]) * 60 +  parseInt(prevDarray[2]);
        // console.log("sub",tDarray,darray,tdSeconds,dSeconds,tdSeconds- dSeconds,formatTime(tdSeconds + dSeconds));
        setTotalBillableHours(`${tdSeconds -pdSeconds + udSeconds}`)
    }
}


    const checkUserCanEditBillableDuration = (record) => {
        if(checkPermission(permissionRoutes.toggleReportP,permissionTypes.billableP)) {
            const currentUsersManagerProjectIDList = getCurrentUserProjectIDMangerList();

            if(currentUsersManagerProjectIDList.find((i) => i == record.project_id) != null){
                setToUpdateRecord(record);
                // console.log("checkUserCanEditBillableDuration",record.billable_duration);
                setCurrentUpdateBillableRecord(record.billable_duration);
                setShowBillableForm(true);
                return true;
            } else {
                return false;
            }
        } else {
            toast.error("You Don't have Permission for this!");
        }

    }
    const [toUpdateRecord,setToUpdateRecord] = useState({});
const [showBillableForm,setShowBillableForm] = useState(false);
const [updateBillableLoading,setUpdateBillableLoading] = useState(false);
    const updateBillable = () => {
        let date = moment(toUpdateRecord.billable_duration,["hh:mm:ss"]).format("hh:mm:ss")
        if(date != "Invalid date") {
            let payload = {
                id: toUpdateRecord.id,
                duration: toUpdateRecord.billable_duration
            }
            setUpdateBillableLoading(true);
            reportsAPI.updateBillableDuration(payload).then((res) => {
                if(res.status) {
                    // console.log("res",res.data);
                    let fList = [...filterList];
                     let filterIndex = fList.findIndex((i) => i.id == toUpdateRecord.id);
                     let filterData = fList[filterIndex];
                    // console.log("checkUserCanEditBillableDuration", filterData.billable_duration,toUpdateRecord.billable_duration);
                    changeTotalBillableDuration(filterData.billable_duration,toUpdateRecord.billable_duration);
                    filterData.billable_duration = toUpdateRecord.billable_duration;
                     fList[filterIndex] = filterData;
                    setFilterList([...fList]);
                    // changeBillableTotalDuration();
                }
                setUpdateBillableLoading(false);
                setShowBillableForm(false);
            });
        } else {
            toast.error("Invalid Duration entered!");
            setShowBillableForm(false);
        }
    }
    const setUpdateDuration = (e) => {
        setToUpdateRecord({...toUpdateRecord,billable_duration : e.target.value});
        // console.log(toUpdateRecord.billable_duration);
    }
    const setUpdateDes = (e) => {
        setToUpdateRecord({...toUpdateRecord,description : e.target.value});
    }

    const [showEditDescription,setShowEditDescription] = useState(false);
    const [updateDesLoading,setUpdateDesLoading] = useState(false);
    const updateDesc = async () => {
        // console.log("update des",toUpdateRecord);
        setUpdateDesLoading(true);
        await toggleCrudAPI.updateToggleDescriptionAPI(toUpdateRecord,toUpdateRecord.id).then((res) =>{
            if(res.status) {
                // console.log(res.data);
                const newFilterList = filterList
                const toChangeIndex = newFilterList.findIndex((i) => i.id == toUpdateRecord.id);
                newFilterList[toChangeIndex] = toUpdateRecord;
                setFilterList([...newFilterList]);
                // console.log("des",toChangeIndex,toUpdateRecord,newFilterList);
                setShowEditDescription(false);
            }
            setUpdateDesLoading(false);
        })
    }
    const showWholeDescription = () => {
        return ( <Modal
                centered
                visible={showEditDescription}
                destroyOnClose={true}
                confirmLoading={updateDesLoading}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={async () => {await updateDesc()}}
                onCancel={() => {setToUpdateRecord({...toUpdateRecord,billable_duration : currentUpdateBillableRecord}); setShowEditDescription(false);}}
            >
                <div>
                    <div><h4>Update Description</h4></div>
                    <Divider />
                    <TextArea placeholder={"Enter Description"} autoSize style={textAreaStyle} autoFocus={true}
                              defaultValue={toUpdateRecord.description}
                              // value={toUpdateRecord.description}
                              onChange={setUpdateDes}
                              bordered={false}/>
                </div>
            </Modal>
        )
    }
    const editBillableDuration = () => {
        return ( <Modal
                centered
                visible={showBillableForm}
                destroyOnClose={true}
                confirmLoading={updateBillableLoading}
                okButtonProps= {
                    {
                        style: dialogButtonStyle
                    }}
                cancelButtonProps ={
                    {
                        style: dialogButtonStyle
                    }}
                onOk={async () => {await updateBillable()}}
                onCancel={() => {setToUpdateRecord({...toUpdateRecord,billable_duration : currentUpdateBillableRecord}); setShowBillableForm(false);}}
            >
                <div>
                    <div><h4>Update Billable</h4></div>
                    <Divider />
                    <div style={inputRowStyle}>
                        <Input
                            addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Duration:</span>}
                            defaultValue={toUpdateRecord.billable_duration} onChange={setUpdateDuration}
                        >
                        </Input>
                    </div>
                </div>
            </Modal>
        )
    }

    return (
        <div className="page-wrapper">
            <Helmet>
                <title>Toggle Reports</title>
                <meta name="description" content="Login page"/>
            </Helmet>
            {/* Page Header */}
            <div className="page-header">
                {/*Search Filter */}
                <div className="col">
                    <h3 className="page-title text-white">Toggl Report</h3>
                    <ul className="breadcrumb text-white">
                        <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Dashboard</Link></li>
                        {/*/TODO: change to dashboard when created */}
                        <li className="breadcrumb-item active text-white">Reports</li>
                    </ul>
                </div>
                {/* Search Filter */}
            </div>
            {/*/Page Header */}
            {/* Page Content */}
            <div>
                {/*FILTER SEARCH BAR*/}
                <FilterComponent  parameters={filterParameters}/>
                {/*FILTER SEARCH BAR*/}
            </div>
            {/*SEARCH TABLE*/}
            <div style={searchDivStyle}>
                <div style={totalHoursDivStyle}>
                    <span className="text-white"><span style={{fontWeight: "500"}}>Total Hours: </span>{totalHours === "00:00:00" ? "00:00:00":  formatTime(totalHours)}</span>
                </div>
                {checkPermission(permissionRoutes.toggleReportP, permissionTypes.billableP) ?
                    <div style={totalHoursDivStyle}>
                        <span className="text-white"><span
                            style={{fontWeight: "500"}}>Total Billable Hours: </span>{totalBillableHours === "00:00:00" ? "00:00:00" : formatTime(totalBillableHours)}</span>
                    </div> : <></>}
            </div>
            {/*SEARCH TABLE*/}
            {/*CHART*/}
            <div>
                {checkPermission(permissionRoutes.toggleReportP, permissionTypes.readP) ?
                    <FilterDataChart chartData={chartData}/> :
                    <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                           description="You Have No Permission To Read!"/>}
            </div>
            {/*CHART*/}
            {/*DATA TABLE*/}
            <div>
                {checkPermission(permissionRoutes.toggleReportP, permissionTypes.readP) ?<div style={tableDivStyle}>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="table-responsive">
                                <Table className="table-striped text-white"
                                       pagination= { {total : totalRecords,current: currentPage,
                                           onChange: (page, pageSize) => {
                                               changeCurrentPage(page);
                                               if(filterList.length <=  (page -1) * 10) {
                                                   setPaginationTableLoading(true);
                                                   reportsAPI.getFilterResultsAPI(defaultPayload,0,page * 10).then((res) => {
                                                       if(res.status) {
                                                           // console.log("searchFilter api result",res.data,res.total_records);
                                                           setTotalRecords(res.total_records);
                                                           setFilterList(res.data);
                                                       }
                                                       setPaginationTableLoading(false);
                                                   });
                                               }
                                           },
                                           showTotal : (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,showSizeChanger: false,
                                           itemRender : itemRender }
                                       }
                                       style = {{overflowX : 'auto'}}
                                       columns={columns}
                                       loading={paginationTableLoading}
                                       dataSource={uiFilterList}
                                       rowKey={record => record.id}
                                />
                            </div>
                        </div>
                    </div></div> : <></>}
            </div>
            {/*DATA TABLE*/}
            {/*EDIT BILLABLE DURATION MODAL*/}
            {editBillableDuration()}
            {/*EDIT BILLABLE DURATION MODAL*/}
            {/*SHOW DESCRIPTION MODEL*/}
            {showWholeDescription()}
            {/*SHOW DESCRIPTION MODEL*/}
            </div>
    );

}


export default withRouter(ToggleReport);

