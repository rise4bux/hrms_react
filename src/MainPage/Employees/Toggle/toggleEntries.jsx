
import React, {useEffect, useRef, useState,} from 'react';
import { Helmet } from "react-helmet";
import {Link, useHistory, withRouter} from 'react-router-dom';
import 'antd/dist/antd.css';
import "../../antdstyle.css"
import {
    dashBoardHelmet,
     permissionRoutes, permissionTypes, preventALinkDefault,

} from "../../../constants";
import {toggleEntriesStyle, toggleEntryDataObj} from "./toggleConstants";
import ToggleList from "./ToggleComponents/ToggleList";
import ToggleEntryCard from "./ToggleComponents/ToggleEntryCard";
import { Empty} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {timerAPI} from "../../../Services/apiServices";
import {timerActions} from "../../../Redux/Actions/TimerActions";
import {checkPermission} from "../../../permissionConstants";
import * as workerTimers from 'worker-timers';
import {homePageURL, loginRouteURL} from "../../../Services/routeServices";

//----STYLES----
const tabStyle = {
    padding: "2px 10px",
    fontSize: "14px",
}

 function ToggleEntries() {

//-----AUTH CHECK-----
let history = useHistory();
const userStatus =  useSelector((store) => store.db.userStatus);
    // useEffect(
    //     () => {
    //         if(!userStatus) {
    //             history.push(loginRouteURL);
    //         }
    //     }, []
    // )
    //-----------------------------------
    //----CONSTANTS--------
    const tab1BodyRef = useRef();
    const tab2BodyRef = useRef();
    const tab1Ref = useRef();
    const tab2Ref = useRef();
    const timerData  = useSelector((store) => store.timer);
    const [toggleTab,setToggleTab] = useState(false);
        const dispatch = useDispatch();

     useEffect(() => {
         let isMounted = true;
         const handleActivityFalse = () => {
             // console.log("focus false")
         };

         const handleActivityTrue = () => {
             // console.log("focus true");
             // if(timerData.timerStarted) {
                 // console.log("timerStarted");
                 // timerAPI.getUserTimerStatusAPI().then((res) => {
                 //     if(isMounted && res.status) {
                 //         if(res.data.is_timer_on === 1) {
                 //             // console.log("getUserTimerStatusAPI",res)
                 //             dispatch(timerActions.timerStatusChange({
                 //                 status: true,
                 //                 timerData: res.data.timer_entry_data,
                 //                 pauseStatus : res.data.timer_entry_data.is_paused,
                 //                 pauseDuration : res.data.timer_entry_data.break_timings,
                 //             }
                 //             ))
                 //             dispatch(timerActions.timerEntryID(res.data.timer_entry_data.id))
                 //         } else {
                 //             dispatch(timerActions.timerStatusChange({status: false}))
                 //         }
                 //     }
                 // })
             // }
             return () => { isMounted = false };
         };

         window.addEventListener('focus', handleActivityTrue);
         window.addEventListener('blur', handleActivityFalse);

         return () => {
             window.removeEventListener('focus', handleActivityTrue);
             window.removeEventListener('blur', handleActivityFalse);
         };
     }, []);




    return (
        <div className="page-wrapper" style={toggleEntriesStyle.toggleEntriesBodyStyle}>
            <Helmet>
                <title>Toggl Entries</title>
                <meta name="description" content="Toggle Entries Page"/>
            </Helmet>
            {/* Page Header */}
            <div className="page-header">
                {/*Search Filter */}
                <div className="row filter-row">
                    <div className="col-sm-6 col-md-8">
                        <div className="col">
                            <h3 className="page-title text-white">Toggl Entries</h3>
                            <ul className="breadcrumb text-white">
                                <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Dashboard</Link></li>
                                {/*/TODO: change to dashboard when created */}
                                <li className="breadcrumb-item active text-white">Entries</li>
                            </ul>
                        </div>
                    </div>
                    {/*<div className="col-sm-6 col-md-4" style={{display: "flex", alignItems: "end", justifyContent: "end"}}>*/}
                    {/*    <div style={toggleEntriesStyle.toggleEntryHeaderStyle}>*/}
                    {/*        <div style={toggleEntriesStyle.tabContainerStyle}>*/}
                    {/*            <ul className="nav nav-tabs nav-tabs-solid nav-tabs-rounded">*/}
                    {/*                <li className="nav-item">*/}
                    {/*                    {toggleTab ? <a className="nav-link" ref={tab1Ref} style={tabStyle} onClick={preventALinkDefault}*/}
                    {/*                                    href="#solid-rounded-tab1">Normal Mode</a> :*/}
                    {/*                        <a className="nav-link"   ref={tab1Ref} style={tabStyle} onClick={preventALinkDefault}  href="#solid-rounded-tab1" data-toggle="tab" >Normal Mode</a>}*/}
                    {/*                </li>*/}
                    {/*                <li className="nav-item "><a className="nav-link "  ref={tab2Ref} style={tabStyle} onClick={preventALinkDefault} href="#solid-rounded-tab2" data-toggle="tab">Timer Mode</a></li>*/}
                    {/*            </ul>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
                {/* Search Filter */}
            </div>
            {/*/Page Header */}
            {/*TOGGLE BODY*/}
            <div >
                {/*<ToggleEntryCard timerMode={false}/>*/}
                <ToggleEntryCard/>
                {/*<div className="tab-content" style={{padding: "0px"}}>*/}
                {/*    <div  ref={tab1BodyRef} className="tab-pane text-white" id="solid-rounded-tab1">*/}
                {/*        /!*TOGGLE ENTRY CARD*!/*/}

                {/*    </div>*/}
                {/*    <div ref={tab2BodyRef} className="tab-pane text-white" id="solid-rounded-tab2">*/}

                {/*    </div>*/}

                {/*</div>*/}
            </div>

            {/*<Divider className="bg-white"/>*/}
            <div>
                {/*ALL TOGGLE LIST*/}
                {checkPermission(permissionRoutes.toggleEntriesP,permissionTypes.readP) ? <ToggleList/> : <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE} description="You Have No Permission To Read!"/>}

            </div>
        </div>
    );

}


export default withRouter(ToggleEntries);



































