import React, {useRef} from "react";

export default function TagIcon({color})  {

    const ref = useRef()
    const  mouseEnter = () => {
        ref.current.style.fill = "#e75480";
    }
    const  mouseLeave = () => {
        ref.current.style.fill = "white";
    }

    const  svgStyle = {
        fill: "white",
        // color,
        width: "14px"
    }

    return (
        <svg ref={ref} style={svgStyle} className="eln66rt1 css-1bfz2hu-Root-TriggerIcon eafgtu70" width="17"  height="17" viewBox="0 0 17 17" onMouseEnter={mouseEnter} onMouseLeave={mouseLeave}>
            <path
                d="M0 6.002c0 1.103.633 2.63 1.416 3.414l6.168 6.168a1.996 1.996 0 002.828.004l5.176-5.176c.78-.78.78-2.045-.004-2.828L9.416 1.416C8.634.634 7.113 0 6.002 0H1.998A1.993 1.993 0 000 1.998v4.004zM4 6a2 2 0 100-4 2 2 0 000 4z"
                fillRule="evenodd"></path>
        </svg>

    )
}