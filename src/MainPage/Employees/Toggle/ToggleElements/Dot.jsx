import React from "react";


export default function Dot({color,size= "10px",marginRight = "10px"}) {

    const styleDot = {
        background: color,
        display: "inline-block",
        width: size,
        // border: `0.5px solid ${color}`,
        marginRight: marginRight,
        borderRadius: "50%",
        height: size
    }

    return(
        <div style={styleDot}></div>
    )

}