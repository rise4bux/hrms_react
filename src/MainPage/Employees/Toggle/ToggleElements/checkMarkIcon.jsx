//todo:----change this image to asset file later--------------------
import React from "react";
import {CheckCircleFilled} from "@ant-design/icons";



export default function CheckMarkIcon({onClick}) {






    const checkMarkStyle = {
        maxHeight: "5px",
        maxWidth: "5px",
        paddingBottom: "4px",
        marginLeft: "5px"
    }
    return (
        <span onClick={onClick} style={checkMarkStyle}>
         <CheckCircleFilled style={{fontSize: "20px"}}/>
        </span>
    )
}