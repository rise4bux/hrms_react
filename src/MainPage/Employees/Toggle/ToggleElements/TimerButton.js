//todo:----change this image to asset file later--------------------
import React from "react";

import { PlayCircleFilled  ,PauseCircleFilled} from '@ant-design/icons';
import {useCountUp} from "react-countup";

export default function TimerButton({onClick,timerStarted}) {




    const dustBInStyle = {
        // height: "18px"
        // paddingBottom: "13px",
      fontSize: "35px",
        // color: "#721c24",
        color: "white",
        cursor: "pointer"
    }
    const spanStyle = {
        background: "white",
        fontSize: "20px",
        textAlign: "center",
        padding: "10px",
        borderRadius: "100%",
        color: "black",
        display: "inline-block"
    }



    return (
        <span onClick={onClick} style={dustBInStyle}>
            {/*<div  style={timerCountDownBoxStyle} ref={countUpRef} />*/}
            {timerStarted ? <div style={spanStyle}><div style={{background: "black",width: "10px" ,height: "10px"}}></div></div> : <PlayCircleFilled/>}
            {/*<img width="18px" height="18px"  onMouseEnter={(e)=> {e.target.style.cursor = "pointer"}} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAAAYElEQVRIie2WQQrAIAwEp31d+/PSZ2j/ofeiHmIEAzuQk2QnmosgNuYFyq+eleHWag51dsTF6wbOWYE5BmdeT9R09Ha8HIklllhiiSWOJf4c8rOl6QYS9k9eAq7JwUVgKgNJLj5W0H5KAAAAAElFTkSuQmCC"/>*/}
        </span>
    )
}