//todo:----change this image to asset file later--------------------
import React from "react";
import {CloseCircleFilled} from "@ant-design/icons";



export default function CancelIcon({onClick}) {






    const checkMarkStyle = {
        maxHeight: "5px",
        maxWidth: "5px",
        paddingBottom: "4px",
        marginRight: "5px"
    }
    return (
        <span onClick={onClick} style={checkMarkStyle}>
          <CloseCircleFilled  style={{fontSize: "20px"}}/>
        </span>
    )
}