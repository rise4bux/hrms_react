


//---DUMMY OBJECT----
import moment from "moment";

export const getDate = (hours) => {
    let date = new Date();
    return moment(date.setHours(date.getHours() + hours)).format("hh:mm A")

}


export var toggleEntryDataObj = {
    description: "",
    selectedProject : {
    // "id": 0,
    // "name": "",
    // "color_hex": "#f43b48"
    },
    selectedTag : {
        // "id" : 0,
        // "name" : ""
    },
    startTime: getDate(0),
    user_from_time: 0,
    user_to_time : 0,
        // "09:30 AM",
    endTime:  getDate(4),
    selectedDate:  moment(Date().toString()).format("YYYY-MM-DD")
}

export const resetToggleEntryDataObject = () => {
    const defaultToggleObject = {
        description: "",
        selectedProject : {},
        selectedTag : {},
        startTime: getDate(0),
        // "09:30 AM",
        endTime:  getDate(4),
        user_from_time: 0,
        user_to_time : 0,
        selectedDate:  moment(Date().toString()).format("YYYY-MM-DD")
    }
    toggleEntryDataObj = {...defaultToggleObject};
}








//----------------CONSTANT STYLES------------

//----------TOGGLE ENTRIES PAGE-------------------------
const toggleEntryInputDiv = (condition) => {
    return  {
        opacity:  condition ? "0.6" : "1.0",
        // filter: condition ? "alpha(opacity = 60)" : "alpha(opacity = 100)",
        background: "#232526",

            // "white",
        // background: "#F5F5F5",
        // padding: "20px 20px",
        padding: "0px 5px",
        margin: "10px 10px 20px 10px",
        borderRadius: "5px",
        // boxShadow: "0px 0px 5px 10px #F5F5F5",
        // border: "1px solid #dadada",
        // border: "1px solid #f5f5f5",
        display:"flex",
        justifyContent: "center",
        alignItems: "center"


    }
}
const toggleEntryInput = {
    outline: "none",
    border: "none",
    wordSpacing: "1px",
    whiteSpace: "normal",
    flexGrow: "1",
    color: "white",
    fontSize: "16px",
    display: "inline-block",
    width: "60%",
    padding: "10px 5px",
    margin: "5px 15px"
}
const toggleEntriesBodyStyle = {
    background: "black"
    // "white",
        // "rgb(248, 215, 218)",
        // "#F5F5F5",
}
const addEntryStyle = {
    all: "unset",
    cursor: "pointer",
    width: "40px",
    margin: "0px 0px 0px 0px"

}
const entryButtonsBoxStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
}
const tabContainerStyle = {
    display: "inline-block",
    margin: "0px 15px 0px 0px"


}
const toggleEntryHeaderStyle = {
    display: "flex",
    justifyContent: "space-between",
    alignItems:  "center",
    margin: "0px 15px"
    // background: "red"
}
const titleBlockStyle = {
    display: "inline-block",
}

const loadMoreDivStyle = {
    padding: "20px 0px",
    display: "flex",
    justifyContent: "center"
    // background: "red",

}
const loadMoreButtonStyle = {
    // display: "inherit",
    width: "150px",
    height: "40px",
    color: "white",
    background: "#DD1845",
        // "linear-gradient(to right, #f43b48 0%, #453a94 100%)",
    border: "none"
    // margin: "0px auto",
}

export const toggleEntriesStyle = {
    toggleEntriesBodyStyle,
    toggleEntryInputDiv,toggleEntryHeaderStyle,titleBlockStyle,tabContainerStyle,
    toggleEntryInput,
    addEntryStyle,
    entryButtonsBoxStyle,
    loadMoreDivStyle,
    loadMoreButtonStyle
}



//--------------TOGGLE PROJECT BUTTON PAGE---------------
const dropDownBoxStyle = {
    padding: "5px 5px",
    maxHeight: "300px",
    overflowX: "hidden",
    overflowY: "auto",
    border: "1px solid #f5f5f5",
    borderRadius: "10px",
    // width: "100%"
}
const projectButtonStyle = {
    fontSize: "12px",
}

export const projectButtonDropDownStyles = {
    dropDownBoxStyle,
    projectButtonStyle
}


//---TOGGLE TAG BUTTON PAGE STYLES------
const dropDownItemStyle  = {
    padding: "0px 10px"
}
const textStyle = {
    // color: "#721c24",
    color: "white",
    fontSize: "14px",
    marginLeft: "5px",
}
const emptySpanStyle = {
    padding: "5px 5px"
}
const setButtonStyle =  (condition) => {
   return {
        background: "transparent",
       margin: "0px 5px 0px 5px",
       border: "none",
            // border: condition ? "1px solid transparent" : "1px solid #A1A5B7",
        outline: "none",
        "> svg": {
        fill: "white"
            // "#fce57e"
    }
    }
}

export const tagButtonDropDownStyles = {
    dropDownBoxStyle,
    dropDownItemStyle,
    textStyle,
    emptySpanStyle,
    setButtonStyle
}

//------------TIME BLOCK STYLES------------
const  timePickerStyle = {
    border: "none",
    outline: "none",
    borderRadius: "10px",
    width: "70px",
    fontSize: "14px",
    fontWeight: "400",
    color: "black",
    // verticalAlign: "text-top",
    textAlign: "center",
    margin : "0px auto"
}
const dateAndTimePickerStyle = {
    // border: "0.5px solid #A1A5B7",
    borderRadius: "5px",
    width: "130px",
    padding:"2px 5px",
    background: "white",
    display: "inline-block",
    // verticalAlign: "text-top"

}
const dateAndTimePickerStyle2 = {
    // border: "0.5px solid #A1A5B7",
    borderRadius: "5px",
    padding:"2px 5px",
    background: "white",
    display: "inline-block",
    // verticalAlign: "text-top"
}
const datePickerStyle = {
    width: "40px",
    outline: "none",
    margin: "0px",
    padding: "0px",
    border: "none",
    // verticalAlign: "text-top"

}
const dividerStyle = {
    height: "2px",
    width: "0.5px",
    margin: "0px 6px 0px 1px",
    borderLeft: "0.5px solid #A1A5B7",
    // verticalAlign: "text-top"
}
const timeBlockStyle2 = {
    margin: "0px 10px 0px 0px",
    fontSize: "14px",
    width: "200px",
    paddingTop: "0px"
}
const timeBlockStyle = {
    margin: "0px 10px 0px 0px",
    fontSize: "14px",
    width: "235px",

}

export const timeBlockStyles = {timePickerStyle,dateAndTimePickerStyle,dateAndTimePickerStyle2,datePickerStyle,dividerStyle,timeBlockStyle,timeBlockStyle2}



//---TOGGLE CARD STYLES-----

const toggleHeaderStyle  = {
    margin: "0px 0px",
    borderRadius: "5px",
    background: "#DD1845",
        // "linear-gradient(to right, #f43b48 0%, #453a94 100%)",
    padding: "10px 5px",
    color: "white",
    fontSize: "16px",
    fontWeight: "500"
}
const toggleHeaderDateTimeDivStyle = {
    display: "inline-block",
    margin: "0px 0px 0px 15px",
    // fontStyle: "italic"
}
const  toggleHeaderTotalTimeStyle = {
    display: "inline-block",
    float: "right",
    margin: "0px 15px 0px 0px",

}
 const toggleCardStyle = {
    borderRadius: "5px",
     background: "rgba(255,255,255,0.25)",
        // "white",
    padding: "5px",
    // border: "1px solid #dadada",
    margin: "15px 10px"
}
const toggleTileBoxStyle = (editing,isLate) => {
    return {
        display: "flex",
        justifyContent:"space-evenly",
        alignItems: "center",
        background:
            isLate ? "rgba(120, 20, 40,0.5)" :
                "#232526",
        // "#232526",
        // background:
        // "#f8d7da",
        borderRadius: "5px",
        borderLeft: editing ? "4px solid #DD1845" : "none",
        // borderBottom:"1px solid #DD1845",
        // borderTop:"1px solid #DD1845",
        // border: "1px solid #DD1845",
        // color: "#721c24",
        color: "white",
        marginTop: "5px",
        padding: "0px 10px 0px 0px",
        cursor: "pointer"
    }
}

const toggleTileDescriptionStyle = {
    outline: "none",
    border: "none",
    // color: "#721c24",
    color: "white",
    flexGrow: "1",
    fontSize: "14px",
    display: "inline-block",
    width: "60%",
    padding: "5px 5px",
    margin: "5px 15px",
    cursor: "pointer"

}

const toggleTileButtonsBoxStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
}

export const toggleCardStyles = {
    toggleHeaderStyle,toggleHeaderDateTimeDivStyle,toggleHeaderTotalTimeStyle,toggleCardStyle,
    toggleTileBoxStyle,toggleTileDescriptionStyle,toggleTileButtonsBoxStyle
}