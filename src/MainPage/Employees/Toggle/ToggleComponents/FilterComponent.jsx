import React, {useEffect, useState} from "react";
import SearchInput from "./SearchInput";
import {Button, Checkbox, DatePicker, Empty} from "antd";
import {apiData, reportsAPI} from "../../../../Services/apiServices";
import {useDispatch, useSelector} from "react-redux";
import {reportActions} from "../../../../Redux/Actions/ReportsActions";
import {actions} from "../../../../Redux/Actions/Actions";
import IconProvider, {CloudDownloadOutlined} from "@ant-design/icons";
import {timeBlockStyles} from "../toggleConstants";
import moment from "moment";
import ArrowBox from "../ToggleElements/ArrowBox";
import {permissionRoutes, permissionTypes, saveFileAsCSV} from "../../../../constants";
import {checkPermission} from "../../../../permissionConstants";
import {toast} from "react-toastify";
import ReportPageSearchInput from "./reportPageSearchInput";

//--TOGGLE REPORT FILTER STYLES----
const filterTileStyle = {
    margin: "20px 5px",
    borderRadius: "5px",
    background: "rgba(255, 255, 255, 0.3)",
    padding: "5px 20px",

}
const filterButtonStyle = {
    outline: "none",
    border: "none",
    background: "black",
    borderRadius: "10px",
    padding: "5px 10px 5px 5px",
    margin: "5px 10px",
    textAlign: "center"

}
const countSpanStyle = {
    background: "rgba(255, 255, 255, 0.3)",
    padding: "6px 8px",
    width: "15px",
    height: "15px",
    fontSize: "12px",
    borderRadius: "7px",
    margin: "0px 6px 0px 0px "
}
const buttonTextStyle = {
    fontSize: "14px",
}
const  searchStyle = {
    margin: "0px 10px 0px 10px",
    padding: "0px 0px 0px 10px",
    height: "35px",
    background: "black",
    border: "none",
    width: "200px",
    color: "white",
    fontSize: "14px",
    outline: "none",
    borderRadius: "10px"
}
const applyButtonStyle = {
    outline: "none",
    border: "none",
    background: "black",
    borderRadius: "10px",
    padding: "5px 10px 5px 10px",
    margin: "5px 5px",
    textAlign: "center"
}
const downloadButtonStyle = {
    outline: "none",
    border: "none",
    background: "black",
    borderRadius: "10px",
    padding: "5px 10px 5px 10px",
    margin: "5px 5px",
    textAlign: "center",
    width: "80px"
}
const dropDownBoxStyle = {
    padding: "0px 0px 10px 0px",
    borderRadius: "10px",
    maxHeight: "250px",
    overflowY: "auto",
    overflowX: "auto",
    minWidth: "250px",
}
const datePickerStyle = {
    background: "black",
    color: "white",
    width: "80px",
    padding: "0px",
}
const datePickerRangeDivStyle = {
        display: "inline-block",
        background: "black",
        borderRadius: "10px",
        margin: "0px 10px",
        padding: "6px 10px"
    }
//    ---MultiselectDropDownStyle-----
const dropDownStyleCheck = (list,item) => {
    if(list.find((i) => i.id === item.id) == null) {
        return {
            padding: "5px 10px",
            background: "white",
            cursor: "pointer"
        }
    } else {
        return {
            padding: "5px 10px",
            background: "whitesmoke",
            cursor: "pointer"
        }
    }
}

export default  function FilterComponent({parameters}) {

    const userClientsList = useSelector((store) => store.db.userClients);
    const userProjectsList = useSelector((store) => store.db.userProjectsList);
    const userTeamList = useSelector((store) => store.db.userTeam);
    const dispatch = useDispatch();
    const [teamsList,setTeamsList] = useState([]);
    const [projectsList,setProjectsList] = useState([]);
    const [clientsList,setClientsList] = useState([]);
    const [tagsList,setTagsList] = useState([]);
    const [selectedProjectsList,setSelectedProjectList] = useState([]);
    const [selectedTeamsList,setSelectedTeamsList] = useState([]);
    const [selectedClientsList,setSelectedClientsList] = useState([]);
    const [selectedTagsList,setSelectedTagsList] = useState([]);
    const [searchDescriptionString,setSearchDescriptionString] = useState("");
    const dateFormat = 'DD-MM-YYYY';
    const [fromDate,setFromDate] = useState(moment(new Date()).subtract(1,"months").format(dateFormat));
    const [toDate,setToDate] = useState(moment(new Date()).format(dateFormat));

    //----------API CALLS---
    useEffect(() => {
        if(userProjectsList.length === 0) {
            apiData.getProjectsList().then((res) => {
                if(res.status) {
                    dispatch(actions.setUserProjects(res.data))
                }
            });
        }
        if(userClientsList.length === 0) {
            reportsAPI.getClientsByUserProjectsAPI().then((res) => {
                if(res.status) {
                    dispatch(reportActions.setUserClients(res.data))
                }
            });
        }
        if(userTeamList.length === 0) {
            reportsAPI.getTeamMembersAPI().then((res) => {
                if(res.status) {
                    dispatch(reportActions.setUserTeam(res.data))
                }
            });
        }



    }, []);
    //--UPDATING CLIENTS LIST==
    useEffect(() => {
        setClientsList(userClientsList);
    }, [userClientsList])
    //--UPDATING TEAMS LIST==
    useEffect(() => {
        setTeamsList(userTeamList);
    }, [userTeamList])
    useEffect(() => {
        setProjectsList(userProjectsList);
    }, [userProjectsList])


//-------------SELECT FILTERS---
const selectTeamMember = (e,item) => {
        // console.log(item,selectedTeamsList);
    if(selectedTeamsList.find((i) => i.id === item.id) == null) {
        setSelectedTeamsList([...selectedTeamsList,item])
    } else {
        let selectedList = selectedTeamsList.filter((i) => i.id != item.id);
        setSelectedTeamsList([...selectedList]);
        }
}
    const selectClients = (e,item) => {
        // console.log(item,selectedClientsList);
        if(selectedClientsList.find((i) => i.id === item.id) == null) {
            setSelectedClientsList([...selectedClientsList,item])
        } else {
            let selectedList = selectedClientsList.filter((i) => i.id != item.id);
            setSelectedClientsList([...selectedList]);
        }
    }
    const selectProjects = (e,item) => {
        // console.log(item,selectedProjectsList);
        if(selectedProjectsList.find((i) => i.id === item.id) == null) {
            setSelectedProjectList([...selectedProjectsList,item])
            setTagsList([...item.tags.filter((t) => t != null)]);
            setSelectedTagsList([]);
            setTagsBackupList([])
        } else {
            let selectedList = selectedProjectsList.filter((i) => i.id != item.id);
            setSelectedProjectList([...selectedList]);
            setTagsList([]);
            setSelectedTagsList([]);
            setTagsBackupList([]);
        }
    }
    const selectTags = (e,item) => {
        // console.log(item,selectedTagsList);
        if(selectedTagsList.find((i) => i.id === item.id) == null) {
            setSelectedTagsList([...selectedTagsList,item])
        } else {
            let selectedList = selectedTagsList.filter((i) => i.id != item.id);
            setSelectedTagsList([...selectedList]);
        }
    }
    //------SEARCH FILTER BUTTON
    const searchFilterButton = () => {
        let payload = {
            "project_ids": selectedProjectsList.map((item,index)=> item.id), // [ 56,35 ]
            "tag_ids" :  selectedTagsList.map((item,index)=> item.id), // [ 56,35 ]
            "client_ids" :  selectedClientsList.map((item,index)=> item.id), // [ 153,63 ]
            "team_ids" :  selectedTeamsList.map((item,index)=> item.id), // [ 56,63 ]
            "from_date" : fromDate,
            "to_date" : toDate,
            "search" : searchDescriptionString,
        }
        parameters.searchFilter(payload);

    }
    //---CLEAR SEARCH BUTTON
    const clearFilterButton = () => {
        setSelectedProjectList([]);
        setSelectedTagsList([]);
        setSelectedClientsList([]);
        setSelectedTeamsList([]);
        setSearchDescriptionString("");
        parameters.setFilterList([]);
        parameters.setTotalRecords(0);
        parameters.setTotalHours("00:00:00");
        parameters.setTotalBillableHours("00:00:00");
        parameters.setChartData({});
        // parameters.searchFilter({
        //     "project_ids": [],
        //     "tag_ids" :  [],
        //     "client_ids" :  [],
        //     "team_ids" :  [],
        //     "from_date" : moment(new Date()).subtract(1,"months").format('DD-MM-YYYY'),
        //     "to_date" : moment(new Date()).format('DD-MM-YYYY')
        // });
    }
    //FILTER TAGS LIST----
    const [tagsBackUpList,setTagsBackupList] = useState([]);
    const onFilterTagsList =(searchString) => {
        if(tagsBackUpList.length == 0) {
            setTagsBackupList(tagsList.filter((t) => t != null));
        }
        setTagsList(tagsBackUpList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
    }
    const onFilterProjectsList =(searchString) => {
        // console.log("list",searchString);
        setProjectsList(userProjectsList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
    }
    const onFilterClientsList =(searchString) => {
        setClientsList(userClientsList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
    }
    const onFilterTeamsList =(searchString) => {
        setTeamsList(userTeamList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
    }
    //--------------------------------
    const csvDownload = (e) => {
        // console.log("csv Download",e);
        if(checkPermission(permissionRoutes.toggleReportP, permissionTypes.csvP)) {
            let payload = {
                "project_ids": selectedProjectsList.map((item,index)=> item.id), // [ 56,35 ]
                "tag_ids" :  selectedTagsList.map((item,index)=> item.id), // [ 56,35 ]
                "client_ids" :  selectedClientsList.map((item,index)=> item.id), // [ 153,63 ]
                "team_ids" :  selectedTeamsList.map((item,index)=> item.id), // [ 56,63 ]
                "from_date" : fromDate,
                "to_date" : toDate,
            }
            reportsAPI.downloadCSVFileAPI(payload).then((res)=> {
                if(res.status) {
                    const base64String = res.data['base64_string'];
                    saveFileAsCSV(`report_${payload.from_date}_${payload.to_date}`,base64String);
                }
            });
        } else {
            toast.error("You don't have Permission for this!");
        }

    }




const checkBoxStyle = {
        // color: "red",
    margin: "0px 10px 0px 0px"
}

    $(document).on('click', '.btn-group .dropdown-menu', function (e) {
        e.stopPropagation();
    });


    return (
        <div className="text-white" style={filterTileStyle}>
            <span>Filter:</span>
            {/*TEAMS*/}
            <div className="btn-group">
                <button  className="dropdown-toggle" data-toggle="dropdown" style={filterButtonStyle}><span style={countSpanStyle}>{selectedTeamsList.length}</span><span style={buttonTextStyle}>Teams</span></button>
                <div className="dropdown-menu"   onChange={(e) => {
                    console.log("dropdown",e)}}  style={dropDownBoxStyle}>
                    <ReportPageSearchInput params={{onFilterList: onFilterTeamsList}}/>
                       {teamsList.length == 0 ?     ( <Empty key={"EMPTY"} className="dropDownEmpty2" style={{marginTop: "30px",color: "black",background: "white"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                             description="No Data" />) : teamsList.map((item,index) =>
                           (
                               //
                               <div key={index} style={dropDownStyleCheck(selectedTeamsList,item)} onClick={(e) => {e.preventDefault(); selectTeamMember(e,item)}}>
                                   <Checkbox  style={checkBoxStyle} checked={selectedTeamsList.find((i) => i.id === item.id) == null ? false : true} onClick={(e)=> {selectTeamMember(e,item)}}/>
                                   <span>{item.name}</span>
                               </div>
                               // </Checkbox>

                           )
                       )}
                </div>
            </div>
            {/*TEAMS*/}
            {/*CLIENTS*/}
            <div className="btn-group">
                <button  className="dropdown-toggle"   data-toggle="dropdown" style={filterButtonStyle}><span style={countSpanStyle}>{selectedClientsList.length}</span><span style={buttonTextStyle}>Clients</span></button>
                <div className="dropdown-menu" style={dropDownBoxStyle}>
                    <SearchInput params={{onFilterList: onFilterClientsList}}/>
                    {clientsList.length == 0 ?   ( <Empty key={"EMPTY"} className="dropDownEmpty2" style={{marginTop: "30px",color: "black",background: "white"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                          description="No Data" />) : clientsList.map((item,index) =>
                        (<div key={index} style={dropDownStyleCheck(selectedClientsList,item)} onClick={(e) => {selectClients(e,item)}}>
                            <Checkbox  style={checkBoxStyle} checked={selectedClientsList.find((i) => i.id === item.id) == null ? false : true} onClick={(e)=> {selectClients(e,item)}}/>
                            <span>{item.name}</span></div>)
                    )}
                </div>
            </div>
            {/*CLIENTS*/}
            {/*PROJECTS*/}
            <div className="btn-group">
                <button  className="dropdown-toggle"   data-toggle="dropdown" style={filterButtonStyle}><span style={countSpanStyle}>{selectedProjectsList.length}</span><span style={buttonTextStyle}>Projects</span></button>
                <div className="dropdown-menu" style={dropDownBoxStyle}>
                    <SearchInput params={{onFilterList: onFilterProjectsList}}/>
                    {projectsList.length == 0 ?    (<Empty key={"EMPTY"} className="dropDownEmpty2" style={{marginTop: "30px",color: "black",background: "white"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                           description="No Data" />) : projectsList.map((item,index) =>
                        (<div key={index}  style={dropDownStyleCheck(selectedProjectsList,item)} onClick={(e) => {selectProjects(e,item)}}>
                            <Checkbox  style={checkBoxStyle} checked={selectedProjectsList.find((i) => i.id === item.id) == null ? false : true} onClick={(e)=> {selectProjects(e,item)}}/>
                            <span>{item.name}</span></div>)
                    )}
                </div>
            </div>
            {/*PROJECTS*/}
        {/*    TAGS*/}
            <div className="btn-group">
                <button  className="dropdown-toggle"   data-toggle="dropdown" style={filterButtonStyle}><span style={countSpanStyle}>{selectedTagsList.length}</span><span style={buttonTextStyle}>Tags</span></button>
                <div className="dropdown-menu" style={dropDownBoxStyle}>
                    <SearchInput params={{onFilterList: onFilterTagsList}}/>
                    {tagsList.length == 0 ?     (<Empty key={"EMPTY"} className="dropDownEmpty2" style={{marginTop: "30px",color: "black",background: "white"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                        description="No Data" />) : tagsList.map((item,index) =>
                        (<div key={index} style={dropDownStyleCheck(selectedTagsList,item)} onClick={(e) => {selectTags(e,item)}}>
                            <Checkbox  style={checkBoxStyle} checked={selectedTagsList.find((i) => i.id === item.id) == null ? false : true} onClick={(e)=> {selectTags(e,item)}}/>
                            <span>{item.name}</span></div>)
                    )}
                </div>
            </div>
            {/*    TAGS*/}
            {/*DATE PICKER*/}
            <div style={datePickerRangeDivStyle}>
                <DatePicker inputReadOnly bordered={false}  allowClear={false} picker={"date"}
                            style={datePickerStyle} suffixIcon={null} className="customDatePicker"
                            defaultValue={moment(fromDate, dateFormat)}
                            format={dateFormat} onChange={(moment, dateString) => {
                    // console.log("selected date",moment._d, dateString);
                    setFromDate(dateString);
                }}/>
                <ArrowBox/>
                <DatePicker inputReadOnly bordered={false}  allowClear={false} picker={"date"}
                            style={datePickerStyle} suffixIcon={null} className="customDatePicker"
                            defaultValue={moment(toDate, dateFormat)}
                            format={dateFormat} onChange={(moment, dateString) => {
                    // console.log("selected date",moment._d, dateString);
                    setToDate(dateString);
                }}/>
            </div>

            {/*DATE PICKER*/}
            {/*DESCRIPTION*/}
            <input type="text"  value={searchDescriptionString} onChange={(e) => {setSearchDescriptionString(e.target.value)}} placeholder="Search Description"  style={searchStyle}  />
            {/*DESCRIPTION*/}
            <button  style={applyButtonStyle} onClick={searchFilterButton}><span style={buttonTextStyle}>Apply</span></button>
            <button  style={applyButtonStyle} onClick={clearFilterButton}><span style={buttonTextStyle}>Clear</span></button>
            <Button style={downloadButtonStyle} className="text-white" icon={<CloudDownloadOutlined style={{ verticalAlign: "text-bottom" }} /> }  onClick={csvDownload}>CSV</Button>
        </div>
    )
}