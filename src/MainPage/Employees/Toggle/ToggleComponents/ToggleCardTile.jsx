import React, {useEffect, useRef, useState} from "react";
import {getFormattedTime, getTimeDifference, setFormattedDateAndTime} from "../../../../constants";
import TextArea from "antd/es/input/TextArea";
import SelectProjectButton from "./SelectProjectButton";
import DustBin from "../ToggleElements/Dustbin";
import {toggleCardStyles} from "../toggleConstants";
import {SelectTagButton} from "./SelectTagButton";
import {TimeSelectBlock} from "./DateAndTimeBlock";
import {toggleCrudAPI} from "../../../../Services/apiServices";
import {useDispatch} from "react-redux";
import {toggleCrudActions} from "../../../../Redux/Actions/Actions";
import {Button, Modal,} from 'antd';
import CheckMarkIcon from "../ToggleElements/checkMarkIcon";
import CancelIcon from "../ToggleElements/cancelIcon";
import {toast} from "react-toastify";
// import { CheckCircleFilled } from "@ant-design/icons";




export default function ToggleTile({entry}) {
    const dispatch = useDispatch();
    const [edit,setEdit] = useState(false);
    const [editTime,setEditTime] = useState(false);
    const descriptionRef = useRef();
    const [toggleEntryData,setToggleEntryData] = useState({
        description: entry.description,
        selectedProject : entry.projects,
        selectedTag : entry.tags.length > 0  ? entry.tags[0] : {},
        startTime:  getFormattedTime(entry.user_from_time),
        endTime: getFormattedTime(entry.user_to_time),
        userDuration: getTimeDifference(entry.user_from_time, entry.user_to_time),
            // getTimeDifference(getFormattedTime(entry.user_from_time), getFormattedTime(entry.user_to_time)),
        selectedDate:  entry.date
    })



    useEffect(()=> {
        // console.log("toggle tile",entry);
        setToggleEntryData(
            {
                description: entry.description,
                selectedProject : entry.projects,
                selectedTag : entry.tags.length > 0  ? entry.tags[0] : {},
                startTime:  getFormattedTime(entry.user_from_time),
                endTime: getFormattedTime(entry.user_to_time),
                userDuration:  getTimeDifference(entry.user_from_time, entry.user_to_time),
                // getTimeDifference(getFormattedTime(entry.user_from_time), getFormattedTime(entry.user_to_time)),
                selectedDate:  entry.date
            }
        )
        // console.log("toggle ToggleTile",entry);
    },[entry])

    const dataMap = {
        toggleEntryData: toggleEntryData,
        setToggleEntryData: setToggleEntryData,
        entryID: entry.id,
        editTime: editTime,
        setEditTime: setEditTime
    }

    useEffect(()=> {
        // console.log("TOGGLE CATDF",toggleEntryData);
    },[toggleEntryData])

    const [prevDateAndTime,setPrevDateAndTime] = useState({});
    useEffect(()=> {
        if(editTime) {
            // console.log("edit time mode");
            setPrevDateAndTime(toggleEntryData);
        } else {
            // console.log("edit time mode off");
            setPrevDateAndTime({});
        }
    },[editTime])

    //--------DELETE DIALOG-------
    const  dialogButtonStyle = {
        border: "1px solid #f43b48",
        borderRadius: "10px",
        background: "#f43b48",
        color: "white"
    }
    const handleOk = async () => {
        // console.log("okay");
        await toggleCrudAPI.removeToggleEntryAPI(entry.id).then((res) => {
            if(res.status) {
                // console.log("onDeleteTile",res,toggleEntryData,entry.id);
                dispatch(toggleCrudActions.deleteToggleEntry(entry.id));
            }
        });
    };
    const onDeleteTile = async () => {
        // console.log("delete entry");
        Modal.confirm({
            title: 'Confirm',
            centered: true,
            width: "350px",
            content: 'Are you sure you want to delete this entry?',
            okText: "YES",
            cancelText: 'NO',
            onOk: handleOk,
            okButtonProps: {style: dialogButtonStyle},
            cancelButtonProps :{style: dialogButtonStyle}
        });

    }
    //--------------------------------------------
    const [prevDesValue,setPrevDesValue] = useState(toggleEntryData.description);
    const changeDescription = async (e) => {
        // console.log(prevDesValue,toggleEntryData);
        if(prevDesValue.trim() != toggleEntryData.description.trim()){
            if(toggleEntryData.description.trim() == "") {
                setEdit(false);
                setToggleEntryData({...toggleEntryData,description: prevDesValue});
                toast.error("Description cannot be empty!");
            } else {
                // e.target.style.background = "inherit";
                // e.target.style.color = "white";
                await toggleCrudAPI.updateToggleDescriptionAPI(toggleEntryData,entry.id).then((res) => {
                    if(res.status) {
                        // console.log("changeDescription",res,toggleEntryData,entry.id);
                        dispatch(toggleCrudActions.updateToggleDescription({description: toggleEntryData.description,id: entry.id}))
                        setEdit(false);
                    }
                });
            }

        } else {
            setEdit(false);
        }
    }

    const checkDateAndTimeChanged = () => {
        // console.log(prevDateAndTime);
        if(prevDateAndTime.selectedDate == toggleEntryData.selectedDate && prevDateAndTime.startTime == toggleEntryData.startTime && prevDateAndTime.endTime == toggleEntryData.endTime ) {
            // console.log("same");
            return false;
        } else {
            // console.log("changed");
            return  true;
        }


    }
    const [clickLoader,setClickLoader] = useState(false);
    const timeEditSave = async  () => {
        // console.log("time edit save",toggleEntryData);
        setClickLoader(true)
        if(checkDateAndTimeChanged()) {
            await toggleCrudAPI.updateDateAndTimeAPI(toggleEntryData,entry.id).then((res) => {
                if(res.status) {
                    // console.log("timeEditSave",res,toggleEntryData,entry.id);
                    dispatch(toggleCrudActions.updateToggleDateAndTime({data: toggleEntryData,id: entry.id}))
                    setEditTime(false);
                }
            });
        } else {
            setEditTime(false);
        }
        setClickLoader(false)
    }
    const timeEditCancel = () => {
        // console.log("cncel");
        setToggleEntryData(
            {
                description: entry.description,
                selectedProject : entry.projects,
                selectedTag : entry.tags.length > 0  ? entry.tags[0] : {},
                startTime:  getFormattedTime(entry.user_from_time),
                endTime: getFormattedTime(entry.user_to_time),
                userDuration:  getTimeDifference(entry.user_from_time, entry.user_to_time),
                // getTimeDifference(getFormattedTime(entry.user_from_time), getFormattedTime(entry.user_to_time)),
                selectedDate:  entry.date
            }
        )
        setEditTime(false);
    }

    const editDateTimeSaveButtonStyle = {
        background: "transparent",
        border: "none",
        outline: "none",
    }
    const buttonDivStyle = {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center"
    }

    const onTextAreaFocus = (e) =>
    {
        setPrevDesValue(e.target.value);
        e.target.style.cursor = "text";
        // e.target.style.background = "white";
        // e.target.style.color = "black";
        setEdit(true);
    }
    useEffect(() => {
        // console.log(descriptionRef.current.resizableTextArea.props.style);
        if(!edit) {
            // descriptionRef.current.resizableTextArea.props.style.background = "white";
            // descriptionRef.current.resizableTextArea.props.style.color = "black"
        } else {
            // descriptionRef.current.resizableTextArea.props.style.background = "inherit";
            // descriptionRef.current.resizableTextArea.props.style.color = "white"
        }

    },[edit])

    return (
        // <div style={{background:  "#232526"}}>
        <div style={toggleCardStyles.toggleTileBoxStyle(edit,entry.is_late)}>
                <TextArea  ref={descriptionRef}
                           onFocus={onTextAreaFocus}
                           onMouseEnter={(e) => {e.target.style.cursor = "pointer"}} readOnly={!edit} onBlur={changeDescription}
                           value={toggleEntryData.description}

                           onChange={(i) => {setToggleEntryData({...toggleEntryData,description: i.target.value})}}
                           autoSize style={toggleCardStyles.toggleTileDescriptionStyle}  bordered={false}/>
                <div style={toggleCardStyles.toggleTileButtonsBoxStyle}>
                    <SelectProjectButton parameters={dataMap} fromProjectTile={true}/>
                    <SelectTagButton parameters={dataMap} fromToggleTile={true}/>
                    <TimeSelectBlock parameters={dataMap} />
                    {/*<DustBin onClick={onDeleteTile}/>*/}
                    {editTime ?
                        <div style={buttonDivStyle}>
                            <span onClick={timeEditCancel}><CancelIcon /></span>
                            <span onClick={clickLoader ? () => {} : timeEditSave}><CheckMarkIcon/></span>
                            {/*<button style={editDateTimeSaveButtonStyle}  ></button>*/}
                        </div> :
                        <DustBin onClick={onDeleteTile}/>}
                </div>
            </div>

        // </div>
    )

}