import React from "react";
import SearchIcon from "../ToggleElements/SearchIcon";
import {Input} from "antd";

export default function SearchInput({params,addAutoFocus = true}) {
    const inputStyle = {
        display: "inline-block",
        border : "none",
        background: "inherit",
        marginLeft: "5px",
        outline: "none",
        width: "80%",
        color: "#696969"
    }
    const searchBarStyle = {
        padding: "10px 10px 0px 10px",
        margin: "0px 5px 0px 5px",
        // borderBottom: "1px solid rgba(255,255,255,0.3)",
        background: "inherit",
        display: "flex",
        justifyContent: "start",
        // position: "sticky",
        // top: "0",
        // zIndex: "5",
        borderRadius: "10px",
        alignItems: "center"
    }
    return (
        <div style = {
            {
                position: "sticky",
                top: "0",
                zIndex: "5",
                background:"white",
                padding: "0px 0px 5px 0px",
                borderRadius: "10px",
            }
        }>
            <div style={searchBarStyle}>
                <SearchIcon />
                <input type="search"
                       style={inputStyle}
                       autoFocus={addAutoFocus}
                       onChange={(e) => {
                    params.onFilterList(e.target.value)
                }} placeholder="Search"/>
            </div>
            <div className="dropdown-divider" style={
                {
                    margin: "5px 10px 5px 10px",
                    position: "sticky",
                    top: "0",
                    zIndex: "5",
                }}/>

        </div>
    )


}