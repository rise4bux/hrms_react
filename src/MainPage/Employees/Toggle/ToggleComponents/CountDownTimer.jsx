

import React, {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";


const CountDownTimer = () => {
    const timerData = useSelector(store => store.timer)
    const [localTimerData,setLocalTimerData] = useState({hours: timerData.hours,seconds: timerData.seconds,minutes: timerData.minutes});
        const timerCountDownBoxStyle = {
        display: "inline-block",
        margin:"0px 15px 0px 0px",
        color : "white",
            fontSize: "16px",
            width: "80px",
        padding: "0px 0px 0px 0px"
    }

    useEffect(()=>{
        // console.log("count down timer...");
        setLocalTimerData(timerData);
    }, [timerData.seconds,timerData.minutes,timerData.hours]);

    return (
        <div style={timerCountDownBoxStyle} >
            <h4 className="text-white m-0">{localTimerData.hours<10?`0${localTimerData.hours}`:localTimerData.hours}:{localTimerData.minutes<10?`0${localTimerData.minutes}`:localTimerData.minutes}:{localTimerData.seconds<10?`0${localTimerData.seconds}`:localTimerData.seconds}</h4>
        </div>
    )
}

export default CountDownTimer;
