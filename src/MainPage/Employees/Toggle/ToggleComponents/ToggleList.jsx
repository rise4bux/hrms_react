import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import {apiData} from "../../../../Services/apiServices";
import {actions} from "../../../../Redux/Actions/Actions";
import {getDaysArray, groupBy} from "../../../../constants";
import ToggleCard from "./ToggleCard";
import {Button, Spin} from "antd";
import {toggleEntriesStyle} from "../toggleConstants";



export default  function ToggleList() {

    const dispatch = useDispatch();
    const dataOffset =  useSelector((store)=> store.db.toggleListOffset);
    const toggleListDB = useSelector((store)=> store.db.toggleList)
    const totalToggle = useSelector((store)=> store.db.totalToggleEntries)
    const [toggleList, setToggleList] = useState([]);
    const [listLoading,setListLoading] = useState(true);
    //-------------SCROLL LISTENER---------------
    // useEffect(() => {
    //     const onScroll = () => {
    //         loadMore();
    //     };
    //     // clean up code
    //     window.removeEventListener('scroll', onScroll);
    //     window.addEventListener('scroll', onScroll, { passive: true });
    //     return () => window.removeEventListener('scroll', onScroll);
    // }, [dataOffset]);
    // const loadMore = () => {
    //     if (window.innerHeight + document.documentElement.scrollTop === document.scrollingElement.scrollHeight) {
    //         console.log("LOAD MORE",dataOffset);
    //         apiData.getToggleEntriesList(dataOffset).then((res) => {
    //             if(res.status) {
    //                 dispatch(actions.setToggleList(res.data.entries))
    //                 dispatch(actions.increaseToggleOffset(11));
    //                 console.log("getting toggle entries..",res.data,dataOffset);
    //             }
    //         })
    //     }
    // }

    //----------FOR TOGGLE ENTRIES--------------
    //---------GETTING TOGGLE ENTRIES------------
    useEffect(() => {
              let isMounted = true;
            if(toggleListDB.length == 0 && isMounted) {
                 apiData.getToggleEntriesList(dataOffset).then((res) => {
                    if(res.status && isMounted) {
                        // console.log("toggle entries",res.data)
                        dispatch(actions.setToggleList({list: res.data.entries,total:res.data.total_entries}));
                        dispatch(actions.increaseToggleOffset(res.data.entries.length));
                    }
                     setListLoading(false);
                })
            } else {
                setListLoading(false);
            }
              return () => {setToggleList([]); isMounted = false };
        }, []
    )
    //--------SET TOGGLE LIST--------------------
    useEffect(()=> {
        _setToggleList(toggleListDB)
    },[toggleListDB])




    const _setToggleList = (toggleListDB) => {
        let toggleEntriesUIList = [];
        if(toggleListDB != null && toggleListDB != []) {
            for (let [key, value]  of Object.entries(groupBy(toggleListDB, 'date')) ) {
                toggleEntriesUIList = [...toggleEntriesUIList,{date : key, dataList : value}];
            }
            toggleEntriesUIList.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
        }
        setToggleList(toggleEntriesUIList);
    }


    const loadMoreButton = async  () => {
        setListLoading(true);
        await apiData.getToggleEntriesList(dataOffset).then((res) => {
            // console.log("dataoffset",dataOffset);
            if(res.status) {
                dispatch(actions.setToggleList({list: res.data.entries,total:res.data.total_entries}))
                dispatch(actions.increaseToggleOffset(10));
            }
            setListLoading(false);
        })
    }

const spinStyle = {
        color: "linear-gradient(to right, #f43b48 0%, #453a94 100%)",
}

    return (
        <>
            {toggleList.map((toggleCardData,index) =>(<ToggleCard key={index} toggleCardData={toggleCardData}/>))}
            <div style={toggleEntriesStyle.loadMoreDivStyle}>
                { listLoading ? <Spin style={spinStyle} size="large"/>:toggleList.length != 0 ? toggleList.length !== totalToggle ? <button style={toggleEntriesStyle.loadMoreButtonStyle} onClick={loadMoreButton} type="button" className="btn btn-secondary mr-1">Load More</button> : <></>: <span style={{color: "white",fontSize: "16px"}}>No Data</span>}
            </div>
        </>

    )
}