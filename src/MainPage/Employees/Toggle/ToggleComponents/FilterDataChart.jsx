import React, {useEffect, useState} from 'react';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import {formatTime} from "../../../../constants";

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

export const options = {
    responsive: true,
    scales: {
        y: {
            // type: 'time',
            // data: {
            //     displayFormats: {
            //         quarter: ' YYYY'
            //     }
            // }
        }
    },
    plugins: {

        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: 'Filter Reports',
        },
    },

};

// const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
//
// export const data = {
//     labels,
//     datasets: [
//         {
//             label: 'Dataset 1',
//             data: labels.map(() => 10),
//             backgroundColor: 'rgba(255, 99, 132, 0.5)',
//         },
//         // {
//         //     label: 'Dataset 2',
//         //     data: labels.map(() => 20),
//         //     backgroundColor: 'rgba(53, 162, 235, 0.5)',
//         // },
//     ],
// };
const chartDivStyle = {
    width: "95%",
    // height: "500px"
    background:"rgb(35, 37, 38)",
    margin: "0px auto",
    padding: "10px",
    borderRadius: "10px",
}

export function FilterDataChart({chartData}) {

    const [labels,setLabels] = useState([]);
    const [dataSet,setDataSet] = useState({
        label: 'Hours',
        data: [],
        displays:[],
        backgroundColor: '#DD1845',
    })


    useEffect(() => {
        // console.log("chartData",chartData);
        setLabels(chartData.dates);
        if(chartData.final_data != null) {
            // console.log("final; data", chartData.final_data,chartData.final_data.map((data) => data.y))
            setDataSet({
                label: 'Hours',
                data: chartData.final_data.map((data) => (data.y/ 3600)),
                displays: chartData.final_data.map((data) => formatTime(data.y)),
                backgroundColor: '#DD1845',
            });
        }
    },[chartData]);

    return (
        <div style={chartDivStyle}>
            <Bar options={options} type="bar" data={{labels, datasets: [dataSet]}} />
        </div>
    );
}
