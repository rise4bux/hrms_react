import React, {useEffect, useState} from "react";
import {toggleCardStyles} from "../toggleConstants";
import {checkDateIsYesterday, copyToClip, getFormattedDate} from "../../../../constants";
import {CopyFilled} from "@ant-design/icons";
import {toggleCrudAPI} from "../../../../Services/apiServices";
import moment from "moment";

export default function ToggleHeader({params}) {

    const [state,setState] = useState(true);
    useEffect(()=> {
        // console.log("toggle header",params)
        setState(!state);
    },[params])

    const copyToggleData = (e) => {
        // console.log("copy data", params.dateTime,);
        toggleCrudAPI.copyEntryAPI(moment(params.dateTime,"dddd, Do MMMM YYYY").format("YYYY-MM-DD")).then((res) => {
            if(res.status) {
                // console.log("response",res.data['html']);
                copyToClip(res.data['html']);
            }
        })
    }



    return (
        <>
            <div style={toggleCardStyles.toggleHeaderStyle}>
                <div style={toggleCardStyles.toggleHeaderDateTimeDivStyle}>{checkDateIsYesterday(params.dateTime)} <CopyFilled style={{
                    fontSize: "14px",
                    margin: "5px 0px 0px 10px",
                    verticalAlign: "top"
                }} onClick={copyToggleData}/></div>

                <div style={toggleCardStyles.toggleHeaderTotalTimeStyle}>{params.totalTime}</div>
            </div>
            {/*<div className="dropdown-divider" style={{margin: "5px 10px 0px 10px"}}/>*/}
        </>
    )
}