import {Button} from "antd";
import React from "react";
import Dot from "../ToggleElements/Dot";

export default function ProjectListTile({parameters}) {
    const dropDownItemStyle  = {
        padding: "0px 10px",
        margin: "0px auto",

    }
    const textStyle = {
        width: "100%",
        color: parameters.item.color_hex,
        // textAlign: "center"
    }

    return (
        <Button className="dropdown-item" style={dropDownItemStyle} key={parameters.item.id} onClick={() => {parameters.setSelectedProject(parameters.item)}}> <Dot color={parameters.item.color_hex}/><span style={textStyle}>{parameters.item.name}</span></Button>

    )

}