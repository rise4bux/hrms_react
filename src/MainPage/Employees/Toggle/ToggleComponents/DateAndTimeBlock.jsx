import React, {useRef, useState} from "react";
import {Calendar, DatePicker, Popover} from "antd";
import moment from "moment";
import {timeBlockStyles} from "../toggleConstants";
import ArrowBox from "../ToggleElements/ArrowBox";
import {getTimeDifference, setFormattedDateAndTime} from "../../../../constants";



export function TimeInputBlock({parameters}) {

        const [startTime,setStartTime] = useState(parameters.toggleEntryData.startTime);
    const [endTime,setEndTime] = useState(parameters.toggleEntryData.endTime);
    const [selectedDate,setSelectedDate] = useState(parameters.toggleEntryData.selectedDate);
    const dateFormat = 'DD/MM';

    function setDateTimeToRequired(fullDate,inputDate) {
        parameters.setToggleEntryData({...parameters.toggleEntryData,selectedDate: moment(fullDate).format("YYYY-MM-DD"),});
    }



    const changeStartTime = () => {
        let date = moment(startTime.toString(),
            ["hh:mm A"]
        ).format("hh:mm A");
        // console.log("start time",date);
        if(date === "Invalid date") {
            setStartTime(parameters.toggleEntryData.startTime);
        } else {
            setStartTime(date);
            parameters.setToggleEntryData({...parameters.toggleEntryData,startTime:date ,userDuration: getTimeDifference(setFormattedDateAndTime(selectedDate,date.toString()),setFormattedDateAndTime(selectedDate,endTime.toString()))})
        }
    };
    const changeEndTime = () => {
        let date = moment(endTime.toString(),
            ["hh:mm A"]
        ).format("hh:mm A");
        // console.log("end time",date);
        if(date === "Invalid date") {
            setEndTime(parameters.toggleEntryData.endTime);
        } else {
            setEndTime(date);
            parameters.setToggleEntryData({...parameters.toggleEntryData,endTime: date ,userDuration: getTimeDifference(setFormattedDateAndTime(selectedDate,startTime.toString()),setFormattedDateAndTime(selectedDate,date.toString()))})
        }
    };

    return (
        <div style={timeBlockStyles.timeBlockStyle}>
            <div style={timeBlockStyles.dateAndTimePickerStyle} >
            <input style={timeBlockStyles.timePickerStyle} value={startTime.toString()} onChange={(e) => setStartTime(e.target.value)} onBlur={changeStartTime}/>
            <span style={timeBlockStyles.dividerStyle}></span>
                <DatePicker inputReadOnly bordered={false} allowClear={false} picker={"date"}
                            style={timeBlockStyles.datePickerStyle} suffixIcon={null}
                            defaultValue={moment(moment(parameters.toggleEntryData.selectedDate).format("DD/MM"), dateFormat)}
                            format={dateFormat} onChange={(moment, dateString) => {
                    setSelectedDate(moment._d);
                    setDateTimeToRequired(moment._d, dateString)
                }}/>
            </div>
            <ArrowBox/>
            <div style={timeBlockStyles.dateAndTimePickerStyle2}>
                <input style={timeBlockStyles.timePickerStyle} value={endTime.toString()} onChange={(e) => setEndTime(e.target.value)} onBlur={changeEndTime}></input>
            </div>
        </div>
    )

}



export function TimeSelectBlock({parameters}) {

    const selectedTimeBoxStyle = {
        display: "inline-block",
        width: "220px"
    }
    // const [showCalender,setShowCalender] = useState(false);
    // const onClickDatePicker = () => {
    //     console.log("clicked!!!",parameters);
    //     setShowCalender(true)
    // }
    // const popOverTitleStyle = {
    //     padding: "10px 10px"
    // }
    // const closeButtonStyle = {
    //     float: "right",
    //     width: "10px",
    //     height: "10px",
    //     background: "red"
    // }
    // const popOverTitleBlock = (
    //     <TimeInputBlock parameters={parameters} showDatePicker={false}/>
    // );
    // const calenderStyle = {
    //     width: "300px",
    // }
    // const dateFormat = 'DD/MM';
    // function setDateTimeToRequired(fullDate,inputDate) {
    //     console.log(moment(fullDate).format("YYYY-MM-DD"));
    //     setShowCalender(false);
    //     parameters.setToggleEntryData({...parameters.toggleEntryData,selectedDate: moment(fullDate).format("YYYY-MM-DD")});
    //     console.log("setDateTimeToRequired",parameters.toggleEntryData)
    // }
    // /*const headerRender= ({ value, type, onChange, onTypeChange }) => {
    //     const start = 0;
    //     const end = 12;
    //     const monthOptions = [];
    //     const current = value.clone();
    //     const localeData = value.localeData();
    //     const months = [];
    //     for (let i = 0; i < 12; i++) {
    //         current.month(i);
    //         months.push(localeData.monthsShort(current));
    //     }
    //     for (let index = start; index < end; index++) {
    //         monthOptions.push(
    //             <Select.Option className="month-item" key={`${index}`}>
    //                 {months[index]}
    //             </Select.Option>,
    //         );
    //     }
    //     const month = value.month();
    //     const year = value.year();
    //     const options = [];
    //     for (let i = year - 10; i < year + 10; i += 1) {
    //         options.push(
    //             <Select.Option key={i} value={i} className="year-item">
    //                 {i}
    //             </Select.Option>,
    //         );
    //     }
    //     return (
    //         <div style={{ padding: 8 }}>
    //             <Row gutter={8}>
    //                 <Col>
    //                     <Select
    //                         size="small"
    //                         dropdownMatchSelectWidth={false}
    //                         className="my-year-select"
    //                         onChange={newYear => {
    //                             const now = value.clone().year(newYear);
    //                             onChange(now);
    //                         }}
    //                         value={year}
    //                     >
    //                         {options}
    //                     </Select>
    //                 </Col>
    //                 <Col>
    //                     <Select
    //                         size="small"
    //                         dropdownMatchSelectWidth={false}
    //                         value={month}
    //                         onChange={selectedMonth => {
    //                             const newValue = value.clone();
    //                             newValue.month(parseInt(selectedMonth, 10));
    //                             onChange(newValue);
    //                         }}
    //                     >
    //                         {monthOptions}
    //                     </Select>
    //                 </Col>
    //             </Row>
    //         </div>
    //     );
    // };*/
    // const popOverContentBlock = (
    //     <Calendar
    //         defaultValue={moment(moment(parameters.toggleEntryData.selectedDate).format("DD/MM"), dateFormat)}
    //         fullscreen={false} style={calenderStyle}  onChange={(moment, dateString) => {
    //         setDateTimeToRequired(moment._d, dateString)
    //     }}/>
    //
    // )






    return (
        // <Popover visible={showCalender}  onVisibleChange={ (e)=> {setShowCalender(e)}}
        //          placement="bottom" title={popOverTitleBlock} content={popOverContentBlock} trigger="click">
        // <div style={selectedTimeBoxStyle} onClick={onClickDatePicker}>
        //         <span>{parameters.toggleEntryData.startTime}</span>
        //         <span> - </span>
        //         <span>{parameters.toggleEntryData.endTime}</span>
        //         <span> {parameters.toggleEntryData.userDuration}</span>
        // </div>
        // </Popover>
            <div style={parameters.editTime ?  timeBlockStyles.timeBlockStyle : timeBlockStyles.timeBlockStyle2}>
                { parameters.editTime ?    <TimeInputBlock parameters={parameters}/> :    <>
                    <div style={selectedTimeBoxStyle} onClick={() => {parameters.setEditTime(true)}}>
                        <span>{parameters.toggleEntryData.startTime}</span>
                        <span> - </span>
                        <span>{parameters.toggleEntryData.endTime}</span>
                        <span style={{marginLeft: "5px"}}>{parameters.toggleEntryData.userDuration}</span>
                    </div>
                </>}
            </div>



    )
}