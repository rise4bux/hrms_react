import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import SearchInput from "./SearchInput";
import ProjectListTile from "./ProjectListTile";
import {projectButtonDropDownStyles} from "../toggleConstants";
import {toggleCrudAPI} from "../../../../Services/apiServices";
import {toggleCrudActions} from "../../../../Redux/Actions/Actions";
import Dot from "../ToggleElements/Dot";
import {CheckCircleFilled, LoadingOutlined, PlusCircleFilled} from '@ant-design/icons';
import {wc_hex_is_light} from "../../../../constants";
import {Menu, Dropdown, Button, Space, Empty, Select} from 'antd';
const antIcon = <LoadingOutlined style={{ fontSize: 16,fontWeight: "800", color: "white" }} spin />;

export default  function SelectProjectButton({parameters,fromProjectTile = false})  {
    const dbProjectsList  = useSelector((store) => store.db.userProjectsList );
    const timerData  = useSelector((store) => store.timer );
    const [projectList,setProjectList] = useState([]);
    const buttonRef = useRef();
    const dispatch = useDispatch();
    const [addingProject,setAddingProject] = useState(false);
    //---CHANGE BUTTON STYLE ACCORDING TO SELECTED PROJECT----------
    const  changeButtonStyle = () => {
        if(buttonRef != null ) {
            // console.log("change style",parameters.toggleEntryData)
            if(parameters.toggleEntryData.selectedProject != {} && parameters.toggleEntryData.selectedProject.color_hex != null) {
                buttonRef.current.style.background =wc_hex_is_light(parameters.toggleEntryData.selectedProject.color_hex) ? "black" :parameters.toggleEntryData.selectedProject.color_hex;
                buttonRef.current.style.border = `1px solid ${wc_hex_is_light(parameters.toggleEntryData.selectedProject.color_hex) ? "black" :parameters.toggleEntryData.selectedProject.color_hex}`

            } else {
                buttonRef.current.style.background = "#f43b48";
                buttonRef.current.style.border = `1px solid #f43b48`
            }
        }

    }
    const onFilterList = (text) => setProjectList(dbProjectsList.filter((i) => i.name.toLowerCase().match(text.toLowerCase())))
    const setSelectedProject =  (item) => {
        setAddingProject(true);
        if(fromProjectTile) {
            // console.log("setSelectedProject",parameters.toggleEntryData,parameters.entryID);
             toggleCrudAPI.updateToggleProjectAPI(parameters.entryID,item.id).then((res) => {
                if(res.status) {
                    parameters.setToggleEntryData({...parameters.toggleEntryData, selectedProject: item, selectedTag: {}})
                    // console.log("updateToggleProjectAPI",res,item.id,parameters.entryID);
                    dispatch(toggleCrudActions.updateToggleProject({projects : item, id : parameters.entryID}));
                }
            });
        } else {
            if(timerData.timerStarted) {
                // console.log("timer started project change", item.id, timerData.entryID);
                toggleCrudAPI.updateToggleProjectAPI(timerData.entryID,item.id).then((res) => {
                    if(res.status) {
                        parameters.setToggleEntryData({...parameters.toggleEntryData, selectedProject: item, selectedTag: {}})

                    }
                });
            } else {
                parameters.setToggleEntryData({...parameters.toggleEntryData, selectedProject: item, selectedTag: {}})
            }
        }
        setAddingProject(false);

    }


    useEffect(
        ()=> {
            // console.log("proejct",parameters.toggleEntryData)
            setProjectList(dbProjectsList);
            changeButtonStyle();
        }, [dbProjectsList,parameters.toggleEntryData.selectedProject]
    )
    const menuItemStyle = {

    }
    const menuStyle = {
        padding: "0px 0px 0px 0px",
        background: "whitesmoke",
        borderRadius: "5px",
        // maxHeight:320,
        overflowY:'hidden'

    }
    const menu = (

            <Menu style={menuStyle} >
                <SearchInput params={{onFilterList: onFilterList}} addAutoFocus={fromProjectTile ? false : true}/>
                {/*<Menu.Item*/}
                {/*           style={{color: "#f43b48",*/}
                {/*               margin: "0px 4px",*/}
                {/*               textAlign: "center",*/}
                {/*               background: "whitesmoke",*/}
                {/*               borderRadius: "10px",*/}
                {/*               border: "1px solid white"*/}
                {/*           }}*/}
                {/*           key={"no project"}*/}
                {/*           onClick={(e)=> {setSelectedProject({})}}>*/}
                {/*    Clear Selected*/}
                {/*    /!*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*!/*/}
                {/*</Menu.Item>*/}
                <div style={{overflowY: "auto", maxHeight: "300px",padding: "5px 0px 5px 0px"}}>
                {projectList.length != 0 ?
                    projectList.map((item,index) =>
                        (  <Menu.Item itemID={item.id}
                                      // eventKey={item.id.toString()}

                                      style={{
                                          color: wc_hex_is_light( item.color_hex) ? "black" : item.color_hex,
                                          margin: "0px 4px",
                                          // textAlign: "center",
                                          background: "white",
                                          borderRadius: "10px",
                                          border: "1px solid whitesmoke",
                                      }}
                                      key={index}
                                      onClick={(e)=> {setSelectedProject(item)}}>
                            {item.name}
                            {/*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*/}
                        </Menu.Item>)
                    ) :
                    <Menu.Item
                        // eventKey={"no project"}
                        style={{color: "#696969",
                            margin: "0px 4px",
                            textAlign: "center",
                            background: "whitesmoke",
                            borderRadius: "10px",
                            border: "1px solid whitesmoke"
                        }}
                        disabled
                        key={"no project"}
                        onClick={null}>
                        <Empty className="dropDownEmpty" style={{marginTop: "30px",color: "black",background: "white"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                               description="No Data" />
                    </Menu.Item>
                }
                </div>
            </Menu>


    );


const buttonStyle = {
    background: "#f43b48",
    border: "1px solid #f43b48",
    borderRadius: "5px",
    color: "white",
    fontWeight: "500",
    fontSize: "14px",
    padding: "0px 8px",
    display: "flex",
    alignItems: "center",
    height: "28px",

}

    return (
        <Dropdown
            destroyPopupOnHide={true}
            onVisibleChange={(e) => {
                if(!e) {
                    setProjectList(dbProjectsList);
                }
            }}
            overlay={menu} placement="bottomRight" trigger={"click"}>
            <Button ref={buttonRef}
                    // eventKey={"button"}
                    icon={parameters.toggleEntryData.selectedProject != null && parameters.toggleEntryData.selectedProject.name == null ? <PlusCircleFilled style={{ verticalAlign: "middle" }} /> : <></>}
                    style={buttonStyle}>
                {parameters.toggleEntryData.selectedProject != null && parameters.toggleEntryData.selectedProject.name == null ? "Projects" : parameters.toggleEntryData.selectedProject.name}
            </Button>
        </Dropdown>

    );
}