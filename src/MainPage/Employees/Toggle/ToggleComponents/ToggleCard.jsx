//ENTRY CARD

import React, {useEffect, useState} from "react";
import {getDate, getFormattedTime, getTotalTime} from "../../../../constants";
import moment from "moment";
import TextArea from "antd/es/input/TextArea";
import ToggleHeader from "./ToggleCardHeader";
import ToggleTile from "./ToggleCardTile";
import {toggleCardStyles} from "../toggleConstants";
import {useSelector} from "react-redux";

export default function ToggleCard({toggleCardData}) {
    const [headerParams,setHeaderParams] = useState({
        totalTime: getTotalTime(toggleCardData.dataList) ,
        dateTime: getDate( toggleCardData.date)
    })
    useEffect(() => {
        setHeaderParams({
            totalTime: getTotalTime(toggleCardData.dataList) ,
            dateTime: getDate( toggleCardData.date)
        });
    },[toggleCardData])




    return (
        <div style={toggleCardStyles.toggleCardStyle}>
            <ToggleHeader params={headerParams}/>
            <div>
                {toggleCardData.dataList.map((entry,index )=> (
                    <ToggleTile key={index} entry={entry}/>
                ))}
            </div>
        </div>
    )

}




