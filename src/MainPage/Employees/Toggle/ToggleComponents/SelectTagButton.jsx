import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import {Button, Dropdown, Empty, Menu} from "antd";
import {tagButtonDropDownStyles} from "../toggleConstants";
import TagIcon from "../ToggleElements/TagIcon";
import SearchInput from "./SearchInput";
import {toggleCrudAPI} from "../../../../Services/apiServices";
import {toggleCrudActions} from "../../../../Redux/Actions/Actions";
import {PlusCircleFilled} from "@ant-design/icons";


export function SelectTagButton({parameters,fromToggleTile = false})  {
    const dbProjectsList  = useSelector((store) => store.db.userProjectsList);
    const timerData  = useSelector((store) => store.timer );
    const [tagsList,setTagsList] = useState([]);
    const dispatch = useDispatch();
    useEffect(
        ()=> {
            // console.log("tag button",parameters.toggleEntryData.selectedProject);
            if(parameters.toggleEntryData.selectedProject != null) {
                let selectedProjectID = parameters.toggleEntryData.selectedProject.id;
                let project = dbProjectsList.find((project) => project.id === selectedProjectID);
                // console.log("tag button-1",project);
                if(project != null) {
                    setTagsList(project.tags.filter((t) => t != null));
                }
            }
        }, [dbProjectsList,parameters.toggleEntryData.selectedProject]
    )






    function onFilterList(text) {
        setTagsList(dbProjectsList.find((project) => parameters.toggleEntryData.selectedProject.id == project.id).tags.filter((i) => i != null && i.name.toLowerCase().match(text.toLowerCase())) ) ;
            // parameters.toggleEntryData.selectedProject.tags.filter((i) => i.name.toLowerCase().match(text.toLowerCase())) ) ;
    }

    function getSelectedTagName() {
        if(parameters.toggleEntryData.selectedTag == null && parameters.toggleEntryData.selectedTag == {}) {
            return "";
        } else {
            return parameters.toggleEntryData.selectedTag.name;
        }
    }
    const setSelectedTag = async  (item) => {
        if(fromToggleTile) {
            // console.log("setSelectedProject",parameters.toggleEntryData,parameters.entryID);
            await toggleCrudAPI.updateToggleTagAPI(parameters.entryID,item.id).then((res) => {
                if(res.status) {
                    parameters.setToggleEntryData({...parameters.toggleEntryData,selectedTag: item})
                    // console.log("updateToggleTagAPI",res,item.id,parameters.entryID);
                    dispatch(toggleCrudActions.updateToggleTag({tags : item, id : parameters.entryID}));
                }
            });
        } else {
            if(timerData.timerStarted) {
                await toggleCrudAPI.updateToggleTagAPI(timerData.entryID,item.id).then((res) => {
                    if(res.status) {
                        parameters.setToggleEntryData({...parameters.toggleEntryData,selectedTag: item})
                    }
                });
            } else {
                parameters.setToggleEntryData({...parameters.toggleEntryData,selectedTag: item})

            }

        }

    }
    const menuStyle = {
        padding: "0px 0px 0px 0px",
        background: "whitesmoke",
        borderRadius: "5px",
        // maxHeight:320,
        overflowY:'hidden'
    }
    const menu = (
        <Menu style={menuStyle}>
            <SearchInput params={{onFilterList: onFilterList}} addAutoFocus={fromToggleTile ? false : true}/>
            {/*<Menu.Item*/}
            {/*           style={{color: "#f43b48",*/}
            {/*               margin: "0px 4px",*/}
            {/*               textAlign: "center",*/}
            {/*               background: "whitesmoke",*/}
            {/*               borderRadius: "10px",*/}
            {/*               border: "1px solid white"*/}
            {/*           }}*/}
            {/*           key={"no project"}*/}
            {/*           onClick={(e)=> {setSelectedProject({})}}>*/}
            {/*    Clear Selected*/}
            {/*    /!*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*!/*/}
            {/*</Menu.Item>*/}
            <div style={{overflowY: "auto", maxHeight: "300px",padding: "5px 0px 5px 0px"}}>
            {tagsList.length != 0 ?
                tagsList.map((item,index) =>
                    (  <Menu.Item itemID={item.id}
                                  style={{color: item.color_hex,
                                      margin: "0px 4px",
                                      textAlign: "left",
                                      background: "white",
                                      borderRadius: "10px",
                                      border: "1px solid whitesmoke"
                                  }}
                                  key={index}
                                  onClick={(e)=> {setSelectedTag(item)}}>
                        {item.name}
                        {/*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*/}
                    </Menu.Item>)
                ) :
                <Menu.Item
                    style={{color: "#696969",
                        margin: "0px 4px",
                        textAlign: "center",
                        background: "whitesmoke",
                        borderRadius: "10px",
                        border: "1px solid whitesmoke"
                    }}
                    disabled
                    key={"no tag"}
                    onClick={null}>
                    <Empty className="dropDownEmpty" style={{marginTop: "30px",color: "black",background: "white"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                           description="No Data" />
                    {/*<ProjectListTile parameters={{setSelectedProject: setSelectedProject,item: item}}/>*/}
                </Menu.Item>
            }
            </div>
        </Menu>
    );


    return (
        <Dropdown overlay={menu} placement="bottomRight" trigger={"click"} >
            <Button icon={<TagIcon color={"#721c24"}/>} style={tagButtonDropDownStyles.setButtonStyle(true)}  disabled={fromToggleTile ? false : timerData.timerStarted || parameters.toggleEntryData.selectedProject.name == null}><span style={tagButtonDropDownStyles.textStyle}>{getSelectedTagName()}</span></Button>
        </Dropdown>

        // <div className="btn-group">
        //     <button type="button"  style={tagButtonDropDownStyles.setButtonStyle(true)} disabled={fromToggleTile ? false : timerData.timerStarted || parameters.toggleEntryData.selectedProject.name == null} className="btn btn-primary dropdown-toggle mr-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <TagIcon color={"#721c24"}/><span style={tagButtonDropDownStyles.textStyle}>{getSelectedTagName()}</span></button>
        //     <div className="dropdown-menu"  style={tagButtonDropDownStyles.dropDownBoxStyle}>
        //         <SearchInput params={{onFilterList: onFilterList}}/>
        //         {tagsList.length == 0 ?    (<Button className="dropdown-item"   style={tagButtonDropDownStyles.dropDownItemStyle} key={"EMPTY"} >EMPTY!</Button>):tagsList.map((item,index) =>
        //             (<Button className="dropdown-item"   style={tagButtonDropDownStyles.dropDownItemStyle} key={item.id} onClick={() => {setSelectedTag(item)}}>{item.name}</Button>)
        //         )}
        //         {/*<div className="dropdown-divider" />*/}
        //     </div>
        // </div>
    );
}