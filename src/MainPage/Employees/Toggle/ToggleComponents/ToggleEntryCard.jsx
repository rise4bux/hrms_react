import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {defaultToggleObject, toggleEntriesStyle, toggleEntryDataObj} from "../toggleConstants";
import moment from "moment";
import {apiData, timerAPI, toggleCrudAPI} from "../../../../Services/apiServices";
import {actions, toggleCrudActions} from "../../../../Redux/Actions/Actions";
import TextArea from "antd/es/input/TextArea";
import SelectProjectButton from "./SelectProjectButton";
import {SelectTagButton} from "./SelectTagButton";
import {TimeInputBlock} from "./DateAndTimeBlock";
import CircleCheckIcon from "../ToggleElements/CircleCheckIcon";
import Dot from "../ToggleElements/Dot";
import {Divider, Spin} from "antd";
import {LoadingOutlined, PauseCircleFilled, PlayCircleFilled} from '@ant-design/icons';
import {toast} from "react-toastify";
import {convertUTCDateToLocalDate, getDate, getTimeDifference, getTimeDifferenceBool} from "../../../../constants";
import TimerButton from "../ToggleElements/TimerButton";
import CountDownTimer from "./CountDownTimer";
import {timerActions} from "../../../../Redux/Actions/TimerActions";
import DateTime from "moment";
import * as workerTimers from 'worker-timers';
const antIcon = <LoadingOutlined style={{ fontSize: 30,fontWeight: "800", color: "red" }} spin />;


const spanStyle = {
    background: "white",
    fontSize: "20px",
    textAlign: "center",
    padding: "12px",

    borderRadius: "100%",
    color: "black",
    display: "inline-block",
    marginLeft: "5px",
    marginRight: "5px",
    cursor:"pointer",
    verticalAlign: "middle"
}
const timerStyleSpan = {
    // padding: "0px 0px 5px 0px",
    // width: "130px",
    width: "40px",
    margin: "0px 0px 0px 0px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
}


export default function ToggleEntryCard() {

    const dispatch = useDispatch();
    const [toggleEntryData,setToggleEntryData] = useState(toggleEntryDataObj);
    const dbProjectsList  = useSelector((store) => store.db.userProjectsList );
    const timerData  = useSelector((store) => store.timer );
    const [addingEntry,setAddingEntry] = useState(false);
    const [addingTimerEntry,setAddingTimerEntry] = useState(false);
    const inputRef = useRef(null);
    const [prevDesValue,setPrevDesValue] = useState(toggleEntryData.description);
    //----FOR PROJECTS LIST-------

    useEffect(
         () => {
             let isMounted = true;
             if(!timerData.timerStarted) {
                 inputRef.current.focus();
             }
             if(dbProjectsList.length === 0) {
                  apiData.getProjectsList().then((r) => {
                    if(isMounted && r.status) {
                        dispatch(actions.setUserProjects(r.data));
                    }
                });
            }
             if(!timerData.timerAPiData) {
                 setAddingEntry(true);
                 timerAPI.getUserTimerStatusAPI().then((res) => {
                     if(isMounted && res.status) {
                         // console.log("getUserTimerStatusAPI",res)
                         if(res.data.is_timer_on === 1) {
                             // console.log("timer status api -> paused time ->",res.data.timer_entry_data.last_paused_time);
                             dispatch(timerActions.timerStatusChange(
                                 {
                                     status: true,
                                     timerData:  {...res.data.timer_entry_data,user_from_time: DateTime(moment(res.data.timer_entry_data.user_from_time)).unix()},
                                     pauseStatus : res.data.timer_entry_data.is_paused,
                                     pauseDuration :  res.data.timer_entry_data.break_timings,
                                     pauseTime: DateTime(res.data.timer_entry_data.last_paused_time).unix(),

                                 }
                             ))
                             dispatch(timerActions.timerEntryID(res.data.timer_entry_data.id))
                             if(res.data.timer_entry_data.is_paused == 1) {
                                 setPauseTimer(true);
                                 setMode(true);
                             } else {
                                 setPauseTimer(false);
                                 const timerID =   workerTimers.setInterval(startDBTimer,1000);
                                 setMode(true);
                                 dispatch(timerActions.startTimer({id: timerID,timerEntryData: {...res.data.timer_entry_data,user_from_time: DateTime(moment(res.data.timer_entry_data.user_from_time)).unix()}}));
                             }
                             setPrevDesValue(res.data.timer_entry_data.description);

                         } else {
                             dispatch(timerActions.timerStatusChange({status: false}))
                         }
                     }
                     setAddingEntry(false);
                 })
             } else {
                 if(timerData.timerStarted) {
                     // console.log("data",timerData.timerEntryData);
                     setToggleEntryData(timerData.timerEntryData);
                     setPrevDesValue(timerData.timerEntryData.description);

                 } else {

                 }
                 setAddingEntry(false);
             }
             return () => { isMounted = false };
        }, []
    )





const validateToggleEntryData = (fromTimer = false) => {
        let validate = true;
        if(toggleEntryData.description.trim() === "") {
            validate = false;
            toast.error("Enter project description!");

        }
        else if(toggleEntryData.selectedProject.id == null) {
            validate = false;
            toast.error("Please select a project!");
        }

        else if(toggleEntryData.selectedTag.id == null) {
            validate = false;
            toast.error("Please select project tag!");
        }
        else if(!timerData.timerStarted && toggleEntryData.startTime === toggleEntryData.endTime) {
            validate = false;
            toast.error("Please select proper time range");
        }
        else if(!fromTimer && !timerData.timerStarted && getTimeDifferenceBool(toggleEntryData.startTime,toggleEntryData.endTime,4)) {
            validate = false;
            toast.error("Cannot make time entry more than 4 hours!");
        }
        return validate;
}

    const addEntry = async () => {
        setAddingEntry(true);
        if(validateToggleEntryData())
        {
            // console.log("toggle Entry",toggleEntryData)
            await toggleCrudAPI.addToggleEntryAPI(toggleEntryData,dispatch).then((res) => {
                    if(res.status) {
                        // console.log("toggle Entry",res.data);
                        setToggleEntryData({...toggleEntryData,description: ""})
                    }

                }
            )
        }
        setAddingEntry(false);
    }

    const dataMap = {
        toggleEntryData: toggleEntryData,
        setToggleEntryData: setToggleEntryData
    }


    const startDBTimer = () => {
        dispatch(timerActions.updateTimer({}));
    }
    const [timerClick, setTimerClick] = useState(false);
    const onTimerClick = () => {
        setTimerClick(true);
        if(validateToggleEntryData(true)) {
            if(!timerData.timerStarted) {
                setAddingTimerEntry(true);
                setPauseTimer(false);
                dispatch(timerActions.timerStatusChange({status: true}));

                const timerID =  workerTimers.setInterval(startDBTimer,1000);
                dispatch(timerActions.startTimer({id: timerID,timerEntryData: {...toggleEntryData,
                        // startTime: DateTime().format("hh:mm:ss A"),
                        user_from_time: DateTime().unix()}}));
                setAddingTimerEntry(false);
                timerAPI.playEntryTimerAPI(toggleEntryData).then((res) => {
                    // console.log("timer data after ::: ", res);
                    if(!res.status) {
                        dispatch(timerActions.stopTimer({}))
                        dispatch(timerActions.timerStatusChange({status: false}))
                    } else {
                        dispatch(timerActions.timerEntryID(res.data.id))
                    }
                });
            } else {
                setAddingTimerEntry(true);
                // dispatch(timerActions.timerStatusChange({status: false}))
                timerAPI.stopEntryTimerAPI(toggleEntryData).then((res) => {
                    if(res.status) {
                        setAddingTimerEntry(false);
                        // dispatch(timerActions.timerStatusChange({status: false}))
                        // toggleTimer(false);
                        dispatch(timerActions.stopTimer({}))
                        setToggleEntryData({...toggleEntryData,description: ""})
                        res.data.forEach((entryData) => {
                            dispatch(toggleCrudActions.addToggleEntry(entryData))
                        });

                    } else {
                        dispatch(timerActions.timerStatusChange({status: true}))
                    }
                });
                //----------VALIDATION CODE--------
                // if(timerData.minutes === 0) {
                //     toast.error("Must have minimum time of 1 min!");
                // } else {
                //     setAddingTimerEntry(true);
                //     dispatch(timerActions.timerStatusChange({status: false}))
                //     timerAPI.pauseEntryTimerAPI().then((res) => {
                //         if(res.status) {
                //             setAddingTimerEntry(false);
                //             dispatch(timerActions.timerStatusChange({status: false}))
                //             // toggleTimer(false);
                //             setToggleEntryData({...toggleEntryData,description: ""})
                //             res.data.forEach((entryData) => {
                //                 dispatch(toggleCrudActions.addToggleEntry(entryData))
                //             });
                //             dispatch(timerActions.stopTimer({}))
                //             // setToggleEntryData({...toggleEntryData,description: ""})
                //         } else {
                //             dispatch(timerActions.timerStatusChange({status: true}))
                //         }
                //     });
                // }
            }
        }
        setTimerClick(false);

    }

    const updateDescription = async (e) => {

    if(timerData.timerStarted) {
        if(toggleEntryData.description.trim() != "" && prevDesValue.trim() != toggleEntryData.description.trim()){
            if(toggleEntryData.description.trim() === "") {
                toast.error("Description cannot be empty!");
            } else {
                // e.target.style.background = "inherit";
                // e.target.style.color = "white";
                await toggleCrudAPI.updateToggleDescriptionAPI(toggleEntryData,timerData.entryID).then((res) => {
                    if(res.status) {
                        // console.log("changeDescription",res,toggleEntryData,timerData.entryID);
                    }
                });
            }
        }

    }
}

    const descriptionChange = async (e) => {
        setToggleEntryData({...toggleEntryData, description: e.target.value})
    }
    const [mode,setMode] = useState(timerData.timerStarted);

    // useEffect(
    //     () => {
    //         console.log("mode change",mode);
    //         if(!mode) {
    //
    //         }
    //     }, [mode]
    // )

    const [pauseTimer, setPauseTimer] = useState(timerData.is_pause == 1);


    const [changingPauseTimer,setChangingPauseTimer] = useState(false);
    const pauseTimerFunction = () => {
        setChangingPauseTimer(true);
        if(!pauseTimer) {
            dispatch(timerActions.pauseTimer({}));
            timerAPI.pauseEntryTimerAPI(toggleEntryData).then((res) => {
                // console.log("pause entry timer RES -> ",res);
                    if(!res.status) {
                        const timerID =  workerTimers.setInterval(startDBTimer,1000);
                        dispatch(timerActions.startTimer({id: timerID,fromPause: true}));
                    }
            });
        } else {

            timerAPI.playEntryTimerAPI(toggleEntryData).then((res) => {
                // console.log("play paused entry timer RES -> ",res);
                if(!res.status) {
                    dispatch(timerActions.pauseTimer({}));
                } else {
                    const timerID =  workerTimers.setInterval(startDBTimer,1000);
                    dispatch(timerActions.startTimer({id: timerID,fromPause: true}));
                }
            });
        }
        setPauseTimer(!pauseTimer);
        setChangingPauseTimer(false);

    }
    // useEffect(() => {
    //     if(timerData.timerAPiData) {
    //         setMode(timerData.timerStarted);
    //     }
    // },[timerData.timerStarted])
    return (
        <div style={timerData.timerStarted ? toggleEntriesStyle.toggleEntryInputDiv(false) :toggleEntriesStyle.toggleEntryInputDiv(addingEntry)}>
            <TextArea ref={inputRef} placeholder={"What are you working on?"} autoSize style={toggleEntriesStyle.toggleEntryInput}
                      // disabled={timerData.timerStarted}
                      //         autoFocus={true}
                      // onBlur={updateDescription}
                      value={toggleEntryData.description} onChange={descriptionChange}
                      bordered={false}/>
            <div style={toggleEntriesStyle.entryButtonsBoxStyle}>
                <SelectProjectButton parameters={dataMap} fromProjectTile={false}/>
                <SelectTagButton parameters={dataMap}  fromToggleTile={false} />
                {mode ?
                    <CountDownTimer/>
                   : <TimeInputBlock parameters={dataMap} />}{

            }
                {mode && timerData.timerStarted ? pauseTimer ?  <PlayCircleFilled  onClick={() => {!changingPauseTimer ? pauseTimerFunction() : () => {}}}
                    style={{fontSize: "35px",marginLeft: "0px",marginRight: "10px",cursor: "pointer",color:"white"}}/>
                    :
                    <PauseCircleFilled  onClick={() => {!changingPauseTimer ? pauseTimerFunction() : () => {}}}
                        style={{fontSize: "35px",marginLeft: "0px",marginRight: "10px",cursor: "pointer",color:"white"}}/>  : <></>}
                { mode ?

                    <span style={timerStyleSpan} onClick={timerClick ? () => {} : ()=> {onTimerClick()}}>

                        {addingTimerEntry ?    <Spin size={"small"} indicator={antIcon}/>  : timerData.timerStarted ?
                            <div style={spanStyle}>
                                <div style={{background: "black", width: "12px", height: "12px"}}></div>
                            </div> :
                            <PlayCircleFilled  style={{fontSize: "35px",marginLeft: "10px",marginRight: "10px",cursor: "pointer",color:"white"}}/>}
                </span> :   <a style={toggleEntriesStyle.addEntryStyle} onClick={timerData.timerStarted  ? null : addingEntry ? () => {} : addEntry}> {addingEntry ?
                    <Spin size={"small"} indicator={antIcon}/> : <CircleCheckIcon fillColor={timerData.timerStarted  ? "rgba(255,255,255,0.3)" :"#4bc800"}/>}</a>
                }


            </div>
            <div style={
                {
                    margin: "0px 0px 0px 10px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent:"space-between",
                    alignItems: "center",
                    width: "30px",
                }
            }>
                <i className="la la-medium" style={{
                    color:     mode ?  "rgba(255,255,255,0.3)" : "white",
                    fontSize: "20px",
                    marginBottom: "3px",
                    cursor: timerData.timerStarted ?  "not-allowed" :"pointer"

                }}
                   onClick={() => {
                       if(!timerData.timerStarted) {
                           setMode(false)
                       }
                       }}
                />
                <i className="la la-stopwatch" style={{
                    color:     !mode ?  "rgba(255,255,255,0.3)" : "white",
                    fontSize: "20px",
                    cursor: "pointer"
                }} onClick={() => {
                    if(!timerData.timerStarted) {
                    setMode(true)
                }
                }
                } />

            </div>
        </div>
    )

}