    import React, { useState,useEffect } from 'react';
    import { Helmet } from "react-helmet";
    import {Link, useHistory} from 'react-router-dom';
    import {Button, Divider, Table, Modal, Input, Empty} from 'antd';
    import 'antd/dist/antd.css';
    import {itemRender,onShowSizeChange} from "../paginationfunction"
    import "../antdstyle.css"
    import { dashBoardHelmet, permissionRoutes, permissionTypes} from "../../constants";
    import {clientsAPI,} from "../../Services/apiServices";
    import {useDispatch, useSelector} from "react-redux";
    import {clientActions} from "../../Redux/Actions/ClientsActions";
    import { Spin } from 'antd';
    import {toast} from "react-toastify";
    import {checkPermission} from "../../permissionConstants";
    import {homePageURL, loginRouteURL} from "../../Services/routeServices";
    import SelectStatus from "../Administration/Employees/Components/SelectStatus";
    import {searchStyle, tableStyles} from "../../global_styles";
    //-----CLIENT PAGE STYLES----
    const inputRowStyle = {
        margin: "20px 0px 10px 0px"

    }
    const  dialogButtonStyle = {
        border: "1px solid #f43b48",
        borderRadius: "10px",
        background: "#f43b48",
        color: "white"
    }

    const spinnerStyle = {
        display: "flex",
        justifyContent: "center",
        marginTop: "20px"
    }
    const addClientButtonStyle = {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        textTransform: "none"
        // margin: "0px 30px 0px 0px"
    }
    const tableDivStyle = {
        background: "black",
        padding: "10px 10px",
        borderRadius: "10px"
    }
    const clientPageStyle = {
        // background: "#232526",
        // borderRadius: "10px",
    }
    //--------------------------------------------------------
    const Clients = () => {





        const dispatch = useDispatch();
        const allClientsList = useSelector((store)=> store.db.allClients);
        const totalClients = useSelector((store)=> store.db.totalClients);
        const [uiClientsList,setUIClientsList] = useState([...allClientsList]);
        const [newClient,setNewClient] = useState({id: Math.floor((Math.random() * 100) + 1), name: "", status: 1});
        const [showForm,setShowForm] = useState(false);
        const [showUpdateForm,setShowUpdateForm] = useState(false);
        const [toUpdateClient,setToUpdateClient] = useState({id: Math.floor((Math.random() * 100) + 1), name: "", status: 1});
        const [state,setState] = useState(false);
        const [loading,setLoading] = useState(false);
        const [paginationTableLoading,setPaginationTableLoading] = useState(false);
        //---TABLE COLUMNS DATA-------
        const columnsData = [
            {
                title: 'Name',
                dataIndex: 'name',
                render: (text, record) =>{

                    return  (
                        <h2 className="table-avatar">
                            {/*<Link to="/app/profile/employee-profile" className="avatar"><img alt="" src={record.image} /></Link>*/}
                            {/*<Link to="/app/profile/employee-profile"></Link>*/}
                            {text}
                        </h2>
                    )
                },
                sorter: (a, b) => a.name.length - b.name.length,
            },
            {
                title: 'Status',
                dataIndex: 'status',
                render: (text, record) => {
                    return (
                        <div className="dropdown">
                            <a href="#" className="btn  btn-sm btn-rounded dropdown-toggle" style={{background: "rgba(255, 255, 255,0.3)", color: "white"}} data-toggle="dropdown"
                               aria-expanded="false">
                                <i className={record.status === 1 ? "fa fa-dot-circle-o text-success" : "fa fa-dot-circle-o text-danger"}/> {record.status === 1 ? "Active" : "Inactive"}
                            </a>
                            <div className="dropdown-menu">
                                <Button className="dropdown-item" style={tableStyles.statusButtonsStyle} onClick={() => {
                                    if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                                        clientStatusSelectToActive(record);

                                    } else {
                                        toast.error("You Don't have Permission for this!");
                                    }
                                }}><i className="fa fa-dot-circle-o text-success"/> <span
                                    style={tableStyles.statusButtonsInnerSpanStyle}>{"Active"}</span></Button>
                                <Button className="dropdown-item" onClick={() => {
                                    if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                                        clientStatusSelectToInActive(record);
                                    } else {
                                        toast.error("You Don't have Permission for this!");
                                    }
                                }}><i className="fa fa-dot-circle-o text-danger"/> <span
                                    style={tableStyles.statusButtonsInnerSpanStyle}>{"Inactive"}</span></Button>
                                {/*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-success" /> Active</a>*/}
                                {/*<a className="dropdown-item" href="#"><i className="fa fa-dot-circle-o text-danger" /> Inactive</a>*/}
                            </div>
                        </div>
                    )
                },
            },
            {
                title: 'Action',
                render: (text, record) => (
                    <div style={tableStyles.actionButtonDivStyle}>
                        <button style={tableStyles.actionButtonStyles} onClick={() => {
                            if(checkPermission(permissionRoutes.ClientsP,permissionTypes.updateP)) {
                                // console.log("record",record);
                                setShowUpdateForm(true);

                                setToUpdateClient(record);
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }

                        }}><i className="fa fa-pencil"/>
                            {/*Edit*/}
                        </button>
                        <button style={tableStyles.actionButtonStyles}  onClick={() => {
                            if(checkPermission(permissionRoutes.ClientsP,permissionTypes.deleteP)) {
                                onDeleteModel(record)
                            } else {
                                toast.error("You Don't have Permission for this!");
                            }
                        }}><i className="fa fa-trash-o" /></button>
                    </div>
                ),
            },
        ]
        const [columns,setColumns] = useState(columnsData)
        //------GET ALL CLIENTS API (limit : 10)
        useEffect( ()=>{
            let isMounted = true;
            if(allClientsList.length == 0) {
                clientsAPI.getClientsAPI(0,10).then((r) => {
                    if(r.status  && isMounted) {
                        // console.log("CLIENTS PAGE",r.data);
                        dispatch(clientActions.setAllClients({data: r.data , total_count: r.total_count}));
                        setUIClientsList(r.data);
                    }
                });
            } else {
                setColumns(columnsData);
            }
            return () => { isMounted = false };
        }, [state]);
        //---- SEARCH UI LIST SET----
        useEffect( ()=>{
            if(searchStatus) {
                setUIClientsList(allClientsList.filter((i) => i.name.toLowerCase().match(searchString.toLowerCase())));
            } else {
                setUIClientsList([...allClientsList]);
            }
        }, [allClientsList]);
        //---------------------------------------------
        const clientStatusSelectToInActive = (record) => {
            // console.log("selected",record);
            if(record.status !== 0) {
                record.status = 0;
                clientsAPI.updateClientAPI(record).then((res) => {
                    if(res.status) {
                        dispatch(clientActions.updateClient(record))
                    }
                })
            }


        }
        const clientStatusSelectToActive = (record) => {
            // console.log("selected",record);
            if(record.status !== 1) {
                record.status = 1;
                clientsAPI.updateClientAPI(record).then((res) => {
                    if(res.status) {
                        dispatch(clientActions.updateClient(record))
                    }
                })
            }


        }
        //-----ADD CLIENT----
        const newClientName = (e) => {
            setNewClient({...newClient,name: e.target.value});
        }
        const newClientStatusSetToInActive = () => {
            setNewClient({...newClient,status: 0})
        }
        const newClientStatusSetToActive = () => {
            setNewClient({...newClient,status: 1})
        }
        const addClient = async () => {
            // console.log("add client called")
            // console.log("add tag called",newClient);
            if(newClient.name == "") {
                toast.error("Enter Client Name!");
            }  else {
                setLoading(true);
                await clientsAPI.addClientAPI(newClient).then((res) => {
                    if(res.status) {
                        setNewClient({id: 0, name: "", status: 1});
                        setShowForm(false);
                        setState(!state);
                        // console.log("ADDED",res.data);
                        dispatch(clientActions.addClient(res.data))
                    }
                    setLoading(false);
                })

            }

        }

        const addClientDropDownStyle = {
            display: "inline-block",
            outline: "none",
            borderRadius: "10px",
            // border: "1px solid  #f0f0f0",
            fontSize: "16px",
            width: "80%",
            margin : "0px 0px 0px 7px"
        }
        const statusButtonStyle = {
            borderRadius:"10px",
            background: "rgba(204, 204, 204,0.5)",
            border: "none",
            outline: "none",
            padding: "5px 15px",
            fontWeight: "500",
            fontSize: '14px'
        }
        const addClientModel = () => {
            return ( <Modal
                    centered
                    visible={showForm}
                    destroyOnClose={true}
                    confirmLoading={loading}
                    okButtonProps= {
                        {
                            style: dialogButtonStyle
                        }}
                    cancelButtonProps ={
                        {
                            style: dialogButtonStyle
                        }}
                    onOk={async () => {await addClient()}}
                    onCancel={() => {
                        setShowForm(false);
                        setNewClient({id: 0, name: "", status: 1});
                    } }
                >
                    <div>
                        <div><h4>Add Client</h4></div>
                        <Divider />
                        <div style={inputRowStyle}>
                            {/*<span style={inputClientsLabelStyle}>Name : </span>*/}
                            {/*<input  style={addClientInputStyle}  type="text" />*/}
                            <Input
                                addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                                defaultValue={newClient.name} onChange={newClientName}
                            >
                            </Input>
                        </div>
                        <div style={inputRowStyle}>
                            <SelectStatus parameters={{setNewData: setNewClient,newData:newClient}}/>
                        </div>
                    </div>
                </Modal>
            )
        }
        //----REMOVE CLIENT----
        const removeClient = async (record) => {
            // console.log("removeClient called")
            await clientsAPI.removeClientAPI(record.id).then((res) => {
                if(res.status) {
                    setState(!state);
                    dispatch(clientActions.removeClient(record))

                    setUIClientsList([...allClientsList.filter((entry,index) => entry.id != record.id)]);
                }        })
        }
        const onDeleteModel =  (entry) => {
            // console.log("delete entry");
            Modal.confirm({
                title: 'Confirm',
                centered: true,
                width: "350px",
                content: 'Are you sure you want to delete this Client?',
                okText: "YES",
                cancelText: 'NO',
                onOk: async () => {await removeClient(entry)},
                okButtonProps: {style: dialogButtonStyle},
                cancelButtonProps :{style: dialogButtonStyle}
            });

        }
        //---UPDATE CLIENT-----
        const updateClientName = (e) => {
            setToUpdateClient({...toUpdateClient,name: e.target.value});
        }
        const updateClientStatusSetToInActive = () => {
            setToUpdateClient({...toUpdateClient,status: 0})
        }
        const updateClientStatusSetToActive = () => {
            setToUpdateClient({...toUpdateClient,status: 1})
        }
        const updateClient = () => {
            setLoading(true);
            // console.log("UPDATE CLIENT!!",toUpdateClient);
            clientsAPI.updateClientAPI(toUpdateClient).then((res) => {
                if(res.status) {
                    setShowUpdateForm(false);
                    setLoading(false);
                    setSearchString("");
                    setSearchStatus(false);
                    setUIClientsList(allClientsList);
                    setState(!state);
                    dispatch(clientActions.updateClient(toUpdateClient))
                }
            })
        }
        const updateClientModel = ()  => {
            return ( <Modal
                    centered
                    visible={showUpdateForm}
                    confirmLoading={loading}
                    destroyOnClose={true}
                    okButtonProps= {
                        {
                            style: dialogButtonStyle
                        }}
                    cancelButtonProps ={
                        {
                            style: dialogButtonStyle
                        }}
                    onOk={updateClient}
                    onCancel={() => {setShowUpdateForm(false); setToUpdateClient({id: Math.floor((Math.random() * 100) + 1), name: "", status: 1})}}
                >
                    <div>
                        <div><h4>Update Client</h4></div>
                        <Divider />
                        <div style={inputRowStyle}>
                            {/*<span style={inputClientsLabelStyle}>Name : </span>*/}
                            {/*<input  style={addClientInputStyle}  type="text" />*/}
                            <Input
                                addonBefore={<span style={{verticalAlign: "middle"}}>
                                    Name:</span>}
                                defaultValue={toUpdateClient.name} onChange={updateClientName}
                            >
                            </Input>
                        </div>
                        <div style={inputRowStyle}>
                            <SelectStatus parameters={{setNewData: setToUpdateClient,newData:toUpdateClient}}/>
                        </div>
                    </div>
                </Modal>
            )
        }
        //----SEARCH CLIENT LIST------
        const [searchStatus,setSearchStatus] = useState(false)
        const [searchString,setSearchString] = useState("");
        const filterClientsList = (e) => {
            setSearchString(e.target.value);
            // console.log(e.target.value)
            if(e.target.value !== "") {
                setSearchStatus(true);
            } else {
                setSearchStatus(false);
            }
            if(allClientsList.length !== totalClients && !paginationTableLoading  && searchString.length <= 1) {
                setPaginationTableLoading(true);
                clientsAPI.getClientsAPI(allClientsList.length,totalClients).then((r) => {
                    if(r.status) {
                        dispatch(clientActions.setAllClients({data: r.data , total_count: r.total_count}));
                    }
                    setPaginationTableLoading(false);
                })
            } else {
                setUIClientsList(allClientsList.filter((i) => i.name.toLowerCase().match(e.target.value.toLowerCase())));
            }
        }
        //-------DATA TABLE-----
        const getClientsDataTable = () => {
            if(allClientsList.length === 0) {
                return <div style={spinnerStyle}><Spin size="large"/></div>
            } else {
                return  <div style={tableDivStyle}>
                    <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <Table className="table-striped text-white"
                                   pagination= { {total : searchStatus ? uiClientsList.length : totalClients,
                                       onChange: (page, pageSize) => {
                                       // console.log("onchange",page,pageSize,allClientsList.length)
                                           if(searchStatus) {
                                           } else {
                                               if(allClientsList.length <=  (page -1) * 10) {
                                                   setPaginationTableLoading(true);
                                                   clientsAPI.getClientsAPI(allClientsList.length,page * 10).then((r) => {
                                                       if(r.status) {
                                                           // console.log("CLIENTS PAGE",r.data);
                                                           dispatch(clientActions.setAllClients({data: r.data , total_count: r.total_count}));
                                                           setUIClientsList([...uiClientsList,...r.data]);
                                                           // setPaginationLength(r.total_count);
                                                           // console.log("CLIENTS PAGE 2",uiClientsList);
                                                           setPaginationTableLoading(false);
                                                       }
                                                   });
                                               } else {
                                               }
                                           }
                                       },
                                       showTotal : (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
                                       showSizeChanger : false,onShowSizeChange: onShowSizeChange ,itemRender : itemRender }
                                   }
                                   style = {{overflowX : 'auto'}}
                                   columns={columns}
                                loading={paginationTableLoading}
                                   dataSource={uiClientsList}
                                   rowKey={record => record.id}
                                   // onChange={console.log("change")}
                            />
                        </div>
                    </div>
                </div></div>
            }
        }
        //----------------------
        return (
            <div className="page-wrapper" style={clientPageStyle}>
                <Helmet>
                    <title>Clients</title>
                    <meta name="description" content="Clients Page"/>
                </Helmet>
                {/* Page Content */}
                <div className="content container-fluid">
                    {/* Page Header */}
                    <div className="page-header">
                        {/*Search Filter */}
                        <div className="row filter-row">
                            <div className="col-sm-6 col-md-8">
                                <div className="col">
                                    <h3 className="page-title text-white">Clients</h3>
                                    <ul className="breadcrumb text-white">
                                        <li className="breadcrumb-item text-white"><Link  className="text-white" to={homePageURL}>Dashboard</Link></li>
                                        {/*/TODO: change to dashboard when created */}
                                        <li className="breadcrumb-item active text-white">Clients</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-sm-6 col-md-4">
                                <div className="row filter-row">
                                    <div className="col-sm-6 col-md-9">
                                        <Input  allowClear type="text"  value={searchString} placeholder="Search Clients" style={searchStyle} onChange={filterClientsList} />
                                    </div>
                                    <div className="col-sm-6 col-md-3">
                                        <Button className="btn btn-primary mr-1"  style={addClientButtonStyle} onClick={
                                            ()=> {
                                                if(checkPermission(permissionRoutes.ClientsP,permissionTypes.addP)) {
                                                    setShowForm(true);
                                                } else {
                                                    toast.error("You Don't have Permission for this!");
                                                }
                                                }} ><span>Create</span></Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* Search Filter */}
                    </div>
                    {/*/Page Header */}
                    {checkPermission(permissionRoutes.ClientsP, permissionTypes.readP) ? getClientsDataTable() :  <Empty style={{marginTop: "30px"}} image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                                                                                                  description="You Have No Permission To Read!"/>}
                </div>
                {/* /Page Content */}
                {/* Add Client Modal */}
                {addClientModel()}
                {/* /Add Client Modal */}
                {/* Edit Client Modal */}
                {updateClientModel()}
                {/* /Edit Client Modal */}
            </div>
        );
    }
    export default Clients;
