




const GET_TOGGLE_LIST = "get_toggle_list";
const SET_USER_DATA = "set_user_data";
const SET_USER_PROJECTS_LIST = "set_user_projects_list";
const INCREASE_TOGGLE_LIST_OFFSET = "increase_toggle_list_offset";
const RESET_REDUX_DB = "reset_db";
const ADD_TOGGLE_ENTRY = "add_toggle_entry";
const DELETE_TOGGLE_ENTRY = "delete_toggle_entry";
const UPDATE_TOGGLE_DESCRIPTION = "update_toggle_description";
const UPDATE_TOGGLE_PROJECT = "update_toggle_project";
const UPDATE_TOGGLE_TAG = "update_toggle_tag";
const UPDATE_TOGGLE_DATE_AND_TIME = "update_toggle_date_and_time";


const GET_CLIENTS = "get_clients";
const ADD_CLIENT = "add_client";
const REMOVE_CLIENT = "remove_client";
const UPDATE_CLIENT = "update_client";

const GET_PROJECTS = "get_projects";
const UPDATE_PROJECTS = "update_projects";
const REMOVE_PROJECTS = "remove_projects";
const ADD_PROJECTS = "add_projects";

const GET_TAGS = "get_tags";
const UPDATE_TAGS = "update_tags";
const REMOVE_TAGS = "remove_tags";
const ADD_TAGS = "add_tags";

const START_TIMER = "start_timer";
const UPDATE_TIMER_ENTRY_ID = "timer_entry_id";
const UPDATE_TIMER = "update_timer";
const STOP_TIMER = "stop_timer";
const PAUSE_TIMER = "pause_timer"
const START_PAUSE_TIMER = "start_pause_timer";
const TIMER_STATUS_CHANGE = "timer_status_change";
const RESET_TIMER = "reset_timer";


const GET_USERS = "get_users";
const ASSIGN_MEMBER_TO_PROJECT = "assign_member_to_project";
const REMOVE_MEMBER_FROM_PROJECT = "remove_member_from_project";
const UPDATE_MANAGER_RIGHTS_OF_MEMBER = "update_manager_rights";


const GET_USER_CLIENTS = "get_user_clients";
const GET_USER_TEAM_MEMBERS = "get_user_team_members";



const GET_DEPARTMENTS = "get_departments";
const ADD_DEPARTMENT = "add_department";
const REMOVE_DEPARTMENT = "remove_department";
const UPDATE_DEPARTMENT = "update_department";
export const departmentTypes = {
    GET_DEPARTMENTS,ADD_DEPARTMENT,REMOVE_DEPARTMENT,UPDATE_DEPARTMENT
};

const GET_DESIGNATIONS = "get_designations";
const ADD_DESIGNATION = "add_designation";
const REMOVE_DESIGNATION = "remove_designation";
const UPDATE_DESIGNATION = "update_designation";
export const designationTypes = {
    GET_DESIGNATIONS,ADD_DESIGNATION,REMOVE_DESIGNATION,UPDATE_DESIGNATION
};

const GET_ROLES = "get_roles";
const ADD_ROLE = "add_role";
const REMOVE_ROLE = "remove_role";
const UPDATE_ROLE = "update_role";
export const roleTypes = {
    GET_ROLES,ADD_ROLE,REMOVE_ROLE,UPDATE_ROLE
};



const GET_EMPLOYEES = "get_employees";
const ADD_EMPLOYEE = "add_employee";
const REMOVE_EMPLOYEE = "remove_employee";
const UPDATE_EMPLOYEE = "update_employee";
export const employeeTypes = {
    GET_EMPLOYEES,ADD_EMPLOYEE,REMOVE_EMPLOYEE,UPDATE_EMPLOYEE
};



const GET_MODULES = "get_modules";
const ADD_MODULE = "add_module";
const REMOVE_MODULE = "remove_module";
const UPDATE_MODULE = "update_module";
export const moduleTypes = {
    GET_MODULES,ADD_MODULE,REMOVE_MODULE,UPDATE_MODULE
};


//-----USER PERMISSIONS-------

const CHECK_LOGIN_STATE = "login_state";



export const reportTypes = {
    GET_USER_TEAM_MEMBERS,
    GET_USER_CLIENTS
}


export const memberTypes = {
    GET_USERS,
    ASSIGN_MEMBER_TO_PROJECT,
    REMOVE_MEMBER_FROM_PROJECT,
    UPDATE_MANAGER_RIGHTS_OF_MEMBER
}



export const timerTypes = {
    START_TIMER,
    STOP_TIMER,
    PAUSE_TIMER,
    START_PAUSE_TIMER,
    UPDATE_TIMER,
    UPDATE_TIMER_ENTRY_ID,
    TIMER_STATUS_CHANGE,
    RESET_TIMER,
}

export const clientTypes = {
    GET_CLIENTS,
    ADD_CLIENT,
    REMOVE_CLIENT,
    UPDATE_CLIENT
}

export const projectTypes = {
    GET_PROJECTS,
    UPDATE_PROJECTS,
    REMOVE_PROJECTS,
    ADD_PROJECTS
}

export const tagsTypes = {
    GET_TAGS,
    UPDATE_TAGS,
    REMOVE_TAGS,
    ADD_TAGS
}


export const toggleCrudTypes = {
    ADD_TOGGLE_ENTRY,
    DELETE_TOGGLE_ENTRY,
    UPDATE_TOGGLE_DESCRIPTION,
    UPDATE_TOGGLE_PROJECT,
    UPDATE_TOGGLE_TAG,
    UPDATE_TOGGLE_DATE_AND_TIME
}

  export  const types ={
     GET_TOGGLE_LIST,
      SET_USER_DATA,
      SET_USER_PROJECTS_LIST,
      INCREASE_TOGGLE_LIST_OFFSET,
      RESET_REDUX_DB,
      CHECK_LOGIN_STATE,
 }