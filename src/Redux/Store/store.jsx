import {applyMiddleware, createStore} from "redux";
import rootReducer from "../Reducers/rootReducer";
import {useHistory} from "react-router-dom";
import {loginRoute, toggleEntries} from "../../constants";

const apiMiddleware = (store) => (next) => (action) => {

    // console.log("middleware called");



    return next(action);
}

const store = createStore(rootReducer, applyMiddleware(apiMiddleware));


export default store;