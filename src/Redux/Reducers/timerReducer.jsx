import {timerTypes} from "../Store/ActionTypes";
import {resetToggleEntryDataObject, toggleEntryDataObj} from "../../MainPage/Employees/Toggle/toggleConstants";
import moment from "moment";
import {
    formatTime,
    formatTime2,
    getTimeDifference,
    getTimeDifference2,
    setFormattedDateAndTime,
    splitTime
} from "../../constants";
import DateTime from "moment";
import * as workerTimers from 'worker-timers';

const timerDataObj = {
    timerAPiData: false,
    timerStarted: false,
    entryID : 0,
    timerEntryData: toggleEntryDataObj,
    breakInterval : 0,
    pause_time: 0,
    is_pause: 0,
    timerID: 0,
    hours: 0,
    minutes: 0,
    seconds: 0
}


export default function timerReducer(timerData = timerDataObj, action) {

    switch (action.type) {
        case timerTypes.START_TIMER:
            return _startTimer(timerData,action);
        case timerTypes.PAUSE_TIMER:
            return _pauseTimer(timerData,action);
        case timerTypes.START_PAUSE_TIMER:
            return _startPauseTimer(timerData,action);
        case timerTypes.STOP_TIMER:
            return _stopTimer(timerData,action);
        case timerTypes.UPDATE_TIMER:
            return _updateTimer(timerData,action);
        case timerTypes.TIMER_STATUS_CHANGE:
            return _timerStatusChange(timerData,action);
        case timerTypes.UPDATE_TIMER_ENTRY_ID:
            return _timerEntryIDUpdate(timerData,action);
        case timerTypes.RESET_TIMER:
            return {
                timerAPiData: false,
                timerStarted: false,
                entryID : 0,
                timerEntryData: toggleEntryDataObj,
                breakInterval : 0,
                pause_time: 0,
                is_pause: 0,
                timerID: 0,
                hours: 0,
                minutes: 0,
                seconds: 0
            };
        default:
            return timerData;
    }
}

function _pauseTimer(timerData,action) {
    timerData.is_pause = 1;
    timerData.pause_time = DateTime().unix();
    // console.log("pause timer ->",timerData.pause_time);
    workerTimers.clearInterval(timerData.timerID);
    return {...timerData};
}


function _startPauseTimer(timerData,action) {
    // console.log("_startPauseTimer",timerData.breakInterval);
    return timerData;
}

function _timerStatusChange(timerData,action) {
    // console.log("_timerStatusChange",action.payload);
    timerData.timerAPiData = true;
    timerData.timerStarted  = action.payload.status;
    if(action.payload.timerData != null) {
        let reduxData = toggleEntryDataObj;
        reduxData['description']  =action.payload.timerData.description;
        reduxData['selectedProject'] = action.payload.timerData.projects;
        reduxData['selectedTag'] = action.payload.timerData.tags[0];
        reduxData['startTime'] =   moment( action.payload.timerData.user_from_time).format("hh:mm:ss A")
        reduxData['endTime'] =  moment( action.payload.timerData.user_to_time).format("hh:mm:ss A")
        reduxData['user_from_time'] = action.payload.timerData.user_from_time;
        // reduxData['user_to_time'] = DateTime(action.payload.timerData.user_to_time).unix();
        // console.log("timer status change -> redux data -> ",reduxData);
        // console.log("timer status change -> time array -> ",formatTime(reduxData['user_to_time'] - reduxData['user_from_time']).split(":"));
        if(action.payload.pauseStatus != null) {
            timerData.is_pause = parseInt(action.payload.pauseStatus);
        }
        if(action.payload.pauseTime != null) {
            // console.log("timer status pause time ->",action.payload.pauseTime)
            // timerData.pause_time = action.payload.pauseTime;
        }
        // console.log("timer status change -> ",action.payload.timerData.user_duration.split(":"))
        if(action.payload.pauseDuration != null) {
            const  array = action.payload.timerData.user_duration.split(":");
            const array2 = action.payload.pauseDuration.split(":");
            // console.log("break interval ->",DateTime().unix() - action.payload.pauseTime,
            //     "array in sec ->",parseInt(array2[0]) * 3600 + parseInt(array2[1]) * 60 + parseInt(array2[2])
            //     ,"array -> ",array2);
            timerData.pause_time = DateTime().unix();
            timerData.breakInterval = parseInt(array2[0]) * 3600 + parseInt(array2[1]) * 60 + parseInt(array2[2]);
            if(timerData.is_pause == 1){
                timerData.hours = parseInt(array[0]);
                timerData.minutes =   parseInt(array[1]);
                timerData.seconds =   parseInt(array[2]);
            } else {
                // console.log("break interval time ->",timerData.breakInterval);
                _updateTimer(timerData,action)
            }
        }
        timerData.timerEntryData = reduxData;
    }
    return {...timerData};
}
function _timerEntryIDUpdate(timerData,action) {
    timerData.entryID = action.payload;
    return {...timerData};
}
function _startTimer(timerData,action) {
    if(action.payload.fromPause != null && action.payload.fromPause) {
        // console.log("start timer from pause->",timerData,action.payload);
        timerData.is_pause = 0;
        const breakTimeSeconds = DateTime().unix() - timerData.pause_time;
        // console.log("pause time->",timerData.pause_time,breakTimeSeconds);
        timerData.breakInterval = timerData.breakInterval + breakTimeSeconds;
    }
    timerData.timerStarted = true;
    timerData.timerID = action.payload.id;
    if(action.payload.timerEntryData != null && action.payload.timerEntryData !== {}) {
        // console.log("start timer timer Entry adding");
        timerData.timerEntryData = action.payload.timerEntryData;
    }
    return {...timerData};
}

function _stopTimer(timerData,action) {
    // console.log("stop timer");
    resetToggleEntryDataObject();
    workerTimers.clearInterval(timerData.timerID);
    let tempTimerData  = {...timerDataObj};
    tempTimerData.seconds = 0;
    tempTimerData.minutes = 0;
    tempTimerData.hours = 0;
    tempTimerData.timerStarted = false;
    tempTimerData.breakInterval = 0;
    tempTimerData.is_pause = 0;
    tempTimerData.pause_time = 0;
    tempTimerData.timerID = 0;
    return {...tempTimerData};
}

function _updateTimer(timerData,action) {
    // console.log("update timer 1->",timerData.timerEntryData.user_from_time,DateTime().unix(),timerData.breakInterval);
    // console.log("update timer 2->",(DateTime().unix() - timerData.timerEntryData.user_from_time))
    // console.log("update timer 3->",(DateTime().unix() - timerData.timerEntryData.user_from_time - timerData.breakInterval))
    if(timerData.timerEntryData.user_from_time !== 0){
        // console.log("update timer 4->",DateTime().unix(),timerData.timerEntryData.user_from_time,timerData.breakInterval)
        const diff = (DateTime().unix() - timerData.timerEntryData.user_from_time) - timerData.breakInterval;
        // console.log("update timer 5->",[parseInt(diff / 60 / 60),parseInt(diff / 60 % 60), parseInt(diff % 60)])
        timerData.hours = parseInt(diff / 60 / 60);
        timerData.minutes =   parseInt(diff / 60 % 60);
        timerData.seconds =   parseInt(diff % 60);
        // if(timerData.seconds % 5 == 0) {
        //     timerData.seconds ++;
        // } else {
        //     const diff = (DateTime().unix() - timerData.timerEntryData.user_from_time) - timerData.breakInterval;
        //     // console.log("update timer 5->",[parseInt(diff / 60 / 60),parseInt(diff / 60 % 60), parseInt(diff % 60)])
        //     timerData.hours = parseInt(diff / 60 / 60);
        //     timerData.minutes =   parseInt(diff / 60 % 60);
        //     timerData.seconds =   parseInt(diff % 60);
        // }
    }
    timerData= {...timerData}
    return timerData;

}