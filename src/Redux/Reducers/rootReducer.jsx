import { combineReducers } from "@reduxjs/toolkit";
import mainReducer from "./mainReducer";
import timerReducer from "./timerReducer";

const rootReducer = combineReducers(
    {
        db : mainReducer,
        timer: timerReducer,
    });

export default rootReducer;