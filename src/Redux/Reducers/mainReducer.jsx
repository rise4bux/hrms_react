import {
    clientTypes, departmentTypes, designationTypes, employeeTypes,
    memberTypes, moduleTypes,
    projectTypes,
    reportTypes, roleTypes,
    tagsTypes,
    toggleCrudTypes,
    types
} from "../Store/ActionTypes";
import {
    getTimeDifference,
    setFormattedDateAndTime,
    tokenKey,
    userData,
    userPermissions,
    userRoutesPermissions
} from "../../constants";
import {toggleCrudAPI} from "../../Services/apiServices";
import moment from "moment";


const defaultDBStruct = {
    userStatus: false,
    userData : {},
    userClients : [],
    userTeam : [],
    userProjectsList: [],
    toggleList : [],
    allClients : [],
    allUsers: [],
    allProjects: [],
    allTags: [],
    allDepartments: [],
    totalDepartments: 0,
    allEmployees: [],
    totalEmployees: 0,
    allDesignations: [],
    totalDesignations: 0,
    allRoles: [],
    totalRoles: 0,
    totalClients: 0,
    totalUsers: 0,
    totalProjects: 0,
    totalTags: 0,
    toggleListOffset: 0,
    allModules: [],
    totalModules: 0,
    totalToggleEntries: 0,
}

const dbStruct = {
    userStatus: false,
    userData : {},
    userClients : [],
    userTeam : [],
    userProjectsList: [],
    toggleList : [],
    allClients : [],
    allUsers: [],
    allProjects: [],
    allTags: [],
    allDepartments: [],
    totalDepartments: 0,
    allEmployees: [],
    totalEmployees: 0,
    allDesignations: [],
    totalDesignations: 0,
    allRoles: [],
    totalRoles: 0,
    totalClients: 0,
    totalUsers: 0,
    totalProjects: 0,
    totalTags: 0,
    toggleListOffset: 0,
    allModules: [],
    totalModules: 0,
    totalToggleEntries: 0,
}


export default function mainReducer(dbStore = dbStruct, action) {
    if(localStorage.getItem(tokenKey) == null ||
        localStorage.getItem(userData) == null ||
        localStorage.getItem(userPermissions) == null ||
        localStorage.getItem(userRoutesPermissions) == null
    ) {
        return {...dbStore,userStatus: false}
    } else {
        switch (action.type) {
            case types.CHECK_LOGIN_STATE:
                return {...dbStore,userStatus: action.payload};
            case types.SET_USER_DATA:
                return _setUserData(dbStore,action);
            case types.SET_USER_PROJECTS_LIST:
                return _setUserProjects(dbStore,action);
            case types.GET_TOGGLE_LIST:
                return _getToggleList(dbStore,action);
            case types.INCREASE_TOGGLE_LIST_OFFSET:
                return _setToggleListLoadOffset(dbStore,action);
            //    -----REPORTS====
            case reportTypes.GET_USER_CLIENTS:
                return _getUserClients(dbStore,action);
            case reportTypes.GET_USER_TEAM_MEMBERS:
                return _getUserTeamMembers(dbStore,action);
            //    -------CRUD--------
            case toggleCrudTypes.ADD_TOGGLE_ENTRY:
                return _addToggleEntry(dbStore,action);
            case toggleCrudTypes.DELETE_TOGGLE_ENTRY:
                return _deleteToggleEntry(dbStore,action);
            case toggleCrudTypes.UPDATE_TOGGLE_DESCRIPTION:
                return _updateToggleDescription(dbStore,action);
            case toggleCrudTypes.UPDATE_TOGGLE_PROJECT:
                return _updateToggleProject(dbStore,action);
            case toggleCrudTypes.UPDATE_TOGGLE_TAG:
                return _updateToggleTag(dbStore,action);
            case toggleCrudTypes.UPDATE_TOGGLE_DATE_AND_TIME:
                return _updateToggleDateAndTime(dbStore,action);
            // -------- CLIENTS -------------
            case clientTypes.GET_CLIENTS:
                return _getClients(dbStore,action);
            case clientTypes.ADD_CLIENT:
                return _addClient(dbStore,action)
            case clientTypes.UPDATE_CLIENT:
                return _updateClient(dbStore,action)
            case clientTypes.REMOVE_CLIENT:
                return _removeClient(dbStore,action)
            //-------TAGS-----------------
            case tagsTypes.GET_TAGS:
                return _getTags(dbStore,action);
            case tagsTypes.ADD_TAGS:
                return _addTag(dbStore,action);
            case tagsTypes.REMOVE_TAGS:
                return _removeTag(dbStore,action);
            case tagsTypes.UPDATE_TAGS:
                return _updateTag(dbStore,action);
            //    ---PROJECTS---
            case projectTypes.GET_PROJECTS:
                return  _getAllProjects(dbStore,action);
            case projectTypes.ADD_PROJECTS:
                return  _addProject(dbStore,action);
            case projectTypes.REMOVE_PROJECTS:
                return  _removeProject(dbStore,action);
            case projectTypes.UPDATE_PROJECTS:
                return  _updateProject(dbStore,action);
            //    ---PROJECT MEMBERS-----
            case memberTypes.GET_USERS:
                return  _getUsers(dbStore,action);
            case memberTypes.ASSIGN_MEMBER_TO_PROJECT:
                return  _assignMemberToProject(dbStore,action);
            case memberTypes.REMOVE_MEMBER_FROM_PROJECT:
                return  _removeMemberFromProject(dbStore,action);
            case memberTypes.UPDATE_MANAGER_RIGHTS_OF_MEMBER:
                return  _updateManagerRightsOfMember(dbStore,action);
            // -------- DEPARTMENTS -------------
            case departmentTypes.GET_DEPARTMENTS:
                return _getDepartments(dbStore,action);
            case departmentTypes.ADD_DEPARTMENT:
                return _addDepartment(dbStore,action)
            case departmentTypes.UPDATE_DEPARTMENT:
                return _updateDepartment(dbStore,action)
            case departmentTypes.REMOVE_DEPARTMENT:
                return _removeDepartment(dbStore,action)
            // -------- DESIGNATIONS -------------
            case designationTypes.GET_DESIGNATIONS:
                return _getDesignations(dbStore,action);
            case designationTypes.ADD_DESIGNATION:
                return _addDesignation(dbStore,action)
            case designationTypes.UPDATE_DESIGNATION:
                return _updateDesignation(dbStore,action)
            case designationTypes.REMOVE_DESIGNATION:
                return _removeDesignation(dbStore,action)
            // -------- ROLES -------------
            case roleTypes.GET_ROLES:
                return _getRoles(dbStore,action);
            case roleTypes.ADD_ROLE:
                return _addRole(dbStore,action)
            case roleTypes.UPDATE_ROLE:
                return _updateRole(dbStore,action)
            case roleTypes.REMOVE_ROLE:
                return _removeRole(dbStore,action)
            // -------- EMPLOYEES -------------
            case employeeTypes.GET_EMPLOYEES:
                return _getEmployees(dbStore,action);
            case employeeTypes.ADD_EMPLOYEE:
                return _addEmployee(dbStore,action)
            case employeeTypes.UPDATE_EMPLOYEE:
                return _updateEmployee(dbStore,action)
            case employeeTypes.REMOVE_EMPLOYEE:
                return _removeEmployee(dbStore,action)
            // -------- MODULES -------------
            case moduleTypes.GET_MODULES:
                return _getModules(dbStore,action);
            case moduleTypes.ADD_MODULE:
                return _addModule(dbStore,action)
            case moduleTypes.UPDATE_MODULE:
                return _updateModule(dbStore,action)
            case moduleTypes.REMOVE_MODULE:
                return _removeModule(dbStore,action)
            case types.RESET_REDUX_DB:
                return _resetDBStruct(dbStore,action);
            default:
                return dbStore;
        }
    }


}

const  _resetDBStruct = (dbStore,action) => {
    // dbStore = defaultDBStruct;
    // console.log("logggg reset",dbStore,dbStruct,defaultDBStruct);
    return {...defaultDBStruct}
}

//---REPORTS---
const _getUserTeamMembers = (dbStore,action) => {
    return  {...dbStore,userTeam: [...dbStore.userTeam, ...action.payload]};
}
const _getUserClients = (dbStore,action) => {
    return  {...dbStore,userClients: [...dbStore.userClients, ...action.payload]};
}


//----MEMBERS-----
const _getUsers = (dbStore,action) => {
    return  {...dbStore,allUsers: [...dbStore.allUsers, ...action.payload.data],totalUsers: action.payload.total_count};
}
const _assignMemberToProject =(dbStore,action) => {
    // console.log("In _assignMemberToProject reducer");
    let selectedIndex = dbStore.allProjects.findIndex((project) => project.id == action.payload.project_id);
    let selectedProject = dbStore.allProjects[selectedIndex];
    if(selectedProject.members.find((m) => m.id === action.payload.member.id) == null) {
        selectedProject.members = [...selectedProject.members,action.payload.member];
    }
    dbStore.allProjects[selectedIndex] = selectedProject;
    return dbStore;
}
const _removeMemberFromProject =(dbStore,action) => {
    // console.log("In _removeClients reducer");
    let allProjectsList = [...dbStore.allProjects]
    const selectedIndex = dbStore.allProjects.findIndex((project) => project.id == action.payload.project_id);
    const selectedProject = allProjectsList[selectedIndex];
    allProjectsList[selectedIndex].members =  selectedProject.members.filter((member) => member.id != action.payload.member.id);
    return {...dbStore,allProjects : allProjectsList};
}
const _updateManagerRightsOfMember =(dbStore,action) => {
    // console.log("In _updateManagerRightsOfMember reducer",action.payload);
    let allProjectsList = [...dbStore.allProjects]
    const selectedIndex = dbStore.allProjects.findIndex((project) => project.id === action.payload.project_id);
    let selectedProject = allProjectsList[selectedIndex];
    let selectedMemberIndex = selectedProject.members.findIndex((member) => member.id == action.payload.member.id) ;
    let selectedMember = selectedProject.members[selectedMemberIndex];
    selectedMember.pivot.is_manager = action.payload.member.pivot.is_manager;
    selectedProject.members[selectedMemberIndex] = selectedMember;
    allProjectsList[selectedIndex] =  selectedProject;
    return {...dbStore,allProjects : allProjectsList};
}


//-----CLIENTS REDUCERS-----

const  _getClients = (dbStore,action) => {
    // console.log("In get clients reducer",action.payload.data);
    return  {...dbStore,allClients: [...dbStore.allClients, ...action.payload.data],totalClients: action.payload.total_count};
}



const  _updateClient = (dbStore,action) => {
    // console.log("In _updateClients reducer");
    let selectedIndex = dbStore.allClients.findIndex((client) => client.id == action.payload.id);
    let selectedClient = dbStore.allClients[selectedIndex];
    selectedClient.name = action.payload.name;
    selectedClient.status = action.payload.status;
    dbStore.allClients[selectedIndex] = selectedClient;
    // return  {...dbStore,allClients: [...dbStore.allClients, ...action.payload]};
    return dbStore;
}


const  _removeClient = (dbStore,action) => {
    // console.log("In _removeClients reducer");
    let allClients = [...dbStore.allClients]
    allClients =allClients.filter((entry,index) => entry.id != action.payload.id);
    dbStore.totalClients -= 1;
    dbStore.allClients = allClients;
    return dbStore;
}


const  _addClient = (dbStore,action) => {
    // console.log("In _addClients reducer");
    let allClientsList = [action.payload,...dbStore.allClients]
    dbStore.totalClients += 1;
    dbStore.allClients = allClientsList;
    return dbStore;
}

//---TAGS reducers.-----

const  _getTags = (dbStore,action) => {
    // console.log("In get tags reducer");
    let allTags = action.payload.data.filter((tag) => tag != null);
    return  {...dbStore,allTags: [...dbStore.allTags, ...allTags],totalTags: action.payload.total_count};
}



const  _updateTag = (dbStore,action) => {
    // console.log("In _updateTag reducer");
    let selectedIndex = dbStore.allTags.findIndex((tag) => tag.id == action.payload.id);
    let selectedTag = dbStore.allTags[selectedIndex];
    selectedTag.name = action.payload.name;
    selectedTag.status = action.payload.status;
    selectedTag.is_default = action.payload.is_default;
    selectedTag.project = action.payload.project;
    selectedTag.project_id = action.payload.project_id;
    dbStore.allTags[selectedIndex] = selectedTag;
    return dbStore;
}


const  _removeTag = (dbStore,action) => {
    // console.log("In _removeTag reducer");
    let allTags = [...dbStore.allTags]
    allTags = allTags.filter((entry,index) => entry.id != action.payload.id);
    dbStore.totalTags -= 1;
    dbStore.allTags = allTags;
    return dbStore;
}


const  _addTag = (dbStore,action) => {
    // console.log("In _addClients reducer");
    let allTagsList = [action.payload,...dbStore.allTags]
    dbStore.totalTags += 1;
    dbStore.allTags = allTagsList;
    return dbStore;
}

//---get all Projects-----
const _getAllProjects = (dbStore,action) => {
    // console.log("In _getAllProjects reducer");
    return  {...dbStore,allProjects: [...dbStore.allProjects, ...action.payload.data],totalProjects: action.payload.total_count};
}

const getTagsFromIDs = (tagIDs) => {

}

const  _updateProject = (dbStore,action) => {
    // console.log("In _updateProject reducer",action.payload);
    let selectedIndex = dbStore.allProjects.findIndex((project) => project.id == action.payload.id);
    let selectedProject = dbStore.allProjects[selectedIndex];
    selectedProject.name = action.payload.name;
    selectedProject.status = action.payload.status;
    selectedProject.client_id = action.payload.client_id;
    // console.log("update rpojec",action.payload, dbStore.allTags.filter((tag) => action.payload.tags.includes(tag.id)));
    selectedProject.tags =  action.payload.tags
    selectedProject.use_default_tag = action.payload.use_default_tag;
    selectedProject.client = action.payload.client;
    selectedProject.color_hex = action.payload.color_hex;
    selectedProject.members = action.payload.members;
    dbStore.allProjects[selectedIndex] = selectedProject;
    return dbStore;
}


const  _removeProject = (dbStore,action) => {
    // console.log("In _removeProject reducer");
    let allProjects = [...dbStore.allProjects]
    allProjects =allProjects.filter((entry,index) => entry.id !== action.payload.id);
    dbStore.totalTags -= 1;
    dbStore.allProjects = allProjects;
    return dbStore;
}


const  _addProject = (dbStore,action) => {
    // console.log("In _addProject reducer");
    let allProjectsList = [action.payload,...dbStore.allProjects]
    dbStore.totalProjects += 1;
    dbStore.allProjects = allProjectsList;
    return dbStore;
}







const  _setToggleListLoadOffset = (dbStore,action) => {
    // console.log("IN REDUCER OFFSET",dbStore.toggleListOffset,action.payload);
    return  {...dbStore,toggleListOffset: dbStore.toggleListOffset + action.payload}
}

const  _setUserData = (dbStore,action) => {
        return {...dbStore,userData: action.payload};
}
const  _setUserProjects = (dbStore,action) => {
    return {...dbStore,userProjectsList: action.payload};
}
const  _getToggleList = (dbStore,action) => {
        let toggleList = [...dbStore.toggleList,...action.payload.list]
    dbStore.totalToggleEntries = action.payload.total;
    dbStore.toggleList = toggleList;
    return {...dbStore};
}

function TDate(selectedDate) {
    var dt = new Date();
    // console.log("TDate",selectedDate,Date.parse(selectedDate),Date.now(), new Date(selectedDate) - new Date(dt.getDate().toString()) >= 0);
    if (Date.parse(selectedDate.getDate()) < Date.parse(dt.getDate().toString())) {
        return false;
    } else {
        return  true;
    }
}
//---CRUD OPERATIONS---
const  _addToggleEntry =  (dbStore,action) => {
    let toggleList = [...dbStore.toggleList]
    let togglData = action.payload;
    if(TDate(new Date(action.payload.date))) {
        // console.log("greater than today");
        togglData['is_late'] = 0;
    } else {
        togglData['is_late'] = 1;
        // console.log("less than today");
    }
    let insertIndex  = toggleList.findIndex((entry) =>  new Date(action.payload.date) - new Date(entry.date) >= 0)
    dbStore.toggleList = [...toggleList.slice(0,insertIndex), togglData,...toggleList.slice(insertIndex)];
    // console.log("_addTOggleList",insertIndex,dbStore.toggleList);
    dbStore.toggleListOffset = dbStore.toggleList.length;
    return {...dbStore};

}
const  _deleteToggleEntry = (dbStore,action) => {
    let toggleList = [...dbStore.toggleList]
toggleList =toggleList.filter((entry,index) => entry.id != action.payload);
    dbStore.toggleList = toggleList;
    dbStore.toggleListOffset = dbStore.toggleList.length;
    return {...dbStore};
}
const  _updateToggleDescription = (dbStore,action) => {
    let toggleList = [...dbStore.toggleList]
    toggleList.find((entry,index) => entry.id === action.payload.id).description = action.payload.description;
    // console.log("ssss",toggleList.find((entry,index) => entry.id === action.payload.id));
    // selectedEntry.description = action.payload.description;
    dbStore.toggleList = [...toggleList];
    return {...dbStore};
}

const  _updateToggleProject = (dbStore,action) => {
    let toggleList = [...dbStore.toggleList]
    // console.log("update toggle projects",action);
    toggleList.find((entry,index) => entry.id === action.payload.id).project_id = action.payload.projects.id;
    toggleList.find((entry,index) => entry.id === action.payload.id).projects = action.payload.projects;
    // console.log("ssss",toggleList.find((entry,index) => entry.id === action.payload.id));
    // selectedEntry.description = action.payload.description;
    dbStore.toggleList = [...toggleList];
    return {...dbStore};
}
const  _updateToggleTag = (dbStore,action) => {
    let toggleList = [...dbStore.toggleList]
    // console.log("update toggle tag",action);
    // toggleList.find((entry,index) => entry.id === action.payload.id).project_id = action.payload.projects.id;
    toggleList.find((entry,index) => entry.id === action.payload.id).tags = [action.payload.tags];
    // console.log("ssss",toggleList.find((entry,index) => entry.id === action.payload.id));
    // selectedEntry.description = action.payload.description;
    dbStore.toggleList = [...toggleList];
    return {...dbStore};
}
const  _updateToggleDateAndTime = (dbStore,action) => {
    let toggleList = [...dbStore.toggleList]
    // console.log("update toggle entry date and time",action);
    toggleList.find((entry,index) => entry.id === action.payload.id).date = action.payload.data.selectedDate;
    toggleList.find((entry,index) => entry.id === action.payload.id).user_duration =
        getTimeDifference(setFormattedDateAndTime(action.payload.data.selectedDate,action.payload.data.startTime.toString()),setFormattedDateAndTime(action.payload.data.selectedDate,action.payload.data.endTime.toString()))
    toggleList.find((entry,index) => entry.id === action.payload.id).user_from_time = setFormattedDateAndTime(action.payload.data.selectedDate,action.payload.data.startTime);
    toggleList.find((entry,index) => entry.id === action.payload.id).user_to_time = setFormattedDateAndTime(action.payload.data.selectedDate,action.payload.data.endTime);
    if(TDate(new Date(action.payload.data.selectedDate))) {
        // console.log("less than today");
        toggleList.find((entry,index) => entry.id === action.payload.id).is_late = 0;

    } else {
        toggleList.find((entry,index) => entry.id === action.payload.id).is_late = 1;
        // console.log("greater than today");
    }
    dbStore.toggleList = [...toggleList];
    return {...dbStore};
}

//-----DEPARTMENT REDUCERS-----

const  _getDepartments = (dbStore,action) => {
    return  {...dbStore,allDepartments: [...dbStore.allDepartments, ...action.payload.data],totalDepartments: action.payload.total_count};
}

const  _updateDepartment = (dbStore,action) => {
    let selectedIndex = dbStore.allDepartments.findIndex((client) => client.id == action.payload.id);
    let selected = dbStore.allDepartments[selectedIndex];
    selected.name = action.payload.name;
    selected.status = action.payload.status;
    dbStore.allDepartments[selectedIndex] = selected;
    return dbStore;
}
const  _removeDepartment = (dbStore,action) => {
    let allDepartments = [...dbStore.allDepartments]
    allDepartments =allDepartments.filter((entry,index) => entry.id != action.payload.id);
    dbStore.totalDepartments -= 1;
    dbStore.allDepartments = allDepartments;
    return dbStore;
}
const  _addDepartment = (dbStore,action) => {
    let allDepartmentsList = [action.payload,...dbStore.allDepartments]
    dbStore.totalDepartments += 1;
    dbStore.allDepartments = allDepartmentsList;
    return dbStore;
}
//-----DESIGNATION REDUCERS-----

const  _getDesignations = (dbStore,action) => {
    return  {...dbStore,allDesignations: [...dbStore.allDesignations, ...action.payload.data],totalDesignations: action.payload.total_count};
}

const  _updateDesignation = (dbStore,action) => {
    let selectedIndex = dbStore.allDesignations.findIndex((client) => client.id == action.payload.id);
    let selected = dbStore.allDesignations[selectedIndex];
    selected.name = action.payload.name;
    selected.status = action.payload.status;
    dbStore.allDesignations[selectedIndex] = selected;
    return dbStore;
}
const  _removeDesignation = (dbStore,action) => {
    let allDesignations = [...dbStore.allDesignations]
    allDesignations =allDesignations.filter((entry,index) => entry.id != action.payload.id);
    dbStore.totalDesignations -= 1;
    dbStore.allDesignations = allDesignations;
    return dbStore;
}
const  _addDesignation = (dbStore,action) => {
    let allDesignationsList = [action.payload,...dbStore.allDesignations]
    dbStore.totalDesignations += 1;
    dbStore.allDesignations = allDesignationsList;
    return dbStore;
}

//-----ROLES REDUCERS-----

const  _getRoles = (dbStore,action) => {
    return  {...dbStore,allRoles: [...dbStore.allRoles, ...action.payload.data],totalRoles: action.payload.total_count};
}

const  _updateRole = (dbStore,action) => {
    let selectedIndex = dbStore.allRoles.findIndex((client) => client.id == action.payload.id);
    // let selected = dbStore.allRoles[selectedIndex];
    // selected.name = action.payload.name;
    // selected.status = action.payload.status;
    // console.log("update ROle",action.payload,selected);
    dbStore.allRoles[selectedIndex] = action.payload;
    return dbStore;
}
const  _removeRole = (dbStore,action) => {
    let allRoles = [...dbStore.allRoles]
    allRoles =allRoles.filter((entry,index) => entry.id != action.payload.id);
    dbStore.totalRoles -= 1;
    dbStore.allRoles = allRoles;
    return dbStore;
}
const  _addRole = (dbStore,action) => {
    let allRolesList = [action.payload,...dbStore.allRoles]
    dbStore.totalRoles += 1;
    dbStore.allRoles = allRolesList;
    return dbStore;
}

//--EMPLOYEE REDUCERS----


const  _getEmployees = (dbStore,action) => {
    return  {...dbStore,allEmployees: [...dbStore.allEmployees, ...action.payload.data],totalEmployees: action.payload.total_count};
}

const  _updateEmployee = (dbStore,action) => {
    let selectedIndex = dbStore.allEmployees.findIndex((client) => client.id == action.payload.id);
    let selected = dbStore.allEmployees[selectedIndex];
    selected.name = action.payload.name;
    selected.email = action.payload.email;
    selected.password = action.payload.password;
    selected.roles = dbStore.allRoles.find((role) => role.id === action.payload.role_id) != null ? [dbStore.allRoles.find((role) => role.id === action.payload.role_id)] : [];
    selected.department_id =action.payload.department_id;
    selected.designation_id =action.payload.designation_id;
    selected.personals.gender = action.payload.gender != null ? action.payload.gender === "Male" ? "male" : action.payload.gender === "Female" ? "female" : "others" : null;
    selected.personals.date_of_birth = action.payload.date_of_birth != null ? moment(action.payload.date_of_birth,["DD/MM/YYYY"]).format("YYYY-MM-DD") : null ;
    selected.personals.blood_group = action.payload.blood_group;
    selected.personals.pancard = action.payload.pancard;
    selected.personals.aadhar_card = action.payload.aadhar_card;
    selected.personals.driving_licence_no = action.payload.driving_licence_no;
    selected.personals.passport_no = action.payload.passport_no;
    selected.personals.bio = action.payload.bio;
    selected.personals.uan_no = action.payload.uan_no;
    selected.personals.esic_no = action.payload.esic_no;
    selected.personals.company_email = action.payload.company_email;
    selected.personals.date_of_joining =  action.payload.date_of_joining != null ? moment(action.payload.date_of_joining,["DD/MM/YYYY"]).format("YYYY-MM-DD") : null ;
    selected.personals.marital_status =  action.payload.marital_status ;
    selected.report_heads =  action.payload.report_heads ;
    selected.personals.aniversary_date =   action.payload.aniversary_date != null ? moment(action.payload.aniversary_date,["DD/MM/YYYY"]).format("YYYY-MM-DD") : null ;;
    selected.status =  action.payload.status ;
        dbStore.allEmployees[selectedIndex] = selected;
    return dbStore;
}
const  _removeEmployee = (dbStore,action) => {
    let allEmployees = [...dbStore.allEmployees]
    allEmployees =allEmployees.filter((entry,index) => entry.id != action.payload.id);
    dbStore.totalEmployees -= 1;
    dbStore.allEmployees = allEmployees;
    return dbStore;
}
const  _addEmployee = (dbStore,action) => {
    // let responseData = action.payload;
    // responseData.personals['aadhar_card'] = action.payload.personals.adhaar_no;
    // responseData.personals['pancard'] = action.payload.personals.pan_no;
    let allEmployees = [action.payload,...dbStore.allEmployees]
    dbStore.totalEmployees += 1;
    dbStore.allEmployees = allEmployees;
    return dbStore;
}


//----------MODULES----

//-----ROLES REDUCERS-----

const  _getModules = (dbStore,action) => {
    return  {...dbStore,allModules: [...dbStore.allModules, ...action.payload.data],totalModules: action.payload.total_count};
}

const  _updateModule = (dbStore,action) => {
    let selectedIndex = dbStore.allModules.findIndex((client) => client.id == action.payload.id);
    let selected = dbStore.allModules[selectedIndex];
    selected.name = action.payload.name;
    selected.url = action.payload.url;
    selected.parent_id = action.payload.parent_id;
    dbStore.allModules[selectedIndex] = selected;
    return dbStore;
}
const  _removeModule = (dbStore,action) => {
    let allModules = [...dbStore.allModules]
    allModules =allModules.filter((entry,index) => entry.id != action.payload.id);
    dbStore.totalModules -= 1;
    dbStore.allModules = allModules;
    return dbStore;
}
const  _addModule = (dbStore,action) => {
    let allModules = [action.payload,...dbStore.allModules]
    dbStore.totalModules += 1;
    dbStore.allModules = allModules;
    return dbStore;
}
