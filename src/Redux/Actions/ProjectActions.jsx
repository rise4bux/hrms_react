import {projectTypes, tagsTypes} from "../Store/ActionTypes";

function setAllProjects(dataList) {
    return {type: projectTypes.GET_PROJECTS,payload: dataList}; // payload as new list
}
function updateProject(data) {
    return {type: projectTypes.UPDATE_PROJECTS,payload: data}; // payload as new data
}
function removeProject(data) {
    return {type: projectTypes.REMOVE_PROJECTS,payload: data}; // payload as new data
}
function addProject(data) {
    return {type: projectTypes.ADD_PROJECTS,payload: data}; // payload as new data
}


export const projectActions = {
    setAllProjects,
    updateProject,
    removeProject,
    addProject
}