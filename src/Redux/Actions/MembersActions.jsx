import {memberTypes, projectTypes} from "../Store/ActionTypes";

function setAllUsers(data) {
    return {type: memberTypes.GET_USERS,payload: data}; // payload as new data
}
function assignMemberToProject(data) {
    return {type: memberTypes.ASSIGN_MEMBER_TO_PROJECT,payload: data}; // payload as new data
}
function removeMemberFromProject(data) {
    return {type: memberTypes.REMOVE_MEMBER_FROM_PROJECT,payload: data}; // payload as new data
}
function updateManagerRightsOfMember(data) {
    return {type: memberTypes.UPDATE_MANAGER_RIGHTS_OF_MEMBER,payload: data}; // payload as new data
}

export const memberActions = {
    setAllUsers,
    assignMemberToProject,
    removeMemberFromProject,
    updateManagerRightsOfMember,
}
