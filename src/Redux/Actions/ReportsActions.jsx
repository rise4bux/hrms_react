import {reportTypes, tagsTypes} from "../Store/ActionTypes";

function setUserTeam(data) {
    return {type: reportTypes.GET_USER_TEAM_MEMBERS,payload: data}; // payload as new data
}
function setUserClients(data) {
    return {type: reportTypes.GET_USER_CLIENTS,payload: data}; // payload as new data
}


export const reportActions = {
    setUserTeam,
    setUserClients
}
