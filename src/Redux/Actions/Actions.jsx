import {toggleCrudTypes, types} from "../Store/ActionTypes";
import {toggleCrudAPI} from "../../Services/apiServices";

function resetDB(data) {
    return {type: types.RESET_REDUX_DB,payload: data}; //payload as user data MAP
}
function setUserLoginState(data) {
    return {type: types.CHECK_LOGIN_STATE,payload: data};
}


 function setUserData(data) {
    return {type: types.SET_USER_DATA,payload: data}; //payload as user data MAP
}

function setUserProjects(data) {
    return {type: types.SET_USER_PROJECTS_LIST,payload: data}; //payload as user data LIST
}

 function setToggleList(data) {
    return {type: types.GET_TOGGLE_LIST,payload: data}; // payload as new entry MAP
}
function increaseToggleOffset(data) {
    return {type: types.INCREASE_TOGGLE_LIST_OFFSET,payload: data}; // payload as new entry MAP
}

//----------TOGGLE CRUD
const addToggleEntry =  (data) => {
    return {type: toggleCrudTypes.ADD_TOGGLE_ENTRY,payload: data}; // payload as new entry MAP
}
function deleteToggleEntry(data) {
    return {type: toggleCrudTypes.DELETE_TOGGLE_ENTRY,payload: data}; // payload as new entry MAP
}
function updateToggleDescription(data) {
    return {type: toggleCrudTypes.UPDATE_TOGGLE_DESCRIPTION,payload: data}; // payload as new entry MAP
}
function updateToggleProject(data) {
    return {type: toggleCrudTypes.UPDATE_TOGGLE_PROJECT,payload: data}; // payload as new entry MAP
}

function updateToggleTag(data) {
    return {type: toggleCrudTypes.UPDATE_TOGGLE_TAG,payload: data}; // payload as new entry MAP
}
function updateToggleDateAndTime(data) {
    return {type: toggleCrudTypes.UPDATE_TOGGLE_DATE_AND_TIME,payload: data}; // payload as new entry MAP
}



export const toggleCrudActions = {
    addToggleEntry,
    deleteToggleEntry,
    updateToggleDescription,
    updateToggleProject,
    updateToggleTag,
    updateToggleDateAndTime,
}


export const actions = {
    setUserData,
    setUserProjects,
    setToggleList,
    increaseToggleOffset,
    resetDB,
    setUserLoginState
}