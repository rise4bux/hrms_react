import {roleTypes} from "../Store/ActionTypes";

function setAll(dataList) {
    return {type: roleTypes.GET_ROLES,payload: dataList}; // payload as new list
}
function update(data) {
    return {type: roleTypes.UPDATE_ROLE,payload: data}; // payload as new data
}
function remove(data) {
    return {type: roleTypes.REMOVE_ROLE,payload: data}; // payload as new data
}
function add(data) {
    return {type: roleTypes.ADD_ROLE,payload: data}; // payload as new data
}



export const rolesActions = {
    setAll,
    update,
    remove,
    add
}