import {departmentTypes} from "../Store/ActionTypes";

function setAll(dataList) {
    return {type: departmentTypes.GET_DEPARTMENTS,payload: dataList}; // payload as new list
}
function update(data) {
    return {type: departmentTypes.UPDATE_DEPARTMENT,payload: data}; // payload as new data
}
function remove(data) {
    return {type: departmentTypes.REMOVE_DEPARTMENT,payload: data}; // payload as new data
}
function add(data) {
    return {type: departmentTypes.ADD_DEPARTMENT,payload: data}; // payload as new data
}



export const departmentActions = {
    setAll,
    update,
    remove,
    add
}