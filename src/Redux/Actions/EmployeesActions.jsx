import {employeeTypes} from "../Store/ActionTypes";

function setAll(dataList) {
    return {type: employeeTypes.GET_EMPLOYEES,payload: dataList}; // payload as new list
}
function update(data) {
    return {type: employeeTypes.UPDATE_EMPLOYEE,payload: data}; // payload as new data
}
function remove(data) {
    return {type: employeeTypes.REMOVE_EMPLOYEE,payload: data}; // payload as new data
}
function add(data) {
    return {type: employeeTypes.ADD_EMPLOYEE,payload: data}; // payload as new data
}



export const employeesActions = {
    setAll,
    update,
    remove,
    add
}