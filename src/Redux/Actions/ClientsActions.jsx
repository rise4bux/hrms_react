import {clientTypes, toggleCrudTypes} from "../Store/ActionTypes";

function setAllClients(dataList) {
    return {type: clientTypes.GET_CLIENTS,payload: dataList}; // payload as new list
}
function updateClient(data) {
    return {type: clientTypes.UPDATE_CLIENT,payload: data}; // payload as new data
}
function removeClient(data) {
    return {type: clientTypes.REMOVE_CLIENT,payload: data}; // payload as new data
}
function addClient(data) {
    return {type: clientTypes.ADD_CLIENT,payload: data}; // payload as new data
}



export const clientActions = {
    setAllClients,
    updateClient,
    removeClient,
    addClient
}