import { timerTypes} from "../Store/ActionTypes";

function startTimer(data) {
    return {type: timerTypes.START_TIMER,payload: data}; // payload as new data
}
function stopTimer(data) {
    return {type: timerTypes.STOP_TIMER,payload: data}; // payload as new data
}
function pauseTimer(data) {
    return {type: timerTypes.PAUSE_TIMER,payload: data}; // payload as new data
}
function startPauseTimer(data) {
    return {type: timerTypes.START_PAUSE_TIMER,payload: data}; // payload as new data
}


function updateTimer(data) {
    return {type: timerTypes.UPDATE_TIMER,payload: data}; // payload as new data
}
function timerEntryID(data) {
    return {type: timerTypes.UPDATE_TIMER_ENTRY_ID,payload: data}; // payload as new data
}

function timerStatusChange(data) {
    return {type: timerTypes.TIMER_STATUS_CHANGE,payload: data}; // payload as new data
}
function timerReset(data) {
    return {type: timerTypes.RESET_TIMER,payload: data}; // payload as new data
}



export const timerActions = {
    startTimer,
    stopTimer,
    updateTimer,
    timerEntryID,
    timerStatusChange,
    startPauseTimer,
    pauseTimer,
    timerReset,
}