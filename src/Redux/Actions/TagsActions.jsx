import {clientTypes, tagsTypes, toggleCrudTypes} from "../Store/ActionTypes";

function setAllTags(dataList) {
    return {type: tagsTypes.GET_TAGS,payload: dataList}; // payload as new list
}
function updateTag(data) {
    return {type: tagsTypes.UPDATE_TAGS,payload: data}; // payload as new data
}
function removeTag(data) {
    return {type: tagsTypes.REMOVE_TAGS,payload: data}; // payload as new data
}
function addTag(data) {
    return {type: tagsTypes.ADD_TAGS,payload: data}; // payload as new data
}



export const tagsActions = {
    setAllTags,
    updateTag,
    removeTag,
    addTag
}