import {moduleTypes, roleTypes} from "../Store/ActionTypes";

function setAll(dataList) {
    return {type: moduleTypes.GET_MODULES,payload: dataList}; // payload as new list
}
function update(data) {
    return {type: moduleTypes.UPDATE_MODULE,payload: data}; // payload as new data
}
function remove(data) {
    return {type: moduleTypes.REMOVE_MODULE,payload: data}; // payload as new data
}
function add(data) {
    return {type: moduleTypes.ADD_MODULE,payload: data}; // payload as new data
}



export const modulesActions = {
    setAll,
    update,
    remove,
    add
}