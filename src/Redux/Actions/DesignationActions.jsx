import {designationTypes} from "../Store/ActionTypes";

function setAll(dataList) {
    return {type: designationTypes.GET_DESIGNATIONS,payload: dataList}; // payload as new list
}
function update(data) {
    return {type: designationTypes.UPDATE_DESIGNATION,payload: data}; // payload as new data
}
function remove(data) {
    return {type: designationTypes.REMOVE_DESIGNATION,payload: data}; // payload as new data
}
function add(data) {
    return {type: designationTypes.ADD_DESIGNATION,payload: data}; // payload as new data
}



export const designationActions = {
    setAll,
    update,
    remove,
    add
}