import {getCurrentUserPermissions} from "./constants";

let permissionList = [];

export const setCurrentUserPermission = () => {
    // permissionList = getCurrentUserPermissions();
}


const checkPermissionList = (list,name) => {
    for(let i = 0; i < list.length; i++) {
        if(list[i].children == null) {
            if(list[i].name === name) {
                currentPermissionData = list[i];
                // console.log("found permission",list[i],name);
                return;
            }
        } else {
            checkPermissionList(list[i].children,name)
        }
    }

}
let currentPermissionData = null;
const checkPermission =  (name, propertyName) => {
    // return true;
    const  permissionList = getCurrentUserPermissions();
    if(permissionList == null) {
        return false;
    } else {
        checkPermissionList(permissionList,name);
        if(currentPermissionData != null) {
            return currentPermissionData['permissions'].find((i) => i.name === propertyName).selected === 1;
        } else {
            return  false;
        }

    }

}

export  { checkPermission } ;
