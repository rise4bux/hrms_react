

const actionButtonDivStyle = {
    display: "flex",
    justifyContent: "start",
    alignItems: "center"
}

const actionButtonStyles = {
    border: "none",
    outline: "none",
    background : "rgba(255, 255, 255, 0.3)",
    color: "white",
    margin: "0px 3px",
    borderRadius: "10px",
    fontSize: "14px",
    padding: "5px 10px",

}
const statusButtonsStyle = {
    display: "flex",
    justifyContent: "start",
    alignItems: "center",

}
const statusButtonsInnerSpanStyle = {
    margin: "0px 0px 0px 10px"
}


export const tableStyles = {
    actionButtonDivStyle,
    actionButtonStyles,
    statusButtonsStyle,
    statusButtonsInnerSpanStyle,
}


export const searchStyle = {
    margin: "0px 10px 10px 10px",
    // padding: "0px 0px 0px 10px",
    height: "35px",
    // width: "200px",
    // background: "rgba(255, 255, 255,0.3)",
    border: "1px solid transparent",
    color: "white"
}
