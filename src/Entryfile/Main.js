import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Route, Switch, useHistory} from 'react-router-dom';
import App from '../initialpage/App';
import 'font-awesome/css/font-awesome.min.css';
import '../assets/css/font-awesome.min.css';
import '../assets/css/line-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "../assets/css/bootstrap.min.css";
// Custom Style File
import '../assets/js/bootstrap.min.js';
import '../assets/css/select2.min.css';
import '../assets/js/popper.min.js';
import '../assets/js/app.js';
import '../assets/js/select2.min.js';
import '../assets/js/jquery-3.2.1.min.js';
import '../assets/js/jquery.slimscroll.min.js';
import "../assets/js/bootstrap-datetimepicker.min.js";
import "../assets/js/jquery-ui.min.js";
import "../assets/js/task.js";
import "../assets/js/multiselect.min.js";
import "../assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css";
import "../assets/css/bootstrap-datetimepicker.min.css";
import '../assets/css/style.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {getRouteComponent, loginRouteURL} from "../Services/routeServices";
import LoginPage from "../initialpage/loginpage";
import {checkAllDataInLocalStorage, getCurrentUserRoutesPermissions} from "../constants";
import {useDispatch} from "react-redux";
import {apiData} from "../Services/apiServices";
import {actions} from "../Redux/Actions/Actions";

const MainApp = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    useEffect(() => {
        // console.log("MainApp",location.pathname);
        // if (location.pathname.includes("by-pass-login")
        //     // && document.referrer.includes("hrms.weboccult.com")
        // ) {
        //     console.log("includes", location.pathname);
        //     let encryptedData = location.pathname.split("/")[2];
        //     // console.log("redirected from old",encryptedData);
        //     // history.push(toggleEntries);
        //     // setPageLoading(true);
        //     apiData.loginUserWithToken(encryptedData).then((res) => {
        //         if (res.status) {
        //             if (checkAllDataInLocalStorage()) {
        //                 // history.push("/");
        //                 dispatch(actions.setUserLoginState(true));
        //                 dispatch(actions.setUserData(res.data));
        //             } else {
        //                 dispatch(actions.setUserLoginState(false));
        //                 // history.push(loginRouteURL);
        //             }
        //         }
        //         // setPageLoading(false);
        //     })
        // }
    },[])

    return  (
        <>
            <ToastContainer
                // position="bottom-right"
                autoClose={2000}
                hideProgressBar
                newestOnTop={false}
                closeOnClick
                rtl={false}
                // pauseOnFocusLoss
                draggable
                // pauseOnHover
            />
            <Router >
                <Switch>
                    <Route exact path={loginRouteURL} component={LoginPage} />
                    <Route path="/" component={App} />
                </Switch>
            </Router>
        </>

    );
}

export default MainApp;