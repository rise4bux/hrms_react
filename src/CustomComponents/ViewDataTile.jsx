import React, {useState} from "react";
import {Divider, Modal, Tag, Tooltip} from "antd";
import {checkPermission} from "../permissionConstants";
import {permissionRoutes, permissionTypes} from "../constants";
import {toast} from "react-toastify";




const tileLabelStyle = {
    background:
        // "rgba(0, 0, 0, 0.85)",
    "rgba(255, 255, 255,0.3)",
    borderRadius: "5px",
    padding: "5px 10px",
    // border: "1px solid rgba(255, 255, 255,0.3)",
    fontWeight: "600"
}
const tileDataStyle = {
    display: "inline-block",
    padding: "5px 15px",
    maxHeight: "400px",
    overflowY: "auto",

}

export default function ViewDataTile({parameters,isListData = false,isColorData = false,compactTile = false})  {

    const tileStyle = {
        // background: "rgba(255, 255, 255, 0.25)",
        // color: "white",
        borderRadius: "5px",
        padding: compactTile ?"0px" :"5px 0px",
        border:
            "1px solid rgba(0, 0, 0,0.3)",
        // "1px solid rgba(255, 255, 255,0.3)",
        margin: "5px 0px",
        display: "flex",

        alignItems: "center",
        justifyContent: "start",
    }


    return (
        <div style={tileStyle}>
            <span style={tileLabelStyle} >{parameters.label}:</span><span
            style={ tileDataStyle}
        >{isListData ?
            parameters.data != null ? parameters.data.map((member,key) => {
                return (<Tag color= "rgba(0, 0, 0,0.3)"

                             style={{margin: "4px"}} key={key}>{member.name}</Tag>)
            }) : "No Data": isColorData ? <Tag color={parameters.data}>{parameters.data}</Tag>  :  parameters.data
            }
        </span>
        </div>
    )
}


//------------------------BASIC DATA FOR VIEW----------------------------------
//
// const [viewForm,setViewForm] = useState(false);
// const [toViewData,setToViewData] = useState({});
//
// const viewModel = () => {
//     return ( <Modal
//             centered
//             footer={null}
//             visible={viewForm}
//             destroyOnClose={true}
//             style={{
//                 background: "black",
//                 border: "1px solid rgba(255, 255, 255,0.25)",
//                 borderRadius: "10px",
//                 color: "white",
//             }}
//             bodyStyle={{
//                 background: "black",
//                 // "rgba(0, 0, 0, 0.5)",
//                 color: "white",
//             }}
//             okButtonProps= {
//                 {
//                     style: dialogButtonStyle
//                 }}
//             cancelButtonProps ={
//                 {
//                     style: dialogButtonStyle
//                 }}
//             onCancel={() => {setViewForm(false); setToViewData(projectDataObj)}}
//         >
//             <div>
//                 <div><h4 className="text-white">Project: {toViewData.name}</h4></div>
//                 <Divider  style={{background: "whitesmoke",margin: "10px 5px 10px 0px"}} />
//                 <ViewDataTile parameters={{label: "Name",data: toViewData.name}}/>
//                 <ViewDataTile parameters={{label: "Color_Hex",data: toViewData.color_hex}} isColorData={true}/>
//                 <ViewDataTile parameters={{label: "Client",data: toViewData.clients}} isListData={true}/>
//                 <ViewDataTile parameters={{label: "Members",data: toViewData.members}} isListData={true}/>
//                 <ViewDataTile parameters={{label: "Tags",data: toViewData.tags}} isListData={true}/>
//             </div>
//         </Modal>
//     )
// }

//
// <Tooltip title="view record" placement="left">
//     <button style={actionButtonStyles}  onClick={() => {
//         if(checkPermission(permissionRoutes.ProjectsP,permissionTypes.readP)) {
//             console.log("view data",record);
//             setToViewData(record);
//             setViewForm(true);
//         } else {
//             toast.error("You Don't have Permission for this!");
//         }
//
//     }}><i className="fa fa-eye" ></i></button>
// </Tooltip>


// {/*VIEW MODEL*/}
// {viewModel()}
// {/*VIEW MODEL*/}